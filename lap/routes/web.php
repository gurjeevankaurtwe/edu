<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//       return view('frontend.login');
// });
Route::get('/', 'FrontendController@index');
Route::get('/lapLogin', 'FrontendController@userLogin');
Route::any('/demo', 'HomeController@demoPage');
Route::any('/guest_user_rating', 'HomeController@guestUserRating');

Auth::routes();

Route::group(['middleware' => ['web']], function () 
{

      //Route::get('/front', 'FrontendController@index');
      /*Route::any('/aakash','FrontendController@aakash');*/
      Route::any('/home', 'FrontendController@index');

      Route::any('/my-profile', 'FrontendController@myProfile');

      Route::get('/students', 'StudentController@index');

      Route::any('/teachers', 'FrontendController@teacherList');

      Route::get('/add-students', 'StudentController@addStudent');

      Route::any('/edit-teacher/{id}', 'FrontendController@editTeacher');

      Route::any('/view-teacher-detail/{id}', 'FrontendController@viewTeacherDetail');

      Route::any('/update-teacher-status/{id}/{status}', 'FrontendController@updateTeacherStatus');

      Route::any('/delete-teacher/{id}', 'FrontendController@deleteTeacher');

      Route::any('/edit-student/{id}', 'FrontendController@editStudent');

      Route::any('/view-student-detail/{id}', 'FrontendController@viewStudentDetail');

      Route::any('/update-student-status/{id}/{status}', 'FrontendController@updateStudentStatus');

      Route::any('/delete-student/{id}', 'FrontendController@deleteStudent');

      Route::any('/add-teacher', 'FrontendController@addTeacher');

      Route::post('/add-students', 'StudentController@addStudent');

      Route::get('/students', 'StudentController@index');

      Route::get('/student_temo_trainer', 'FrontendController@studentTemoTrainer');

      Route::get('/add_beep_time', 'FrontendController@addBeepTime');

      Route::get('/student_average_score', 'FrontendController@studentAverageScore');

      Route::get('/done_temo_trainer', 'FrontendController@doneTempoTrainer');
      
      Route::get('/levels_sub_levels/{id}', 'FrontendController@levelSubLevel');
      
      Route::get('/level_questions_/{id}', 'FrontendController@levelQuestions');

      Route::get('/sublevel_sublevels/{id}', 'FrontendController@subLevelsLevel');
      
      Route::get('/users/level_questions_/{id}', 'FrontendController@levelQuestions');
      
      Route::get('/level_questions_/user/get-questions/{id}', 'FrontendController@getQuestions');
      
      Route::get('/level_questions_/user/get-questions-next/{id}/{qu_id}', 'FrontendController@getQuestionsNext');
      
      Route::get('/level_questions_/user/get-questions-previous/{id}/{qu_id}', 'FrontendController@getQuestionsPrev');

      Route::group(['middleware' => ['is_admin']], function () 
      {

            Route::get('/admin', 'AdminController@admin')->name('admin');

            Route::get('/manage-users', 'AdminController@manage_users');

            Route::get('/admin/guest_users', 'AdminController@guestUsers');

            Route::get('/admin/manage-guest-users', 'AdminController@manageGuestUsers');

            Route::match(['get','post'],'admin/add-teacher', 'AdminController@addTeacher');

            Route::match(['get','post'],'admin/add-student', 'AdminController@addStudent');

            Route::match(['get','post'],'admin/admin-profile/{id}', 'AdminController@admin_profile');

            Route::match(['get','post'],'admin/admin-setting/{id}', 'AdminController@admin_setting');

            Route::match(['get','post'],'admin/add-user', 'AdminController@add_user');

            Route::match(['get','post'],'admin/manage-users', 'AdminController@manage_users');

            Route::match(['get','post'],'admin/update-users-status/{id}', 'AdminController@update_users_status');

            Route::match(['get','post'],'admin/edit-user/{id}', 'AdminController@edit_user');

             Route::match(['get','post'],'admin/delete-user/{id}', 'AdminController@delete_user');

            Route::match(['get','post'],'admin/add-user-type', 'AdminController@add_user_type');

            Route::match(['get','post'],'admin/manage-users-type', 'AdminController@manage_users_type');

            Route::match(['get','post'],'admin/update-users-type-status/{id}', 'AdminController@update_users_type_status');

            Route::match(['get','post'],'admin/edit-user-type/{id}', 'AdminController@edit_user_type');

            Route::match(['get','post'],'admin/delete-user-type/{id}', 'AdminController@delete_user_type');

            Route::match(['get','post'],'admin/add-student-teacher/{id}', 'AdminController@add_stu_teacher');

            Route::match(['get','post'],'admin/add-inst-users', 'AdminController@add_inst_users');

            Route::match(['get','post'],'admin/manage-inst-users', 'AdminController@manage_inst_users');

            Route::match(['get','post'],'admin/add-session', 'AdminController@add_session');

            Route::match(['get','post'],'admin/manage-session', 'AdminController@manage_session');

            Route::match(['get','post'],'admin/update-session-status/{id}', 'AdminController@update_session_status');

            Route::match(['get','post'],'admin/delete-session/{id}', 'AdminController@delete_session');

            Route::match(['get','post'],'admin/edit-session/{id}', 'AdminController@edit_session');

            Route::match(['get','post'],'admin/add-class', 'AdminController@add_class');

            Route::match(['get','post'],'admin/manage-class', 'AdminController@manage_class');

            Route::match(['get','post'],'admin/update-class-status/{id}', 'AdminController@update_class_status');

            Route::match(['get','post'],'admin/delete-class/{id}', 'AdminController@delete_class');

            Route::match(['get','post'],'admin/edit-class/{id}', 'AdminController@edit_class');

            Route::match(['get','post'],'admin/add-questions/{id}', 'AdminController@add_questions');

            Route::match(['get','post'],'admin/sublevel-add-questions/{id}', 'AdminController@sublevel_add_questions');

            Route::match(['get','post'],'admin/manage-questions/{id}', 'AdminController@manage_questions');

            Route::match(['get','post'],'admin/sublevel-manage-questions/{id}', 'AdminController@sublevel_manage_questions');

            Route::match(['get','post'],'admin/update-questions-status/{id}', 'AdminController@update_questions_status');

            Route::match(['get','post'],'admin/delete-questions/{id}', 'AdminController@delete_questions');

            Route::match(['get','post'],'admin/edit-questions/{id}', 'AdminController@edit_questions');

            Route::match(['get','post'],'admin/sublevel-edit-questions/{id}', 'AdminController@sublevel_edit_questions');

            Route::match(['get','post'],'admin/add-package', 'PackageController@add');

            Route::match(['get','post'],'admin/packages', 'PackageController@index');

            Route::match(['get','post'],'admin/edit-package/{id}', 'PackageController@edit_package');

            Route::match(['get','post'],'admin/delete-package/{id}', 'PackageController@delete_package');

            Route::match(['get','post'],'admin/update-package-status/{id}', 'PackageController@update_package_status');

            Route::match(['get','post'],'admin/levels', 'AdminController@manage_levels');

            Route::match(['get','post'],'admin/sub-levels/{id}', 'AdminController@sub_levels');

            Route::match(['get','post'],'admin/level-sub-levels/{id}', 'AdminController@levelSubLevels');

            Route::match(['get','post'],'admin/update-level-status/{id}', 'AdminController@update_level_status');

            Route::match(['get','post'],'admin/update-sub-level-status/{id}', 'AdminController@update_sub_level_status');

            Route::match(['get','post'],'admin/edit-sub-level/{id}', 'AdminController@edit_sub_level');

            Route::match(['get','post'],'admin/edit-level-sub-level/{id}', 'AdminController@edit_level_sub_level');

            Route::match(['get','post'],'admin/edit-level-sub-level', 'AdminController@edit_level_sub_level');

            Route::match(['get','post'],'admin/edit-level/{id}', 'AdminController@edit_level');

            Route::match(['get','post'],'admin/delete-level/{id}', 'AdminController@delete_level');

      });
});








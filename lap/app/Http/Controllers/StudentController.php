<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use App\User;
use App\StudentDetail;
use App\Session;
use App\UserType;
use App\Classes;
use DB;
use Auth;


class StudentController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() 
    {
        if(Auth::user()->guest_status == 1)
        {
            $student = User::where('type','student')->first();
            return view('frontend.students',['student'=>$student]);
        }
        else
        {
            if(Auth::user()->type == 'teacher')
            {
                $student = User::where('teacher_id',Auth::user()->id)->get();
            }
            elseif(Auth::user()->type == 'Institute')
            {
                if(isset($_GET['teacherId']))
                {
                    $teaId = decrypt($_GET['teacherId']);
                    $student = User::where('teacher_id',$teaId)->get();
                }
                else
                {
                     $student = User::where('institute_id',Auth::user()->id)->with('students')->get();
                     //echo "<pre>"; print_r($student);die;
                }
            }
        }
        
    	return view('frontend.students',['student'=>$student]);
    }

    public function addStudent(Request $request)
    {

    	if($request->all()) 
    	{
                if($request->id == '')
                {
                    $user = new User();
                }
                else
                {
                    $user = User::find($request->id);
                }

                if(Auth::user()->type == 'teacher')
                {
                    $user->teacher_id = Auth::user()->id;
                }
                else
                {
                    $user->institute_id = Auth::user()->id;
                }

                if($request->teacher_id != '')
                {
                    $user->teacher_id = $request->teacher_id;
                }

                $user->name = $request->name;
                $user->name = $request->name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->password_hint = $request->password;
                $user->dob = $request->dob;
                $user->type = 'student';
                $user->status = 1;
                $user->save();

                if($request->id == '')
                {
                    $studentDetail = new StudentDetail();
                    $studentDetail->student_id = $user->id;
                }
                else
                {
                    $stDetail = StudentDetail::where('student_id',$request->id)->first();
                    if($stDetail == '')
                    {
                        $studentDetail = new StudentDetail();
                    }
                    else
                    {
                        $studentDetail = $stDetail;
                    }
                    $studentDetail->student_id = $request->id;
                }
                
                $studentDetail->reference_code = $request->reference_code;
                $studentDetail->grade = $request->grade;
                $studentDetail->gender = $request->gender;
                $studentDetail->timings = $request->timing;
                $studentDetail->save();

                return redirect('/students')->with('success', 'Congrats! new student has been added to your system');
        }
    	return view('frontend.add-students');
    }
}

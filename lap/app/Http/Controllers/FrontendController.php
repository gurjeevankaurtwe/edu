<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use App\Level;
use App\User;
use App\SubLevel;
use App\Question;
use App\PackageManagement;
use App\StudentLevelDetail;
use App\StudentDetail;
use App\LevelSubLevel;
use App\Session;
use App\UserType;
use App\Classes;
use App\StudentTempoTrainer;
use App\StudentTempoTrainerScore;
use DB;
use Redirect;
use Auth;
use Image;

class FrontendController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }
    
    /*======show levels list=====*/

    public function userLogin()
    {
        Auth::logout();
        return redirect('/login');
    }

    
    public function index() 
    {
       if(Auth::user())
        {
            /*if(Auth::user()->guest_status == 1)
            {
                $student = User::where('type','student')->first();
                return view('frontend.students',['student'=>$student]);
            }
            else
            {*/
                if((Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute') && !isset($_GET['student']))
                {
                    if(Auth::user()->type == 'teacher')
                    {
                        $student = User::where('teacher_id',Auth::user()->id)->get();
                    }
                    elseif(Auth::user()->type == 'Institute')
                    {
                        $student = User::where('institute_id',Auth::user()->id)->with('students')->get();
                    }
                    return view('frontend.students',['student'=>$student]);
                }
                elseif(Auth::user()->type == 'admin')
                {
                    return view('admin.admin');
                }
                else
                {
                    $studentId = '';
                    if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute')
                    {
                        if(isset($_GET['student']))
                        {
                            $studentId = decrypt($_GET['student']);
                        }
                    }
                    elseif(Auth::user()->type == 'student')
                    {
                        $studentId = Auth::user()->id;
                    }
                    $level = Level::where('status',1)->with('levelSubLevels')->get();
                    $user = User::where('id',$studentId)->first();
                    return view('frontend.index',compact('level','studentId','user'));
                }
            /*}*/
            
        }
        else
        {
            return redirect('/login');
        }
    }

    /*=======get level sub levels=======*/

    public function levelSubLevel($id)
    {
        if(Auth::user())
        {
            if((Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute') && !isset($_GET['student']))
            {
                if(Auth::user()->type == 'teacher')
                {
                    $student = User::where('teacher_id',Auth::user()->id)->get();
                }
                elseif(Auth::user()->type == 'Institute')
                {
                    $student = User::where('institute_id',Auth::user()->id)->with('students')->get();
                }
                return view('frontend.students',['student'=>$student]);
            }
            elseif(Auth::user()->type == 'student' && !isset($_GET['student']))
            {
                $studentId = Auth::user()->id;
                $level = Level::where('status',1)->with('levelSubLevels')->get();
                return view('frontend.index',compact('level','studentId'));
            }
            elseif(Auth::user()->type == 'admin')
            {
                return redirect('/admin');
            }
            else
            {
                $studentId = decrypt($_GET['student']);
                $levelId = decrypt($id);
                $levelSubLevel = SubLevel::where('level_id',$levelId)->where('status',1)->with(['subLevelsQues','levelSubLevel'])->orderBy('id','ASC')->get();
                return view('frontend.sub-level',compact('levelSubLevel','studentId'));
            }
            
        }
        else
        {
        	return redirect('/login');
        }
        
    }

    /*======show level questions====*/
   
    public function levelQuestions($id)
    {
        if(Auth::user())
        {
            if((Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute') && !isset($_GET['student']))
            {
                if(Auth::user()->type == 'teacher')
                {
                    $student = User::where('teacher_id',Auth::user()->id)->get();
                }
                elseif(Auth::user()->type == 'Institute')
                {
                    $student = User::where('institute_id',Auth::user()->id)->with('students')->get();
                }
                return view('frontend.students',['student'=>$student]);
            }
            elseif(Auth::user()->type == 'student' && !isset($_GET['student']))
            {
                $studentId = Auth::user()->id;
                $level = Level::where('status',1)->with('levelSubLevels')->get();
                return view('frontend.index',compact('level','studentId'));
            }
            elseif(Auth::user()->type == 'admin')
            {
                return redirect('/admin');
            }
            else
            {
                $studentId = decrypt($_GET['student']);

                $subLevelId = decrypt($id);
                if(isset($_GET['subLevel']))
                {
                    $ques = Question::where('level_sub_level_id',$subLevelId)->where('status',1)->first();
                }
                else
                {
                    $ques = Question::where('sub_level_id',$subLevelId)->where('status',1)->first();
                }
                if($subLevelId == 64 || $subLevelId == 65 || $subLevelId == 66 || $subLevelId == 67 || $subLevelId == 68 || $subLevelId == 69 || $subLevelId == 70)
                {
                    $subLevels = SubLevel::where('id',$subLevelId)->first();
                    $levelDetail = Level::where('id',$subLevels->level_id)->first();
                }
                else
                {
                    if($ques != '')
                    {
                        if(isset($_GET['subLevel']))
                        {
                            $subLevels = LevelSubLevel::where('id',$ques->level_sub_level_id)->first();
                            $levelDetail = Level::where('id',$subLevels->level_id)->first();
                        }
                        else
                        {
                            $subLevels = SubLevel::where('id',$ques->sub_level_id)->first();
                            $levelDetail = Level::where('id',$subLevels->level_id)->first();
                        }
                    }
                    else
                    {
                        $levelDetail = '';
                        $subLevels = '';
                    }
                }
                return view('frontend.level-detail',compact('ques','subLevelId','subLevels','levelDetail','studentId'));
            }
        }
        else
        {
        	return redirect('/login');
        }
        
    }

    /*======get first question for level====*/

    public function getQuestions($id)
    {
        $prevqueId = '';
        $subLevelId = $id;
        $studentId = $_GET['studentId'];
        $teacherId = Auth::user()->id;
        $subSubLevelId = $_GET['subLevelIds'];
        if(Auth::user()->type == 'teacher')
        {
            if($subSubLevelId != 0)
            {
                $stuCount = StudentLevelDetail::where('student_id',$studentId)->where('teacher_id',$teacherId)->where('sublevel_id',$subLevelId)->count();
            }
            else
            {
                $stuCount = 0;
            }
            
        }
        elseif(Auth::user()->type == 'student' || Auth::user()->type == 'Institute')
        {
            if($subSubLevelId != 0)
            {
                $stuCount = StudentLevelDetail::where('student_id',$studentId)->where('sublevel_id',$subLevelId)->count();
            }
            else
            {
                $stuCount = 0;
            }
        }
        if($stuCount>0)
        {
            $data=DB::select("SELECT  *
                FROM    questions e
                WHERE sub_level_id='$subLevelId' AND  NOT EXISTS
                        (
                        SELECT  * 
                        FROM    student_level_details d
                        WHERE   d.question_id = e.id
                        )");
            if(count($data)>0)
            {
                $ques = $data[0];
                $data=DB::select("SELECT  *
                FROM    questions e
                WHERE sub_level_id='$subLevelId' AND  EXISTS
                        (
                        SELECT  * 
                        FROM    student_level_details d
                        WHERE   d.question_id = e.id
                        )");
                foreach($data as $val)
                {
                    $prevId[] = $val->id;
                }
                $prevqueId = implode(",",$prevId);
            }
            else
            {
                $ques = Question::where('sub_level_id',$subLevelId)->where('status',1)->first();
                $prevqueId = '';
            }
        }
        else
        {
            if($subSubLevelId != 0)
            {
                $ques = Question::where('sub_level_id',$subLevelId)->where('session_id','!=',0)->where('status',1)->first();
                $prevqueId = '';
            }
            else
            {
                $ques = Question::where('session_id',0)->where('level_sub_level_id',$subLevelId)->where('status',1)->first();
                $prevqueId = '';
            }
        }

        if($subSubLevelId != 0)
        {
            $levels = SubLevel::where('id',$subLevelId)->first();
        }
        else
        {
            $levels = LevelSubLevel::where('id',$subLevelId)->first();
        }
        if(Auth::user()->type == 'teacher')
        {
            $teacherId = Auth::user()->id;
            $studentId = $_GET['studentId'];
            if($subSubLevelId != 0)
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->whereNull('session_id')->where('teacher_id',$teacherId)->where('sublevel_id',$subLevelId)->get();
            }
            else
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->where('session_id',0)->where('teacher_id',$teacherId)->where('sublevel_id',$subLevelId)->get();
            }
        }
        if(Auth::user()->type == 'student' || Auth::user()->type == 'Institute')
        {
            if(Auth::user()->type == 'student')
            {
                $studentId = Auth::user()->id;
            }
            elseif(Auth::user()->type == 'Institute')
            {
                $studentId =$_GET['studentId'];
            }
            if($subSubLevelId != 0)
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->whereNull('session_id')->where('sublevel_id',$subLevelId)->get();
            }
            else
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->where('session_id',0)->where('sublevel_id',$subLevelId)->get();
            }
            
        }
        if($subSubLevelId != 0)
        {
            $getTotalQues = Question::where('sub_level_id',$subLevelId)->where('session_id','!=',0)->where('status',1)->get();
        }
        else
        {
            $getTotalQues = Question::where('level_sub_level_id',$subLevelId)->where('session_id','=',0)->where('status',1)->get();
        }
        
        if($subLevelId == 64 || $subLevelId == 65 || $subLevelId == 66 || $subLevelId == 67 || $subLevelId == 68 || $subLevelId == 69 ||  $subLevelId == 70)
        {
            $getTotalQuess = 1;
            if($subLevelId == 67 || $subLevelId == 68 || $subLevelId == 69 ||  $subLevelId == 70)
            {
                DB::table('student_tempo_trainers')->where('sub_level_id', $subLevelId)->where('student_id',$studentId)->delete();
            }
        }
        else
        {
            $getTotalQuess = count($getTotalQues);
        }

        $totalWidth =  (count($compSubLevels)*100)/$getTotalQuess;
        return view('frontend.include.ajax_que',compact('ques','levels','subLevelId','studentId','totalWidth','prevqueId'));
    }

    /*=====get next question for level on click next button======*/
    
    public function getQuestionsNext($id,$que_id)
    {
        $subLevelId = $id;
        $prevqueId = $que_id;
        if(Auth::user()->type == 'teacher')
        {
            $teacherId = Auth::user()->id;
            $studentId = $_GET['studentId'];
        }
        if(Auth::user()->type == 'student' || Auth::user()->type == 'Institute')
        {
            $stuDetail = User::where('id',Auth::user()->id)->first();
            if($stuDetail->teacher_id != '')
            {
                $teacherId = $stuDetail->teacher_id;
            }
            else
            {
                $teacherId = NULL;
            }
            if(Auth::user()->type == 'student')
            {
                $studentId = Auth::user()->id;
            }
            elseif(Auth::user()->type == 'Institute')
            {
                $studentId =$_GET['studentId'];
            }
            
        }
        $allPrevVals = explode(",",$_GET['all_prev']);
        $allPrevValss = array_unique($allPrevVals);
        $allPrevVal = implode(",",$allPrevValss);
        if($teacherId != '')
        {
            if($_GET['sessionId'] == 0)
            {
                $stu = StudentLevelDetail::where('student_id',$studentId)->where('session_id',0)->where('teacher_id',$teacherId)->where('question_id',$que_id)->where('sublevel_id',$subLevelId)->first();
            }
            else
            {
                $stu = StudentLevelDetail::where('student_id',$studentId)->whereNull('session_id')->where('teacher_id',$teacherId)->where('question_id',$que_id)->where('sublevel_id',$subLevelId)->first();
            }
        }

        else
        {
            if($_GET['sessionId'] == 0)
            {
                $stu = StudentLevelDetail::where('student_id',$studentId)->where('session_id',0)->where('question_id',$que_id)->where('sublevel_id',$subLevelId)->first();
            }
            else
            {
                $stu = StudentLevelDetail::where('student_id',$studentId)->whereNull('session_id')->where('question_id',$que_id)->where('sublevel_id',$subLevelId)->first();
            }
        }
        
        if($stu == '')
        {
            $studentLevelDetail = new StudentLevelDetail;
            $studentLevelDetail->student_id = $studentId;
            $studentLevelDetail->teacher_id = $teacherId;
            $studentLevelDetail->question_id = $que_id;
            $studentLevelDetail->sublevel_id = $subLevelId;
            if($_GET['sessionId'] == 0)
            {
                $studentLevelDetail->session_id = 0;
            }
            $studentLevelDetail->save();
        }
        
        if($_GET['all_prev']=='')
        {
            if($_GET['sessionId'] == 0)
            {
                $ques = Question::where('level_sub_level_id',$subLevelId)->where('session_id',$_GET['sessionId'])->where('id','!=',$prevqueId)->where('status',1)->first();
                $levels = LevelSubLevel::where('id',$subLevelId)->first();
                $status = 0;
            }
            else
            {
                $ques = Question::where('sub_level_id',$subLevelId)->where('id','!=',$prevqueId)->where('status',1)->first();
                $levels = SubLevel::where('id',$subLevelId)->first();
                $status = 1;
            }
        }
        else
        {

            $dataIds=explode(',',$allPrevVal);
            $all_ids=array_filter($dataIds);

            if($_GET['sessionId'] == 0)
            {
                $ques = Question::where('level_sub_level_id',$subLevelId)->where('session_id',$_GET['sessionId'])->whereNotIn('id', $all_ids)->where('status',1)->first();
                $levels = LevelSubLevel::where('id',$subLevelId)->first();
                $status = 0;
            }
            else
            {
                $ques = Question::where('sub_level_id',$subLevelId)->whereNotIn('id', $all_ids)->where('status',1)->first();
                $levels = SubLevel::where('id',$subLevelId)->first();
                $status = 1;
            }
        }

        if(Auth::user()->type == 'teacher')
        {
            if($_GET['sessionId'] == 0)
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->where('session_id',0)->where('teacher_id',$teacherId)->where('sublevel_id',$subLevelId)->get();
            }
            else
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->whereNull('session_id')->where('teacher_id',$teacherId)->where('sublevel_id',$subLevelId)->get();
            }
        }
        elseif(Auth::user()->type == 'student' || Auth::user()->type == 'Institute')
        {
            if($_GET['sessionId'] == 0)
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->where('session_id',0)->where('sublevel_id',$subLevelId)->get();
            }
            else
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->whereNull('session_id')->where('sublevel_id',$subLevelId)->get();
            }
        }
        if($_GET['sessionId'] == 0)
        {
             $getTotalQuess = Question::where('level_sub_level_id',$subLevelId)->where('session_id',0)->where('status',1)->get();
        }
        else
        {
            $getTotalQuess = Question::where('sub_level_id',$subLevelId)->where('session_id','!=',0)->where('status',1)->get();
        }
        $totalWidth =  (count($compSubLevels)*100)/count($getTotalQuess);
        return view('frontend.include.ajax_que_next',compact('ques','status','subLevelId','levels','prevqueId','studentId','allPrevVal','totalWidth','getTotalQues','compSubLevels','teacherId'));
    }

    /*======get previous question for level on click previos button=====*/

    public function getQuestionsPrev($subLevelId,$prevqueId)
    {
        
        $explodeallPrevQueId = explode(",",$_GET['allPrevId']);
        $allPrevQueId = array_unique($explodeallPrevQueId);
        $uniqueAllPrevQueId = array_unique($explodeallPrevQueId);
        
        $arrayPop = end($allPrevQueId);
        
        /*if(count($allPrevQueId) > 1)
        {*/
        array_pop($allPrevQueId); 
       /* }*/
        
        $allPrevVal = implode(",",$allPrevQueId);

        if($_GET['sessionId'] == 0)
        {
            $ques = Question::where('level_sub_level_id',$subLevelId)->where('session_id',$_GET['sessionId'])->where('id', end($uniqueAllPrevQueId))->where('status',1)->first();
            $levels = LevelSubLevel::where('id',$subLevelId)->first();
            $status = 0;
        }
        else
        {

            $ques = Question::where('sub_level_id',$subLevelId)->where('id', end($uniqueAllPrevQueId))->where('status',1)->first();
            $levels = SubLevel::where('id',$subLevelId)->first();
            $status = 1;
        }

        if(Auth::user()->type == 'teacher')
        {
            $teacherId = Auth::user()->id;
            $studentId = $_GET['studentId'];
            if($_GET['sessionId'] == 0)
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->where('session_id',0)->where('teacher_id',$teacherId)->where('sublevel_id',$subLevelId)->get();
            }
            else
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->whereNull('session_id')->where('teacher_id',$teacherId)->where('sublevel_id',$subLevelId)->get();
            }
        }
        if(Auth::user()->type == 'student' || Auth::user()->type == 'Institute')
        {
            $teacherId = '';
            if(Auth::user()->type == 'student')
            {
                $studentId = Auth::user()->id;
            }
            elseif(Auth::user()->type == 'Institute')
            {
                $studentId =$_GET['studentId'];
            }
            if($_GET['sessionId'] == 0)
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->where('session_id',0)->where('sublevel_id',$subLevelId)->get();
            }
            else
            {
                $compSubLevels = StudentLevelDetail::where('student_id',$studentId)->whereNull('session_id')->where('sublevel_id',$subLevelId)->get();
            }
            
        }
        if($_GET['sessionId'] == 0)
        {
             $getTotalQues = Question::where('level_sub_level_id',$subLevelId)->where('session_id',0)->where('status',1)->get();
        }
        else
        {
            $getTotalQues = Question::where('sub_level_id',$subLevelId)->where('session_id','!=',0)->where('status',1)->get();
        }
        $totalWidth =  (count($compSubLevels)*100)/count($getTotalQues);

        return view('frontend.include.ajax_que_next',compact('ques','status','subLevelId','totalWidth','prevqueId','studentId','allPrevVal','levels'));
    }

    /*=============Teacher Management teacher according to institute===========*/

    public function teacherList()
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
    			$teacher = User::where('institute_id',Auth::user()->id)->where('type','teacher')->orderBy('id','DESC')->get();
    			return view('frontend.teacher',compact('teacher'));
    		}
    		elseif(Auth::user()->type == 'admin')
            {
                return redirect('/admin');
            }
    	}
    	else
        {
        	return view('auth.login');
        }
    }

    /*===========Institute add teacher============*/

    public function addTeacher(Request $request)
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
    			if($request->all())
    			{
    				if($request->id == '')
	            	{
	    				$validator = Validator::make(Input::all(), [
	                        'name' => 'required',
	                        'last_name' => 'required',
	                        'dob' => 'required',
	                        'password' => 'required|min:5',
	                        'email' => 'unique:users|required|email'
	                            ], [
	                        'name.required' => ' Sorry! You can not leave name empty.',
	                        'last_name.required' => ' Sorry! You can not leave lastname empty.',
	                        'gender.required' => ' Sorry! You can not leave gender empty.',
	                        'password.required' => ' Sorry! You can not leave password empty.',
	                        'email.required' => 'Sorry! You can not leave email empty.',
	                        'email.unique' => 'Sorry! This email is already registered.',
	                        'email.email' => 'Email type must be accordingly.',
	                        'password.min' => 'Sorry! Please enter atleast value 5'
			            ]);
			        }
	    			else
	    			{
	    				$validator = Validator::make(Input::all(), [
	                        'name' => 'required',
	                        'last_name' => 'required',
	                        'dob' => 'required',
	                        'password' => 'required|min:5',
	                        'email' => 'required|email'
	                            ], [
	                        'name.required' => ' Sorry! You can not leave name empty.',
	                        'last_name.required' => ' Sorry! You can not leave lastname empty.',
	                        'dob.required' => ' Sorry! You can not leave date of birth empty.',
	                        'password.required' => ' Sorry! You can not leave password empty.',
	                        'email.required' => 'Sorry! You can not leave email empty.',
	                        'email.email' => 'Email type must be accordingly.',
	                        'password.min' => 'Sorry! Please enter atleast value 5'
			            ]);
			        }
	    			if ($validator->fails()) 
		            {
		            	if($request->id == '')
	            		{
	            			return redirect('/add-teacher')->withErrors($validator)->withInput();
	            		}
	            		else
	            		{
	            			return redirect('/edit-teacher/'.encrypt($request->id))->withErrors($validator)->withInput();
	            		}
		                
		            }
		            
		            else 
		            {
		            	if($request->id == '')
		            	{
		            		$user = new User;
			                $user->name = $request->name;
			                $user->email = $request->email;
			                $user->dob = $request->dob;
			                $user->last_name = $request->last_name;
			                $user->password = Hash::make($request->password);
			                $user->password_hint = $request->password;
			                $user->type = 'teacher';
			                $user->institute_id = Auth::user()->id;
		            	}
		            	else
		            	{
		            		$user = User::find($request->id);
		            		$user->name = $request->name;
			                $user->email = $request->email;
			                $user->dob = $request->dob;
			                $user->last_name = $request->last_name;
			                $user->password = Hash::make($request->password);
			                $user->password_hint = $request->password;
			                 $user->type = 'teacher';
			                $user->institute_id = Auth::user()->id;
		            	}
			                
		                $user->save();
						return redirect('/teachers')->with('success', 'Congrats! new teacher has been added to your system');
		            }
    			}
    			else
    			{
    				return view('frontend.add-teacher');
    			}
    		}
    		elseif(Auth::user()->type == 'student')
    		{

    		}
    		elseif(Auth::user()->type == 'teacher')
    		{
    			 //$student = User::where('teacher_id',Auth::user()->id)->get();
    		}
    		elseif(Auth::user()->type == 'admin')
            {
                return redirect('/admin');
            }
    	}
    	else
        {
        	return view('auth.login');
        }

    }

    /*===========Edit teacher by institute==============*/

    public function editTeacher($id)
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
				$teaDetail = User::where('id',decrypt($id))->first();
    			return view('frontend.edit-teacher',compact('teaDetail'));
    		}
    		elseif(Auth::user()->type == 'admin')
    		{
    			return redirect('/admin');
    		}
    		elseif(Auth::user()->type == 'student')
    		{

    		}
    		elseif(Auth::user()->type == 'teacher')
    		{
    			 //$student = User::where('teacher_id',Auth::user()->id)->get();
    		}
    	}
    	else
    	{
    		return view('auth.login');
    	}
    }

    /*==========Delete teacher by institute=============*/

    public function deleteTeacher($id)
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
    			$user = User::find(decrypt($id));    
				$user->delete();
				return redirect('/teachers')->with('success', 'Record is no more with your system!');
    		}
    		elseif(Auth::user()->type == 'admin')
    		{
    			return redirect('/admin');
    		}
    		elseif(Auth::user()->type == 'student')
    		{

    		}
    		elseif(Auth::user()->type == 'teacher')
    		{
    			 //$student = User::where('teacher_id',Auth::user()->id)->get();
    		}
    	}
    	else
    	{
    		return view('auth.login');
    	}
    }

    /*===========View teacher detail==========*/

    public function viewTeacherDetail($id)
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
    			$teaDetail = User::where('id',decrypt($id))->first();
    			return view('frontend.view-teacher-detail',compact('teaDetail'));
    		}
    		elseif(Auth::user()->type == 'admin')
    		{
    			return redirect('/admin');
    		}
    		elseif(Auth::user()->type == 'student')
    		{

    		}
    		elseif(Auth::user()->type == 'teacher')
    		{
    			 //$student = User::where('teacher_id',Auth::user()->id)->get();
    		}
    	}
    	else
    	{
    		return view('auth.login');
    	}
    }

    /*===========Update teacher status===========*/

    public function updateTeacherStatus($id,$status)
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
    			$user = User::find(decrypt($id));
    			$user->status = $status;
    			$user->save();
    			if($status == 0)
    			{
    				return redirect('/teachers')->with('success', 'Teacher deactivated successfully!');
    			}
    			else
    			{
    				return redirect('/teachers')->with('success', 'Teacher deactivated successfully!');
    			}
    		}
    		elseif(Auth::user()->type == 'admin')
    		{
    			return redirect('/admin');
    		}
    		elseif(Auth::user()->type == 'student')
    		{

    		}
    		elseif(Auth::user()->type == 'teacher')
    		{
    			 //$student = User::where('teacher_id',Auth::user()->id)->get();
    		}
    	}
    	else
    	{
    		return view('auth.login');
    	}

    }

    /*===========Update student status by institute===========*/

    public function updateStudentStatus($id,$status)
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
    			$user = User::find(decrypt($id));
    			$user->status = $status;
    			$user->save();
    			if($status == 0)
    			{
    				return redirect('/students')->with('success', 'Teacher deactivated successfully!');
    			}
    			else
    			{
    				return redirect('/students')->with('success', 'Teacher deactivated successfully!');
    			}
    		}
    		elseif(Auth::user()->type == 'admin')
    		{
    			return redirect('/admin');
    		}
    		elseif(Auth::user()->type == 'student')
    		{

    		}
    		elseif(Auth::user()->type == 'teacher')
    		{
    			 //$student = User::where('teacher_id',Auth::user()->id)->get();
    		}
    	}
    	else
    	{
    		return view('auth.login');
    	}
	}

    /*==========Delete student by institute=============*/

    public function deleteStudent($id)
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
    			$user = User::find(decrypt($id));    
				$user->delete();
				return redirect('/students')->with('success', 'Record is no more with your system!');
    		}
    		elseif(Auth::user()->type == 'admin')
    		{
    			return redirect('/admin');
    		}
    		elseif(Auth::user()->type == 'student')
    		{

    		}
    		elseif(Auth::user()->type == 'teacher')
    		{
    			 //$student = User::where('teacher_id',Auth::user()->id)->get();
    		}
    	}
    	else
    	{
    		return view('auth.login');
    	}
    }

    /*===========Edit student by institute==============*/

    public function editStudent($id)
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
				$stuDetail = User::where('id',decrypt($id))->first();
				$stuFurDetail = StudentDetail::where('student_id',$stuDetail->id)->first();
                $teacher = User::where('institute_id',Auth::user()->id)->where('type','teacher')->where('status',1)->orderBy('id','DESC')->get();
    			return view('frontend.edit-student',compact('stuDetail','stuFurDetail','teacher'));
    		}
    		elseif(Auth::user()->type == 'admin')
    		{
    			return redirect('/admin');
    		}
    		elseif(Auth::user()->type == 'student')
    		{

    		}
    		elseif(Auth::user()->type == 'teacher')
    		{
    			 //$student = User::where('teacher_id',Auth::user()->id)->get();
    		}
    	}
    	else
    	{
    		return view('auth.login');
    	}
    }

    /*===========View student detail==========*/

    public function viewStudentDetail($id)
    {
    	if(Auth::user())
    	{
    		if(Auth::user()->type == 'Institute')
    		{
    			$stuDetail = User::where('id',decrypt($id))->first();
				$stuFurDetail = StudentDetail::where('student_id',$stuDetail->id)->first();
                $teacher = User::where('institute_id',Auth::user()->id)->where('type','teacher')->where('status',1)->orderBy('id','DESC')->get();
    			return view('frontend.view-student-detail',compact('stuDetail','stuFurDetail','teacher'));
    		}
    		elseif(Auth::user()->type == 'admin')
    		{
    			return redirect('/admin');
    		}
    		elseif(Auth::user()->type == 'student')
    		{

    		}
    		elseif(Auth::user()->type == 'teacher')
    		{
    			 //$student = User::where('teacher_id',Auth::user()->id)->get();
    		}
    	}
    	else
    	{
    		return view('auth.login');
    	}
    }

/*====================Edit user profile================*/

    public function myProfile(Request $request)
    {
        $userData = User::where('id',Auth::user()->id)->first();
        if($userData->type == 'student')
        {
            $userDetail = StudentDetail::where('student_id',$userData->id)->first();
        }
        if ($request->all()) 
        {
            if($userData->type == 'teacher')
            {
                $validator = Validator::make(Input::all(), [
                            'first_name' => 'required',
                            'last_name' => 'required',
                            'dob' => 'required',
                            'email' => 'required|email'
                                ], [
                            'first_name.required' => ' Sorry! You can not leave first name empty.',
                            'last_name.required' => ' Sorry! You can not leave last name empty.',
                            'dob.required' => ' Sorry! You can not leave dob empty.',
                            'email.required' => 'Sorry! You can not leave email empty.',
                            'email.email' => 'Email type must be accordingly.'
                ]);
            }
            else if($userData->type == 'Institute')
            {
                $validator = Validator::make(Input::all(), [
                    'name' => 'required',
                    'email' => 'required|email'
                        ], [
                    'name.required' => ' Sorry! You can not leave first name empty.',
                    'email.required' => 'Sorry! You can not leave email empty.',
                    'email.email' => 'Email type must be accordingly.'
                ]);
            }
            else if($userData->type == 'student')
            {
                $validator = Validator::make(Input::all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'dob' => 'required',
                    'grade' => 'required',
                    'gender' => 'required',
                    'email' => 'required|email'
                        ], [
                    'first_name.required' => ' Sorry! You can not leave first name empty.',
                    'last_name.required' => ' Sorry! You can not leave last name empty.',
                    'dob.required' => ' Sorry! You can not leave dob empty.',
                    'grade.required' => ' Sorry! You can not leave grade empty.',
                    'gender.required' => ' Sorry! You need to select gender.',
                    'email.required' => 'Sorry! You can not leave email empty.',
                    'email.email' => 'Email type must be accordingly.'
                ]);
            }
            if ($validator->fails()) 
            {
                return redirect('/my-profile')->withErrors($validator)->withInput();
            } 
            else 
            {
                //dd($request->old_image);

                if($request->user_type == 'teacher')
                {
                    $user = User::find($request->id);
                    $user->name = $request->first_name;
                    $user->dob = $request->dob;
                    $user->last_name = $request->last_name;
                    $user->email = $request->email;
                    $user->type = $request->user_type;
                    if($request->old_image == '')
                    {
                        if (isset($request->profile_image)) 
                        {
                            $file = $request->profile_image;
                            $imageType = $file->getClientmimeType();
                            $fileName = $file->getClientOriginalName();
                            $fileNameUnique = time() . '_' . $fileName;
                            $destinationPath = public_path() . '/frontend/images/faces/';
                            $file->move($destinationPath, $fileNameUnique);
                            $imagename = $fileNameUnique;

                        }
                        else
                        {
                            $imagename = '';
                        } 
                        $user->image = $imagename;
                    }
                    $user->save();
                    return redirect('/my-profile')->with('success', 'You have updated the record!');
                }
                else if($request->user_type == 'student')
                {
                    $user = User::find($request->id);
                    $user->name = $request->first_name;
                    $user->dob = $request->dob;
                    $user->last_name = $request->last_name;
                    $user->email = $request->email;
                    $user->type = $request->user_type;
                    if($request->old_image == '')
                    {
                        if (isset($request->profile_image)) 
                        {
                            $file = $request->profile_image;
                            $imageType = $file->getClientmimeType();
                            $fileName = $file->getClientOriginalName();
                            $fileNameUnique = time() . '_' . $fileName;
                            $destinationPath = public_path() . '/frontend/images/faces/';
                            $file->move($destinationPath, $fileNameUnique);
                            $imagename = $fileNameUnique;

                        }
                        else
                        {
                            $imagename = '';
                        } 
                        $user->image = $imagename;
                    }
                    $user->save();

                    $stuDetail = StudentDetail::where('student_id',$user->id)->first();
                    $stuDetail->student_id = $user->id;
                    $stuDetail->reference_code = $request->reference_code;
                    $stuDetail->grade = $request->grade;
                    $stuDetail->gender = $request->gender;
                    $stuDetail->timings = $request->timing;
                    $stuDetail->save();
                    return redirect('/my-profile')->with('success', 'You have updated the record!');
                }
                else if($request->user_type == 'Institute')
                {
                    $user = User::find($request->id);
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->type = $request->user_type;
                    if($request->old_image == '')
                    {
                        if (isset($request->profile_image)) 
                        {
                            $file = $request->profile_image;
                            $imageType = $file->getClientmimeType();
                            $fileName = $file->getClientOriginalName();
                            $fileNameUnique = time() . '_' . $fileName;
                            $destinationPath = public_path() . '/frontend/images/faces/';
                            $file->move($destinationPath, $fileNameUnique);
                            $imagename = $fileNameUnique;

                        }
                        else
                        {
                            $imagename = '';
                        } 
                        $user->image = $imagename;
                    }
                    $user->save();
                    return redirect('/my-profile')->with('success', 'You have updated the record!');
                }
            }
        }
        return view('frontend.my-profile',get_defined_vars());
    }

    public function subLevelsLevel($id)
    {
        $Id = decrypt($id);
        if(Auth::user())
        {
            if((Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute') && !isset($_GET['student']))
            {
                if(Auth::user()->type == 'teacher')
                {
                    $student = User::where('teacher_id',Auth::user()->id)->get();
                }
                elseif(Auth::user()->type == 'Institute')
                {
                    $student = User::where('institute_id',Auth::user()->id)->with('students')->get();
                }
                return view('frontend.students',['student'=>$student]);
            }
            elseif(Auth::user()->type == 'student' && !isset($_GET['student']))
            {
                $studentId = Auth::user()->id;
                $level = Level::where('status',1)->with('levelSubLevels')->get();
                return view('frontend.index',compact('level','studentId'));
            }
            elseif(Auth::user()->type == 'admin')
            {
                return redirect('/admin');
            }
            else
            {
                $studentId = decrypt($_GET['student']);
                $levelId = decrypt($id);
                $levelSubLevel = LevelSubLevel::where('sub_level_id',$levelId)->where('status',1)->with('subLevelsQues')->orderBy('id','ASC')->get();
                return view('frontend.subLevelsLevel',get_defined_vars());
            }
            
        }
        else
        {
            return redirect('/login');
        }
    }

/*==============Add beep time for student tempo trainer(Level 4)=============*/

    public function addBeepTime()
    {
        $milliseconds = round(microtime(true) * 1000);
        $stuId = $_GET['stuId'];
        $subLevelId = $_GET['subLevelId'];
        $eartype = $_GET['eartype'];
        $lastBeepRecord = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->orderBy('id','desc')->first();
        if($lastBeepRecord == '')
        {
            $tempo = new StudentTempoTrainer();
            $tempo->sub_level_id = $subLevelId;
            $tempo->student_id = $stuId;
            $tempo->beep_time = $milliseconds;
            $tempo->ear_type = $eartype;
            $tempo->save();
        }
        else
        {

            if($lastBeepRecord->totalKeys > 0)
            {

                $anti = $lastBeepRecord->last_keystrock_time;
                $status = 1;
            }
            else
            {
                $anti = '';
                $status = 0;
            }
            if($lastBeepRecord->first_keystrock_time == '')
            {
                $updatePreviousStatus = StudentTempoTrainer::find($lastBeepRecord->id);
                $updatePreviousStatus->status = 0;
                $updatePreviousStatus->save();
            }

            $tempo = new StudentTempoTrainer();
            $tempo->sub_level_id = $subLevelId;
            $tempo->student_id = $stuId;
            $tempo->beep_time = $milliseconds;
            $tempo->ear_type = $eartype;
            $tempo->first_keystrock_time = $anti;
            $tempo->status = $status;
            $tempo->save();

            $getTotalTempoTrainer = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->orderBy('id','desc')->get();
            if(count($getTotalTempoTrainer) == 25)
            {
                $sumAverage = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->where('status',2)->sum('average_milli_sec');
                $response = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->where('status',2)->get();
                if(count($response) == 0)
                {
                    $cal = 0;
                }
                else
                {
                    $cal = $sumAverage/count($response);
                }
                $averageScore = new StudentTempoTrainerScore();
                $averageScore->sub_level_id = $subLevelId;
                $averageScore->student_id = $stuId;
                $averageScore->ear_type = $eartype;
                $averageScore->average_accuracy = round($cal);
                $averageScore->save();
            }
        }
        
        $result['success'] = 'done';
        echo json_encode($result);exit;
    }

/*===============Student Temo Trainer record(Level 4)===================*/
    
    public function studentTemoTrainer()
    {
        $milliseconds = round(microtime(true) * 1000);
        $stuId = $_GET['stuId'];
        $subLevelId = $_GET['subLevelId'];
        $eartype = $_GET['eartype'];
        $tempo = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->get();
        if(count($tempo) > 0)
        {
            $lastBeepRecord = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->orderBy('id','desc')->first();
            if($lastBeepRecord->first_keystrock_time == '')
            {
                
                $keyTime = StudentTempoTrainer::find($lastBeepRecord->id);
                $keyTime->first_keystrock_time = $milliseconds;
                $keyTime->average_milli_sec = $milliseconds-$lastBeepRecord->beep_time;
                $keyTime->totalKeys = 0;
                $keyTime->status = 2;
                $keyTime->save();
                if($keyTime->id != '')
                {
                    $result['success'] = true;
                }
                else
                {
                    $result['success'] = false;
                }
            }
            else
            {
                $keyTime = StudentTempoTrainer::find($lastBeepRecord->id);
                $keyTime->totalKeys = $lastBeepRecord->totalKeys+1;
                $keyTime->last_keystrock_time = $milliseconds;
                $keyTime->save();
                if($keyTime->id != '')
                {
                    $result['success'] = true;
                }
                else
                {
                    $result['success'] = false;
                }
            }
        }
        else
        {
            $result['success'] = false;
        }
        echo json_encode($result);exit;
    }

/*==============Get student average score after tempo trainer completion============*/
    
    public function studentAverageScore()
    {
        $stuId = $_GET['stuId'];
        $subLevelId = $_GET['subLevelId'];
        $eartype = $_GET['eartype'];
        if($subLevelId == 67 || $subLevelId == 68)
        {
            $noResponse = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->where('status',0)->get();
            $response = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->where('status',2)->get();
            $anti = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->where('status',1)->get();
            $sumAverage = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('ear_type',$eartype)->where('status',2)->sum('average_milli_sec');
        }
        if($subLevelId == 69 ||  $subLevelId == 70)
        {
            $noResponse = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('status',0)->get();
            $response = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('status',2)->get();
            $anti = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('status',1)->get();
            $sumAverage = StudentTempoTrainer::where('student_id',$stuId)->where('sub_level_id',$subLevelId)->where('status',2)->sum('average_milli_sec');
        }
        if(count($response) == 0)
        {
            $cal = 0;
            $seconds = 0;
        }
        else
        {
            $cal = $sumAverage/count($response);
        }
        $result['no'] = count($noResponse);
        $result['res'] = count($response);
        $result['anti'] = count($anti);
        $result['ave'] =round($cal);
        echo json_encode($result);exit;
    }

/*===============Done tempo trainer===========*/

    public function doneTempoTrainer()
    {
        $stuId = $_GET['stuId'];
        $subLevelId = $_GET['subLevelId'];
        $eartype = $_GET['eartype'];
        DB::table('student_tempo_trainers')->where('sub_level_id', $subLevelId)->delete();
        $result['redirectUrl'] = url('levels_sub_levels/'.encrypt(4).'?student='.encrypt($stuId));
        echo json_encode($result);exit;
    }
}
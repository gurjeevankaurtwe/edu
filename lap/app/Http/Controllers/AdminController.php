<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Session;
use App\UserType;
use App\Classes;
use App\StudentDetail;
use App\GuestUser;
use App\SubLevel;
use App\LevelSubLevel;
use DB;
use Auth;
use Image;

class AdminController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    //Admin dashboard View START
    public function admin() {
        return view('admin.admin');
    }

    //Admin dashboard View END
    //Admin profile page START
    public function admin_profile(Request $request, $id) {

        $user_id = decrypt($id);
        $userData = DB::table('users')->where('id', $user_id)->first();
        if ($request->all()) {
            if (Input::hasFile('image')) {

                //If image is selected START
                $validator = Validator::make(Input::all(), [
                            'username' => 'required',
                            //'image' => 'mimes:jpg,png,gif,jpeg'
                                ], [
                            'username.required' => ' Sorry! You can not leave username empty.',
                ]);

                if ($validator->fails()) 
                {
                    $user_id = encrypt($user_id);
                    return redirect('admin/admin-profile/' . $user_id)->withErrors($validator)->withInput();
                } 
                else 
                {
                    $file = Input::file('image');
                    $name = time() . '-' . $file->getClientOriginalName();
                    if ($image = $file->move(storage_path() . '/files/', $name)) {
                        DB::table('users')
                                ->where('id', $user_id)
                                ->update(['name' => $request->username, 'image' => $name]);
                        $user_id = encrypt($user_id);
                        return redirect('admin/admin-profile/' . $user_id)->with('success', 'You have updated the record!');
                    } else {
                        die('here');
                    }
                }
            } 
            else {
                //If image is not selected START
                $validator = Validator::make(Input::all(), [
                            'username' => 'required'
                                ], [
                            'username.required' => ' Sorry! You can not leave username empty.',
                ]);
                if ($validator->fails()) {
                    $user_id = encrypt($user_id);
                    return redirect('admin/admin-profile/' . $user_id)->withErrors($validator)->withInput();
                } else {
                    DB::table('users')
                            ->where('id', $user_id)
                            ->update(['name' => $request->username]);
                    $user_id = encrypt($user_id);
                    return redirect('admin/admin-profile/' . $user_id)->with('success', 'You have updated the record!');
                }
            }
        }
        return view('admin.admin_profile', compact('userData'));
    }

    //Admin profile page END
    //Setting profile password START
    public function admin_setting(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('users')->where('id', $user_id)->first();
        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'current_password' => 'required',
                        'password' => 'required|same:password',
                        'password_confirmation' => 'required|same:password',
            ]);
            if ($validator->fails()) {
                $user_id = encrypt($user_id);
                return redirect('admin/admin-setting/' . $user_id)->withErrors($validator)->withInput();
            } else {
                $current_password = Auth::User()->password;
                if (Hash::check($request->current_password, $current_password)) {
                    DB::table('users')
                            ->where('id', $user_id)
                            ->update(['password' => Hash::make($request->password)]);
                    $user_id = encrypt($user_id);
                    return redirect('admin/admin-setting/' . $user_id)->with('success', 'Congrats! password has been updated successfully');
                } else {
                    $user_id = encrypt($user_id);
                    return redirect('admin/admin-setting/' . $user_id)->with('error', 'Oops! fill your correct current password');
                }
            }
        }

        return view('admin.admin_setting');
    }

    //Setting profile password END


    /*     * ***************** User Mangement ********************* */
    //Add new user START
    public function add_user(Request $request) {

        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'username' => 'required',
                        'user_type' => 'required',
                        'email' => 'unique:users|required|email'
                            ], [
                        'username.required' => ' Sorry! You can not leave username empty.',
                        'user_type.required' => ' Sorry! You can not leave user type unselected.',
                        'email.required' => 'Sorry! You can not leave email empty.',
                        'email.unique' => 'Sorry! This email is already registered.',
                        'email.email' => 'Email type must be accordingly.'
            ]);
            if ($validator->fails()) {
                return redirect('admin/add-user')->withErrors($validator)->withInput();
            } 
            else 
            {
                $password = mt_rand(100000, 999999);
                $user = new User;
                $user->name = $request->username;
                $user->email = $request->email;
                $user->dob = $request->dob;
                $user->last_name = $request->last_name;
                $user->password = Hash::make($request->password);
                $user->password_hint = $request->password;
                $user->type = $request->user_type;
                $user->save();

                if($request->user_type == 'student')
                {
                    $studentDetail = new StudentDetail();
                    $studentDetail->student_id = $user->id;
                    $studentDetail->reference_code = $request->reference_code;
                    $studentDetail->grade = $request->grade;
                    $studentDetail->gender = $request->gender;
                    $studentDetail->timings = $request->timing;
                    $studentDetail->save();
                }
                return redirect('admin/manage-users')->with('success', 'Congrats! new member has been added to your system');
            }
        }
        $user_types = DB::table('user_types')->where('id', '4')->get();
        return view('admin.user_management.add_user', get_defined_vars());
    }

    //Add new user END

    public function add_stu_teacher(Request $request,$inst_id) {
        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'username' => 'required',
                        'user_type' => 'required',
                        'institute_id' => 'required',
                        'email' => 'unique:users|required|email'
                            ], [
                        'username.required' => ' Sorry! You can not leave username empty.',
                        'user_type.required' => ' Sorry! You can not leave user type unselected.',
                        'institute_id.required' => ' Sorry! You can not leave institute unselected.',
                        'email.required' => 'Sorry! You can not leave email empty.',
                        'email.unique' => 'Sorry! This email is already registered.',
                        'email.email' => 'Email type must be accordingly.'
            ]);
            if ($validator->fails()) {
                return redirect('admin/add-student-teacher')->withErrors($validator)->withInput();
            } else {
                $password = mt_rand(100000, 999999);
                $user = new User;
                $user->name = $request->username;
                $user->dob = $request->dob;
                $user->last_name = $request->last_name;
                $user->institute_id = $request->institute_id;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->password_hint = $request->password;
                $user->type = $request->user_type;
                $user->save();
                return redirect('admin/manage-users')->with('success', 'Congrats! new member has been added to your system');
            }
        }
        $user_types = DB::table('user_types')->where('id', '!=', '4')->where('status','1')->get();
        $all_institutes = DB::table('users')->where('type', 'Institute')->where('status', '1')->get();
        return view('admin.user_management.add_stu_teacher', get_defined_vars());
    }

    public function add_inst_users(Request $request) {
        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'username' => 'required',
                        'user_type' => 'required',
                        'password' => 'required',
                        'email' => 'unique:users|required|email'
                            ], [
                        'username.required' => ' Sorry! You can not leave username empty.',
                        'user_type.required' => ' Sorry! You can not leave user type unselected.',
                        'email.required' => 'Sorry! You can not leave email empty.',
                        'password.required' => 'Sorry! You can not leave password empty.',
                        'email.unique' => 'Sorry! This email is already registered.',
                        'email.email' => 'Email type must be accordingly.'
            ]);
            if ($validator->fails()) {
                return redirect('admin/add-inst-users')->withErrors($validator)->withInput();
            } else {
//                $password = mt_rand(100000, 999999);
                $password = $request->password;
                $user = new User;
                $user->name = $request->username;
                $user->institute_id = Auth::user()->id;
                $user->email = $request->email;
                $user->password = Hash::make($password);
                $user->password_hint = $password;
                $user->type = $request->user_type;
                $user->save();
                return redirect('admin/manage-inst-users')->with('success', 'Congrats! new member has been added to your system');
            }
        }
        $user_types = DB::table('user_types')->where('id', '!=', '4')->get();
        $all_institutes = DB::table('users')->where('type', 'Institute')->where('status', '1')->get();
        return view('admin.inst_management.add_stu_teacher', get_defined_vars());
    }

    //Manage users START
    public function manage_users(Request $request) 
    {
        $type = '';
        if(isset($_GET['type']) && !isset($_GET['teacherId']) && $_GET['type'] == 'teacher')
        {
            $allUsers = DB::table('users')->where('type', '=', 'teacher')->orderBy('institute_id','ASC')->get();
        }
        else if(isset($_GET['type']) && !isset($_GET['teacherId']) && $_GET['type'] == 'student')
        {
            $allUsers = DB::table('users')->where('type', '=', 'student')->orderBy('id','DESC')->get();
        }
        else if(isset($_GET['type']) && !isset($_GET['teacherId']) && $_GET['type'] == 'Institute')
        {
            $allUsers = DB::table('users')->where('type', '=', 'Institute')->get();
        }
        else if(isset($_GET['type']) && !isset($_GET['teacherId']) && $_GET['type'] == 'institute')
        {
            $allUsers = DB::table('users')->where('type', '=', 'Institute')->get();
        }
        else if(isset($_GET['type']) && !isset($_GET['teacherId']) && $_GET['type'] == 'admin')
        {
            $allUsers = DB::table('users')->where('type', '=', 'admin')->get();
        }
        else if(isset($_GET['teacherId']) && $_GET['type'] == 'teacher')
        {

            $teacId = decrypt($_GET['teacherId']);
            $allUsers = DB::table('users')->where('institute_id',$teacId)->where('type', '=', 'teacher')->get();
        }
        else if(isset($_GET['teacherId']) && $_GET['type'] == 'student')
        {

            $teacId = decrypt($_GET['teacherId']);
            $allUsers = DB::table('users')->where('institute_id',$teacId)->where('type', '=', 'student')->get();
        }
        else
        {
            $allUsers = DB::table('users')->where('type', '!=', 'admin')->get();
        }
        if(isset($_GET['type']))
        {
            $type = $_GET['type'];
        }
        
        return view('admin.user_management.manage_users', get_defined_vars());
    }

    //Manage users START
    public function manage_inst_users(Request $request) {
        $allUsers = DB::table('users')->where('institute_id', Auth::user()->id)->get();
        foreach ($allUsers as $key => $allUser) {
            if (isset($allUser->class_id) && !empty($allUser->class_id)) {
                $class = DB::table('classes')->where('id', $allUser->class_id)->first();
                $allUsers[$key]->class_name = $class->class;
            } else {
                $allUsers[$key]->class_name = 'N/A';
            }
        }
        return view('admin.inst_management.manage_users', get_defined_vars());
    }

    //Manage users END
    //Update users status START
    public function update_users_status(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('users')->where('id', $user_id)->first();
        if ($userData->status == '1') {
            DB::table('users')
                    ->where('id', $user_id)
                    ->update(['status' => '0']);
        }
        if ($userData->status == '0') {
            DB::table('users')
                    ->where('id', $user_id)
                    ->update(['status' => ' 1']);
        }
        return redirect()->back()->with('success', 'Record has been updated successfully!');
    }

    //Update users status END
    //Delete user START
    public function delete_user(Request $request, $id) {
        $user_id = decrypt($id);
        DB::table('users')->where('id', $user_id)->delete();
        return redirect()->back()->with('success', 'Record is no more with your system!');
    }

    //Delete user END
    //Update users status START


    //Update users status END
    /*     * ***************** User Mangement ********************* */

    /*     * ***************** User Type Mangement ********************* */
    //Add new user type START
    public function add_user_type(Request $request) {
        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'title' => 'required',
                            ], [
                        'title.required' => ' Sorry! You can not leave title empty.',
            ]);
            if ($validator->fails()) {
                return redirect('admin/add-user-type')->withErrors($validator)->withInput();
            } else {
                $password = mt_rand(100000, 999999);
                $user_type = new UserType;
                $user_type->title = $request->title;
                $user_type->save();
                return redirect('admin/manage-users-type')->with('success', 'Congrats! new record has been added to your system');
            }
        }
        return view('admin.user_type_management.add_user_type');
    }

    //Add new user type END
    //Manage users type START
    public function manage_users_type(Request $request) {
        $allUserTypes = DB::table('user_types')->where('title','!=','admin')->get();
        return view('admin.user_type_management.manage_users_type', compact('allUserTypes'));
    }

    //Manage users type END
    //Update users status START
    public function update_users_type_status(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('user_types')->where('id', $user_id)->first();
        if ($userData->status == '1') {
            DB::table('user_types')
                    ->where('id', $user_id)
                    ->update(['status' => '0']);
        }
        if ($userData->status == '0') {
            DB::table('user_types')
                    ->where('id', $user_id)
                    ->update(['status' => ' 1']);
        }
        return redirect()->back()->with('success', 'Record has been updated successfully!');
    }

    //Update users status END
    //Delete user START
    public function delete_user_type(Request $request, $id) {
        $user_id = decrypt($id);
        DB::table('user_types')->where('id', $user_id)->delete();
        return redirect()->back()->with('success', 'Record is no more with your system!');
    }

    //Delete user END
    //Update users status START
    public function edit_user_type(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('user_types')->where('id', $user_id)->first();
        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'title' => 'required'
                            ], [
                        'title.required' => ' Sorry! You can not leave title empty.',
            ]);
            if ($validator->fails()) {
                return redirect('admin/edit-user-type/' . $id)->withErrors($validator)->withInput();
            } else {
                DB::table('user_types')
                        ->where('id', $user_id)
                        ->update(['title' => $request->title]);
                return redirect('admin/manage-users-type')->with('success', 'You have updated the record!');
            }
        }
        return view('admin.user_type_management.edit_user_type', compact('userData'));
    }

    //Update users status END
    /*     * ***************** User Type Mangement ********************* */


    /*     * ***************** Session Mangement ********************* */
    //ADD session START
    public function add_session(Request $request) {
        if ($request->all()) {
//            echo "<pre>"; print_r($request->all()); die;
            $validator = Validator::make(Input::all(), [
                        'name' => 'required',
                        'teacher_id' => 'required',
                        'student_id' => 'required',
                        'subject_id' => 'required',
                        'class_id' => 'required',
                        'session_date' => 'required',
                        'session_start_time' => 'required',
                        'session_end_time' => 'required',
                            ], [
                        'name.required' => ' Sorry! You can not leave username empty.',
                        'teacher_id.required' => ' Sorry! You can not leave teacher empty.',
                        'student_id.required' => ' Sorry! You can not leave student empty.',
                        'subject_id.required' => ' Sorry! You can not leave subject empty.',
                        'class_id.required' => ' Sorry! You can not leave class empty.',
                        'session_date.required' => ' Sorry! You can not leave session date empty.',
                        'session_start_time.required' => ' Sorry! You can not leave session start time empty.',
                        'session_end_time.required' => ' Sorry! You can not leave session end time empty.',
            ]);
            if ($validator->fails()) {
                return redirect('admin/add-session')->withErrors($validator)->withInput();
            } else {
                $all_days = array_filter($request->days);
                $imp_days = implode(',', $all_days);
                if (count($all_days) == '0') {
                    return redirect('admin/add-session')->with('error', 'Please select session day.');
                }
//                print_r($all_days); die;
                //check if teacher is already assgined for same day and time slot 
//                DB::select("SELECT * FROM `members` WHERE `membership_number` IN (1,2,3)");

                $get_records = DB::select("SELECT * FROM sessions  WHERE teacher_id = $request->teacher_id AND session_date = '$request->session_date' AND session_start_time >= CAST('$request->session_start_time' AS time) AND session_end_time <= CAST('$request->session_end_time' AS time)");
                if (count($get_records) != '0') {
//                   echo $imp_days; die;
                    foreach ($get_records as $get_record) {
                        $count[] = DB::select("SELECT * FROM `session_days` WHERE `day` IN ($imp_days) and session_id=$get_record->id");
                    }
//                    print_r(count(array_filter($count))); die;
                    if (count(array_filter($count)) != '0') {
                        return redirect('admin/add-session')->with('error', 'This teacher is already assgined to some other session with same date, time or day.');
                    } else {
                        $user = new Session;
                        $user->name = $request->name;
                        $user->institute_id = Auth::user()->id;
                        $user->teacher_id = $request->teacher_id;
                        $user->student_id = $request->student_id;
                        $user->subject_id = $request->subject_id;
                        $user->session_date = $request->session_date;
                        $user->session_start_time = $request->session_start_time;
                        $user->session_end_time = $request->session_end_time;
                        $user->save();

                        foreach ($all_days as $days) {
                            DB::table('session_days')->insert([
                                'session_id' => $user->id,
                                'day' => $days,
                            ]);
                        }
                    }
                } else {
                    $user = new Session;
                    $user->name = $request->name;
                    $user->institute_id = Auth::user()->id;
                    $user->teacher_id = $request->teacher_id;
                    $user->student_id = $request->student_id;
                    $user->subject_id = $request->subject_id;
                    $user->class_id = $request->class_id;
                    $user->session_date = $request->session_date;
                    $user->session_start_time = $request->session_start_time;
                    $user->session_end_time = $request->session_end_time;
                    $user->save();

                    foreach ($all_days as $days) {
                        DB::table('session_days')->insert([
                            'session_id' => $user->id,
                            'day' => $days,
                        ]);
                    }
                }

//               $count = [];


                return redirect('admin/manage-session')->with('success', 'Congrats! new member has been added to your system');
            }
        }
        $user_types = DB::table('user_types')->where('id', '!=', '4')->get();
        $all_teachers = DB::table('users')->where('institute_id', Auth::user()->id)->where('type', 'teacher')->where('status', '1')->get();
        $all_subjects = DB::table('subjects')->where('status', '1')->get();
        $all_classes = DB::table('classes')->where('institute_id', Auth::user()->id)->where('status', '1')->get();
        $all_students = DB::table('users')->where('institute_id', Auth::user()->id)->where('type', 'student')->where('status', '1')->get();
        return view('admin.session_management.add_session', get_defined_vars());
    }

    //ADD session END
    //Manage session START
    public function manage_session(Request $request) {


        if (Auth::user()->type == "teacher") {
            $all_sessions = DB::table('sessions')->where('teacher_id', Auth::user()->id)->where('status', '1')->get();
        } else {
            $all_sessions = DB::table('sessions')->where('institute_id', Auth::user()->id)->get();
        }


        return view('admin.session_management.manage_session', get_defined_vars());
    }

    //Manage session END
    //Update session status START
    public function update_session_status(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('sessions')->where('id', $user_id)->first();
        if ($userData->status == '1') {
            DB::table('sessions')
                    ->where('id', $user_id)
                    ->update(['status' => '0']);
        }
        if ($userData->status == '0') {
            DB::table('sessions')
                    ->where('id', $user_id)
                    ->update(['status' => ' 1']);
        }
        return redirect()->back()->with('success', 'Record has been updated successfully!');
    }

    //Update session status END
    //Delete session START
    public function delete_session(Request $request, $id) {
        $user_id = decrypt($id);
        DB::table('sessions')->where('id', $user_id)->delete();
        return redirect()->back()->with('success', 'Record is no more with your system!');
    }

    //Delete session END
    //Update users status START
    public function edit_session(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('sessions')->where('id', $user_id)->first();
        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'name' => 'required'
                            ], [
                        'name.required' => ' Sorry! You can not leave name empty.',
            ]);
            if ($validator->fails()) {
                return redirect('admin/edit-session/' . $id)->withErrors($validator)->withInput();
            } else {
                DB::table('sessions')
                        ->where('id', $user_id)
                        ->update(['name' => $request->name]);
                return redirect('admin/manage-session')->with('success', 'You have updated the record!');
            }
        }
        return view('admin.session_management.edit_session', compact('userData'));
    }

    //Update users status END

    /*     * ***************** Session Mangement ********************* */


    /*     * ***************** Class Mangement ********************* */
    //ADD classes START
    public function add_class(Request $request) {
        if ($request->all()) {
//            echo "<pre>"; print_r($request->all()); die;
            $validator = Validator::make(Input::all(), [
                        'class' => 'required'
                            ], [
                        'class.required' => ' Sorry! You can not leave class empty.'
            ]);
            if ($validator->fails()) {
                return redirect('admin/add-class')->withErrors($validator)->withInput();
            } else {

                $user = new Classes;
                $user->institute_id = Auth::user()->id;
                $user->class = $request->class;
                $user->save();
                return redirect('admin/manage-class')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        return view('admin.class_management.add_class', get_defined_vars());
    }

    //ADD classes END
    //Manage classes START
    public function manage_class(Request $request) {
        $all_sessions = DB::table('classes')->where('institute_id', Auth::user()->id)->get();
        return view('admin.class_management.manage_class', get_defined_vars());
    }

    //Manage classes END
    //Update classes status START
    public function update_class_status(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('classes')->where('id', $user_id)->first();
        if ($userData->status == '1') {
            DB::table('classes')
                    ->where('id', $user_id)
                    ->update(['status' => '0']);
        }
        if ($userData->status == '0') {
            DB::table('classes')
                    ->where('id', $user_id)
                    ->update(['status' => ' 1']);
        }
        return redirect()->back()->with('success', 'Record has been updated successfully!');
    }

    //Update classes status END
    //Delete session START
    public function delete_class(Request $request, $id) {
        $user_id = decrypt($id);
        DB::table('classes')->where('id', $user_id)->delete();
        return redirect()->back()->with('success', 'Record is no more with your system!');
    }

    //Delete session END
    //Update users status START
    public function edit_class(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('classes')->where('id', $user_id)->first();
        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'class' => 'required'
                            ], [
                        'class.required' => ' Sorry! You can not leave class empty.',
            ]);
            if ($validator->fails()) {
                return redirect('admin/edit-class/' . $id)->withErrors($validator)->withInput();
            } else {
                DB::table('classes')
                        ->where('id', $user_id)
                        ->update(['class' => $request->class]);
                return redirect('admin/manage-class')->with('success', 'You have updated the record!');
            }
        }
        return view('admin.class_management.edit_class', compact('userData'));
    }

    //Update class status END

    /*     * ***************** Class Mangement ********************* */

    /*     * ***************** Question Mangement ********************* */
     public function sublevel_add_questions(Request $request, $id) {
        $session_id = decrypt($id);
        if ($session_id == '1' || $session_id == '2' || $session_id == '4' || $session_id == '8' || $session_id == '10' || $session_id == '3' || $session_id == '7' || $session_id == '9' || $session_id == '11') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        DB::table('questions')->insert([
                            'session_id' => 0,
                            'level_sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => '',
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }
        else
        {
            return view('admin.questions_management.sublevel_add_question', get_defined_vars());
            return redirect()->back()->with('error', 'We are under process with this level');
        }
        return view('admin.questions_management.sublevel_add_question', get_defined_vars());
    }

    //add questions to session START
    public function add_questions(Request $request, $id) {
        $session_id = decrypt($id);
        if ($session_id == '1') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                        if (isset($ques_arrays['option1_img'])) 
                        {
                            $file = $ques_arrays['option1_img'];
                            $imageType = $file->getClientmimeType();
                            $fileName = $file->getClientOriginalName();
                            $fileNameUnique = time() . '_' . $fileName;
                            $destinationPath = public_path() . '/question_images/';
                            $file->move($destinationPath, $fileNameUnique);
                            $imageOption1 = $fileNameUnique;
                        } 
                        else 
                        {
                            $imageOption1 = '';
                        }

                        if (isset($ques_arrays['answer_record'])) 
                        {
                            $file3 = $ques_arrays['answer_record'];
                            $imageType3 = $file3->getClientmimeType();
                            if ($imageType3 == 'audio/mpeg' || $imageType3  == 'audio/mp3') 
                            {
                                $fileName3 = $file3->getClientOriginalName();
                                $fileNameUnique3 = time() . '_' . $fileName3;
                                
                                $destinationPath3 = public_path() . '/answer_record/';
                                $file3->move($destinationPath3, $fileNameUnique3);
                                $imageOption3 = $fileNameUnique3;
                                $FileUrl = asset('answer_record/'.$imageOption3);
                                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileNameUnique3);
                                $data = json_decode(file_get_contents('http://api.rest7.com/v1/sound_convert.php?url=' . $FileUrl . '&format=wav'));
                                if (@$data->success !== 1)
                                {
                                    die('Failed');
                                }
                                $wave = file_get_contents($data->file);
                               file_put_contents(public_path() . '/answer_record/'.$withoutExt . '.wav', $wave);
                               $imageOption3 = $withoutExt . '.wav';
                            } 
                            else 
                            {
                                return redirect()->back()->with('error', 'File is not .mp3');
                            }
                        } 
                        else 
                        {
                            $imageOption3 = '';
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => $imageOption1,
                            'answer_record' => $imageOption3,
                            'answer' => $ques_arrays['option1'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }


//            $number = count($request->question);
//            for($i = 0; $i < $number; $i++){
//                if(trim($request->question[$i]) != ''){
//                   
//                    
//                    
//                    if ($request->file('option2_img')[$i]) {
//                        $file2 = $request->file('option2_img')[$i];
//                        $imageType2 = $file2->getClientmimeType();
//                        //  echo "<pre>"; print_r($file->getClientOriginalName()); die;
//                        $fileName2 = $file2->getClientOriginalName();
//                        $fileNameUnique2 = time() . '_' . $fileName2;
//                        $destinationPath2 = public_path() . '/question_images/';
//                        $file2->move($destinationPath2, $fileNameUnique2);
//                        $imageOption2 = $fileNameUnique2;
//                    } else {
//                        $imageOption2 = '';
//                    }
//                    
//                    if ($request->file('answer_record')) {
//                        $file3 = $request->file('answer_record')[$i];
//                        $imageType3 = $file3->getClientmimeType();
//                        if($imageType3=='audio/mpeg'){
//                            $fileName3 = $file3->getClientOriginalName();
//                            $fileNameUnique3 = time() . '_' . $fileName3;
//                            $destinationPath3 = public_path() . '/answer_record/';
//                            $file3->move($destinationPath3, $fileNameUnique3);
//                            $imageOption3 = $fileNameUnique3;
//                        }else{
//                            return redirect()->back()->with('error', 'File is not .mp3');
//                        }
//                        
//                    } else {
//                        $imageOption3 = '';
//                    }
//                    
//                    DB::table('questions')->insert([
//                        'session_id'=>$session_id,
//                        'sub_level_id'=>$session_id,
//                        'question'=>$request->question[$i],
//                        'option1'=>$request->option1[$i],
//                        'option1_img'=>$imageOption1,
//                        'option2'=>$request->option2[$i],
//                        'option2_img'=>$imageOption2,
//                        'answer_record'=>$imageOption3,
//                        'answer'=>$request->answer[$i],
//                    ]);
//                    
//                }
//            }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }
        elseif ($session_id == '2') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                
//                $ques_array = array_map('array_filter', $ques_array);
//                $ques_array = array_filter($ques_array);
                if (count($ques_array1) > 0) 
                {
//                    echo "<pre>";print_r($ques_array1);die;
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        //
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        if (isset($ques_arrays['option1_img'])) 
                        {
                            $file = $ques_arrays['option1_img'];
                            $imageType = $file->getClientmimeType();
                            $fileName = $file->getClientOriginalName();
                            $fileNameUnique = time() . '_' . $fileName;
                            $destinationPath = public_path() . '/question_images/';
                            $file->move($destinationPath, $fileNameUnique);
                            $imageOption1 = $fileNameUnique;
                        } 
                        else 
                        {
                            $imageOption1 = '';
                        }
                        
                        if (isset($ques_arrays['option2_img'])) 
                        {
                            $file1 = $ques_arrays['option2_img'];
                            $imageType1= $file1->getClientmimeType();
                            $fileName1 = $file1->getClientOriginalName();
                            $fileNameUnique1 = time() . '_' . $fileName1;
                            $destinationPath1 = public_path() . '/question_images/';
                            $file1->move($destinationPath1, $fileNameUnique1);
                            $imageOption2 = $fileNameUnique1;
                        } 
                        else 
                        {
                            $imageOption2 = '';
                        }
                        
                        if (isset($ques_arrays['option3_img'])) 
                        {
                            $file2 = $ques_arrays['option3_img'];
                            $imageType2= $file2->getClientmimeType();
                            $fileName2 = $file2->getClientOriginalName();
                            $fileNameUnique2 = time() . '_' . $fileName2;
                            $destinationPath1 = public_path() . '/question_images/';
                            $file2->move($destinationPath2, $fileNameUnique2);
                            $imageOption3 = $fileNameUnique2;
                        } 
                        else 
                        {
                            $imageOption3 = '';
                        }
                        
                        if (isset($ques_arrays['option4_img'])) 
                        {
                            $file3 = $ques_arrays['option4_img'];
                            $imageType3= $file3->getClientmimeType();
                            $fileName3 = $file3->getClientOriginalName();
                            $fileNameUnique3 = time() . '_' . $fileName3;
                            $destinationPath3 = public_path() . '/question_images/';
                            $file2->move($destinationPath3, $fileNameUnique3);
                            $imageOption4 = $fileNameUnique3;
                        } 
                        else 
                        {
                            $imageOption4 = '';
                        }

                        if (isset($ques_arrays['answer_record'])) 
                        {
                            $file3 = $ques_arrays['answer_record'];
                            $imageType3 = $file3->getClientmimeType();
                            if ($imageType3 == 'audio/mpeg' || $imageType3  == 'audio/mp3') 
                            {
                                $fileName3 = $file3->getClientOriginalName();
                                $fileNameUnique3 = time() . '_' . $fileName3;
                                $destinationPath3 = public_path() . '/answer_record/';
                                $file3->move($destinationPath3, $fileNameUnique3);
                                $imageOption3 = $fileNameUnique3;
                                $FileUrl = asset('answer_record/'.$imageOption3);
                                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileNameUnique3);
                                $data = json_decode(file_get_contents('http://api.rest7.com/v1/sound_convert.php?url=' . $FileUrl . '&format=wav'));
                                if (@$data->success !== 1)
                                {
                                    die('Failed');
                                }
                                $wave = file_get_contents($data->file);
                               file_put_contents(public_path() . '/answer_record/'.$withoutExt . '.wav', $wave);
                               $imageOption3 = $withoutExt . '.wav';
                            } 
                            else 
                            {
                                return redirect()->back()->with('error', 'File is not .mp3');
                            }
                        } 
                        else 
                        {
                            $imageOption3 = '';
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => $ques_arrays['option4'],
                            'option1_img' => $imageOption1,
                            'option2_img' => $imageOption2,
                            'option3_img' => $imageOption3,
                            'option4_img' => $imageOption4,
                            'answer_record' => $imageOption3,
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }
        elseif ($session_id == '3') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                        if (isset($ques_arrays['option1_img'])) 
                        {
                            $file = $ques_arrays['option1_img'];
                            $imageType = $file->getClientmimeType();
                            $fileName = $file->getClientOriginalName();
                            $fileNameUnique = time() . '_' . $fileName;
                            $destinationPath = public_path() . '/question_images/';
                            $file->move($destinationPath, $fileNameUnique);
                            $imageOption1 = $fileNameUnique;
                        } 
                        else 
                        {
                            $imageOption1 = '';
                        }

                        if (isset($ques_arrays['answer_record'])) 
                        {
                            $file3 = $ques_arrays['answer_record'];
                            $imageType3 = $file3->getClientmimeType();
                            if ($imageType3 == 'audio/mpeg' || $imageType3  == 'audio/mp3') 
                            {
                                $fileName3 = $file3->getClientOriginalName();
                                $fileNameUnique3 = time() . '_' . $fileName3;
                                $destinationPath3 = public_path() . '/answer_record/';
                                $file3->move($destinationPath3, $fileNameUnique3);
                                $imageOption3 = $fileNameUnique3;
                                $FileUrl = asset('answer_record/'.$imageOption3);
                                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileNameUnique3);
                                $data = json_decode(file_get_contents('http://api.rest7.com/v1/sound_convert.php?url=' . $FileUrl . '&format=wav'));
                                if (@$data->success !== 1)
                                {
                                    die('Failed');
                                }
                                $wave = file_get_contents($data->file);
                               file_put_contents(public_path() . '/answer_record/'.$withoutExt . '.wav', $wave);
                               $imageOption3 = $withoutExt . '.wav';
                            } 
                            else 
                            {
                                return redirect()->back()->with('error', 'File is not .mp3');
                            }
                        } 
                        else 
                        {
                            $imageOption3 = '';
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => $imageOption1,
                            'answer_record' => $imageOption3,
                            'answer' => $ques_arrays['option1'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*====Level 5===*/

        else if ($session_id == '5') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {
                    foreach ($ques_array as $ques_arrays) 
                    {
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['option1'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*====Level 6===*/

        elseif ($session_id == '6') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        if (isset($ques_arrays['answer_record'])) 
                        {
                            $file3 = $ques_arrays['answer_record'];
                            $imageType3 = $file3->getClientmimeType();
                            if ($imageType3 == 'audio/mpeg' || $imageType3  == 'audio/mp3') 
                            {
                                $fileName3 = $file3->getClientOriginalName();
                                $fileNameUnique3 = time() . '_' . $fileName3;
                                $destinationPath3 = public_path() . '/answer_record/';
                                $file3->move($destinationPath3, $fileNameUnique3);
                                $imageOption3 = $fileNameUnique3;
                                $FileUrl = asset('answer_record/'.$imageOption3);
                                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileNameUnique3);
                                $data = json_decode(file_get_contents('http://api.rest7.com/v1/sound_convert.php?url=' . $FileUrl . '&format=wav'));
                                if (@$data->success !== 1)
                                {
                                    die('Failed');
                                }
                                $wave = file_get_contents($data->file);
                               file_put_contents(public_path() . '/answer_record/'.$withoutExt . '.wav', $wave);
                               $imageOption3 = $withoutExt . '.wav';
                            } 
                            else 
                            {
                                return redirect()->back()->with('error', 'File is not .mp3');
                            }
                        } 
                        else 
                        {
                            $imageOption3 = '';
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => $imageOption3,
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*====Level 7===*/

        elseif ($session_id == '7') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        if (isset($ques_arrays['answer_record'])) 
                        {
                            $file3 = $ques_arrays['answer_record'];
                            $imageType3 = $file3->getClientmimeType();
                            if ($imageType3 == 'audio/mpeg' || $imageType3  == 'audio/mp3') 
                            {
                                $fileName3 = $file3->getClientOriginalName();
                                $fileNameUnique3 = time() . '_' . $fileName3;
                                $destinationPath3 = public_path() . '/answer_record/';
                                $file3->move($destinationPath3, $fileNameUnique3);
                                $imageOption3 = $fileNameUnique3;
                                $FileUrl = asset('answer_record/'.$imageOption3);
                                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileNameUnique3);
                                $data = json_decode(file_get_contents('http://api.rest7.com/v1/sound_convert.php?url=' . $FileUrl . '&format=wav'));
                                if (@$data->success !== 1)
                                {
                                    die('Failed');
                                }
                                $wave = file_get_contents($data->file);
                               file_put_contents(public_path() . '/answer_record/'.$withoutExt . '.wav', $wave);
                               $imageOption3 = $withoutExt . '.wav';
                            } 
                            else 
                            {
                                return redirect()->back()->with('error', 'File is not .mp3');
                            }
                        } 
                        else 
                        {
                            $imageOption3 = '';
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => $imageOption3,
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*====Level 8===*/

        elseif ($session_id == '8') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*====Level 10===*/

        elseif ($session_id == '10') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        if (isset($ques_arrays['answer_record'])) 
                        {
                            $file3 = $ques_arrays['answer_record'];
                            $imageType3 = $file3->getClientmimeType();
                            if ($imageType3 == 'audio/mpeg' || $imageType3  == 'audio/mp3') 
                            {
                                $fileName3 = $file3->getClientOriginalName();
                                $fileNameUnique3 = time() . '_' . $fileName3;
                                $destinationPath3 = public_path() . '/answer_record/';
                                $file3->move($destinationPath3, $fileNameUnique3);
                                $imageOption3 = $fileNameUnique3;
                                $FileUrl = asset('answer_record/'.$imageOption3);
                                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileNameUnique3);
                                $data = json_decode(file_get_contents('http://api.rest7.com/v1/sound_convert.php?url=' . $FileUrl . '&format=wav'));
                                if (@$data->success !== 1)
                                {
                                    die('Failed');
                                }
                                $wave = file_get_contents($data->file);
                               file_put_contents(public_path() . '/answer_record/'.$withoutExt . '.wav', $wave);
                               $imageOption3 = $withoutExt . '.wav';
                            } 
                            else 
                            {
                                return redirect()->back()->with('error', 'File is not .mp3');
                            }
                        } 
                        else 
                        {
                            $imageOption3 = '';
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => $imageOption3,
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*====Level 11===*/

        elseif ($session_id == '11') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*========Level 1.12 || Level 1.13 || Lebel 1.14======*/

        elseif ($session_id == '42' || $session_id == '43' || $session_id == '44' || $session_id == '52' || $session_id == '50' || $session_id == '55' || $session_id == '56' || $session_id == '62' || $session_id == '63' || $session_id == '14' || $session_id == '15' || $session_id == '22' || $session_id == '28' || $session_id == '29' || $session_id == '34' || $session_id == '36' || $session_id == '37' || $session_id == '19' || $session_id == '25') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                
                if (count($ques_array1) > 0) 
                {
                 
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => $ques_arrays['option4'],
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => '',
                            'answer' => '',
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=== Level 1.15 , Level 2.9===*/

        elseif ($session_id == '45' || $session_id == '20' || $session_id=='30' || $session_id=='35' || $session_id=='39' || $session_id=='40' || $session_id=='24' || $session_id=='49' || $session_id=='48' || $session_id=='16' || $session_id=='23') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*====Level 1.16===*/

        elseif ($session_id == '47') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        if (isset($ques_arrays['answer_record'])) 
                        {
                            $file3 = $ques_arrays['answer_record'];
                            $imageType3 = $file3->getClientmimeType();
                            if ($imageType3 == 'audio/mpeg' || $imageType3  == 'audio/mp3') 
                            {
                                $fileName3 = $file3->getClientOriginalName();
                                $fileNameUnique3 = time() . '_' . $fileName3;
                                $destinationPath3 = public_path() . '/answer_record/';
                                $file3->move($destinationPath3, $fileNameUnique3);
                                $imageOption3 = $fileNameUnique3;
                                $FileUrl = asset('answer_record/'.$imageOption3);
                                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileNameUnique3);
                                $data = json_decode(file_get_contents('http://api.rest7.com/v1/sound_convert.php?url=' . $FileUrl . '&format=wav'));
                                if (@$data->success !== 1)
                                {
                                    die('Failed');
                                }
                                $wave = file_get_contents($data->file);
                               file_put_contents(public_path() . '/answer_record/'.$withoutExt . '.wav', $wave);
                               $imageOption3 = $withoutExt . '.wav';
                            } 
                            else 
                            {
                                return redirect()->back()->with('error', 'File is not .mp3');
                            }
                        } 
                        else 
                        {
                            $imageOption3 = '';
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => $imageOption3,
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=== Level 1.23 ===*/

        elseif ($session_id == '53' ||  $session_id == '51' || $session_id=='49') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => '',
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=== Level 1.27 ===*/

        elseif ($session_id == '57' || $session_id == '46') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => '',
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }


        /*=== Level 2.1 ===*/

        elseif ($session_id == '12' || $session_id == '32') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                       
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['option1'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=== Level 2.2 ===*/

        elseif ($session_id == '13') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=== Level 2.10 ===*/

        elseif ($session_id == '21') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=== Level 2.6 ===*/

        if ($session_id == '17' || $session_id=='38') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                       
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['option1'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*====Level 2.7===*/

        elseif ($session_id == '18' || $session_id=='54') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => '',
                            'answer' => '',
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*====Level 2.15===*/

        elseif ($session_id == '26') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => '',
                            'option4' => '',
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['answer'],
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=== Level 2.19 ===*/

        elseif ($session_id == '31') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                       
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['option1'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=== Level 2.21 ===*/

        elseif ($session_id == '33') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                       
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['option1'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=== Level 2.29 ===*/

        elseif ($session_id == '41') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array = $data['data'];
                $ques_array = array_map('array_filter', $ques_array);
                $ques_array = array_filter($ques_array);
                if (count($ques_array) > 0) 
                {

                    foreach ($ques_array as $ques_arrays) 
                    {
                       
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option1_img' => '',
                            'answer_record' => '',
                            'answer' => $ques_arrays['option1'],
                        ]);
                    }
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }
                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }

        /*=====Level 3.1=====*/

        elseif ($session_id == '59' || $session_id == '60' || $session_id == '61') 
        {
            if ($request->all()) 
            {
                $data = $request->all();
                $ques_array1 = $data['data'];
                
                if (count($ques_array1) > 0) 
                {
                    foreach ($ques_array1 as $key=>$ques_arrays) 
                    {
                        if($ques_arrays['question']=='')
                        {
                            unset($ques_array1[$key]);
                        }
                        DB::table('questions')->insert([
                            'session_id' => $session_id,
                            'sub_level_id' => $session_id,
                            'question' => $ques_arrays['question'],
                            'option1' => $ques_arrays['option1'],
                            'option2' => $ques_arrays['option2'],
                            'option3' => $ques_arrays['option3'],
                            'option4' => $ques_arrays['option4'],
                            'option5' => $ques_arrays['option5'],
                            'option6' => $ques_arrays['option6'],
                            'option1_img' => '',
                            'option2_img' => '',
                            'option3_img' => '',
                            'option4_img' => '',
                            'answer_record' => '',
                            'answer' => '',
                        ]);
                    }
                    DB::table('questions')->where('question',null)->delete();
                    return redirect($request->url)->with('success', 'Questions have been added successfully.');
                } 
                else 
                {
                    return redirect()->back()->with('error', 'fields are required');
                }

                return redirect('admin/manage-session')->with('success', 'Congrats! new class has been added to your system');
            }
        }


        else
        {
            return view('admin.questions_management.add_question', get_defined_vars());
            return redirect()->back()->with('error', 'We are under process with this level 1.2');
        }



        return view('admin.questions_management.add_question', get_defined_vars());
    }

    //add questions to session END
    //Manage questions START
    public function manage_questions(Request $request, $id) {
        $session_id = decrypt($id);
        $ids = $id;
        $all_questions = DB::table('questions')->where('sub_level_id', $session_id)->where('delete_status', '0')->get();
        foreach ($all_questions as $key => $all_question) {
            $session_data = DB::table('sub_levels')->where('id', $session_id)->first();
            $all_questions[$key]->session_name = $session_data->title;
        }
//        echo "<pre>"; print_r($all_questions); die;
        return view('admin.questions_management.manage_questions', get_defined_vars());
    }

    public function sublevel_manage_questions(Request $request, $id) 
    {
        $session_id = decrypt($id);
        $ids = $id;
        $all_questions = DB::table('questions')->where('level_sub_level_id', $session_id)->where('delete_status', '0')->get();
        foreach ($all_questions as $key => $all_question) 
        {
            $session_data = DB::table('sub_levels')->where('id', $session_id)->first();
            $all_questions[$key]->session_name = $session_data->title;
        }
        return view('admin.questions_management.level_manage_questions', get_defined_vars());
    }

    

    //Manage questions END
    //Update questions status START
    public function update_questions_status(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('questions')->where('id', $user_id)->first();
        if ($userData->status == '1') {
            DB::table('questions')
                    ->where('id', $user_id)
                    ->update(['status' => '0']);
        }
        if ($userData->status == '0') {
            DB::table('questions')
                    ->where('id', $user_id)
                    ->update(['status' => ' 1']);
        }
        return redirect()->back()->with('success', 'Record has been updated successfully!');
    }

    //Update questions status END
    //Delete questions START
    public function delete_questions(Request $request, $id) {
        $user_id = decrypt($id);
        DB::table('questions')->where('id', $user_id)->update([
            'delete_status' => '1',
        ]);
        return redirect()->back()->with('success', 'Record is no more with your system!');
    }

    //Delete session END
    //Update users status START
    public function edit_questions(Request $request, $id) {
        $user_id = decrypt($id);

        $userData = DB::table('questions')->where('id', $user_id)->first();
        if ($request->all()) {
//                    echo "<pre>";print_r($request->all()); die;

            if ($request->file('option1_img')) {
                $file = $request->file('option1_img');
                $imageType = $file->getClientmimeType();
                $fileName = $file->getClientOriginalName();
                $fileNameUnique = time() . '_' . $fileName;
                $destinationPath = public_path() . '/question_images/';
                $file->move($destinationPath, $fileNameUnique);
                $imageOption1 = $fileNameUnique;
            } else {
                $imageOption1 = $userData->option1_img;
            }

            if ($request->file('option2_img')) {
//                        die('hh');
                $file2 = $request->file('option2_img');
                $imageType2 = $file2->getClientmimeType();
//                          echo "<pre>"; print_r($file->getClientOriginalName()); die;
                $fileName2 = $file2->getClientOriginalName();
                $fileNameUnique2 = time() . '_' . $fileName2;
                $destinationPath2 = public_path() . '/question_images/';
                $file2->move($destinationPath2, $fileNameUnique2);
                $imageOption2 = $fileNameUnique2;
            } else {
                $imageOption2 = $userData->option2_img;
            }

            if ($request->file('answer_record')) {
                $file3 = $request->file('answer_record');
                $imageType3 = $file3->getClientmimeType();
                //  echo "<pre>"; print_r($file->getClientOriginalName()); die;
                $fileName3 = $file3->getClientOriginalName();
                $fileNameUnique3 = time() . '_' . $fileName3;
                $destinationPath3 = public_path() . '/answer_record/';
                $file3->move($destinationPath3, $fileNameUnique3);
                $imageOption3 = $fileNameUnique3;
                $FileUrl = asset('answer_record/'.$imageOption3);
                $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileNameUnique3);
                $data = json_decode(file_get_contents('http://api.rest7.com/v1/sound_convert.php?url=' . $FileUrl . '&format=wav'));
                if (@$data->success !== 1)
                {
                    die('Failed');
                }
                $wave = file_get_contents($data->file);
               file_put_contents(public_path() . '/answer_record/'.$withoutExt . '.wav', $wave);
               $imageOption3 = $withoutExt . '.wav';
            } else {
                $imageOption3 = $userData->answer_record;
            }

            DB::table('questions')->where('id', $user_id)->update([
                'question' => $request->question,
                'option1' => $request->option1,
                'option1_img' => $imageOption1,
                'option2' => $request->option2,
                'option2_img' => $imageOption2,
                'answer' => $request->answer,
                'answer_record' => $imageOption3,
            ]);
            $session_id1 = encrypt($userData->session_id);
            return redirect('admin/manage-questions/' . $session_id1)->with('success', 'Congrats! new class has been added to your system');
        }
        return view('admin.questions_management.edit_questions', compact('userData'));
    }

    //Update class status END

    /*     * ***************** Question Mangement ********************* */

    /*     * ***************** Levels Mangement ********************* */
    //manage levels START
    public function manage_levels(Request $request) {
        $allUserTypes = DB::table('levels')->get();
        return view('admin.levels_management.index', compact('allUserTypes'));
    }

    //manage levels END
    //Update users status START
    public function update_level_status(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('levels')->where('id', $user_id)->first();
        if ($userData->status == '1') {
            DB::table('levels')
                    ->where('id', $user_id)
                    ->update(['status' => '0']);
        }
        if ($userData->status == '0') {
            DB::table('levels')
                    ->where('id', $user_id)
                    ->update(['status' => ' 1']);
        }
        return redirect()->back()->with('success', 'Record has been updated successfully!');
    }

    //Update users status END
    //Delete user START
    public function delete_level(Request $request, $id) {
        $user_id = decrypt($id);
        DB::table('levels')->where('id', $user_id)->delete();
        return redirect()->back()->with('success', 'Record is no more with your system!');
    }

    //Delete user END
    //Update users status START
    public function edit_level(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('levels')->where('id', $user_id)->first();
        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'title' => 'required',
                        'description' => 'required'
                            ], [
                        'title.required' => ' Sorry! You can not leave title empty.',
                        'description.required' => ' Sorry! You can not leave description empty.',
            ]);
            if ($validator->fails()) {
                return redirect('admin/edit-level/' . $id)->withErrors($validator)->withInput();
            } else {
                DB::table('levels')
                        ->where('id', $user_id)
                        ->update(['title' => $request->title,'description' => $request->description]);
                return redirect('admin/levels')->with('success', 'You have updated the record!');
            }
        }
        return view('admin.levels_management.edit_level', compact('userData'));
    }

    //Update users status END
    //manage sub levels START 
    public function sub_levels(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('levels')->where('id', $user_id)->first();
        $allUserTypes = SubLevel::where('level_id', $user_id)->with('levelSubLevel')->orderBy('id','ASC')->get();
        return view('admin.levels_management.sub_levels', compact('allUserTypes', 'userData','id'));
    }

    public function levelSubLevels($id)
    {
        $subLevelId = decrypt($id);
        $levelSubLevel = LevelSubLevel::where('sub_level_id', $subLevelId)->with('subLevel')->orderBy('id','ASC')->get();
        return view('admin.levels_management.level_sub_levels', compact('levelSubLevel','id'));
    }

    //manage levels END
    //Update users status START
    public function update_sub_level_status(Request $request, $id) {
        $user_id = decrypt($id);
        $userData = DB::table('sub_levels')->where('id', $user_id)->first();
        if ($userData->status == '1') {
            DB::table('sub_levels')
                    ->where('id', $user_id)
                    ->update(['status' => '0']);
        }
        if ($userData->status == '0') {
            DB::table('sub_levels')
                    ->where('id', $user_id)
                    ->update(['status' => ' 1']);
        }
        return redirect()->back()->with('success', 'Record has been updated successfully!');
    }

    //Update users status END
    //Delete user START
    public function delete_sub_level(Request $request, $id) {
        $user_id = decrypt($id);
        DB::table('sub_levels')->where('id', $user_id)->delete();
        return redirect()->back()->with('success', 'Record is no more with your system!');
    }

    //Delete user END
    //Update users status START
    public function edit_sub_level(Request $request, $id) {
        $user_id = $id;
        $userData = DB::table('sub_levels')->where('id', $user_id)->first();
        $l_id = encrypt($userData->level_id);
        if ($request->all()) {
            $validator = Validator::make(Input::all(), [
                        'title' => 'required'
                            ], [
                        'title.required' => ' Sorry! You can not leave title empty.',
            ]);
            if ($validator->fails()) 
            {
                return redirect('admin/sub-levels/' . $l_id)->withErrors($validator)->withInput();
            } 
            else 
            {
                if (isset($request->audio)) 
                {
                    $file = $request->audio;
                    $imageType = $file->getClientmimeType();
                    $fileName = $file->getClientOriginalName();
                    $fileNameUnique = time() . '_' . $fileName;
                    $destinationPath = public_path() . '/audio/';
                    $file->move($destinationPath, $fileNameUnique);
                    $audio = $fileNameUnique;
                } 
                
                DB::table('sub_levels')
                        ->where('id', $user_id)
                        ->update(['title' => $request->title,'description'=>$request->add_description,'audio'=>$audio]);
                return redirect('admin/sub-levels/' . $l_id)->with('success', 'You have updated the record!');
            }
        }

        return view('admin.levels_management.edit_sub_level', compact('userData'));
    }

/*=================Edit Sub Level under sub level=================*/

    public function edit_level_sub_level(Request $request, $id) 
    {
        $user_id = $id;
        $userData = DB::table('level_sub_levels')->where('id', $user_id)->first();
        $l_id = encrypt($userData->sub_level_id);
        if ($request->all()) 
        {
            
            if (isset($request->audio)) 
            {
                $file = $request->audio;
                $imageType = $file->getClientmimeType();
                $fileName = $file->getClientOriginalName();
                $fileNameUnique = time() . '_' . $fileName;
                $destinationPath = public_path() . '/audio/';
                $file->move($destinationPath, $fileNameUnique);
                $audio = $fileNameUnique;
            } 
            
            DB::table('level_sub_levels')
                    ->where('id', $user_id)
                    ->update(['title' => $request->title,'description'=>$request->add_description,'audio'=>$audio]);
            return redirect('admin/level-sub-levels/' . $l_id)->with('success', 'You have updated the record!');
           
        }
        return view('admin.levels_management.edit-level-sub-level', compact('userData'));
    }

/*=================Add Teacher by admin===========*/

    public function addTeacher(Request $request) 
    {
        if ($request->all()) 
        {
            $validator = Validator::make(Input::all(), [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'dob' => 'required',
                        'password' => 'required|min:4',
                        'email' => 'unique:users|required|email'
                            ], [
                        'first_name.required' => ' Sorry! You can not leave first name empty.',
                        'last_name.required' => ' Sorry! You can not leave last name empty.',
                        'dob.required' => ' Sorry! You can not leave dob empty.',
                        'password.required' => ' Sorry! You can not leave password empty.',
                        'password.min' => ' Sorry! Enter password min value of 4.',
                        'email.required' => 'Sorry! You can not leave email empty.',
                        'email.unique' => 'Sorry! This email is already registered.',
                        'email.email' => 'Email type must be accordingly.'
            ]);
            if ($validator->fails()) 
            {
                return redirect('admin/add-teacher')->withErrors($validator)->withInput();
            } 
            else 
            {
                $user = new User;
                $user->name = $request->first_name;
                $user->dob = $request->dob;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->password_hint = $request->password;
                $user->type = $request->user_type;
                $user->save();
                return redirect('admin/manage-users?type=teacher')->with('success', 'Congrats! new member has been added to your system');
            }
        }
        return view('admin.user_management.add-teacher',get_defined_vars());
    }

/*=================Add Student by admin===========*/

    public function addStudent(Request $request) 
    {
        if ($request->all()) 
        {
            $validator = Validator::make(Input::all(), [
                        'reference_code' => 'required',
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'dob' => 'required',
                        'grade' => 'required',
                        'gender' => 'required',
                        'timing' => 'required',
                        'password' => 'required|min:4',
                        'email' => 'unique:users|required|email'
                            ], [
                        'reference_code.required' => ' Sorry! You can not leave reference code empty.',
                        'first_name.required' => ' Sorry! You can not leave first name empty.',
                        'last_name.required' => ' Sorry! You can not leave last name empty.',
                        'dob.required' => ' Sorry! You can not leave dob empty.',
                        'grade.required' => ' Sorry! You can not leave grade empty.',
                        'gender.required' => ' Sorry! You need to select gender.',
                        'timing.required' => ' Sorry! You can not leave timing empty.',
                        'password.required' => ' Sorry! You can not leave password empty.',
                        'password.min' => ' Sorry! Enter password min value of 4.',
                        'email.required' => 'Sorry! You can not leave email empty.',
                        'email.unique' => 'Sorry! This email is already registered.',
                        'email.email' => 'Email type must be accordingly.'
            ]);
            if ($validator->fails()) 
            {
                return redirect('admin/add-student')->withErrors($validator)->withInput();
            } 
            else 
            {
                $user = new User;
                $user->name = $request->first_name;
                $user->dob = $request->dob;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->password_hint = $request->password;
                $user->type = $request->user_type;
                $user->save();

                $stuDetail = new StudentDetail;
                $stuDetail->student_id = $user->id;
                $stuDetail->reference_code = $request->reference_code;
                $stuDetail->grade = $request->grade;
                $stuDetail->gender = $request->gender;
                $stuDetail->timings = $request->timing;
                $stuDetail->save();

                return redirect('admin/manage-users?type=student')->with('success', 'Congrats! new member has been added to your system');
            }
        }
        return view('admin.user_management.add-student',get_defined_vars());
    }

/*=================Edit User by admin==============*/

    public function edit_user(Request $request, $id) 
    {
        $user_id = decrypt($id);
        $userData = DB::table('users')->where('id', $user_id)->first();
        if($userData->type == 'student')
        {
            $userDetail = StudentDetail::where('student_id',$userData->id)->first();
        }
        if ($request->all()) 
        {
            if($userData->type == 'teacher')
            {
                $validator = Validator::make(Input::all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'dob' => 'required',
                    'password' => 'required|min:4',
                    'email' => 'required|email'
                        ], [
                    'first_name.required' => ' Sorry! You can not leave first name empty.',
                    'last_name.required' => ' Sorry! You can not leave last name empty.',
                    'dob.required' => ' Sorry! You can not leave dob empty.',
                    'password.required' => ' Sorry! You can not leave password empty.',
                    'password.min' => ' Sorry! Enter password min value of 4.',
                    'email.required' => 'Sorry! You can not leave email empty.',
                    'email.email' => 'Email type must be accordingly.'
                ]);
            }
            else if($userData->type == 'Institute')
            {
                $validator = Validator::make(Input::all(), [
                    'username' => 'required',
                    'password' => 'required|min:4',
                    'email' => 'required|email'
                        ], [
                    'username.required' => ' Sorry! You can not leave first name empty.',
                    'password.required' => ' Sorry! You can not leave password empty.',
                    'password.min' => ' Sorry! Enter password min value of 4.',
                    'email.required' => 'Sorry! You can not leave email empty.',
                    'email.email' => 'Email type must be accordingly.'
                ]);
            }
            else if($userData->type == 'student')
            {
                $validator = Validator::make(Input::all(), [
                    'reference_code' => 'required',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'dob' => 'required',
                    'grade' => 'required',
                    'gender' => 'required',
                    'timing' => 'required',
                    'password' => 'required|min:4',
                    'email' => 'required|email'
                        ], [
                    'reference_code.required' => ' Sorry! You can not leave reference code empty.',
                    'first_name.required' => ' Sorry! You can not leave first name empty.',
                    'last_name.required' => ' Sorry! You can not leave last name empty.',
                    'dob.required' => ' Sorry! You can not leave dob empty.',
                    'grade.required' => ' Sorry! You can not leave grade empty.',
                    'gender.required' => ' Sorry! You need to select gender.',
                    'timing.required' => ' Sorry! You can not leave timing empty.',
                    'password.required' => ' Sorry! You can not leave password empty.',
                    'password.min' => ' Sorry! Enter password min value of 4.',
                    'email.required' => 'Sorry! You can not leave email empty.',
                    'email.email' => 'Email type must be accordingly.'
                ]);
            }
            if ($validator->fails()) 
            {
                return redirect('admin/edit-user/' . $id)->withErrors($validator)->withInput();
            } 
            else 
            {
                if($request->user_type == 'teacher')
                {
                    $user = User::find($request->id);
                    $user->name = $request->first_name;
                    $user->dob = $request->dob;
                    $user->last_name = $request->last_name;
                    $user->email = $request->email;
                    $user->password = Hash::make($request->password);
                    $user->password_hint = $request->password;
                    $user->type = $request->user_type;
                    $user->save();

                    return redirect('admin/manage-users?type=teacher')->with('success', 'You have updated the record!');
                }
                else if($request->user_type == 'student')
                {
                    $user = User::find($request->id);
                    $user->name = $request->first_name;
                    $user->dob = $request->dob;
                    $user->last_name = $request->last_name;
                    $user->email = $request->email;
                    $user->password = Hash::make($request->password);
                    $user->password_hint = $request->password;
                    $user->type = $request->user_type;
                    $user->save();

                    $stuDetail = StudentDetail::where('student_id',$user->id)->first();
                    $stuDetail->student_id = $user->id;
                    $stuDetail->reference_code = $request->reference_code;
                    $stuDetail->grade = $request->grade;
                    $stuDetail->gender = $request->gender;
                    $stuDetail->timings = $request->timing;
                    $stuDetail->save();
                    return redirect('admin/manage-users?type=student')->with('success', 'You have updated the record!');
                }
                else if($request->user_type == 'Institute')
                {
                    $user = User::find($request->id);
                    $user->name = $request->username;
                    $user->email = $request->email;
                    $user->password = Hash::make($request->password);
                    $user->password_hint = $request->password;
                    $user->type = $request->user_type;
                    $user->save();
                    return redirect('admin/manage-users?type=institute')->with('success', 'You have updated the record!');
                }
            }
        }
        return view('admin.user_management.edit_user', get_defined_vars());
    }

    public function guestUsers()
    {
        //$guestUser = GuestUser::where('id','!=','')->with('user')->get();
        $guestUser = User::where('guest_status',1)->get();
        return view('admin.guest_users', get_defined_vars());
    }

    public function manageGuestUsers()
    {
        $userId = decrypt($_GET['userId']);
        $userRating = GuestUser::where('user_id',$userId)->with('level')->get();
        return view('admin.guest_user_rating', get_defined_vars());
    }
}

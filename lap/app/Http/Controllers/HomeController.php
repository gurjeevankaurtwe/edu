<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use App\GuestUser;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.home');
    }

    public function userLogin()
    {
        Auth::logout();
         return view('frontend.login');
    }

    /*============Demo Page=========*/

    public function demoPage(Request $request)
    {
        if($request->all())
        {
            $validator = Validator::make(Input::all(), [
                'name' => 'required',
                'email' => 'unique:users|required|email'
                    ], [
                'name.required' => ' Sorry! You can not leave first name empty.',
                'email.unique' => 'Sorry! This email is already registered.',
                'email.required' => 'Sorry! You can not leave email empty.',
                'email.email' => 'Email type must be accordingly.'
            ]);
            if ($validator->fails()) 
            {
                return redirect('/demo')->withErrors($validator)->withInput();
            }
            else
            {
                $user = new User();
                $user->name = $request->name;
                $user->last_name = $request->last_name;
                $user->company_name = $request->company_name;
                $user->email = $request->email;
                $user->title = $request->title;
                $user->phone = $request->phone;
                $user->guest_status = 1;
                $user->type = 'teacher';
                $user->save();

                $user = User::find($user->id);
                Auth::login($user);

               /* session(['userId' =>$user->id]);*/

                return redirect('/students');
            }
        }
        Auth::logout();
        return view('frontend.demo');
    }

    public function guestUserRating(Request $request)
    {
        $user = new GuestUser();
        $user->rating = $request->rating;
        $user->description = $request->description;
        $user->user_id = Auth::user()->id;
        $user->level_id = $request->level_id;
        $user->save();
        return redirect($request->redirectUrl);
    }
}

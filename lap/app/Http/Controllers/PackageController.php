<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Hash;
use Illuminate\Support\Facades\Input;
use App\User;
use App\PackageManagement;
use App\Session;
use App\UserType;
use App\Classes;
use DB;
use Auth;
use Image;

class PackageController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /*===Package listing===*/

    public function index() 
    {
        $packageDetail = PackageManagement::all();
        return view('admin.packages_management.manage_package',['packageDetail'=>$packageDetail]);
    }

    /*=====Add Package Detail=====*/

    public function add(Request $request)
    {
        if ($request->all()) 
        {
            $validator = Validator::make(Input::all(), [
                        'title' => 'required',
                        'add_description' => 'required',
                        'price' => 'required',
                        'concurrent_session' => 'required',
                        'title.required' => ' Sorry! You can not leave title empty.',
                        'add_description.required' => ' Sorry! You can not leave description empty.',
                        'price.required' => 'Sorry! You can not leave price empty.',
                        'concurrent_session.required' => 'Sorry! You can not leave concurrent session empty.',
            ]);
            if ($validator->fails()) 
            {
                return redirect('admin/add-package')->withErrors($validator)->withInput();
            } 
            else 
            {
                if($request->sale_price>$request->price){
                    return redirect('admin/add-package')->with('error', 'Sorry! selling price can not be greater than actual price.');
                }
                $package = new PackageManagement;
                $package->title = $request->title;
                $package->price = $request->price;
                $package->description = $request->add_description;
                $package->sale_price = $request->sale_price;
                $package->concurrent_session = $request->concurrent_session;
                $package->status = 'Active';
                $package->save();
                return redirect('admin/packages')->with('success', 'Congrats! new package has been added to your system');
            }
        }
        return view('admin.packages_management.add_package',get_defined_vars());
    }

    /*=====Update Package status=====*/

    public function update_package_status(Request $request, $id) 
    {
        $package_id = decrypt($id);
        $packageData = DB::table('package_managements')->where('id', $package_id)->first();
        if ($packageData->status == 'Active') 
        {
            DB::table('package_managements')
                    ->where('id', $package_id)
                    ->update(['status' => 'In-active']);
        }
        if ($packageData->status == 'In-active') 
        {
            DB::table('package_managements')
                    ->where('id', $package_id)
                    ->update(['status' => 'Active']);
        }
        return redirect()->back()->with('success', 'Record has been updated successfully!');
    }

    /*==========Edit Package Detail=======*/

    public function edit_package(Request $request, $id) 
    {
        $package_id = decrypt($id);
        $packageData = DB::table('package_managements')->where('id', $package_id)->first();
        if ($request->all()) 
        {

            $validator = Validator::make(Input::all(), [
                        'title' => 'required',
                        'edit_description' => 'required',
                        'price' => 'required',
                        'concurrent_session' => 'required',
                        'title.required' => ' Sorry! You can not leave title empty.',
                        'edit_description.required' => ' Sorry! You can not leave description empty.',
                        'price.required' => 'Sorry! You can not leave price empty.',
                        'concurrent_session.required' => 'Sorry! You can not leave concurrent session empty.',
            ]);
            if ($validator->fails())
            {
                return redirect('admin/edit-package/' . $id)->withErrors($validator)->withInput();
            } 
            else 
            {
                //dd($request->all());
                if($request->sale_price>$request->price){
                    return redirect('admin/edit-package/' . $id)->with('error', 'Sorry! selling price can not be greater than actual price.');
                }
                DB::table('package_managements')
                        ->where('id', $package_id)
                        ->update(['title' => $request->title,'description' => "$request->edit_description",'price' => $request->price, 'sale_price' => $request->sale_price, 'concurrent_session' => $request->concurrent_session]);
                return redirect()->back()->with('success', 'You have updated the record!');
            }
        }
        return view('admin.packages_management.edit_package', compact('packageData'));
    }

    /*======Delete Package=====*/

    public function delete_package(Request $request, $id) 
    {
        $package_id = decrypt($id);
        DB::table('package_managements')->where('id', $package_id)->delete();
        return redirect()->back()->with('success', 'Record is no more with your system!');
    }

}
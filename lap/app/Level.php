<?php



namespace App;



use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;



class Level extends Authenticatable

{

    use Notifiable;

    public function levelSubLevels()
    {
    	return $this->hasMany('App\SubLevel','level_id','id');
	}


}


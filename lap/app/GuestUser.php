<?php



namespace App;



use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;



class GuestUser extends Authenticatable

{

    use Notifiable;

   	public function user()
    {
    	return $this->belongsTo('App\User','user_id');
	}

	public function level()
	{
		return $this->belongsTo('App\SubLevel','level_id');
	}

}


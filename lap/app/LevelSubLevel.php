<?php



namespace App;



use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;



class LevelSubLevel extends Authenticatable

{

    use Notifiable;

    public function subLevel()
	{
		return $this->belongsTo('App\SubLevel','sub_level_id','id');
	}
	public function subLevelsQues()
    {
    	return $this->hasMany('App\Question','level_sub_level_id','id');
	}


}


<?php



namespace App;



use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;



class SubLevel extends Authenticatable

{

    use Notifiable;

    public function subLevelsQues()
    {
    	return $this->hasMany('App\Question','sub_level_id','id');
	}

	public function levelSubLevel()
	{
		return $this->hasMany('App\LevelSubLevel','sub_level_id');
	}


}


<ul class="sidebar-nav">
                               
                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Users</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{url('admin/add-user')}}">Add User</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/manage-users')}}">Manage Users</a>
                                        </li>
                                    </ul>
                                </li>
                                
                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">User Types</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{url('admin/add-user-type')}}">Add User Type</a>
                                        </li>
                                        <li>
                                            <a href="{{url('admin/manage-users-type')}}">Manage Users Type</a>
                                        </li>
                                    </ul>
                                </li>
                                
                            </ul>
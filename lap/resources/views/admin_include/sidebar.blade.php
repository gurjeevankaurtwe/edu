<ul class="sidebar-nav">

    @if(Auth::user()->type=='admin')

    <li>

        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Users</span></a>

        <ul>

            <!-- <li>

                <a href="{{url('admin/add-user')}}">Add Institute</a>

            </li> -->
            <!-- <li>

                <a href="{{url('admin/manage-users')}}">Add Trainer/Student</a>

            </li> -->
            <li>

                <a href="{{url('admin/manage-users?type=institute')}}">Institute</a>

            </li>
            <li>

                <a href="{{url('admin/manage-users?type=teacher')}}">Teacher</a>

            </li>
            <li>

                <a href="{{url('admin/manage-users?type=student')}}">Student</a>

            </li>
            
            <li>

                <a href="{{url('admin/manage-users')}}">Manage Users</a>

            </li>

        </ul>

    </li>



    <li>

        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">User Types</span></a>

        <ul>

            <!-- <li>

                <a href="{{url('admin/add-user-type')}}">Add User Type</a>

            </li> -->

            <li>

                <a href="{{url('admin/manage-users-type')}}">Manage Users Type</a>

            </li>

        </ul>

    </li>

    

    <li>

        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Packages</span></a>

        <ul>

            <li>

                <a href="{{url('admin/add-package')}}">Add Package</a>

            </li>

            <li>

                <a href="{{url('admin/packages')}}">Manage Packages</a>

            </li>

        </ul>

    </li>

    

    <li>

        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Levels</span></a>

        <ul>

            <li>

                <a href="{{url('admin/levels')}}">Manage levels</a>

            </li>

        </ul>

    </li>

     <li>
        <a href="{{url('admin/guest_users')}}"><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Guest Users</span></a>

        

    </li>

    @endif

    

    @if(Auth::user()->type=='Institute')

    <li>

        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Student/Teacher</span></a>

        <ul>

            <li>

                <a href="{{url('admin/add-inst-users')}}">Add Student/Teacher</a>

            </li>

            <li>

                <a href="{{url('admin/manage-inst-users')}}">Manage Institute Users</a>

            </li>

        </ul>

    </li>

    

    <li>

        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Sessions</span></a>

        <ul>

            <li>

                <a href="{{url('admin/add-session')}}">Add Session</a>

            </li>

            <li>

                <a href="{{url('admin/manage-session')}}">Manage Session</a>

            </li>

        </ul>

    </li>

    

    <li>

        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Classes</span></a>

        <ul>

            <li>

                <a href="{{url('admin/add-class')}}">Add Class</a>

            </li>

            <li>

                <a href="{{url('admin/manage-class')}}">Manage Classes</a>

            </li>

        </ul>

    </li>

    @endif

    

    @if(Auth::user()->type=='teacher')

    <li>

        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Sessions</span></a>

        <ul>

            <li>

                <a href="{{url('admin/manage-session')}}">Manage Session</a>

            </li>

        </ul>

    </li>

    @endif



</ul>
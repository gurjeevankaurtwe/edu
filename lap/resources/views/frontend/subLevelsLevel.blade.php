@extends('frontend.include.layout')

@section('middle_content')


		
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Sub Level
					</h3>
				</div>
				<div class="course_wrap">
					<div class="sub_level_wrap">
						<div class="row">
							@php
					          $i=0;
					        @endphp
					        @foreach($levelSubLevel as $levSubLev)

					            @php
					                if($i == 0)
					                {
					                  $disabled = "";
					                }
					                else
					                {
					                  $disabled = "disabled";
					                }
					            @endphp
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="sublev_box">
										<?php
											if(Auth::user()->type == 'teacher')
									        {
									            $teacherId = Auth::user()->id;
									            $studentIds = $studentId;
									            $compSubLevels = DB::table('student_level_details')->where('student_id',$studentIds)->where('teacher_id',$teacherId)->where('sublevel_id',$levSubLev->id)->where('session_id',0)->get();
									        }
									        if(Auth::user()->type == 'student' || Auth::user()->type == 'Institute')
									        {
									            $teacherId = '';
									            if(Auth::user()->type == 'student')
									            {
									            	$studentIds = Auth::user()->id;
									            }
									            elseif(Auth::user()->type == 'Institute')
									            {
									            	$studentIds = $studentId;
									            }
									            $compSubLevels = DB::table('student_level_details')->where('student_id',$studentIds)->where('sublevel_id',$levSubLev->id)->where('session_id',0)->get();
									        }
									        $getTotalQues = DB::table('questions')->where('level_sub_level_id',$levSubLev->id)->where('status',1)->get();

									        
									        if(count($getTotalQues) > 0)
									        {

									        	$totalWidth =  (count($compSubLevels)*100)/count($getTotalQues);
									        }
									        else
									        {
									        	$totalWidth = 0;
									        }

									        $nextLevel = '';

									        
								        	$preLevQuesDetail = array();
								        	$getPrevTotalQues = array();
								        	$prevLevel = DB::table('sub_levels')->where('level_id',1)->where('id', '<', $levSubLev->id)->orderBy('id','DESC')->first();
											if($prevLevel != '')
								        	{
								        		$preLevQuesDetail = DB::table('student_level_details')->where('student_id',$studentIds)->where('sublevel_id',$prevLevel->id)->get();
								        		
								        		$getPrevTotalQues = DB::table('questions')->where('sub_level_id',$prevLevel->id)->where('status',1)->get();
								        	}

								        	if(!empty($preLevQuesDetail))
								        	{
								        		if(count($preLevQuesDetail) == count($getPrevTotalQues))
									        	{
									        		$lock = '';
									        		$onClick = '';
									        	}
									        	else
									        	{
									        		$lock = '<i class="fa fa-lock" aria-hidden="true"></i>';
									        		$onClick = 'onclick="return false"';
									        	}
								        	}
								        	else
								        	{
								        		$lock = '<i class="fa fa-lock" aria-hidden="true"></i>';
								        		$onClick = 'onclick="return false"';
								        	}

									   	?>
								
										<div class="overlay">
											<h3>{{$levSubLev->title}}</h3>
											<div class="progress">
												
												<div class="progress-bar" style="width:{{$totalWidth.'%'}}" aria-valuemax="100"></div>
											</div>									
											<div class="d-flex align-items-center">
												
													<h6>Questions: {{count($levSubLev->subLevelsQues)}}</h6>
													<a href="{{url('/level_questions_/'.encrypt($levSubLev->id).'?student='.encrypt($studentId).'&subLevel=1')}}" class="ml-auto">Continue</a>
												
												
											</div>																		
										</div>									
									</div>
								</div>
							@endforeach
						</div>													
					</div>
				</div>									
			</div>
@endsection
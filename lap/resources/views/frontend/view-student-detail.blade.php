@extends('frontend.include.layout')

@section('middle_content')

		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						View Student
					</h3>
				</div>
				<div class="course_wrap">
					<div id="btnContainer" class='text-right my-3'>
	        <a href="{{url('/students')}}"><button class="btn btn-primary">Back</button></a>
	    </div>
			<form action="{{url('/add-students')}}" method="post" autocomplete="off">
	        	@csrf
	        	<input type="hidden" name="id" value="{{$stuDetail->id}}">
			  	<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputEmail4">Student Refrence Code</label>
				      	<input type="text" class="form-control" name="reference_code" placeholder="Reference Code" value="@if ($stuFurDetail != '') {{$stuFurDetail->reference_code }} @endif" readonly>
				    </div>
				    <div class="form-group col-md-6">
				      	<label for="inputPassword4">Email</label>
				      	<input type="email" class="form-control" name="email" placeholder="Email" value="{{$stuDetail->email}}" readonly>
				    </div>
			  	</div>
			  	<div class="form-row">
				    <div class="form-group col-md-6">
					    <label for="inputAddress">First Name</label>
					    <input type="text" class="form-control" name="name" value="{{$stuDetail->name}}" placeholder="First Name" readonly>
				  	</div>

				  	<div class="form-group col-md-6">
					    <label for="inputAddress">Last Name</label>
					    <input type="text" class="form-control" name="last_name" value="{{$stuDetail->last_name}}" placeholder="Last Name" readonly>
				  	</div>
				    
			  	</div>
			  	<div class="form-row">
			  		<div class="form-group col-md-6">
					    <label for="inputAddress2">Date of Birth</label>
					    <input type="text" class="form-control datepicker" name="dob" value="{{$stuDetail->dob}}" placeholder="Date of Birth" readonly>
				  	</div>
				  	<div class="form-group col-md-6">
					    <label for="inputAddress2">Password</label>
					    <input type="text" class="form-control" name="password" value="{{$stuDetail->password_hint}}" placeholder="Password" min="5" readonly>
				  	</div>
				</div>
				<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputCity">Grade or N/A</label>
				      	<input type="text" class="form-control" name="grade" value="@if ($stuFurDetail != '') {{$stuFurDetail->grade }} @endif" placeholder="Grade or N/A" readonly>
				    </div>
				    <div class="form-group col-md-6">
				      	<label for="inputState">Gender</label>
				      	<select name="gender" class="form-control" name="gender" disabled> 
					        <option value="">Select Gender...</option>
					        <option value="Female" <?php if($stuFurDetail != ''){ if($stuFurDetail->gender == 'Female'){ echo 'selected="selected"';}}?>>Female</option>
					        <option value="male" <?php if($stuFurDetail != ''){ if($stuFurDetail->gender == 'male'){ echo 'selected="selected"';}}?>>Male</option>
				      	</select>
				    </div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
				      <label for="inputZip">Setup Timing</label>
				      <input type="text" class="form-control" name="timing" value="@if ($stuFurDetail != '') {{$stuFurDetail->timings }} @endif" placeholder="Timing" readonly>
				    </div>
				    <div class="form-group col-md-6">
				      	<label for="inputState">Assigned Trainer</label>
				      	<select class="form-control" name="teacher_id" disabled> 
					        <option value="">Select Trainer...</option>
					        @if(count($teacher) > 0)
					        	@foreach($teacher as $val)
					        		<option value="{{$val->id}}" <?php if($val->id == $stuDetail->teacher_id){ echo 'selected="selected"';} ?>>{{ucfirst($val->name)}}</option>
					        	@endforeach
					        @endif
					    </select>
				    </div>
				</div>
			  	
			</form>
				</div>									
			</div>

		
  	
@endsection
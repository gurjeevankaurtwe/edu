@extends('frontend.include.layout')

@section('middle_content')
		
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Trainer List
					</h3>
					<a href="{{url('/add-teacher')}}"><button type="button" class="btn btn-primary">Add Trainer</button></a>
				</div>
				<div class="course_wrap">
					<div class="row">						
						@foreach($teacher as $tea)
							<div class="col-sm-4">
								<div class="course_card color_1">
									<div class="overlay">
										<div class="custom_btn text-right">
											<a title="Edit teacher detail" href="{{url('/edit-teacher/'.encrypt($tea->id))}}" style="color:#4BA04A;"><i class="fas fa-edit"></i></a>
											<a title="View teacher detail" href="{{url('/view-teacher-detail/'.encrypt($tea->id))}}" style="color:#3D83C1;"><i class="fas fa-eye"></i></a>
											<a title="View delete detail" onclick="return confirm('Are you sure you want to delete this item?');" href="{{url('/delete-teacher/'.encrypt($tea->id))}}" class="pull-right"style="color:#C63531;" ><i class="fas fa-trash-alt"></i></a>
										</div>
										<h2>{{ucfirst($tea->name)}}</h2>
										<h6>Email: {{ucfirst($tea->email)}}</h6>																			
										@if($tea->status == 1)
											<a title="Click to Deactive" href="{{url('/update-teacher-status/'.encrypt($tea->id).'/0')}}">Activated</a>
										@else
											<a title="Click to Activate" href="{{url('/update-teacher-status/'.encrypt($tea->id).'/1')}}" >Deactivated</a>
										@endif
										<a title="Teacher's Student" href="{{url('/students?teacherId='.encrypt($tea->id))}}" >Students</a>									
									</div>								
								</div>
							</div>
						@endforeach
						
					</div>
				</div>									
			</div>
@endsection


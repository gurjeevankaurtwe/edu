@extends('frontend.include.layout')

@section('middle_content')


		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">

					@if($levelDetail != '')
						<h3 class="page-title">
							{{$subLevels->title}}
						</h3>
					@else
						<img src="{{url('/public/frontend/images/coming-soon.png')}}" style="display:table;margin:0 auto;margin-top:50px;">
					@endif
				</div>
				
				<div class="course_wrap">
					@if($levelDetail != '')
					<input type="hidden" class="studentId" value="{{$studentId}}">
					<div class="row">
						<div class="col-sm-10 offset-sm-1">
							<div class="level_detail_wrap">								
								<div class="top_content">
									<div class="row">
										<div class="col-xl-8 col-lg-12 order-2 order-xl-1">
											<div class="content_bx">
												<h3>
													{{$subLevels->title}}

												</h3>
									
												<p>{!! $subLevels->description !!}</p>
												@if($subLevels->id == '42' || $subLevels->id == '43' || $subLevels->id == '44' || $subLevels->id == '52' || $subLevels->id == '50' || $subLevels->id == '55' || $subLevels->id == '56' || $subLevels->id == '62' || $subLevels->id == '63' || $subLevels->id == '14' || $subLevels->id == '15' || $subLevels->id == '22' || $subLevels->id == '28' || $subLevels->id == '29' || $subLevels->id == '34' || $subLevels->id == '36' || $subLevels->id == '37')
													<div class="btn_opt mt-4 met-btn">
														<a href="javascript:void(0);" id="metronome_ID" data-attr="metronome" class="rythm-task" level-id="{{$subLevelId}}">Metronome beat</a>
									                    <a href="javascript:void(0);" data-attr="pulse" class="rythm-task" level-id="{{$subLevelId}}">Pulse-light-up</a>
									                    <a href="javascript:void(0);" data-attr="off" class="rythm-task" level-id="{{$subLevelId}}">Off</a>
													</div>
									                <div class="btn_opt mt-3 startbtn" style="display:none">
									                	@if($subLevelId == 64 || $subLevelId == 65 || $subLevelId == 66 || $subLevelId == 67 || $subLevelId == 68 || $subLevelId == 69 || $subLevelId == 70)
															<input type="hidden" class="sub_level_ids" value="{{$levelDetail->id}}">
														@else
															<input type="hidden" class="sub_level_ids" value="{{$ques->session_id}}">
														@endif
														<a href="javascript:void(0);" id="lets_start_rth" level-id="{{$subLevelId}}">Let's Go</a>
														<a href="javascript:void(0);" id="lets_cancel">Cancel</a>
													</div>
												@else
													<div class="btn_opt mt-3">
														@if($subLevelId == 64 || $subLevelId == 65 || $subLevelId == 66 || $subLevelId == 67 || $subLevelId == 68 || $subLevelId == 69 || $subLevelId == 70)
															<input type="hidden" class="sub_level_ids" value="{{$levelDetail->id}}">
														@else
															<input type="hidden" class="sub_level_ids" value="{{$ques->session_id}}">
														@endif
														<a href="javascript:void(0);" id="lets_start" level-id="{{$subLevelId}}">Let's Go</a>
													</div>
												@endif

												
											</div>
										</div>
										<div class="col-xl-4 col-lg-12 order-1 order-xl-2">
											<div class="imgbx">
												<img src="{{url('/public/frontend/images/others/styudy-boy.jpg')}}">
											</div>
										</div>
									</div>
									
									
								</div>
								
          
          						<div id="question-data"></div>
								
								<div class="audio_player mt-3">
									<!-- <audio  controlsList="nodownload" loop="" id="vid" style="width: 100%;">
				                      	<source src="{{url('/public/audio/left.wav')}}" type="audio/wav">
				                  	</audio> -->
									@if($subLevelId != 67 && $subLevelId != 68 && $subLevelId != 69 && $subLevelId != 70)
										@if($subLevelId == 64 || $subLevelId == 65 || $subLevelId == 66)
											@if(Auth::user()->type == 'student')
			                
							                  	<audio controlsList="nodownload" loop="" id="vid" style="width: 100%;">
							                      	<source src="{{url('/public/audio/'.$subLevels->audio)}}" type="audio/wav">
							                  	</audio>
							                @else
							                  <div class="audio_player" >
							                    <a href="javascript:void(0)" id="stopAudio">
							                    	<audio controls controlsList="nodownload" loop="" id="vid" style="width: 100%;">
							                        	<source src="{{url('/public/audio/'.$subLevels->audio)}}" type="audio/wav">
							                    	</audio>
							                    </a>
							                  </div>
							                @endif
										@else
											@if(Auth::user()->type == 'student')
			                
							                  <audio autoplay controlsList="nodownload" loop="" id="vid" style="width: 100%;">
							                      <source src="{{url('/public/audio/'.$subLevels->audio)}}" type="audio/wav">
							                  </audio>
							                @else
							                  <div class="audio_player" >
							                    <a href="javascript:void(0)" id="stopAudio"><audio controls autoplay controlsList="nodownload" loop="" id="vid" style="width: 100%;">
							                        <source src="{{url('/public/audio/'.$subLevels->audio)}}" type="audio/wav">
							                    </audio>
							                    </a>
							                  </div>
							                @endif
							            @endif
							        @endif
				            	</div>
							</div>
						</div>						
					</div>
					@endif
				</div>									
			</div>
@endsection
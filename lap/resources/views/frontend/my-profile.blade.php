@extends('frontend.include.layout')

@section('middle_content')

	<div class="main-panel">
		<div class="content-wrapper">
			<div class="page-header">
				<h3 class="page-title">
					My Profile
				</h3>
			</div>
			<div class="course_wrap">
				<div id="btnContainer" class='text-right my-3'>
					@if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute')
	    				<a href="{{url('/students')}}"><button class="btn btn-primary">Back</button></a>
	    			@elseif(Auth::user()->type == 'student')
	    				<a href="{{url('/home')}}"><button class="btn btn-primary">Back</button></a>
	    			@endif
				</div>
				<form action="{{url('/my-profile')}}" method="post" autocomplete="off" enctype="multipart/form-data">
	    			@csrf
	    			<input type="hidden" name="id" value="{{$userData->id}}">
	    			<input type="hidden" name="user_type" value="{{Auth::user()->type}}">
	    			@if(Auth::user()->type == 'teacher')
					  	<div class="form-row">
						    <div class="form-group col-md-6">
						      	<label for="inputEmail4">First Name</label>
						      	<input type="text" class="form-control" value="{{$userData->name}}" name="first_name" placeholder="First Name" required>
						      	<span class="text-danger">{{ $errors->first('first_name') }}</span>
						    </div>
						    <div class="form-group col-md-6">
						      	<label for="inputEmail4">Last Name</label>
						      	<input type="text" class="form-control" value="{{$userData->last_name}}" name="last_name" placeholder="Last Name" required>
						      	<span class="text-danger">{{ $errors->first('last_name') }}</span>
						    </div>
						    <div class="form-group col-md-6">
						      	<label for="inputPassword4">Date of Birth</label>
						      	<input type="text" class="form-control trainer-datepicker" value="{{$userData->dob}}" name="dob" placeholder="Date of Birth" required>
						      	<span class="text-danger">{{ $errors->first('dob') }}</span>
						    </div>
						    <div class="form-group col-md-6">
							    <label for="inputAddress">Email</label>
							    <input type="email" class="form-control" value="{{$userData->email}}" name="email" placeholder="Email" required>
							    <span class="text-danger">{{ $errors->first('email') }}</span>
						  	</div>
						  	<div class="form-group col-md-6">
							    <label for="inputAddress">Profile Image</label>
							    @if(Auth::user()->image != '')
							    	<input type="hidden" class="old_image" name="old_image" value="{{asset('frontend/images/faces/'.Auth::user()->image)}}">
							    @else
							    	<input type="hidden" name="old_image" value="">
							    @endif
							    <input type="file" class="form-control" id="imgInp" name="profile_image">
							   	<div class="img-pre" <?php if(Auth::user()->image != ''){ echo 'style="display:block"';}else{echo 'style="display:none"';} ?>>
							    	<img id="blah" src="{{asset('/public/frontend/images/faces/'.Auth::user()->image)}}" alt="your image" width="200" height="150"/>
							    </div>
							</div>
						</div>
					@elseif(Auth::user()->type == 'student')
						<div class="form-row">
						    <div class="form-group col-md-6">
						      	<label for="inputEmail4">Student Refrence Code</label>
						      	<input type="text" class="form-control" name="reference_code" value="{{$userDetail->reference_code}}" placeholder="Reference Code">
						      	<span class="text-danger">{{ $errors->first('reference_code') }}</span>
						    </div>
						    <div class="form-group col-md-6">
						      	<label for="inputPassword4">Email</label>
						      	<input type="email" class="form-control" value="{{$userData->email}}" name="email" placeholder="Email" required>
						      	<span class="text-danger">{{ $errors->first('email') }}</span>
						    </div>
			  			</div>
					  	<div class="form-row">
						    <div class="form-group col-md-6">
							    <label for="inputAddress">First Name</label>
							    <input type="text" class="form-control" value="{{$userData->name}}" name="first_name" placeholder="First Name" required>
							    <span class="text-danger">{{ $errors->first('first_name') }}</span>
						  	</div>

						  	<div class="form-group col-md-6">
							    <label for="inputAddress">Last Name</label>
							    <input type="text" class="form-control" value="{{$userData->last_name}}" name="last_name" placeholder="Last Name" required>
							    <span class="text-danger">{{ $errors->first('last_name') }}</span>
						  	</div>
						    
					  	</div>
					  	<div class="form-row">
					  		<div class="form-group col-md-6">
							    <label for="inputAddress2">Date of Birth</label>
							    <input type="text" class="form-control datepicker" value="{{$userData->dob}}" name="dob" placeholder="Date of Birth" required>
							    <span class="text-danger">{{ $errors->first('dob') }}</span>
						  	</div>
						  	<div class="form-group col-md-6">
						      	<label for="inputCity">Grade or N/A</label>
						      	<input type="text" class="form-control" name="grade" value="{{$userDetail->grade}}" placeholder="Grade or N/A" required>
						      	<span class="text-danger">{{ $errors->first('grade') }}</span>
						    </div>
						  	
						</div>
						<div class="form-row">
						    
						    <div class="form-group col-md-6">
						      	<label for="inputState">Gender</label>
						      	<select name="gender" class="form-control" name="gender" required> 
							        <option value="">Select Gender...</option>
							        <option value="Female" <?php if($userDetail->gender == 'Female'){ echo 'selected="selected"';}?>>Female</option>
							        <option value="male" <?php if($userDetail->gender == 'male'){ echo 'selected="selected"';}?>>Male</option>
						      	</select>
						      	<span class="text-danger">{{ $errors->first('gender') }}</span>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="inputZip">Setup Timing</label>
						      <input type="text" class="form-control" name="timing" value="{{$userDetail->timings}}" placeholder="Timing">
						      <span class="text-danger">{{ $errors->first('timing') }}</span>
						    </div>
					  	</div>
					  	<div class="form-group col-md-6">
							    <label for="inputAddress">Profile Image</label>
							    @if(Auth::user()->image != '')
							    	<input type="hidden" class="old_image" name="old_image" value="{{asset('frontend/images/faces/'.Auth::user()->image)}}">
							    @else
							    	<input type="hidden" name="old_image" value="">
							    @endif
							    <input type="file" class="form-control" id="imgInp" name="profile_image">
							   	<div class="img-pre" <?php if(Auth::user()->image != ''){ echo 'style="display:block"';}else{echo 'style="display:none"';} ?>>
							    	<img id="blah" src="{{asset('/public/frontend/images/faces/'.Auth::user()->image)}}" alt="your image" width="200" height="150"/>
							    </div>
							</div>
					@elseif(Auth::user()->type == 'Institute')
					  	<div class="form-row">
						    <div class="form-group col-md-6">
						      	<label for="inputEmail4">Institute Name</label>
						      	<input type="text" class="form-control" value="{{$userData->name}}" name="name" placeholder="Institute Name" required>
						      	<span class="text-danger">{{ $errors->first('name') }}</span>
						    </div>
						    <div class="form-group col-md-6">
							    <label for="inputAddress">Email</label>
							    <input type="email" class="form-control" value="{{$userData->email}}" name="email" placeholder="Email" required>
							    <span class="text-danger">{{ $errors->first('email') }}</span>
						  	</div>
						</div>
						<div class="form-group col-md-6">
						    <label for="inputAddress">Profile Image</label>
						    @if(Auth::user()->image != '')
						    	<input type="hidden" class="old_image" name="old_image" value="{{asset('frontend/images/faces/'.Auth::user()->image)}}">
						    @else
						    	<input type="hidden" name="old_image" value="">
						    @endif
						    <input type="file" class="form-control" id="imgInp" name="profile_image">
						   	<div class="img-pre" <?php if(Auth::user()->image != ''){ echo 'style="display:block"';}else{echo 'style="display:none"';} ?>>
						    	<img id="blah" src="{{asset('/public/frontend/images/faces/'.Auth::user()->image)}}" alt="your image" width="200" height="150"/>
						    </div>
						</div>
					@endif
		  			<div class="text-center my-3">
		  				<button type="submit" class="btn btn-primary">Update</button>
		  			</div>
				</form>
			</div>									
		</div>
	
@endsection
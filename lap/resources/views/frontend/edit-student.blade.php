@extends('frontend.include.layout')

@section('middle_content')

		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Edit Student
					</h3>
				</div>
				<div class="course_wrap">
					<div id="btnContainer" class='text-right my-3'>
	        <a href="{{url('/students')}}"><button class="btn btn-primary">Back</button></a>
	    </div>
			<form action="{{url('/add-students')}}" method="post" autocomplete="off">
	        	@csrf
	        	<input type="hidden" name="id" value="{{$stuDetail->id}}">
			  	<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputEmail4">Student Refrence Code</label>
				      	<input type="text" class="form-control" name="reference_code" value="@if ($stuFurDetail != '') {{$stuFurDetail->reference_code }} @endif" placeholder="Reference Code">
				      	<span class="text-danger">{{ $errors->first('reference_code') }}</span>
				    </div>
				    <div class="form-group col-md-6">
				      	<label for="inputPassword4">Email</label>
				      	<input type="email" class="form-control" name="email" placeholder="Email" value="{{$stuDetail->email}}" required>
				      	<span class="text-danger">{{ $errors->first('email') }}</span>
				    </div>
			  	</div>
			  	<div class="form-row">
				    <div class="form-group col-md-6">
					    <label for="inputAddress">First Name</label>
					    <input type="text" class="form-control" name="name" value="{{$stuDetail->name}}" placeholder="First Name" required>
					    <span class="text-danger">{{ $errors->first('name') }}</span>
				  	</div>

				  	<div class="form-group col-md-6">
					    <label for="inputAddress">Last Name</label>
					    <input type="text" class="form-control" name="last_name" value="{{$stuDetail->last_name}}" placeholder="Last Name" required>
					    <span class="text-danger">{{ $errors->first('last_name') }}</span>
				  	</div>
				    
			  	</div>
			  	<div class="form-row">
			  		<div class="form-group col-md-6">
					    <label for="inputAddress2">Date of Birth</label>
					    <input type="text" class="form-control datepicker" name="dob" value="{{$stuDetail->dob}}" placeholder="Date of Birth" required>
					    <span class="text-danger">{{ $errors->first('dob') }}</span>
				  	</div>
				  	<div class="form-group col-md-6">
					    <label for="inputAddress2">Password</label>
					    <input type="text" class="form-control" name="password" value="{{$stuDetail->password_hint}}" placeholder="Password" min="5" required>
					    <span class="text-danger">{{ $errors->first('password') }}</span>
				  	</div>
				</div>
				<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputCity">Grade or N/A</label>
				      	<input type="text" class="form-control" name="grade" value="@if ($stuFurDetail != '') {{ $stuFurDetail->grade }} @endif" placeholder="Grade or N/A" required>
				      	<span class="text-danger">{{ $errors->first('grade') }}</span>
				    </div>
				    <div class="form-group col-md-6">
				      	<label for="inputState">Gender</label>
				      	<select class="form-control" name="gender" required> 
					        <option value="">Select Gender...</option>
					        <option value="Female" <?php if($stuFurDetail != ''){ if($stuFurDetail->gender == 'Female'){ echo 'selected="selected"';}}?>>Female</option>
					        <option value="male" <?php if($stuFurDetail != ''){ if($stuFurDetail->gender == 'Female'){ echo 'selected="selected"';}}?>>Male</option>
				      	</select>
				      	<span class="text-danger">{{ $errors->first('gender') }}</span>
				    </div>
				</div>
				<div class="form-row">
				    <div class="form-group col-md-6">
				      <label for="inputZip">Setup Timing</label>
				      <input type="text" class="form-control" name="timing" placeholder="Timing" value="@if ($stuFurDetail != '') {{ $stuFurDetail->timings }} @endif">
				      <span class="text-danger">{{ $errors->first('timing') }}</span>
				    </div>
				    <div class="form-group col-md-6">
				      	<label for="inputState">Assign Trainer</label>
				      	<select class="form-control" name="teacher_id" required> 
					        <option value="">Select Trainer...</option>
					        @if(count($teacher) > 0)
					        	@foreach($teacher as $val)
					        		<option value="{{$val->id}}" <?php if($val->id == $stuDetail->teacher_id){ echo 'selected="selected"';} ?>>{{ucfirst($val->name)}}</option>
					        	@endforeach
					        @endif
					    </select>
				    </div>
			  	</div>

			  	<div class="text-center my-3"><button type="submit" class="btn btn-primary">Update</button></div>
			  	
			</form>
				</div>									
			</div>

		
  	
@endsection
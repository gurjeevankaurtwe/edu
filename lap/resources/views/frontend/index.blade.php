@extends('frontend.include.layout')

@section('middle_content')
		
		<div class="main-panel">
			<div class="content-wrapper">
				<p>
					<h5>{{ucfirst($user->name)}}</h5>
				<p>
				<div class="page-header">
					
					<h3 class="page-title">

						Courses List
					</h3>
				</div>
				<div class="course_wrap">
					<div class="row">
						@foreach($level as $lev)
							<div class="col-xl-4 col-lg-6 col-md-6 col-12">
								<div class="course_card color_1">
									<div class="overlay">
										<h2>{{$lev->title}}</h2>
										<h6>Sub-Levels: {{count($lev->levelSubLevels)}}</h6>
										<!-- <p>{{$lev->description}}</p> -->
										<a href="{{url('/levels_sub_levels/'.encrypt($lev->id).'?student='.encrypt($studentId))}}">Continue</a>
									</div>								
								</div>
							</div>
						@endforeach
					</div>
				</div>									
			</div>
@endsection
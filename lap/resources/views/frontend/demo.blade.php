@extends('frontend.include.login_layout')
@section('title', 'Guest User')
@section('middle_content')

	<div class="login_wrapper">
	
		<div class="login_banner">
			<div class="carousel slide" data-ride="carousel" data-interval="4000">
				<div class="carousel-inner">
					<div class="carousel-item active" style="background-image: url('<?php echo url('/public/login-img/11.jpg');?>')"></div>
					<div class="carousel-item" style="background-image: url('<?php echo url('/public/login-img/13.jpg');?>')"></div>
					<div class="carousel-item" style="background-image: url('<?php echo url('/public/login-img/14.jpg');?>')"></div>
				</div>
			</div>
		</div>


		
		<div class="login_box">
                    <p><img src="{{url('/public/image/ltlogo2.png')}}" height="150" width="200"></p>
                    @if(session()->has('success'))
			    <div class="alert alert-success">
			        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			        {{ session()->get('success') }}
			    </div>
		    @endif
			<form method="POST" action="{{ url('/demo') }}" aria-label="{{ __('Login') }}">
                @csrf
                <div class="form-group">
					<select class="form-control" name="title" autofocus> 
				        <option value="">Select Title...</option>
				        <option value="Dr.">Dr.</option>
				        <option value="Mr.">Mr.</option>
				        <option value="Mrs.">Mrs.</option>
				        <option value="Miss.">Miss.</option>
				    </select>
				     @if ($errors->has('title'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
				</div>
				<div class="form-group">
					<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="First Name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
				</div>
				<div class="form-group">
					<input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" autofocus>

                    @if ($errors->has('last_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
				</div>
				<div class="form-group">
					<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
				</div>
				<div class="form-group">
					<input id="company_name" type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" placeholder="Company Name" value="{{ old('company_name') }}" autofocus>

                    @if ($errors->has('company_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('company_name') }}</strong>
                        </span>
                    @endif
				</div>

				<div class="form-group">
					<input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="Phone" value="{{ old('phone') }}" autofocus>

                    @if ($errors->has('phone'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
				</div>
				<div class="text-center btn_optn">
					<button type="submit" class="btn btn-info btn-md btn-block btn_design">Submit</button>
				</div>
			</form>
		</div>
	
	</div>
	
	
@endsection


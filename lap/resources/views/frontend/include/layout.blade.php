<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 

    <title>Edu Therapeutics - @yield('title')</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute')
      
      <link rel="stylesheet" href="{{asset('/public/frontend/vendors/iconfonts/font-awesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{asset('/public/frontend/vendors/css/vendor.bundle.base.css')}}">
      <link rel="stylesheet" href="{{asset('/public/frontend/vendors/css/vendor.bundle.addons.css')}}">
      <link rel="stylesheet" href="{{asset('/public/frontend/css/main.css')}}">
      <link rel="stylesheet" href="{{asset('/public/frontend/css/stylenew.css')}}">
    @else
      <link rel="stylesheet" href="{{asset('/public/frontend/student/vendors/iconfonts/font-awesome/css/all.min.css')}}">
      <link rel="stylesheet" href="{{asset('/public/frontend/student/vendors/css/vendor.bundle.base.css')}}">
      <link rel="stylesheet" href="{{asset('/public/frontend/student/vendors/css/vendor.bundle.addons.css')}}">
      <link rel="stylesheet" href="{{asset('/public/frontend/student/css/main.css')}}">
      <link rel="stylesheet" href="{{asset('/public/frontend/student/css/vg.css')}}">
    @endif


</head>

<body>

    <div class="container-scroller">

        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row default-layout-navbar main_nav">
          <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
            @if(Auth::user()->type == 'teacher')
              <a class="navbar-brand brand-logo" href="{{url('/students')}}"><img src="{{url('/public/frontend/images/logo.png')}}" alt="logo"/></a>
              <a class="navbar-brand brand-logo-mini" href="{{url('/students')}}"><img src="{{url('/public/frontend/images/logo.png')}}" alt="logo"/></a>
            @elseif(Auth::user()->type == 'Institute')
              <a class="navbar-brand brand-logo" href="{{url('/students')}}"><img src="{{url('/public/frontend/images/logo.png')}}" alt="logo"/></a>
              <a class="navbar-brand brand-logo-mini" href="{{url('/students')}}"><img src="{{url('/public/frontend/images/logo.png')}}" alt="logo"/></a>
            @elseif(Auth::user()->type == 'student')
              <a class="navbar-brand brand-logo" href="{{url('/home')}}"><img src="{{url('/public/frontend/images/logo.png')}}" alt="logo"/></a>
              <a class="navbar-brand brand-logo-mini" href="{{url('/home')}}"><img src="{{url('/public/frontend/images/logo.png')}}" alt="logo"/></a>
            @endif
          </div>
          <div class="navbar-menu-wrapper d-flex align-items-stretch">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="fas fa-bars"></span>
            </button>
            <ul class="navbar-nav">
              <li class="nav-item nav-search d-none d-md-flex">
                <div class="nav-link">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="fas fa-search"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Search" aria-label="Search">
                  </div>
                </div>
              </li>
            </ul>
            
              <ul class="navbar-nav navbar-nav-right">                
                
                <li class="nav-item nav-profile dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                      @if(Auth::user()->image)
                        <img src="{{asset('/public/frontend/images/faces/'.Auth::user()->image)}}" alt="image"/>
                      @else
                        <img src="{{url('/public/frontend/images/faces/face5.jpg')}}" alt="profile"/>
                      @endif
                  </a>
                  <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                    <a href="{{url('/my-profile')}}" class="dropdown-item">
                      <i class="fas fa-cog text-primary"></i>
                      Settings
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="javascript:void();"class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    <!-- <a href="javascript:void();" class="dropdown-item">
                      <i class="fas fa-power-off text-primary"></i>
                      Logout
                    </a> -->
                  </div>
                </li>       
              </ul>
           
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="fas fa-bars"></span>
            </button>
          </div>
        </nav>
  
        <div class="container-fluid page-body-wrapper"> 
  
            <nav class="sidebar sidebar-offcanvas main_sidebar" id="sidebar">
                <ul class="nav">
                  <li class="nav-item nav-profile">
                    <div class="nav-link">
                      <div class="profile-image">
                        @if(Auth::user()->image)
                          <img src="{{asset('/public/frontend/images/faces/'.Auth::user()->image)}}" alt="image"/>
                        @else
                          <img src="{{url('/public/frontend/images/faces/face5.jpg')}}" alt="profile"/>
                        @endif
                      </div>
                      <div class="profile-name">
                        <p class="name">Welcome {{Auth::user()->name}}</p>
                        @if(Auth::user()->guest_status == 0)
                          <p class="designation">{{ucfirst(Auth::user()->type)}}</p>
                        @else
                          <p class="designation">{{'Guest User'}}</p>
                        @endif
                      </div>
                    </div>
                  </li>
                  @if(Auth::user()->type=='teacher')
                    <li class="nav-item active">
                      <a class="nav-link" href="{{url('/students')}}">
                        <i class="fa fa-puzzle-piece menu-icon"></i>
                        <span class="menu-title">Students</span>
                      </a>
                    </li>  
                  @elseif(Auth::user()->type=='Institute')
                    <li class="nav-item {{ (request()->is('/teachers')) ? 'active' : '' }}">
                      <a class="nav-link" href="{{url('/teachers')}}">
                        <i class="fa fa-puzzle-piece menu-icon"></i>
                        <span class="menu-title">Trainer Management</span>
                      </a>
                    </li> 
                    <li class="nav-item {{ (request()->is('/students')) ? 'active' : '' }}">
                      <a class="nav-link" href="{{url('/students')}}">
                        <i class="fa fa-puzzle-piece menu-icon"></i>
                        <span class="menu-title">Student Management</span>
                      </a>
                    </li> 
                  @elseif(Auth::user()->type=='student')
                     <li class="nav-item active">
                      <a class="nav-link" href="{{url('/home')}}">
                        <i class="fa fa-puzzle-piece menu-icon"></i>
                        <span class="menu-title">Dashboard</span>
                      </a>
                    </li> 
                  @endif    
                </ul>
            </nav>

            @yield("middle_content")
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">copyright &copy; {{date('Y')}} Michael Martinez Music / <a href="https://www.michaelmartinezmusic.com">www.michaelmartinezmusic.com</a></span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="far fa-heart text-danger"></i></span>
                </div>
            </footer>
        </div>
    </div>
</div>
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    @if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute')

      <script src="{{url('/public/frontend/vendors/js/vendor.bundle.base.js')}}"></script>
      <script src="{{url('/public/frontend/vendors/js/vendor.bundle.addons.js')}}"></script>
      <script src="{{url('/public/frontend/js/off-canvas.js')}}"></script>
      <script src="{{url('/public/frontend/js/hoverable-collapse.js')}}"></script>
      <script src="{{url('/public/frontend/js/misc.js')}}"></script>
      <script src="{{url('/public/frontend/js/settings.js')}}"></script>
      <script src="{{url('/public/frontend/js/todolist.js')}}"></script>
      <script src="{{url('/public/frontend/js/dashboard.js')}}"></script>
      <script src="{{url('/public/login-img/edu.js')}}"></script>
      <script src="{{url('/public/frontend/js/frontend.js')}}"></script>
    @else
      <script src="{{url('/public/frontend/student/vendors/js/vendor.bundle.base.js')}}"></script>
      <script src="{{url('/public/frontend/student/vendors/js/vendor.bundle.addons.js')}}"></script>
      <script src="{{url('/public/frontend/student/js/off-canvas.js')}}"></script>
      <script src="{{url('/public/frontend/student/js/hoverable-collapse.js')}}"></script>
      <script src="{{url('/public/frontend/student/js/misc.js')}}"></script>
      <script src="{{url('/public/frontend/student/js/settings.js')}}"></script>
      <script src="{{url('/public/frontend/student/js/todolist.js')}}"></script>
      <script src="{{url('/public/frontend/student/js/dashboard.js')}}"></script>
      <!-- <script src="{{url('/public/login-img/edu.js')}}"></script> -->
      <script src="{{url('/public/frontend/js/frontend.js')}}"></script>
    @endif
    <script>
        
        $(document).ready(function()
        {
          $( function(){
              $( ".datepicker" ).datepicker({
              autoclose: true,
            });
          });
        
          $(".data_drop").click(function()
          {
              if ($('.dropdown-menu-list').css('display') == 'block')
              {
                $(".dropdown-menu-list").hide();
              }
              else
              {
                $(".dropdown-menu-list").show();
              }
          });
          $("#lets_start").click(function()
          {
              $(".ques_head").hide(); 
          });
      });

      jQuery(document).off('click','#lets_start').on('click','#lets_start',function()
      {

          var myAudio = document.getElementById('vid');
          if(myAudio)
          {
            myAudio.play();
            var isPlaying = false;

            myAudio.onplaying = function() {
              isPlaying = true;
            };
            myAudio.onpause = function() {
              isPlaying = false;
            };
          }
          
   
          var prevIDs = $("#all-prev-ids").val();
          var dateData = $(this).attr('level-id');
          var studentId = $('.studentId').val();
          var subLevelIds = $('.sub_level_ids').val();
          $.ajax({
              dataType: "html",
              url: "user/get-questions/" + dateData+'?studentId='+studentId+'&subLevelIds='+subLevelIds,
              success:function(data){

                  $('#question-data').html(data);
                  $('.top_content').css('display','none');
                  $("#lets_start").hide();
                  
              }
          });
      });

//new one START
      // jQuery(document).off('click','#metronome_ID').on('click','#metronome_ID',function()
      // {
      //     var myAudio = document.getElementById('my_metronome');
      //     if(myAudio)
      //     {
      //       myAudio.play();
      //       var isPlaying = false;

      //       myAudio.onplaying = function() {
      //         isPlaying = true;
      //       };
      //       myAudio.onpause = function() {
      //         isPlaying = false;
      //       };
      //     }
   
      // });
      //new one END

      function togglePlay() {
        if (isPlaying) {
          myAudio.pause()
        } else {
          myAudio.play();
        }
      };

      jQuery(document).on('click','#lets_start_rth',function()
      {
          var type = jQuery(this).attr('data-attr');
          if(type == 'metronome')
          {
            var audioSrc = '<?php echo url('/public/audio/2SEC.mp3'); ?>';
            var myAudio = document.getElementById('vid');
            myAudio.setAttribute('src', audioSrc);
            if(myAudio)
            {
              myAudio.play();
              var isPlaying = false;

              myAudio.onplaying = function() {
                isPlaying = true;
              };
              myAudio.onpause = function() {
                isPlaying = false;
              };
            }
          }
          var prevIDs = $("#all-prev-ids").val();
          var dateData = $(this).attr('level-id');
          var studentId = $('.studentId').val();
          var subLevelIds = $('.sub_level_ids').val();
          $.ajax({
              dataType: "html",
              url: "user/get-questions/" + dateData+'?studentId='+studentId+'&subLevelIds='+subLevelIds,
              success:function(data){
                  $('#question-data').html(data);
                  $('.top_content').css('display','none');
                  $("#lets_start").hide();
                  jQuery('.rhythm-task-option').css('display','block');
                  jQuery('.rthym-btn').css('display','none');
                  jQuery('.rythm-task-type').val(type);
              }
          });
      });

      jQuery(document).on('click','#lets_cancel',function(){
          jQuery('.startbtn').css('display','none');
          jQuery('.met-btn').css('display','block');
      });

      jQuery(document).on('click','.rythm-task',function()
      {
          var type = jQuery(this).attr('data-attr');
          jQuery('.startbtn').css('display','block');
          jQuery('.met-btn').css('display','none');
          jQuery('#lets_start_rth').attr('data-attr',type);
      });


      $(function () {
          $('.datepicker').datepicker();
          $('.trainer-datepicker').datepicker({
              format: 'mm/dd',
          });
      });

      function readURL(input) 
      {
          if (input.files && input.files[0]) 
          {
              var reader = new FileReader();
          
              reader.onload = function(e) 
              {
                  $('#blah').attr('src', e.target.result);
              }
          
              reader.readAsDataURL(input.files[0]);
          }
      }

      $("#imgInp").change(function() 
      {
          jQuery('.img-pre').css('display','block');
          jQuery('.old_image').val('');
          readURL(this);
      });
  </script>
</body>
</html>
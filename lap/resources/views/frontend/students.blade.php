@extends('frontend.include.layout')

@section('middle_content')
		
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Student List
					</h3>
					@if(Auth::user()->guest_status != 1)
						<a href="{{url('/add-students')}}"><button type="button" class="btn btn-primary">Add Student</button></a>
					@endif
				</div>
				<div class="course_wrap">
					<div class="row">
						@if(Auth::user()->guest_status == 1)
							@if($student != '')
								<div class="col-xl-4 col-lg-6 col-md-6 col-12">
									<div class="course_card color_1">
										<div class="overlay">
											
											<h2><?php echo isset($student->name)?ucfirst($student->name):'';?></h2>
											<h6>Email: <?php echo isset($student->email)?$student->email:'';?></h6>
											<?php
											if(empty($student->id)){

												?>
												<a href="{{url('/home?student='.encrypt(31))}}">Courses</a>
												<?php
											}else{
												?>
												<a href="{{url('/home?student='.encrypt($student->id))}}">Courses</a>
											
												<?php
											}
											?>
											</div>								
									</div>
								</div>
							@endif
						@else
							@if(Auth::user()->type == 'teacher')
								@foreach($student as $stu)
									<div class="col-xl-4 col-lg-6 col-md-6 col-12">
										<div class="course_card color_1">
											<div class="overlay">
												<h2>{{ucfirst($stu->name)}}</h2>
												<h6>Email: {{$stu->email}}</h6>
												<a href="{{url('/home?student='.encrypt($stu->id))}}">Courses</a>
											</div>								
										</div>
									</div>
								@endforeach
							@elseif(Auth::user()->type == 'Institute')
								@foreach($student as $stu)

									@if($stu->type == 'student')
										<div class="col-xl-4 col-lg-6 col-md-6 col-12">
											<div class="course_card color_1">											
												<div class="overlay">
													<div class="custom_btn text-right">
														<a title="Edit student detail" href="{{url('/edit-student/'.encrypt($stu->id))}}" style="color:#4BA04A;"><i class="fas fa-edit"></i></a>
														<a title="View student detail" href="{{url('/view-student-detail/'.encrypt($stu->id))}}" style="color:#3D83C1;"><i class="fas fa-eye"></i></a>
														<a title="Delete student" href="{{url('/delete-student/'.encrypt($stu->id))}}" onclick="return confirm('Are you sure you want to delete this item?');" class="pull-right" style="color:#C63531;"><i class="fas fa-trash-alt"></i></a>
													</div>
													<h2>{{ucfirst($stu->name)}}</h2>
													<h6>Email: {{$stu->email}}</h6>
													<div class="btn_opt">
														<a href="{{url('/home?student='.encrypt($stu->id))}}">Courses</a>																							
														@if($stu->status == 1)
															<a title="Click to Deactive" href="{{url('/update-student-status/'.encrypt($stu->id).'/0')}}">Activated</a>
														@else
															<a title="Click to Activate" href="{{url('/update-student-status/'.encrypt($stu->id).'/1')}}" >Deactivated</a>
														@endif
													</div>																							
												</div>								
											</div>
										</div>
									@elseif($stu->type == 'teacher')
										@foreach($stu->students as $stud)
											
											<div class="col-xl-4 col-lg-6 col-md-6 col-12">
												<div class="course_card color_1">
													<div class="overlay">
														<div class="custom_btn text-right">
															<a title="Edit student detail" href="{{url('/edit-student/'.encrypt($stud->id))}}" style="color:#4BA04A;"><i class="fas fa-edit"></i></a>
															<a title="View student detail" href="{{url('/view-student-detail/'.encrypt($stud->id))}}" style="color:#3D83C1;"><i class="fas fa-eye"></i></a>
															<a title="Delete student" href="{{url('/delete-student/'.encrypt($stud->id))}}" onclick="return confirm('Are you sure you want to delete this item?');" class="pull-right" style="color:#C63531;"><i class="fas fa-trash-alt"></i></a>
														</div>
														<h2>{{ucfirst($stud->name)}}</h2>
														<h6>Email: {{$stud->email}}</h6>
														<a href="{{url('/home?student='.encrypt($stud->id))}}">Courses</a>
														@if($stud->status == 1)
															<a title="Click to Deactive" href="{{url('/update-student-status/'.encrypt($stud->id).'/0')}}">Activated</a>
														@else
															<a title="Click to Activate" href="{{url('/update-student-status/'.encrypt($stud->id).'/1')}}" >Deactivated</a>
														@endif
													</div>								
												</div>
										</div>
										@endforeach
									@endif
								@endforeach
							@endif
						@endif
					</div>
				</div>									
			</div>
@endsection


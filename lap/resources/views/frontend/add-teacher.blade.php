@extends('frontend.include.layout')

@section('middle_content')

		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Add Trainer
					</h3>
				</div>
				<div class="course_wrap">
					<div id="btnContainer" class='text-right my-3'>
	        <a href="{{url('/teachers')}}"><button class="btn btn-primary">Back</button></a>
	    </div>
			<form action="{{url('/add-teacher')}}" method="post" autocomplete="off">
	        	@csrf

	        	<input type="hidden" value="" name="id">

			  	<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputPassword4">Email</label>
				      	<input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email') }}" required>
				      	<span class="text-danger">{{ $errors->first('email') }}</span>
				    </div>
				    
				    <div class="form-group col-md-6">
					    <label for="inputAddress2">Password</label>
					    <input type="password" class="form-control" name="password" placeholder="Password" value="{{old('password') }}" min="5" required>
					    <span class="text-danger">{{ $errors->first('password') }}</span>
				  	</div>
				  	
			  	</div>
			  	<div class="form-row">
				    <div class="form-group col-md-6">
					    <label for="inputAddress">First Name</label>
					    <input type="text" class="form-control" name="name" placeholder="First Name" value="{{old('name') }}" required>
					    <span class="text-danger">{{ $errors->first('name') }}</span>
				  	</div>
				  	

				  	<div class="form-group col-md-6">
					    <label for="inputAddress">Last Name</label>
					    <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{old('last_name') }}" required>
					    <span class="text-danger">{{ $errors->first('last_name') }}</span>
				  	</div>
				  	
				    
			  	</div>
			  	<div class="form-row">
			  		<div class="form-group col-md-6">
					    <label for="inputAddress2">Date of Birth</label>
					    <!--trainer-datepicker-->
					    <input type="text" class="form-control trainer-datepicker" name="dob" value="{{old('dob') }}" placeholder="Date of Birth" required>
					    <span class="text-danger">{{ $errors->first('dob') }}</span>
				  	</div>
				</div>
				<div class="text-center my-3">
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>									
	</div>
@endsection
@extends('layouts.admin_dashboard')

@section('content')

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Session Management<br><small>You can add new session from here to submit this form!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Session</li>
    <li><a href="{{url('admin/add-user')}}">Add Session</a></li>
</ul>
<!-- END Datatables Header -->
<div class="col-md-12">
    <!-- Form Validation Example Block -->
    <div class="block">
        <!-- Form Validation Example Title -->
        <div class="block-title">
            <h2><strong>Add</strong> Session</h2>
        </div>
        <!-- END Form Validation Example Title -->
        @if(session()->has('error'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('error') }}
        </div>
        @endif
        <!-- Form Validation Example Content -->
        <form id="form-validation" action="" method="post" class="form-horizontal form-bordered">
            {{ csrf_field() }}
            <fieldset>
                <legend><i class="fa fa-angle-right"></i> Session Info</legend>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Session Name <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_username" value="{{old('name') }}" name="name" class="form-control" placeholder="session name..">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">Institute Teacher <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <select name="teacher_id" class="form-control">
                                <option value="">--Select Teacher--</option>
                                @foreach($all_teachers as $all_teacher)
                                <option value="{{$all_teacher->id}}">{{ucwords($all_teacher->name)}}</option>
                                @endforeach
                            </select>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">Institute Class <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <select name="class_id" class="form-control">
                                <option value="">--Select Class--</option>
                                @foreach($all_classes as $all_class)
                                <option value="{{$all_class->id}}">{{ucwords($all_class->class)}}</option>
                                @endforeach
                            </select>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('class_id') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">Institute Student <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <select name="student_id" class="form-control">
                                <option value="">--Select Student--</option>
                                @foreach($all_students as $all_student)
                                <option value="{{$all_student->id}}">{{ucwords($all_student->name)}}</option>
                                @endforeach
                            </select>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('student_id') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">Session Subject <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <select name="subject_id" class="form-control">
                                <option value="">--Select Subject--</option>
                                @foreach($all_subjects as $all_subject)
                                <option value="{{$all_subject->id}}">{{ucwords($all_subject->name)}}</option>
                                @endforeach
                            </select>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('subject_id') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Session Start Date <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="date" id="val_username" value="{{old('session_date') }}" name="session_date" class="form-control" >
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('session_date') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Session Start Time <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="time" id="val_username" value="{{old('session_start_time') }}" name="session_start_time" class="form-control" >
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('session_start_time') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Session End Time <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="time" id="val_username" value="{{old('session_end_time') }}" name="session_end_time" class="form-control" >
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('session_end_time') }}</span>
                    </div>
                </div>
                
                
                <div class="form-group weekDays-selector">
                    <label class="col-md-4 control-label" for="val_username">Session Days <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group" id="check_box_data">
                            
                            
                            <input type="hidden" name="days[]" id="weekday-mon1" class="weekday" />
                            <input type="hidden" name="days[]" id="weekday-tue1" class="weekday" />
                            <input type="hidden" name="days[]" id="weekday-wed1" class="weekday" />
                            <input type="hidden" name="days[]" id="weekday-thu1" class="weekday" />
                            <input type="hidden" name="days[]" id="weekday-fri1" class="weekday" />
                            <input type="hidden" name="days[]" id="weekday-sat1" class="weekday" />
                            <input type="hidden" name="days[]" id="weekday-sun1" class="weekday" />
                            
                            
                            <input type="checkbox" id="weekday-mon" class="weekday" />
                            <label for="weekday-mon"  id="mon-lab">Mon</label>
                            <input type="checkbox" id="weekday-tue" class="weekday" />
                            <label for="weekday-tue" id="tue-lab">Tue</label>
                            <input type="checkbox" id="weekday-wed" class="weekday" />
                            <label for="weekday-wed" id="wed-lab">Wed</label>
                            <input type="checkbox" id="weekday-thu" class="weekday" />
                            <label for="weekday-thu" id="thu-lab">Thu</label>
                            <input type="checkbox" id="weekday-fri" class="weekday" />
                            <label for="weekday-fri" id="fri-lab">Fri</label>
                            <input type="checkbox" id="weekday-sat" class="weekday" />
                            <label for="weekday-sat" id="sat-lab">Sat</label>
                            <input type="checkbox" id="weekday-sun" class="weekday" />
                            <label for="weekday-sun" id="sun-lab">Sun</label>
                        </div>
                        <span class="text-danger">{{ $errors->first('days') }}</span>
                    </div>   
                </div>
                
                
                
                
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button id="submit_session" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                </div>
            </fieldset>

        </form>
        <!-- END Form Validation Example Content -->


    </div>
    <!-- END Validation Block -->
</div>

<style>
    .weekDays-selector input {
  display: none!important;
}

.weekDays-selector input[type=checkbox] + label {
  display: inline-block;
  border-radius: 6px;
  background: #dddddd;
  height: 40px;
  width: 40px;
  margin-right: 3px;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
}

/*.weekDays-selector input[type=checkbox]:checked + label {
  background: #2AD705;
  color: #ffffff;
}*/
</style>

<script>
    $(document).ready(function(){
       
    $("#mon-lab").click(function() {
        if($("#weekday-mon").attr('checked')) {
            $("#weekday-mon").attr('checked',false);
            $("#weekday-mon1").val('');
            $("#mon-lab").css('background', "" );
        }else{
            $("#weekday-mon").attr('checked',true);
            $("#weekday-mon1").attr('value','1');
            $("#mon-lab").css('background', "#2AD705" );
        }
    }); 
    
    $("#tue-lab").click(function() {
        if($("#weekday-tue").attr('checked')) {
            $("#weekday-tue").attr('checked',false);
            $("#weekday-tue1").val('');
            $("#tue-lab").css('background', "" );
        }else{
            $("#weekday-tue").attr('checked',true);
            $("#weekday-tue1").attr('value','2');
            $("#tue-lab").css('background', "#2AD705" );
        }
    });
    
    $("#wed-lab").click(function() {
        if($("#weekday-wed").attr('checked')) {
            $("#weekday-wed").attr('checked',false);
            $("#weekday-wed1").val('');
            $("#wed-lab").css('background', "" );
        }else{
            $("#weekday-wed").attr('checked',true);
            $("#weekday-wed1").attr('value','3');
            $("#wed-lab").css('background', "#2AD705" );
        }
    });
    
    $("#thu-lab").click(function() {
        if($("#weekday-thu").attr('checked')) {
            $("#weekday-thu").attr('checked',false);
            $("#weekday-thu1").val('');
            $("#thu-lab").css('background', "" );
        }else{
            $("#weekday-thu").attr('checked',true);
            $("#weekday-thu1").attr('value','4');
            $("#thu-lab").css('background', "#2AD705" );
        }
    });
    
    $("#fri-lab").click(function() {
        if($("#weekday-fri").attr('checked')) {
            $("#weekday-fri").attr('checked',false);
            $("#weekday-fri1").val('');
            $("#fri-lab").css('background', "" );
        }else{
            $("#weekday-fri").attr('checked',true);
             $("#weekday-fri1").attr('value','5');
            $("#fri-lab").css('background', "#2AD705" );
        }
    });
    
    $("#sat-lab").click(function() {
        if($("#weekday-sat").attr('checked')) {
            $("#weekday-sat").attr('checked',false);
            $("#weekday-sat1").val('');
            $("#sat-lab").css('background', "" );
        }else{
            $("#weekday-sat").attr('checked',true);
            $("#weekday-sat1").attr('value','6');
            $("#sat-lab").css('background', "#2AD705" );
        }
    });
    
    $("#sun-lab").click(function() {
        if($("#weekday-sun").attr('checked')) {
            $("#weekday-sun").attr('checked',false);
            $("#weekday-sun1").val('');
            $("#sun-lab").css('background', "" );
        }else{
            $("#weekday-sun").attr('checked',true);
            $("#weekday-sun1").attr('value','7');
            $("#sun-lab").css('background', "#2AD705" );
        }
    });
   
});
</script>
@endsection

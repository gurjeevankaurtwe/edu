@extends('layouts.admin_dashboard')

@section('content')

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Session Management<br><small>You can manage your entire registered sessions!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Users</li>
    <li><a href="{{url('manage-session')}}">Manage Sessions</a></li>
</ul>
<!-- END Datatables Header -->

<!-- Datatables Content -->
<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> sessions</h2>
    </div>

    @if(session()->has('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
    </div>
    @endif
    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center"><i class="gi gi-user"></i></th>
                    <th>Name</th>
                    <th>Teacher</th>
                    <th>Student</th>
                    <th>Subject</th>
                    <th>Session Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    @if(Auth::user()->type!='teacher')
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                    @endif
                    
                    @if(Auth::user()->type=='teacher')
                    <th>View</th>
                    <th class="text-center">Add Questions</th>
                    @endif
                    
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1
                @endphp
                @foreach($all_sessions as $allUser)
                @php $encodedUserId = encrypt($allUser->id) ;
                $teacher_data=DB::table('users')->where('id',$allUser->teacher_id)->first();
                $student_data=DB::table('users')->where('id',$allUser->student_id)->first();
                $subject_data=DB::table('subjects')->where('id',$allUser->subject_id)->first();
                $session_days=DB::table('session_days')->where('session_id',$allUser->id)->get();
                @endphp
                <tr>
                    <td class="text-center">{{$i}}</td>
                    <td class="text-center"><img src="{{asset('img/placeholders/avatars/avatar12.jpg')}}" alt="avatar" class="img-circle"></td>
                    <td><a href="javascript:void(0)">{{$allUser->name}}</a></td>
                    <td>{{ucwords($teacher_data->name)}}</td>
                    <td>{{ucwords($student_data->name)}}</td>
                    <td>{{ucwords($subject_data->name)}}</td>
                    <td>{{$allUser->session_date}}</td>
                    <td>{{$allUser->session_start_time}}</td>
                    <td>{{$allUser->session_end_time}}</td>
                    @if(Auth::user()->type!='teacher')
                    <td>
                        <span title="click to change status" onclick="window.location.href='{{url('admin/update-session-status/'.$encodedUserId)}}'" class="label {{$allUser->status=='1' ? 'label-success' : 'label-danger'}}">{{$allUser->status=='1' ? 'Click to Deactive' : 'Click to Activate'}}</span>
                    </td>  
                    <td class="text-center">  
                        <div class="btn-group">
                            <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/edit-session/'.$encodedUserId)}}'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                            <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/delete-session/'.$encodedUserId)}}'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                    @endif
                    
                    @if(Auth::user()->type=='teacher')
                    <td>
                        <span title="click to view" onclick="window.location.href='{{url('admin/manage-questions/'.$encodedUserId)}}'" class="label label-success">View Questions</span>
                    </td>  
                    <td class="text-center">  
                        <span title="click to add question" onclick="window.location.href='{{url('admin/add-questions/'.$encodedUserId)}}'" class="label label-success">Add Questions</span>
                    </td>
                    @endif
                </tr>
                 @php $i++; @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Datatables Content -->

@endsection

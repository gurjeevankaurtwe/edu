@extends('layouts.admin_dashboard')

@section('content')

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Question Management<br><small>You can add new question from here to submit this form!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Session</li>
    <li><a href="{{url('admin/manage-session')}}">Manage</a></li>
</ul>
<!-- END Datatables Header -->
<div class="col-md-12">
    <!-- Form Validation Example Block -->
    <div class="block">
        <!-- Form Validation Example Title -->
        <div class="block-title">
            <h2><strong>Add</strong> Question</h2>
        </div>
        <!-- END Form Validation Example Title -->
        @if(session()->has('error'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('error') }}
        </div>
        @endif
        <!-- Form Validation Example Content -->
        <form id="form-validation" action="" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
            {{ csrf_field() }}
            <fieldset>
                <legend><i class="fa fa-angle-right"></i> Question Info</legend>
                <div class="input_fields_wrap"> 
                    
                    <div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="question[]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="option1[]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="option1_img[]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="option2[]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="option2_img[]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="">
                                <input type="radio" checked="" id="val_username" value="option1" name="answer[]" >option 1
                                <input type="radio" id="val_username" value="option2" name="answer[]" >option 2
                                
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="answer_record[]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                </div>
                
                
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button id="submit_session" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        <button class="btn btn-sm btn-info add_field_button">Add More Questions</button>
                    </div>
                </div>
            </fieldset>

        </form>
        <!-- END Form Validation Example Content -->


    </div>
    <!-- END Validation Block -->
</div>
@endsection

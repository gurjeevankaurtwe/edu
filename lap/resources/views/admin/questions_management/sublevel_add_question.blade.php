@extends('layouts.admin_dashboard')

@section('content')

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Question Management<br><small>You can add new question from here to submit this form!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Session</li>
    <li><a href="{{url('admin/add-questions/'.encrypt($session_id))}}">Question Management</a></li>
</ul>
<!-- END Datatables Header -->
<div class="col-md-12">
    <!-- Form Validation Example Block -->
    <div class="block">
        <!-- Form Validation Example Title -->
        <div class="block-title">
            <h2><strong>Add</strong> Question</h2>
        </div>
        <!-- END Form Validation Example Title -->
        @if(session()->has('error'))
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session()->get('error') }}
        </div>
        @endif
        <!-- Form Validation Example Content -->
        <form id="form-validation" action="" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
            <input type="hidden" name="url" value="{{ URL::previous() }}">
            {{ csrf_field() }}
            <fieldset>
                <legend><i class="fa fa-angle-right"></i> Question Info</legend>
                <div class="input_fields_wrap"> 
                   
                    
                   

                <!-- Level 8 -->

                    

                    <!-- Level 1.10 -->

                    

                    <!-- Level 11 -->

                    @if($session_id=='1' || $session_id=='3' || $session_id == '7' || $session_id == '8'  || $session_id == '9' || $session_id == '11' || $session_id == '2' || $session_id == '4' || $session_id == '10')
                        <div>
                            <h2>Question 1</h2>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                           
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[0][answer]" class="form-control" placeholder="option 2">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <div>
                            <h2>Question 2</h2>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                 <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][answer]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][answer]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                               <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][answer]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][answer]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    @endif
                </div>
                
                
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button id="submit_session" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                </div>
            </fieldset>

        </form>
    </div>
</div>

@endsection

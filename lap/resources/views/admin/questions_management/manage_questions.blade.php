@extends('layouts.admin_dashboard')

@section('content')

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Questions Management<br><small>You can manage your entire registered questions!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Users</li>
    <li><a href="{{url('admin/manage-questions/'.$ids)}}">Manage Questions</a></li>
</ul>
<!-- END Datatables Header -->

<!-- Datatables Content -->
<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> questions</h2>
    </div>

    @if(session()->has('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
    </div>
    @endif
    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Session Name</th>
                    <th>Question</th>
                    <th>Option 1</th>
                    @if($session_id=='1' || $session_id=='3')
                    <th>Option1 Image</th>
                    @endif
                    @if($session_id!='1')
                    <th>Option 2</th>
                    <th>Option 3</th>
                    <th>Option 4</th>
                    @endif
                    <th>Answer</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1
                @endphp
                @foreach($all_questions as $allUser)
                @php $encodedUserId = encrypt($allUser->id) ;
               
                @endphp
                <tr>
                    <td class="text-center">{{$i}}</td>
                    <td>{{$allUser->session_name}}</td>
                    <td>{{$allUser->question}}</td>
                    <td>{{$allUser->option1}}</td>
                    @if($session_id=='1' || $session_id=='3')
                    <td><img src="{{asset('question_images/'.$allUser->option1_img)}}" width="50" height="50"></td>
                    @endif
                    @if($session_id!='1')
                    <td>{{$allUser->option2}}</td>
                    <td>{{$allUser->option3}}</td>
                    <td>{{$allUser->option4}}</td>
                    
                    @endif
                    <td>{{$allUser->answer}}</td>
                    
                    
                    <td>
                        <a href="javascript:void(0)">
                            <span title="click to change status" onclick="window.location.href='{{url('admin/update-questions-status/'.$encodedUserId)}}'" class="label {{$allUser->status=='1' ? 'label-success' : 'label-danger'}}">{{$allUser->status=='1' ? 'Click to Deactive' : 'Click to Activate'}}</span>
                        </a>
                    </td>  
                    <td class="text-center">  
                        <div class="btn-group">
                            @if($session_id=='1' || $session_id=='3')
                            <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/edit-questions/'.$encodedUserId)}}'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                            @endif
                            <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/delete-questions/'.$encodedUserId)}}'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                    
                    
                </tr>
                 @php $i++; @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Datatables Content -->

@endsection

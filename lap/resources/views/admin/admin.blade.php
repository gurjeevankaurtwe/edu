@extends('layouts.admin_dashboard')

@section('content')
<!-- Dashboard 2 Header -->
<div class="content-header">
    <ul class="nav-horizontal text-left">
        <li class="active">
            <a href="javascript:void(0)"><i class="fa fa-user"></i> Users</a>
        </li>

    </ul>
</div>
<!-- END Dashboard 2 Header -->
@endsection

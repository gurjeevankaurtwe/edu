@extends('layouts.admin_dashboard')

@section('content')

<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Package Management<br><small>You can manage your entire pacakges!</small>
        </h1>
    </div>
</div>

<ul class="breadcrumb breadcrumb-top">
    <li>Packages</li>
    <li>
    	<a href="{{url('admin/packages')}}">Manage Packages</a>
    </li>
</ul>

<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> packages</h2>
    </div>

    @if(session()->has('success'))
    
    	<div class="alert alert-success">
	        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        {{ session()->get('success') }}
	    </div>

    @endif

    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	@php
                	$i = 1
                @endphp
                @foreach($packageDetail as $package)
                	@php 
                		$encodedUserId = encrypt($package->id) ;
                	@endphp
                	<tr>
	                    <td class="text-center">{{$i}}</td>
	                    <td class="text-center">{{ucfirst($package->title)}}</td>
	                    <td class="text-center">{{$package->price}}</td>
	                    <td class="text-center">{{$package->sale_price}}</td>
	                 	<td>
	                 		<a href="javascript:void(0)">
                                <span title="click to change status" onclick="window.location.href='{{url('admin/update-package-status/'.$encodedUserId)}}'" class="label {{$package->status=='Active' ? 'label-success' : 'label-danger'}}">{{$package->status=='Active' ? 'Click to Deactive' : 'Click to Activate'}}</span>
                            </a>
	                 	</td>                    
	                 	<td class="text-center">
	                        <div class="btn-group">
	                            <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/edit-package/'.$encodedUserId)}}'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
	                            <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/delete-package/'.$encodedUserId)}}'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
	                        </div>
	                    </td>
	                </tr>
                 	@php $i++; @endphp
                @endforeach
               
            </tbody>
        </table>
    </div>
</div>

@endsection

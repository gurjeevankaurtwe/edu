@extends('layouts.admin_dashboard')

@section('content')

<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>User Management<br><small>You can edit user from here to submit this form!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Users</li>
    <li><a href="{{url('admin/edit-user/'.encrypt($user_id))}}">@if($userData->type == 'Institute') {{'Edit Institute'}} @else {{'Edit User'}} @endif</a></li>
</ul>
<div class="col-md-12">
    <div class="block">
        <div class="block-title">
            <h2><strong>@if($userData->type == 'Institute') {{'Edit Institute'}} @else {{'Edit User'}} @endif</strong></h2>
        </div>
        <form id="form-validation" action="" method="post" class="form-horizontal form-bordered">
            {{ csrf_field() }}
            <fieldset>
                
                <legend><i class="fa fa-angle-right"></i> @if($userData->type == 'Institute') {{'Institute Info'}} @else {{'User Info'}} @endif</legend>

                <input type="hidden" name="id" value="{{$userData->id}}">

                @if($userData->type == 'teacher')

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">First Name <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="{{$userData->name}}" name="first_name" class="form-control" placeholder="Your First Name.." required>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_lastname">Last Name <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_lastname" name="last_name" value="{{$userData->last_name}}" class="form-control" placeholder="Your Last Name.." required>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('last_name') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_dob">Date of birth<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_dob" name="dob" class="form-control datepicker" value="{{$userData->dob}}" placeholder="Your Date of birth.." required>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('dob') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_email">Email <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_email" name="email" value="{{$userData->email}}" class="form-control" placeholder="Your Email" required>
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                    </div>
                
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_email">Password <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="password" id="val_email" required="" name="password" value="{{$userData->password_hint}}" class="form-control" placeholder="Your Password">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                    </div>

                    <input type="hidden" name="user_type" value="teacher">

                @elseif($userData->type == 'student')

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_ref">Student Reference Code<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="reference_code" value="{{$userDetail->reference_code}}" name="reference_code" class="form-control" placeholder="Student Reference Code">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('reference_code') }}</span>
                         </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">First Name <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="{{$userData->name}}" name="first_name" class="form-control" placeholder="Your First Name.." required>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_lastname">Last Name <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_lastname" value="{{$userData->last_name}}" name="last_name" class="form-control" placeholder="Your Last Name.." required>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('last_name') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_dob">Date of birth<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_dob" name="dob" value="{{$userData->dob}}" class="form-control datepicker" placeholder="Your Date of birth.." required>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('dob') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_email">Email <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_email" name="email" value="{{$userData->email}}" class="form-control" placeholder="Your Email" required>
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                    </div>
                
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_email">Password <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="password" id="val_email" required="" name="password" value="{{$userData->password_hint}}" class="form-control" placeholder="Your Password">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_grade">Grade or N/A<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="grade"  name="grade" value="{{$userDetail->grade}}" class="form-control" placeholder="Grade or N/A">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('grade') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_gender">Gender<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select name="gender" id="gender" class="form-control">
                                    <option value="">--Select Gender</option>
                                   
                                        <option value="Female" <?php if($userDetail->gender == 'Female') { echo 'selected="selected"';} ?>>Female</option>
                                        <option value="male" <?php if($userDetail->gender == 'male') { echo 'selected="selected"';} ?>>Male</option>
                                        <option value="prefernottotell" <?php if($userDetail->gender == 'prefernottotell') { echo 'selected="selected"';} ?>>Prefer not to tell</option>
                                   
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('gender') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_timing">SetUp Timings<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="timing" name="timing" value="{{$userDetail->timings}}" class="form-control" placeholder="Your SetUp Timings..">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('timing') }}</span>
                        </div>
                    </div>
                
                    <input type="hidden" name="user_type" value="student">

                @else
                
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Institute Name <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="{{$userData->name}}" name="username" class="form-control" placeholder="Your Institute Name.." required>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('username') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_email">Email <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_email" name="email" value="{{$userData->email}}" class="form-control" placeholder="Your Email" required>
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Password<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="password" id="val_username" value="{{$userData->password_hint}}" required="" name="password" class="form-control" placeholder="Your Password">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                    </div>
                    <input type="hidden" value="Institute" name="user_type">

                @endif
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                </div>
            
            </fieldset>

        </form>
    </div>
</div>


@endsection

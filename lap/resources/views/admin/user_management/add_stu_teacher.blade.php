@extends('layouts.admin_dashboard')

@section('content')

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>User Management<br><small>You can add new student/teacher from here to submit this form!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Users</li>
    <li><a href="{{url('admin/add-student-teacher/'.$inst_id)}}">Add Student/Teacher</a></li>
</ul>
<!-- END Datatables Header -->
<div class="col-md-12">
    <!-- Form Validation Example Block -->
    <div class="block">
        <!-- Form Validation Example Title -->
        <div class="block-title">
            <h2><strong>Add</strong> Student/Teacher</h2>
        </div>
        <!-- END Form Validation Example Title -->

        <!-- Form Validation Example Content -->
        <form id="form-validation" action="" method="post" class="form-horizontal form-bordered">
            {{ csrf_field() }}
            <fieldset>
                <legend><i class="fa fa-angle-right"></i> Trainer Or Student Info</legend>
                <div class="studentFields" style="display:none;">
                 <div class="form-group">
                    <label class="col-md-4 control-label" for="val_ref">Student Reference Code<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="reference_code" name="reference_code" class="form-control" placeholder="Student Reference Code">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                     </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_grade">Grade or N/A<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="grade"  name="grade" class="form-control" placeholder="Grade or N/A">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_timing">SetUp Timings<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="timing" name="timing" class="form-control" placeholder="Your Last Name..">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_gender">Gender<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <select name="gender" id="gender"  class="form-control">
                                <option value="">--Select Gender</option>
                               
                                    <option value="Female">Female</option>
                                    <option value="male">Male</option>
                                    <option value="prefernottotell">Prefer not to tell</option>
                               
                            </select>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                    </div>
                </div>
            </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">User Name <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_username" value="{{old('username') }}" name="username" class="form-control" placeholder="Your username.." required>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('username') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_lastname">Last Name <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_lastname" name="last_name" class="form-control" placeholder="Your Last Name.." required>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('lastname') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_dob">Date of birth<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_dob" name="dob" class="form-control datepicker" placeholder="Your Date of birth.." required>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('dob') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">Email <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_email" name="email" value="{{old('email') }}" class="form-control" placeholder="test@example.com" required>
                            <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">Password <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="password" id="val_email" required="" name="password" value="" class="form-control" placeholder="********">
                            <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">User Type <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <select name="user_type" class="form-control" id="userType" required>
                                <option value="">--Select User Type</option>
                               
                                    <option value="teacher">Teacher</option>
                                    <option value="student">Student</option>
                               
                            </select>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger">{{ $errors->first('user_type') }}</span>
                    </div>
                </div>
                <input type="hidden" name="institute_id" value="{{decrypt($inst_id)}}">
                <!--<div class="form-group">-->
                <!--    <label class="col-md-4 control-label" for="val_email">Institute <span class="text-danger">*</span></label>-->
                <!--    <div class="col-md-6">-->
                <!--        <div class="input-group">-->
                <!--            <select name="institute_id" class="form-control" required>-->
                <!--                <option value="">--Select Institute</option>-->
                <!--                @foreach($all_institutes as $all_institute)-->
                <!--                <option value="{{$all_institute->id}}">{{ucwords($all_institute->name)}}</option>-->
                <!--                @endforeach-->
                <!--            </select>-->
                <!--            <span class="input-group-addon"><i class="gi gi-user"></i></span>-->
                <!--        </div>-->
                <!--        <span class="text-danger">{{ $errors->first('institute_id') }}</span>-->
                <!--    </div>-->
                <!--</div>-->
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                </div>
            </fieldset>

        </form>
        <!-- END Form Validation Example Content -->


    </div>
    <!-- END Validation Block -->
</div>


@endsection

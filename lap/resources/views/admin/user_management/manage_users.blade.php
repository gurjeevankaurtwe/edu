@extends('layouts.admin_dashboard')

@section('content')


<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section"> 
        <h1>
            <i class="fa fa-table"></i>User Management<br><small>You can manage your entire registered users!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Users</li>
    @if($type == 'institute')
        <li><a href="{{url('/admin/manage-users?type=institute')}}">Manage Institutes</a></li>
    @elseif($type == 'teacher')
        <li><a href="{{url('/admin/manage-users?type=teacher')}}">Manage Teachers</a></li>
    @elseif($type == 'student')
        <li><a href="{{url('/admin/manage-users?type=student')}}">Manage Students</a></li>
    @else
        <li><a href="{{url('/admin/manage-users')}}">Manage Users</a></li>
    @endif
</ul>
<!-- END Datatables Header -->

<!-- Datatables Content -->
<div class="block full">
    <div class="block-title">
        @if($type == 'institute')
            <h2><strong>Manage</strong> Institute</h2>
            <a href="{{url('/admin/add-user')}}"><button type="button" class="btn btn-primary">Add Institute</button></a>
        @elseif($type == 'teacher')
            <h2><strong>Manage</strong> Teacher</h2>
            <a href="{{url('/admin/add-teacher')}}"><button type="button" class="btn btn-primary">Add Teacher</button></a>
        @elseif($type == 'student')
            <h2><strong>Manage</strong> Student</h2>
            <a href="{{url('/admin/add-student')}}"><button type="button" class="btn btn-primary">Add Student</button></a>
        @else
            <h2><strong>Manage</strong> users</h2>
        @endif
    </div>

    @if(session()->has('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
    </div>
    @endif
    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center"><i class="gi gi-user"></i></th>
                    <th>Name</th>
                    @if($type == 'teacher')
                        <th>Institute</th>
                    @elseif($type == 'student')
                        <th>Teacher/Institute</th>
                    @else
                        <th>Add Teacher/Student</th>
                    @endif
                    <th>Email</th>
                    <th>Type</th>
                    <th>Status</th>
                    @if($type == 'institute')
                        <th></th>
                    @endif
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1
                @endphp
                @foreach($allUsers as $allUser)
                @php $encodedUserId = encrypt($allUser->id) ;
                    $inst_data=DB::table('users')->where('id',$allUser->institute_id)->first();
                    $stu_data=DB::table('users')->where('id',$allUser->teacher_id)->first();
                @endphp
                <tr>
                    <td class="text-center">{{$i}}</td>
                    <td class="text-center"><img src="{{asset('/public/img/placeholders/avatars/avatar12.jpg')}}" alt="avatar" class="img-circle"></td>
                    <td>{{$allUser->name}}</td>
                    @if($allUser->type=='Institute')
                        <td>
                            <a href="javascript:void(0)">
                                <span title="click to Add Teacher/Student" onclick="window.location.href='{{url('admin/add-student-teacher/'.$encodedUserId)}}'" class="label label-success">Add Teacher/Student</span>
                            </a>
                        </td>
                    @elseif($type == 'teacher' || $type == 'student')
                         @if(isset($inst_data) && !empty($inst_data->name))
                            <td>
                                {{ucwords($inst_data->name)}} Institute
                            </td>
                        @elseif(isset($stu_data) && !empty($stu_data->name))
                            <td>
                                {{ucwords($stu_data->name)}} Teacher
                            </td>
                        @else
                            <td>N/A</td>
                        @endif
                    @else

                        @if(isset($inst_data) && !empty($inst_data->name))
                            <td>
                                {{ucwords($allUser->type)}} Under {{ucwords($inst_data->name)}} Institute
                            </td>
                        @else
                            <td>N/A</td>
                        @endif

                    @endif
                    <td>{{$allUser->email}}</td>
                    <td>{{ucwords($allUser->type)}}</td>
                    <td>
                        <a href="javascript:void(0)">
                            <span title="click to change status" onclick="window.location.href='{{url('admin/update-users-status/'.$encodedUserId)}}'" class="label {{$allUser->status=='1' ? 'label-success' : 'label-danger'}}">{{$allUser->status=='1' ? 'Click to Deactive' : 'Click to Activate'}}</span>
                        </a>
                    </td> 
                    @if($type == 'institute')
                        <td>
                            @if($allUser->type=='Institute')

                                <a href="{{url('manage-users?teacherId='.encrypt($allUser->id).'&type=teacher')}}">
                                    <span title="View Teachers" class="label label-success">View Teachers</span>
                                </a>

                                <a href="{{url('manage-users?teacherId='.encrypt($allUser->id).'&type=student')}}">
                                    <span title="View Students" class="label label-success">View Students</span>
                                </a>

                            @endif
                        </td> 
                    @endif                  
                    <td class="text-center">
                        
                        <div class="btn-group">
                            
                            <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/edit-user/'.$encodedUserId)}}'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>

                            <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/delete-user/'.$encodedUserId)}}'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                </tr>
                 @php $i++; @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Datatables Content -->

@endsection

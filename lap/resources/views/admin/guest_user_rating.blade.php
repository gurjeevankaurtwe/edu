@extends('layouts.admin_dashboard')

@section('content')

<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Guest User Rating Management<br><small>You can manage your entire guest user rating!</small>
        </h1>
    </div>
</div>

<ul class="breadcrumb breadcrumb-top">
    <li>Guest User Rating</li>
    <li>
    	<a href="{{url('admin/guest_users')}}">Manage Guest User Rating</a>
    </li>
</ul>

<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> guest user rating</h2>
    </div>

    @if(session()->has('success'))
    
    	<div class="alert alert-success">
	        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        {{ session()->get('success') }}
	    </div>

    @endif

    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Level</th>
                    <th>Rating</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
            	@php
                	$i = 1
                @endphp
                @if(count($userRating) > 0)
                    @foreach($userRating as $val)
                    	<tr>
    	                    <td class="text-center">{{$i}}</td>
                            <td class="text-center">{{$val->level->title}}</td>
    	                    <td class="text-center">{{$val->rating}}</td>
    	                    <td class="text-center">{{ucfirst($val->description)}}</td>
                        </tr>
                     	@php $i++; @endphp
                    @endforeach
                @endif
               
            </tbody>
        </table>
    </div>
</div>

@endsection

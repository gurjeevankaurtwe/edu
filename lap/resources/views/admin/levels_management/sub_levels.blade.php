@extends('layouts.admin_dashboard')

@section('content')

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Sub Levels Management  For : {{$userData->title}}<br><small>You can manage your entire registered levels!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Levels</li>
    <li><a href="{{url('/admin/sub-levels/'.$id)}}">Manage Sub Levels</a></li> 
</ul>
<!-- END Datatables Header -->

<!-- Datatables Content -->
<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> Sub levels</h2>
    </div>

    @if(session()->has('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session()->get('success') }}
    </div>
    @endif
    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @php
                $i = 1
                @endphp
                @foreach($allUserTypes as $allUser)
                @php $encodedUserId = encrypt($allUser->id) @endphp
                <tr>
                    <td class="text-center">{{$i}}</td> 
                    <td>{{$allUser->title}}</td>
                    <td>
                        <a href="javascript:void(0)">
                            <span title="click to change status" onclick="window.location.href='{{url('admin/update-sub-level-status/'.$encodedUserId)}}'" class="label {{$allUser->status=='1' ? 'label-success' : 'label-danger'}}">{{$allUser->status=='1' ? 'Click to Deactive' : 'Click to Activate'}}</span>
                        </a>
                    </td>                    
                    <td class="text-center"> 
                        <div class="btn-group">
                            @if(count($allUser->levelSubLevel) > 0)
                                <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/edit-sub-level/'.$allUser->id)}}'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default">
                                    <i class="fa fa-pencil"></i>
                                </a>&nbsp;
                                <a href="javascript:void(0)">
                                    <span href="javascript:void(0)" onclick="window.location.href='{{url('admin/level-sub-levels/'.$encodedUserId)}}'" class="label label-success">Sub Levels</span>&nbsp; 
                                </a>
                            @else
                                <a href="javascript:void(0)" onclick="window.location.href='{{url('admin/edit-sub-level/'.$allUser->id)}}'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default">
                                    <i class="fa fa-pencil"></i>
                                </a>&nbsp;
                                <a href="javascript:void(0)">
                                    <span href="javascript:void(0)" onclick="window.location.href='{{url('admin/manage-questions/'.$encodedUserId)}}'" class="label label-success">Questions</span>&nbsp; 
                                </a>
                                <a href="javascript:void(0)">
                                    <span href="javascript:void(0)" onclick="window.location.href='{{url('admin/add-questions/'.$encodedUserId)}}'" class="label label-danger">Add Question</span>
                                </a>
                            @endif
                        </div>
                        
                    </td>
                </tr>
                 @php $i++; @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END Datatables Content -->

@endsection

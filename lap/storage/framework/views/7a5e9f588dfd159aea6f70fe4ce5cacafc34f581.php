<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Edu Therapeutics - <?php echo $__env->yieldContent('title'); ?></title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <?php if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute'): ?>
      
      <link rel="stylesheet" href="<?php echo e(url('frontend/vendors/iconfonts/font-awesome/css/all.min.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(url('frontend/vendors/css/vendor.bundle.base.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(url('frontend/vendors/css/vendor.bundle.addons.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(url('frontend/css/main.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(url('frontend/css/stylenew.css')); ?>">
      
      <link rel="shortcut icon" href="http://www.urbanui.com/">
    <?php else: ?>
      <link rel="stylesheet" href="<?php echo e(url('frontend/student/vendors/iconfonts/font-awesome/css/all.min.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(url('frontend/student/vendors/css/vendor.bundle.base.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(url('frontend/student/vendors/css/vendor.bundle.addons.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(url('frontend/student/css/main.css')); ?>">
      <link rel="stylesheet" href="<?php echo e(url('frontend/student/css/stylenew.css')); ?>">
      <link rel="shortcut icon" href="http://www.urbanui.com/">
    <?php endif; ?>

</head>

<body>

    <div class="container-scroller">

        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row default-layout-navbar main_nav">
          <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
            <?php if(Auth::user()->type == 'teacher'): ?>
              <a class="navbar-brand brand-logo" href="<?php echo e(url('/students')); ?>"><img src="<?php echo e(url('frontend/images/logo.png')); ?>" alt="logo"/></a>
              <a class="navbar-brand brand-logo-mini" href="<?php echo e(url('/students')); ?>"><img src="<?php echo e(url('frontend/images/logo.png')); ?>" alt="logo"/></a>
            <?php elseif(Auth::user()->type == 'Institute'): ?>
              <a class="navbar-brand brand-logo" href="<?php echo e(url('/students')); ?>"><img src="<?php echo e(url('frontend/images/logo.png')); ?>" alt="logo"/></a>
              <a class="navbar-brand brand-logo-mini" href="<?php echo e(url('/students')); ?>"><img src="<?php echo e(url('frontend/images/logo.png')); ?>" alt="logo"/></a>
            <?php elseif(Auth::user()->type == 'student'): ?>
              <a class="navbar-brand brand-logo" href="<?php echo e(url('/home')); ?>"><img src="<?php echo e(url('frontend/images/logo.png')); ?>" alt="logo"/></a>
              <a class="navbar-brand brand-logo-mini" href="<?php echo e(url('/home')); ?>"><img src="<?php echo e(url('frontend/images/logo.png')); ?>" alt="logo"/></a>
            <?php endif; ?>
          </div>
          <div class="navbar-menu-wrapper d-flex align-items-stretch">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="fas fa-bars"></span>
            </button>
            <ul class="navbar-nav">
              <li class="nav-item nav-search d-none d-md-flex">
                <div class="nav-link">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                      <i class="fas fa-search"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Search" aria-label="Search">
                  </div>
                </div>
              </li>
            </ul>
            <ul class="navbar-nav navbar-nav-right">                
              
              <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                    <?php if(Auth::user()->image): ?>
                      <img src="<?php echo e(asset('../storage/files/'.Auth::user()->image)); ?>" alt="image"/>
                    <?php else: ?>
                      <img src="<?php echo e(url('frontend/images/faces/face5.jpg')); ?>" alt="profile"/>
                    <?php endif; ?>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                  <a href="<?php echo e(url('/my-profile')); ?>" class="dropdown-item">
                    <i class="fas fa-cog text-primary"></i>
                    Settings
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="javascript:void();"class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                      Logout
                  </a>
                  <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                      <?php echo csrf_field(); ?>
                  </form>
                  <!-- <a href="javascript:void();" class="dropdown-item">
                    <i class="fas fa-power-off text-primary"></i>
                    Logout
                  </a> -->
                </div>
              </li>       
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="fas fa-bars"></span>
            </button>
          </div>
        </nav>
  
        <div class="container-fluid page-body-wrapper"> 
  
            <nav class="sidebar sidebar-offcanvas main_sidebar" id="sidebar">
                <ul class="nav">
                  <li class="nav-item nav-profile">
                    <div class="nav-link">
                      <div class="profile-image">
                        <?php if(Auth::user()->image): ?>
                          <img src="<?php echo e(asset('../storage/files/'.Auth::user()->image)); ?>" alt="image"/>
                        <?php else: ?>
                          <img src="<?php echo e(url('frontend/images/faces/face5.jpg')); ?>" alt="profile"/>
                        <?php endif; ?>
                      </div>
                      <div class="profile-name">
                        <p class="name">Welcome <?php echo e(Auth::user()->name); ?></p>
                        <p class="designation"><?php echo e(ucfirst(Auth::user()->type)); ?></p>
                      </div>
                    </div>
                  </li>
                  <?php if(Auth::user()->type=='teacher'): ?>
                    <li class="nav-item active">
                      <a class="nav-link" href="<?php echo e(url('/students')); ?>">
                        <i class="fa fa-puzzle-piece menu-icon"></i>
                        <span class="menu-title">Students</span>
                      </a>
                    </li>  
                  <?php elseif(Auth::user()->type=='Institute'): ?>
                    <li class="nav-item <?php echo e((request()->is('/teachers')) ? 'active' : ''); ?>">
                      <a class="nav-link" href="<?php echo e(url('/teachers')); ?>">
                        <i class="fa fa-puzzle-piece menu-icon"></i>
                        <span class="menu-title">Trainer Management</span>
                      </a>
                    </li> 
                    <li class="nav-item <?php echo e((request()->is('/students')) ? 'active' : ''); ?>">
                      <a class="nav-link" href="<?php echo e(url('/students')); ?>">
                        <i class="fa fa-puzzle-piece menu-icon"></i>
                        <span class="menu-title">Student Management</span>
                      </a>
                    </li> 
                  <?php elseif(Auth::user()->type=='student'): ?>
                     <li class="nav-item active">
                      <a class="nav-link" href="<?php echo e(url('/home')); ?>">
                        <i class="fa fa-puzzle-piece menu-icon"></i>
                        <span class="menu-title">Dashboard</span>
                      </a>
                    </li> 
                  <?php endif; ?>    
                </ul>
            </nav>

            <?php echo $__env->yieldContent("middle_content"); ?>
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">copyright &copy; <?php echo e(date('Y')); ?> Michael Martinez Music / <a href="https://www.michaelmartinezmusic.com">www.michaelmartinezmusic.com</a></span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="far fa-heart text-danger"></i></span>
                </div>
            </footer>
        </div>
    </div>
</div>
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <?php if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute'): ?>

      <script src="<?php echo e(url('frontend/vendors/js/vendor.bundle.base.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/vendors/js/vendor.bundle.addons.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/off-canvas.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/hoverable-collapse.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/misc.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/settings.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/todolist.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/dashboard.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/edu.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/frontend.js')); ?>"></script>
    <?php else: ?>
      <script src="<?php echo e(url('frontend/student/vendors/js/vendor.bundle.base.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/student/vendors/js/vendor.bundle.addons.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/student/js/off-canvas.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/student/js/hoverable-collapse.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/student/js/misc.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/student/js/settings.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/student/js/todolist.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/student/js/dashboard.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/edu.js')); ?>"></script>
      <script src="<?php echo e(url('frontend/js/frontend.js')); ?>"></script>
    <?php endif; ?>
    <script>
       $(document).ready(function(){
        $( function(){
          $( ".datepicker" ).datepicker({
            autoclose: true,
          });
        } );
        $(".data_drop").click(function(){
           if ($('.dropdown-menu-list').css('display') == 'block'){
                $(".dropdown-menu-list").hide();
            }else{
                $(".dropdown-menu-list").show();
            }
        });
        $("#lets_start").click(function(){
           $(".ques_head").hide(); 
        });
      });
    </script>
</body>
</html>
<?php $__env->startSection('middle_content'); ?>
		
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Level Details
					</h3>
				</div>
				<div class="course_wrap">
					<div class="row">
						<div class="col-sm-10 offset-sm-1">
							<div class="level_detail_wrap">								
								<div class="top_content">
									<h3><?php echo e($levelDetail->title); ?></h3>
									<!-- <ul>
										<li><img src="<?php echo e(url('frontend/images/icon/arrow-right.png')); ?>">These tasks introduce new information.</li>
										<li><img src="<?php echo e(url('frontend/images/icon/arrow-right.png')); ?>">These tasks introduce new information.</li>
										<li><img src="<?php echo e(url('frontend/images/icon/arrow-right.png')); ?>">These tasks introduce new information.</li>
										<li><img src="<?php echo e(url('frontend/images/icon/arrow-right.png')); ?>">These tasks introduce new information.</li>
										<li><img src="<?php echo e(url('frontend/images/icon/arrow-right.png')); ?>">These tasks introduce new information.</li>
										<li><img src="<?php echo e(url('frontend/images/icon/arrow-right.png')); ?>">These tasks introduce new information.</li>
										<li><img src="<?php echo e(url('frontend/images/icon/arrow-right.png')); ?>">These tasks introduce new information.</li>
									</ul> -->
									<p>These tasks introduce new information – sound/symbol, sound clusters/ syllables, and words. After the instruction screen each task has a letter, syllable, or word that appears singly on the screen. The T voice says the sound, syllable or word while the student (S) sees it on the screen. S repeats the sound and T scores by touching REPEAT (for error) or NEXT (for correct) button. T may have S repeat the total task multiple times. T will have S do the task without the voice prompt to check if S can read the symbol/cluster or word without a prompt.</p>
									<div class="btn_opt text-right mt-3">
										<a href="javascript:void(0);" id="lets_start" level-id="<?php echo e($subLevelId); ?>">Let's Start</a>
									</div>
								</div>
								<input type="hidden" id="all-prev-ids" value="<?php echo e($ques->id); ?>">
          
          						<div id="question-data"></div>
								
								<div class="audio_player mt-5">
									<?php if(Auth::user()->type == 'student'): ?>
	                
					                  <audio autoplay controlsList="nodownload" loop="" id="vid" style="width: 100%;">
					                      <source src="<?php echo e(asset('/audio/AtPeace.wav')); ?>" type="audio/wav">
					                  </audio>
					                <?php else: ?>
					                  <div class="audio_player" >
					                    <audio controls autoplay controlsList="nodownload" loop="" id="vid" style="width: 100%;">
					                        <source src="<?php echo e(asset('/audio/AtPeace.wav')); ?>" type="audio/wav">
					                    </audio>
					                  </div>
					                <?php endif; ?>
				            	</div>
							</div>
						</div>						
					</div>
				</div>									
			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('student.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
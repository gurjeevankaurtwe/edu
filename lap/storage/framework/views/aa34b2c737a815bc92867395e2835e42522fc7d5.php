<?php $__env->startSection('middle_content'); ?>
		
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Trainer List
					</h3>
					<a href="<?php echo e(url('/add-teacher')); ?>"><button type="button" class="btn btn-primary">Add Trainer</button></a>
				</div>
				<div class="course_wrap">
					<div class="row">						
						<?php $__currentLoopData = $teacher; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tea): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-sm-4">
								<div class="course_card color_1">
									<div class="overlay">
										<div class="custom_btn text-right">
											<a title="Edit teacher detail" href="<?php echo e(url('/edit-teacher/'.encrypt($tea->id))); ?>" style="color:#4BA04A;"><i class="fas fa-edit"></i></a>
											<a title="View teacher detail" href="<?php echo e(url('/view-teacher-detail/'.encrypt($tea->id))); ?>" style="color:#3D83C1;"><i class="fas fa-eye"></i></a>
											<a title="View delete detail" onclick="return confirm('Are you sure you want to delete this item?');" href="<?php echo e(url('/delete-teacher/'.encrypt($tea->id))); ?>" class="pull-right"style="color:#C63531;" ><i class="fas fa-trash-alt"></i></a>
										</div>
										<h2><?php echo e(ucfirst($tea->name)); ?></h2>
										<h6>Name: <?php echo e(ucfirst($tea->name)); ?></h6>																			
										<?php if($tea->status == 1): ?>
											<a title="Click to Deactive" href="<?php echo e(url('/update-teacher-status/'.encrypt($tea->id).'/0')); ?>">Activate</a>
										<?php else: ?>
											<a title="Click to Activate" href="<?php echo e(url('/update-teacher-status/'.encrypt($tea->id).'/1')); ?>" >Deactivate</a>
										<?php endif; ?>
										<a title="Teacher's Student" href="<?php echo e(url('/students?teacherId='.encrypt($tea->id))); ?>" >Students</a>									
									</div>								
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						
					</div>
				</div>									
			</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
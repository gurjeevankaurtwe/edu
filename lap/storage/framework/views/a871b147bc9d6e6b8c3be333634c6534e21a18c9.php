<?php $__env->startSection('middle_content'); ?>
		
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Courses List
					</h3>
				</div>
				<div class="course_wrap">
					<div class="row">
						<?php $__currentLoopData = $level; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-xl-4 col-lg-6 col-md-6 col-12">
								<div class="course_card color_1">
									<div class="overlay">
										<h2><?php echo e($lev->title); ?></h2>
										<h6>Sub-Levels: <?php echo e(count($lev->levelSubLevels)); ?></h6>
										<!-- <p><?php echo e($lev->description); ?></p> -->
										<a href="<?php echo e(url('/levels_sub_levels/'.encrypt($lev->id).'?student='.encrypt($studentId))); ?>">Continue</a>
									</div>								
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
				</div>									
			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
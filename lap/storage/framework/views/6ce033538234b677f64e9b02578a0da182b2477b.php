<?php $__env->startSection('middle_content'); ?>

	<div class="main-panel">
		<div class="content-wrapper">
			<div class="page-header">
				<h3 class="page-title">
					My Profile
				</h3>
			</div>
			<div class="course_wrap">
				<div id="btnContainer" class='text-right my-3'>
					<?php if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute'): ?>
	    				<a href="<?php echo e(url('/students')); ?>"><button class="btn btn-primary">Back</button></a>
	    			<?php elseif(Auth::user()->type == 'student'): ?>
	    				<a href="<?php echo e(url('/home')); ?>"><button class="btn btn-primary">Back</button></a>
	    			<?php endif; ?>
				</div>
				<form action="<?php echo e(url('/my-profile')); ?>" method="post" autocomplete="off" enctype="multipart/form-data">
	    			<?php echo csrf_field(); ?>
	    			<input type="hidden" name="id" value="<?php echo e($userData->id); ?>">
	    			<input type="hidden" name="user_type" value="<?php echo e(Auth::user()->type); ?>">
	    			<?php if(Auth::user()->type == 'teacher'): ?>
					  	<div class="form-row">
						    <div class="form-group col-md-6">
						      	<label for="inputEmail4">First Name</label>
						      	<input type="text" class="form-control" value="<?php echo e($userData->name); ?>" name="first_name" placeholder="First Name" required>
						      	<span class="text-danger"><?php echo e($errors->first('first_name')); ?></span>
						    </div>
						    <div class="form-group col-md-6">
						      	<label for="inputEmail4">Last Name</label>
						      	<input type="text" class="form-control" value="<?php echo e($userData->last_name); ?>" name="last_name" placeholder="Last Name" required>
						      	<span class="text-danger"><?php echo e($errors->first('last_name')); ?></span>
						    </div>
						    <div class="form-group col-md-6">
						      	<label for="inputPassword4">Date of Birth</label>
						      	<input type="text" class="form-control trainer-datepicker" value="<?php echo e($userData->dob); ?>" name="dob" placeholder="Date of Birth" required>
						      	<span class="text-danger"><?php echo e($errors->first('dob')); ?></span>
						    </div>
						    <div class="form-group col-md-6">
							    <label for="inputAddress">Email</label>
							    <input type="email" class="form-control" value="<?php echo e($userData->email); ?>" name="email" placeholder="Email" required>
							    <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
						  	</div>
						  	<div class="form-group col-md-6">
							    <label for="inputAddress">Profile Image</label>
							    <?php if(Auth::user()->image != ''): ?>
							    	<input type="hidden" class="old_image" name="old_image" value="<?php echo e(asset('frontend/images/faces/'.Auth::user()->image)); ?>">
							    <?php else: ?>
							    	<input type="hidden" name="old_image" value="">
							    <?php endif; ?>
							    <input type="file" class="form-control" id="imgInp" name="profile_image">
							   	<div class="img-pre" <?php if(Auth::user()->image != ''){ echo 'style="display:block"';}else{echo 'style="display:none"';} ?>>
							    	<img id="blah" src="<?php echo e(asset('/public/frontend/images/faces/'.Auth::user()->image)); ?>" alt="your image" width="200" height="150"/>
							    </div>
							</div>
						</div>
					<?php elseif(Auth::user()->type == 'student'): ?>
						<div class="form-row">
						    <div class="form-group col-md-6">
						      	<label for="inputEmail4">Student Refrence Code</label>
						      	<input type="text" class="form-control" name="reference_code" value="<?php echo e($userDetail->reference_code); ?>" placeholder="Reference Code">
						      	<span class="text-danger"><?php echo e($errors->first('reference_code')); ?></span>
						    </div>
						    <div class="form-group col-md-6">
						      	<label for="inputPassword4">Email</label>
						      	<input type="email" class="form-control" value="<?php echo e($userData->email); ?>" name="email" placeholder="Email" required>
						      	<span class="text-danger"><?php echo e($errors->first('email')); ?></span>
						    </div>
			  			</div>
					  	<div class="form-row">
						    <div class="form-group col-md-6">
							    <label for="inputAddress">First Name</label>
							    <input type="text" class="form-control" value="<?php echo e($userData->name); ?>" name="first_name" placeholder="First Name" required>
							    <span class="text-danger"><?php echo e($errors->first('first_name')); ?></span>
						  	</div>

						  	<div class="form-group col-md-6">
							    <label for="inputAddress">Last Name</label>
							    <input type="text" class="form-control" value="<?php echo e($userData->last_name); ?>" name="last_name" placeholder="Last Name" required>
							    <span class="text-danger"><?php echo e($errors->first('last_name')); ?></span>
						  	</div>
						    
					  	</div>
					  	<div class="form-row">
					  		<div class="form-group col-md-6">
							    <label for="inputAddress2">Date of Birth</label>
							    <input type="text" class="form-control datepicker" value="<?php echo e($userData->dob); ?>" name="dob" placeholder="Date of Birth" required>
							    <span class="text-danger"><?php echo e($errors->first('dob')); ?></span>
						  	</div>
						  	<div class="form-group col-md-6">
						      	<label for="inputCity">Grade or N/A</label>
						      	<input type="text" class="form-control" name="grade" value="<?php echo e($userDetail->grade); ?>" placeholder="Grade or N/A" required>
						      	<span class="text-danger"><?php echo e($errors->first('grade')); ?></span>
						    </div>
						  	
						</div>
						<div class="form-row">
						    
						    <div class="form-group col-md-6">
						      	<label for="inputState">Gender</label>
						      	<select name="gender" class="form-control" name="gender" required> 
							        <option value="">Select Gender...</option>
							        <option value="Female" <?php if($userDetail->gender == 'Female'){ echo 'selected="selected"';}?>>Female</option>
							        <option value="male" <?php if($userDetail->gender == 'male'){ echo 'selected="selected"';}?>>Male</option>
						      	</select>
						      	<span class="text-danger"><?php echo e($errors->first('gender')); ?></span>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="inputZip">Setup Timing</label>
						      <input type="text" class="form-control" name="timing" value="<?php echo e($userDetail->timings); ?>" placeholder="Timing">
						      <span class="text-danger"><?php echo e($errors->first('timing')); ?></span>
						    </div>
					  	</div>
					  	<div class="form-group col-md-6">
							    <label for="inputAddress">Profile Image</label>
							    <?php if(Auth::user()->image != ''): ?>
							    	<input type="hidden" class="old_image" name="old_image" value="<?php echo e(asset('frontend/images/faces/'.Auth::user()->image)); ?>">
							    <?php else: ?>
							    	<input type="hidden" name="old_image" value="">
							    <?php endif; ?>
							    <input type="file" class="form-control" id="imgInp" name="profile_image">
							   	<div class="img-pre" <?php if(Auth::user()->image != ''){ echo 'style="display:block"';}else{echo 'style="display:none"';} ?>>
							    	<img id="blah" src="<?php echo e(asset('/public/frontend/images/faces/'.Auth::user()->image)); ?>" alt="your image" width="200" height="150"/>
							    </div>
							</div>
					<?php elseif(Auth::user()->type == 'Institute'): ?>
					  	<div class="form-row">
						    <div class="form-group col-md-6">
						      	<label for="inputEmail4">Institute Name</label>
						      	<input type="text" class="form-control" value="<?php echo e($userData->name); ?>" name="name" placeholder="Institute Name" required>
						      	<span class="text-danger"><?php echo e($errors->first('name')); ?></span>
						    </div>
						    <div class="form-group col-md-6">
							    <label for="inputAddress">Email</label>
							    <input type="email" class="form-control" value="<?php echo e($userData->email); ?>" name="email" placeholder="Email" required>
							    <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
						  	</div>
						</div>
						<div class="form-group col-md-6">
						    <label for="inputAddress">Profile Image</label>
						    <?php if(Auth::user()->image != ''): ?>
						    	<input type="hidden" class="old_image" name="old_image" value="<?php echo e(asset('frontend/images/faces/'.Auth::user()->image)); ?>">
						    <?php else: ?>
						    	<input type="hidden" name="old_image" value="">
						    <?php endif; ?>
						    <input type="file" class="form-control" id="imgInp" name="profile_image">
						   	<div class="img-pre" <?php if(Auth::user()->image != ''){ echo 'style="display:block"';}else{echo 'style="display:none"';} ?>>
						    	<img id="blah" src="<?php echo e(asset('/public/frontend/images/faces/'.Auth::user()->image)); ?>" alt="your image" width="200" height="150"/>
						    </div>
						</div>
					<?php endif; ?>
		  			<div class="text-center my-3">
		  				<button type="submit" class="btn btn-primary">Update</button>
		  			</div>
				</form>
			</div>									
		</div>
	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
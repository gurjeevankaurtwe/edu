<!DOCTYPE html>
<html class="bootstrap-layout">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Edu Theraputics  <?php echo $__env->yieldContent('title'); ?></title>

  <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
  <meta name="robots" content="noindex">

  <!-- Material Design Icons  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Roboto Web Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- App CSS -->
  <link type="text/css" href="<?php echo e(asset('/frontend/assets/css/style.min.css')); ?>" rel="stylesheet">
  <link type="text/css" href="<?php echo e(asset('/frontend/assets/css/main.css')); ?>" rel="stylesheet">

</head>

	<body>

    <?php echo $__env->yieldContent("middle_content"); ?>

    <script src="<?php echo e(asset('/frontend/assets/vendor/jquery.min.js')); ?>"></script>

    <script src="<?php echo e(asset('/frontend/assets/vendor/tether.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/frontend/assets/vendor/bootstrap.min.js')); ?>"></script>

    <script src="<?php echo e(asset('/frontend/assets/vendor/adminplus.js')); ?>"></script>

    <script src="<?php echo e(asset('/frontend/assets/js/main.min.js')); ?>"></script>

</body>

</html>
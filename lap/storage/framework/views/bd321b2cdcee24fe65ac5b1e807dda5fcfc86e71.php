<?php $__env->startSection('middle_content'); ?>


		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">

					<?php if($levelDetail != ''): ?>
						<h3 class="page-title">
							<?php echo e($subLevels->title); ?>

						</h3>
					<?php else: ?>
						<img src="<?php echo e(url('/public/frontend/images/coming-soon.png')); ?>" style="display:table;margin:0 auto;margin-top:50px;">
					<?php endif; ?>
				</div>
				
				<div class="course_wrap">
					<?php if($levelDetail != ''): ?>
					<input type="hidden" class="studentId" value="<?php echo e($studentId); ?>">
					<div class="row">
						<div class="col-sm-10 offset-sm-1">
							<div class="level_detail_wrap">								
								<div class="top_content">
									<div class="row">
										<div class="col-xl-8 col-lg-12 order-2 order-xl-1">
											<div class="content_bx">
												<h3>
													<?php echo e($subLevels->title); ?>


												</h3>
									
												<p><?php echo $subLevels->description; ?></p>
												<?php if($subLevels->id == '42' || $subLevels->id == '43' || $subLevels->id == '44' || $subLevels->id == '52' || $subLevels->id == '50' || $subLevels->id == '55' || $subLevels->id == '56' || $subLevels->id == '62' || $subLevels->id == '63' || $subLevels->id == '14' || $subLevels->id == '15' || $subLevels->id == '22' || $subLevels->id == '28' || $subLevels->id == '29' || $subLevels->id == '34' || $subLevels->id == '36' || $subLevels->id == '37'): ?>
													<div class="btn_opt mt-4 met-btn">
														<a href="javascript:void(0);" data-attr="metronome" class="rythm-task" level-id="<?php echo e($subLevelId); ?>">Metronome beat</a>
									                    <a href="javascript:void(0);" data-attr="pulse" class="rythm-task" level-id="<?php echo e($subLevelId); ?>">Pulse-light-up</a>
									                    <a href="javascript:void(0);" data-attr="off" class="rythm-task" level-id="<?php echo e($subLevelId); ?>">Off</a>
													</div>
									                <div class="btn_opt mt-3 startbtn" style="display:none">
									                	<?php if($subLevelId != 64): ?>
															<input type="hidden" class="sub_level_ids" value="<?php echo e($ques->session_id); ?>">
														<?php else: ?>
															<input type="hidden" class="sub_level_ids" value="<?php echo e($levelDetail->id); ?>">
														<?php endif; ?>
														<a href="javascript:void(0);" id="lets_start_rth" level-id="<?php echo e($subLevelId); ?>">Let's Go</a>
														<a href="javascript:void(0);" id="lets_cancel">Cancel</a>
													</div>
												<?php else: ?>
													<div class="btn_opt mt-3">
														<?php if($subLevelId != 64): ?>
															<input type="hidden" class="sub_level_ids" value="<?php echo e($ques->session_id); ?>">
														<?php else: ?>
															<input type="hidden" class="sub_level_ids" value="<?php echo e($levelDetail->id); ?>">
														<?php endif; ?>
														<a href="javascript:void(0);" id="lets_start" level-id="<?php echo e($subLevelId); ?>">Let's Go</a>
													</div>
												<?php endif; ?>

												
											</div>
										</div>
										<div class="col-xl-4 col-lg-12 order-1 order-xl-2">
											<div class="imgbx">
												<img src="<?php echo e(url('/public/frontend/images/others/styudy-boy.jpg')); ?>">
											</div>
										</div>
									</div>
									
									
								</div>
								
          
          						<div id="question-data"></div>
								
								<div class="audio_player mt-5">
									<?php if(Auth::user()->type == 'student'): ?>
	                
					                  <audio autoplay controlsList="nodownload" loop="" id="vid" style="width: 100%;">
					                      <source src="<?php echo e(url('/public/audio/'.$subLevels->audio)); ?>" type="audio/wav">
					                  </audio>
					                <?php else: ?>
					                  <div class="audio_player" >
					                    <a href="javascript:void(0)" id="stopAudio"><audio controls autoplay controlsList="nodownload" loop="" id="vid" style="width: 100%;">
					                        <source src="<?php echo e(url('/public/audio/'.$subLevels->audio)); ?>" type="audio/wav">
					                    </audio>
					                    </a>
					                  </div>
					                <?php endif; ?>
				            	</div>
							</div>
						</div>						
					</div>
					<?php endif; ?>
				</div>									
			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
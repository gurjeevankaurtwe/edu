<style>
    .rating {
      float:left;
    }
	.rating:not(:checked) > input {
        position:absolute;
        top:-9999px;
        clip:rect(0,0,0,0);
    }
	.rating:not(:checked) > label {
        float:right;
        width:1em;
        / padding:0 .1em; /
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:300%;
        / line-height:1.2; /
        color:#ddd;
    }
	.rating:not(:checked) > label:before {
        content: '★ ';
    }
	.rating > input:checked ~ label {
        color: dodgerblue;
        
    }
	.rating:not(:checked) > label:hover,
    .rating:not(:checked) > label:hover ~ label {
        color: dodgerblue;
    }
	.rating > input:checked + label:hover,
    .rating > input:checked + label:hover ~ label,
    .rating > input:checked ~ label:hover,
    .rating > input:checked  label:hover  label,
    .rating > label:hover  input:checked  label {
        color: dodgerblue;
    }
	.rating > label:active {
        position:relative;
        top:2px;
        left:2px;
    }
</style>
<div class="progress details_progressbar">
    <div class="progress-bar" style="width:<?php echo e($totalWidth); ?>%" aria-valuemax="100"></div>
</div>
<?php 
    $strLen = strlen($allPrevVal);
    if($strLen == 1)
    {
        $display = 'style="display:none"';
    }
    else
    {
        $display = 'style="display:block"';
    }
?>
<input type="hidden" id="all-prev-ids" value="<?php echo e($allPrevVal); ?>">

<div class="middle_content <?php if($status == 1){ if($subLevelId == 1){ echo 'middle_content2';} } ?>">
    <div class="ques_box">
    	<?php if($ques != ''): ?>
            <div class="ques_panel">
                <div class="overlay"></div>
                <?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
	        		<div class="with-flash">
	        			<div class="ans">
				            <div class="single">
				                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
				                <p class="start-fade-out" style="letter-spacing: 3px;">
				                	<?php echo e($ques->option1); ?>

				                </p>
				                </a>
				            </div>
				        </div>
				    </div>
				    <div class="player btn_opt text-center my-4  go-start-button" style="display:none">
					    <a href="javascript:void(0);" class="get-question-detail">Start</a>
					</div>
	        	<?php endif; ?>
                <?php if($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='48'): ?>
                    <div class="with-flash">
                        <?php
                            if($subLevelId=='48')
                            {
                                $ans = $ques->option1;
                            }
                            else
                            {
                                $answer = $ques->answer;
                                if($answer == 'option1')
                                {
                                    $ans = $ques->option1;
                                }
                                else if($answer == 'option2')
                                {
                                    $ans = $ques->option2;
                                }
                                else if($answer == 'option3')
                                {
                                    $ans = $ques->option3;
                                }
                            }
                        ?>
                        <div class="ans">
                            <div class="single">
                                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                                <p class="fade-out" style="letter-spacing: 3px;"><?php echo e(strtolower($ans)); ?></p>
                                </a>
                            </div>
                        </div>
                        <?php if($subLevelId=='26' || $subLevelId=='24' || $subLevelId=='48'): ?>
                            <div class="player btn_opt text-center my-4  go-button" style="display:none">
                                <a href="javascript:void(0);" class="repeat-flash-word">Repeat</a>
                                <?php if($subLevelId=='48'): ?>
                                    <a href="javascript:void(0);" class="next_three_blanks">Next</a>
                                <?php else: ?>
                                    <a href="javascript:void(0);" class="get-question-detail">Next</a>
                                <?php endif; ?>
                            </div>
                        <?php else: ?>
                            <div class="btn_opt text-center my-4 player go-button" style="display:none">
                            <!--<button type="button" class="btn btn-success get-question-detail">Go</button>-->
                            <a href="javascript:void(0);" class="get-question-detail">Go</a>
                        </div>   
                        <?php endif; ?>
                    </div>
                <?php elseif($subLevelId=='12' || $subLevelId=='31' || $subLevelId=='33' || $subLevelId=='53'): ?>
                <div class="with-flash">
                    <div class="ans">
                        <div class="single">
                            <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
	                            <p class="fade-out" style="letter-spacing: 3px;">
	                                <?php if($subLevelId=='53' ||  $subLevelId=='31'): ?>
	                                    <?php echo e($ques->option1); ?>

                                    <?php elseif($subLevelId=='12'): ?>
                                        <?php echo e(ucfirst($ques->option1)); ?>

                                    <?php elseif($subLevelId=='33'): ?>
                                        <?php echo e(ucfirst($ques->option1)); ?>

	                                <?php else: ?>
	                                    <?php echo e(strtolower($ques->option1)); ?>

	                                <?php endif; ?>
	                            </p>
                            </a>
                        </div>
                    </div>
                    <div class="btn_opt text-center my-4 player go-button" style="display:none">
                        <a href="javascript:void(0);" class="repeat-flash-word">Repeat</a>
                    </div>   
                </div>
                <?php elseif($subLevelId=='26' || $subLevelId=='24'): ?>
                <div class="with-flash-btn">
                    <div class="btn_opt text-center my-4 player go-button">
                        <a href="javascript:void(0);" class="get-flash-detail">Start</a>
                    </div>  
                </div>
                <div class="with-flash" style="display:none">
                    <?php
                        $answer = $ques->answer;
                        if($subLevelId=='26')
                        {
                            if($answer == 'option1')
                            {
                                $ans = $ques->option1;
                            }
                            else if($answer == 'option2')
                            {
                                $ans = $ques->option2;
                            }
                            else if($answer == 'option3')
                            {
                                $ans = $ques->option3;
                            }
                        }
                        else
                        {
                            $ans = $ques->option1;
                        }
                    ?>
                    <div class="ans">
                        <div class="single">
                            <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                            <p class="fade-out-flash" style="letter-spacing: 3px;">
                                <?php echo e(strtolower($ans)); ?>

                            </p>
                            </a>
                        </div>
                    </div>
                   
                    <div class="player btn_opt text-center my-4  go-button" style="display:none">
                        <a href="javascript:void(0);" class="repeat-flash-word">Repeat</a>
                        <a href="javascript:void(0);" class="get-question-detail">Next</a>
                    </div>
                </div>
                <?php elseif($subLevelId=='21'): ?>
                <div class="with-flash">
                    <div class="ans">
                        <div class="single">
                            <a href="javascript:void(0)" class="thumbnail" style="Text-Decoration: None !important; ">
                            <p class="cluster_word" style="letter-spacing: 3px;">
                                <?php echo e($ques->answer); ?>

                            </p>
                            <p class="cluster_second_word" style="letter-spacing: 3px;display:none;">
                                <?php echo e($ques->answer); ?>

                            </p>
                            </a>
                        </div>
                    </div>
                       
                </div>
                <?php elseif($subLevelId=='19' || $subLevelId=='32' || $subLevelId=='38' || $subLevelId=='54'): ?>
                    <div class="with-flash">
                        <div class="btn_opt text-center my-4 player go-button">
                            <?php if($subLevelId=='54'): ?>
                                <a href="javascript:void(0);" class="get-question-detail">Next</a>
                            <?php else: ?>
                                <a href="javascript:void(0);" class="get-question-detail">Start</a>
                            <?php endif; ?>
                        </div>  
                    </div>
                <?php elseif($subLevelId=='25'): ?>
                    <div class="without-flash">
                        <div class="ans multi_word mb-4">
                            <div class="single single2">
                                <?php
                                    $explode_ans = explode(" ",$ques->option1);
                                    $startVal = key($explode_ans);
                                    $lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
                                    for($i=$startVal; $i<=$lastVal;$i++){
                                ?>
                                        <a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail level_two" style="Text-Decoration: None !important; ">
                                            <?php if($i != 0): ?>
                                                <p><?php echo e(' '.$explode_ans[$i]); ?></p>
                                            <?php else: ?>
                                                <p><?php echo e($explode_ans[$i]); ?></p>
                                            <?php endif; ?>
                                        </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="middle_foot d-flex justify-content-center align-items-center">
                            <a href="javascript:void(0);" class="repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>" <?php if($subLevelId == 1): ?> <?php endif; ?>><i class="fas fa-redo-alt"></i> Repeat</a>
                        </div>  
                    </div>
                <?php endif; ?>
                 <?php if($subLevelId=='42' || $subLevelId=='43' || $subLevelId=='44' || $subLevelId=='52' || $subLevelId == '50' || $subLevelId == '55' || $subLevelId == '56' || $subLevelId == '62' || $subLevelId == '63' || $subLevelId == '14' || $subLevelId == '15' || $subLevelId == '22' || $subLevelId == '28' || $subLevelId == '29' || $subLevelId == '34' || $subLevelId == '36' || $subLevelId == '37' || $subLevelId == '21'): ?>

                <!-- <div class="player btn_opt text-center my-4  go-button rthym-btn">
                    <a href="javascript:void(0);" data-attr="metronome" class="rythm-task">Metronome beat</a>
                    <a href="javascript:void(0);" data-attr="pulse" class="rythm-task">Pulse-light-up</a>
                    <a href="javascript:void(0);" data-attr="off" class="rythm-task">Off</a>
                </div> -->

                <div class="rhythm-task-option">
                    <input type="hidden" class="rythm-task-type">
                    <audio src="<?php echo e(url('2SEC.mp3')); ?>" id="my_metronome" loop="loop"></audio> 

                    <?php if($ques->option1 != ''): ?>
                        <div class="ans">
                            <div class="single single2">
                                <?php
                                    $explode_ans = explode(" ",$ques->option1);
                                    $startVal = key($explode_ans);
                                    $lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
                                    for($i=$startVal; $i<=$lastVal;$i++){
                                ?>
                                        <a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
                                            <?php if($i != 0): ?>
                                                <p class="rythm_task_pulse" style="font-weight:100;font-size: 55px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
                                            <?php else: ?>
                                                <p class="rythm_task_pulse" style="font-weight:100;font-size: 55px;"><?php echo e($explode_ans[$i]); ?></p>
                                            <?php endif; ?>
                                        </a>
                                    
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($ques->option2 != ''): ?>
                        <div class="ans">
                            <div class="single single2">
                                <?php
                                    $explode_ans = explode(" ",$ques->option2);
                                    $startVal = key($explode_ans);
                                    $lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
                                    for($i=$startVal; $i<=$lastVal;$i++){
                                ?>
                                        <a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
                                            <?php if($i != 0): ?>
                                                <p class="rythm_task_pulse" style="font-weight:100;font-size: 55px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
                                            <?php else: ?>
                                                <p class="rythm_task_pulse" style="font-weight:100;font-size: 55px;"><?php echo e($explode_ans[$i]); ?></p>
                                            <?php endif; ?>
                                        </a>
                                    
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($ques->option3 != ''): ?>
                        <div class="ans">
                            <div class="single single2">
                                <?php
                                    $explode_ans = explode(" ",$ques->option3);
                                    $startVal = key($explode_ans);
                                    $lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
                                    for($i=$startVal; $i<=$lastVal;$i++){
                                ?>
                                        <a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
                                            <?php if($i != 0): ?>
                                                <p class="rythm_task_pulse" style="font-weight:100;font-size: 55px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
                                            <?php else: ?>
                                                <p class="rythm_task_pulse" style="font-weight:100;font-size: 55px;"><?php echo e($explode_ans[$i]); ?></p>
                                            <?php endif; ?>
                                        </a>
                                    
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($subLevelId=='21'): ?>
                        <div class="middle_foot d-flex justify-content-center align-items-center">
                            <a href="javascript:void(0);" class="repeat_cluster_word" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
                        </div>
                    <?php endif; ?>
                </div>
                
            <?php endif; ?>
                <div class="without-flash" <?php if(($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='43' || $subLevelId=='44' || $subLevelId=='42' || $subLevelId=='12' || $subLevelId=='26' || $subLevelId=='31' || $subLevelId=='33' || $subLevelId == '50' || $subLevelId=='53' || $subLevelId=='52' | $subLevelId=='55' | $subLevelId=='56' || $subLevelId == '62' || $subLevelId == '63' || $subLevelId == '14' || $subLevelId == '15' || $subLevelId == '22' || $subLevelId == '28' || $subLevelId == '29' || $subLevelId == '34' || $subLevelId == '36' || $subLevelId == '37' || $subLevelId == '19' || $subLevelId == '25' || $subLevelId == '32' || $subLevelId=='38' || $subLevelId=='24' || $subLevelId=='54' || $subLevelId=='48' || $subLevelId=='21') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?> style="display:none;" <?php endif; ?>>
                    <?php if($subLevelId=='59' || $subLevelId=='60' || $subLevelId=='61'): ?>
                        <div class="ans">
                            <div class="single">
                                <?php if($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
                                    <p style="color:black"><?php echo e($ques->question); ?></p>
                                <?php else: ?>
                                    <p style="color:black"><?php echo e(strtolower($ques->question)); ?></p>
                                <?php endif; ?>
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='20' || $subLevelId=='30' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?>
                        <?php
                        	if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))
		                	{
		                		$explodeblank = explode("_",$ques->option2);
			                    if($explodeblank[0] == '')
			                    {
			                        $whole_word = '<span class="blank_space_val">__</span>'.$explodeblank[1];
			                    }
			                    elseif($explodeblank[1] == '')
			                    {
			                        $whole_word = $explodeblank[0].'<span class="blank_space_val">__</span>';
			                    }
			                    elseif($explodeblank[0] != '' && $explodeblank[1] != '')
			                    {
			                        $whole_word = $explodeblank[0].'<span class="blank_space_val">__</span>'.$explodeblank[1];
			                    }
		                	}
                            elseif($subLevelId=='49' || $subLevelId=='48' || $subLevelId=='23')
                            {
                                $whole_word = '<span class="blank_space_val_first">__</span>'.' '.'<span class="blank_space_val_two">__</span>'.' '.'<span class="blank_space_val_third">__</span>';
                            }
                            else
                            {
                                if($subLevelId=='16')
                                {
                                    $explodeWord = explode(" ",$ques->option1);

                                    if(isset($explodeWord[0]) && isset($explodeWord[1]) && isset($explodeWord[2]) && !isset($explodeWord[3]))
                                    {
                                        if($explodeWord[0] != '_' && $explodeWord[1] == '_' && $explodeWord[2] == '_')
                                        {
                                            $whole_word = $explodeWord[0].'<span class="blank_space_val_first"> __</span><span class="blank_space_val_two"> __</span>';
                                        }
                                        elseif($explodeWord[0] == '_' && $explodeWord[1] == '_' && $explodeWord[2] != '_')
                                        {
                                            $whole_word = '<span class="blank_space_val_first"> __</span><span class="blank_space_val_two"> __ </span>'.$explodeWord[2];
                                        }
                                        elseif($explodeWord[0] != '_' && $explodeWord[1] == '_' && $explodeWord[2] != '_')
                                        {
                                            $whole_word = $explodeWord[0].'<span class="blank_space_val_first"> __ </span>'.$explodeWord[2];
                                        }
                                    }
                                    elseif(isset($explodeWord[0]) && isset($explodeWord[1]) && isset($explodeWord[2]) && isset($explodeWord[3]))
                                    {
                                        $whole_word = $explodeWord[0].'<span class="blank_space_val_first"> __</span><span class="blank_space_val_two"> __</span><span class="blank_space_val_third"> __</span>';
                                    }
                                    else
                                    {

                                        if($explodeWord[0] == '_' && $explodeWord[1] != '_')
                                        {
                                            $whole_word = '<span class="blank_space_val_first"> __ </span>'.$explodeWord[1];
                                        }
                                        
                                        elseif($explodeWord[0] != '_' && $explodeWord[1] == '_')
                                        {
                                            $whole_word = $explodeWord[0].'<span class="blank_space_val_first"> __</span>';
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    $explodeblank = explode("_",$ques->option1);
                                    if($explodeblank[0] == '')
                                    {
                                        $whole_word = '<span class="blank_space_val">__</span>'.$explodeblank[1];
                                    }
                                    elseif($explodeblank[1] == '')
                                    {
                                        $whole_word = $explodeblank[0].'<span class="blank_space_val">__</span>';
                                    }
                                    elseif($explodeblank[0] != '' && $explodeblank[1] != '')
                                    {
                                        $whole_word = $explodeblank[0].'<span class="blank_space_val">__</span>'.$explodeblank[1];
                                    }
                                }
                            }
                        ?>
                        <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40'): ?>
                            <div class="with-flash">
                                <div class="btn_opt text-center my-4 player go-button">
                                    <a href="javascript:void(0);" class="start-fill-blanks">Start</a>
                                </div>  
                            </div>
                        <?php endif; ?>
                        <div class="ans" <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40'): ?> style="display:none" <?php endif; ?>>
                            <div class="single">
                                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                                <p><?php echo ucfirst($whole_word);?></p>
                                </a>
                            </div>
                        </div>
                    <?php elseif($subLevelId=='24'): ?>
                        <div class="ans multi_word mb-4">
                            <div class="single">
                                <a href="javascript:void(0)" style="Text-Decoration: None !important; ">
                                <p><?php echo '<span class="blank_space_val">__</span>'.$ques->option1;?></p>
                                </a>
                            </div><br>
                            <div class="single single2">
                                <?php
                                    $explode_ans = explode(" ",$ques->answer);
                                    $startVal = key($explode_ans);
                                    $lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
                                    for($i=$startVal; $i<=$lastVal;$i++){
                                ?>
                                        <a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="update_blank_space" style="Text-Decoration: None !important;">
                                            <?php if($i != 0): ?>
                                                <p style="font-size:80px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
                                            <?php else: ?>
                                                <p style="font-size:80px;"><?php echo e($explode_ans[$i]); ?></p>
                                            <?php endif; ?>
                                       </a>
                                    
                                <?php } ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="ans">
                            <div class="single">
                                <?php if($subLevelId=='18'): ?>
                                    <a href="javascript:void(0)" class="highlight_first" data-attr="0" style="Text-Decoration: None !important; ">
                                   
                                        <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
                                            <?php echo e(strtolower($ques->option1)); ?>

                                        </p>
                                       

                                    </a>
                                <?php else: ?>
                                    <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                                        <?php if($subLevelId == 1 && $ques->session_id != 0): ?>
                                            <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:110px">
                                                <?php echo e(strtolower($ques->option1)); ?>

                                            </p>
                                        <?php elseif(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
    						                <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:110px">
    						                	<?php echo e(strtolower($ques->option1)); ?>

    						                </p>
                                        <?php else: ?>
                                            <?php if($subLevelId == 51): ?>
                                                <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
                                                    <?php echo e($ques->option1); ?>

                                                </p>
                                            <?php elseif($subLevelId == 54): ?>
                                                <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
                                                    <?php echo e($ques->option1); ?>

                                                </p>
                                            <?php elseif($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
                                                <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:38px;">
                                                    <?php echo e($ques->option1); ?>

                                                </p>
                                            <?php else: ?>
                                                <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
                                                    <?php echo e(strtolower($ques->option1)); ?>

                                                </p>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($ques->session_id != 0): ?>
                        <div class="ans">
                            <div class="single">
                                <?php if($subLevelId=='18'): ?>
                                    <a href="javascript:void(0)" class="highlight_first" data-attr="0" style="Text-Decoration: None !important; ">
                                   
                                        <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
                                            <?php echo e(strtolower($ques->option2)); ?>

                                        </p>
                                       

                                    </a>
                                <?php else: ?>
                                    <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                                        <?php if($subLevelId == 54): ?>
                                            <p style="letter-spacing: 3px;"><?php echo e($ques->option2); ?></p>
                                        <?php elseif($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
                                            <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:38px;">
                                                <?php echo e($ques->option2); ?>

                                            </p>
                                        <?php else: ?>
                                            <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option2)); ?></p>
                                        <?php endif; ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="ans">
                        <div class="single">
                            <?php if($subLevelId=='18'): ?>
                                <a href="javascript:void(0)" class="highlight_first" data-attr="0" style="Text-Decoration: None !important; ">
                               
                                    <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
                                        <?php echo e(strtolower($ques->option3)); ?>

                                    </p>
                                   

                                </a>
                            <?php else: ?>
                                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                                <?php if($subLevelId == 54): ?>
                                    <p style="letter-spacing: 3px;"><?php echo e($ques->option3); ?></p>
                                <?php elseif($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
                                    <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:38px;">
                                        <?php echo e($ques->option3); ?>

                                    </p>
                                <?php else: ?>
                                    <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option3)); ?></p>
                                <?php endif; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="ans">
                        <div class="single">
                            <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                            <?php if($subLevelId == 54): ?>
                                <p style="letter-spacing: 3px;"><?php echo e($ques->option4); ?></p>
                            <?php elseif($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
                                <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:38px;">
                                    <?php echo e($ques->option4); ?>

                                </p>
                            <?php else: ?>
                                <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option4)); ?></p>
                            <?php endif; ?>
                            </a>
                        </div>
                    </div>
                    <div class="ans">
                    <div class="single">
                        <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                        <?php if($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
                            <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:38px;">
                                <?php echo e($ques->option5); ?>

                            </p>
                        <?php else: ?>
                            <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option5)); ?></p>
                        <?php endif; ?>
                        </a>
                    </div>
                </div>
                <div class="ans">
                    <div class="single">
                        <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                        <?php if($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
                            <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:38px;">
                                <?php echo e($ques->option6); ?>

                            </p>
                        <?php else: ?>
                            <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option6)); ?></p>
                        <?php endif; ?>
                        </a>
                    </div>
                </div>
                    <?php if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='20'  || $subLevelId=='30' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?>
                        <div class="ans multi_word mb-4" <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='48'): ?> style="display:none" <?php endif; ?>>
                        <div class="single single2">
                                <?php
                                    if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='48' || $subLevelId=='16'))
                                    {
                                        $explode_ans = explode(",",$ques->answer);
                                    }
                                    elseif(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))
                                    {
                                        $explode_ans = explode(",",$ques->answer);
                                    }
                                    else
                                    {
                                        $explode_ans = explode(" ",$ques->answer);
                                    }
                                    $startVal = key($explode_ans);
                                    $lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
                                    for($i=$startVal; $i<=$lastVal;$i++){
                                ?>
                                <?php if($subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23'): ?>
                                    <a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="update_three_space" style="Text-Decoration: None !important; ">
                                <?php else: ?>
                                    <a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="update_blank_space" style="Text-Decoration: None !important; ">
                                <?php endif; ?>
                                        <?php if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='48' || $subLevelId=='16')): ?>
                                    
                                            <?php if($i != 0): ?>
                                                <p><?php echo e(','.$explode_ans[$i]); ?></p>
                                            <?php else: ?>
                                                <p><?php echo e($explode_ans[$i]); ?></p>
                                            <?php endif; ?>
                                        <?php else: ?>
                                           
                                            <?php if($i != 0): ?>
                                                <p><?php echo e(' '.$explode_ans[$i]); ?></p>
                                            <?php else: ?>
                                                <p><?php echo e($explode_ans[$i]); ?></p>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </a>
                                    
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
						<div class="middle_foot d-flex justify-content-center align-items-center">
							<a href="javascript:void(0);" class="repeat_blank_space" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
						</div>
			        <?php elseif($subLevelId!='1'): ?>
                        <div class="player">
                            <?php if($ques->answer_record != ''): ?>

                                <!-- Repeat with audio -->

                                <div class="middle_foot d-flex justify-content-center align-items-center">
                                    <a href="javascript:void(0);" class="repeat_audio_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>

                                    <input type="hidden" class="<?php echo 'repeat_'.$ques->id;?>" value="<?php echo e(asset('/answer_record/'.$ques->answer_record)); ?>">

                                    <audio class="repeat_audio ml-3" id="<?php echo 'repeat_'.$ques->id;?>"  controls>
                                        <source src="<?php echo e(asset('/answer_record/'.$ques->answer_record)); ?>" type="audio/mp3">
                                    </audio>
                                </div>
        					<?php else: ?>
                                <?php if($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='20' || $subLevelId=='30' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23' || $subLevelId=='24'): ?>

                                    <!-- Repeat -->

                                    <div class="middle_foot d-flex justify-content-center align-items-center" <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40'): ?> style="display:none !important" <?php endif; ?>>
                                        <?php if($subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23'): ?>
                                            <!-- <?php if($subLevelId=='49'): ?>
                                                <?php if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute' ): ?>
                                                    <p class="three_blanks sit_blank" style="letter-spacing: 3px;">
                                                        <?php echo e($ques->option1); ?>

                                                    </p>
                                                <?php endif; ?>
                                            <?php endif; ?> -->

                                            <?php if($subLevelId=='23'): ?>
                                                <a href="javascript:void(0);" class="repeat_three_space <?php if($subLevelId=='49'): ?> ml-auto <?php endif; ?>" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Next</a>
                                            <?php else: ?>
                                                <a href="javascript:void(0);" class="repeat_three_space <?php if($subLevelId=='49'): ?> ml-auto <?php endif; ?>" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <a href="javascript:void(0);" class="repeat_blank_space" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
                                        <?php endif; ?>
                                    </div>
                                <?php elseif($subLevelId!='17' && $subLevelId!='45' && $subLevelId!='57' && $subLevelId!='13' && $subLevelId!='20' && $subLevelId!='30' && $subLevelId!='35' && $subLevelId!='39' && $subLevelId!='40' && $subLevelId!='49' && $subLevelId!='48' && $subLevelId!='16' && $subLevelId!='24'): ?>

                                    <!-- Repeat -->

                                    <div class="middle_foot d-flex justify-content-center align-items-center">
                                        <a href="javascript:void(0);" class="repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
                                    </div>
                                <?php endif; ?>

                            <?php endif; ?>
                        </div>					
                    <?php else: ?>
                        <?php if($subLevelId!='17'): ?>

                            <!-- Repeat -->

                            <div class="middle_foot d-flex justify-content-center align-items-center">
                                <a href="javascript:void(0);" class="repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>" <?php if($subLevelId == 1): ?> <?php endif; ?>><i class="fas fa-redo-alt"></i> Repeat</a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php else: ?>
            <div class="msg ques_panel">
                <h3 class="text-center" style="color:black;">Congrats! Your level has been completed.</h3>
            </div>  
           
            
        <?php endif; ?>
    </div>
</div>



<!-- Next Previous -->

<?php if($ques != ''): ?>
    <?php
        if($allPrevVal == '')
        {
            $styleDis = "style=display:none";
        }
        else
        {
            $styleDis = "";
        }
    ?>
    <div class="bottom_btn d-md-flex justify-content-md-between ques_btn next-prev-btn" <?php if(($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='12' || $subLevelId=='31' || $subLevelId=='26' || $subLevelId=='33' || $subLevelId=='53' || $subLevelId=='19' || $subLevelId=='32' || $subLevelId=='38' || $subLevelId == '35' || $subLevelId=='38' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='24' || $subLevelId=='54' || $subLevelId=='48') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?> style="display:none !important;" <?php endif; ?>>
        <?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>

            <a href="javascript:void(0);" class="prev" id="prev-question-level-func" data-attr="<?php echo e($ques->session_id); ?>" sub-lev='<?php echo e($ques->level_sub_level_id); ?>' prev='<?php echo e($prevqueId); ?>' <?php echo e($styleDis); ?>>Previous</a>

        <?php else: ?>
             <a href="javascript:void(0);" class="prev" id="prev-question-level-func" sub-lev='<?php echo e($ques->sub_level_id); ?>' prev='<?php echo e($prevqueId); ?>' data-attr="<?php echo e($ques->session_id); ?>" <?php echo e($styleDis); ?>>Previous</a>
        <?php endif; ?>
       
        <!-- <a href="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>">Complete Later</a> -->
        <?php if($ques->session_id == 0): ?>
			<a href="<?php echo e(url('sublevel_sublevels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>">Complete Later</a>
		<?php else: ?>
			<a href="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>">Complete Later</a>
		<?php endif; ?>
        <?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>

            <a href="javascript:void(0);" id="next-question-level" data-attr="<?php echo e($ques->session_id); ?>" sub-lev='<?php echo e($ques->level_sub_level_id); ?>' cur='<?php echo e($ques->id); ?>'>Next</a>

        <?php else: ?>
            <a href="javascript:void(0);" id="next-question-level" data-attr="<?php echo e($ques->session_id); ?>" sub-lev='<?php echo e($ques->sub_level_id); ?>' cur='<?php echo e($ques->id); ?>'>Next</a>
        <?php endif; ?>
    </div>
<?php else: ?>
    <div class="bottom_btn d-flex ques_btn">
        <a href="<?php echo e(url('level_questions_/'.encrypt($subLevelId).'?student='.encrypt($studentId))); ?>">Redo</a>
        <?php if(Auth::user()->guest_status == 0): ?>
            <a href="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>" id="next-question-level2" class="ml-auto">Done</a>
        <?php else: ?>
            <a href="javascript:void(0)" id="guest_user_feedback" class="ml-auto">Done</a>
        <?php endif; ?>
        <?php 
            if(Auth::user()->type == 'teacher')
            {
                $compSubLevelsCount = DB::table('student_level_details')->where('student_id',$studentId)->where('teacher_id',$teacherId)->where('sublevel_id',$subLevelId)->count();
            }
            else
            {
                $compSubLevelsCount = DB::table('student_level_details')->where('student_id',$studentId)->where('sublevel_id',$subLevelId)->count();
            }
            
            $getTotalQuesCount = DB::table('questions')->where('sub_level_id',$subLevelId)->where('status',1)->count();
        ?>
        <?php
            if($compSubLevelsCount==$getTotalQuesCount)
            { 
                $student_last_id = $studentId;

                $last_inserted_data=DB::table('student_level_details')->where('student_id',$studentId)->orderBy('id','desc')->first();

                $latest_sub_level = DB::select("SELECT * FROM sub_levels WHERE id = $subLevelId");
               
        ?>
            <a href="<?php echo e(url('levels_sub_levels/'.encrypt($latest_sub_level[0]->level_id).'?student='.encrypt($studentId))); ?>" id="next-question-level2" class="ml-auto">Go To Next Level</a>
       <?php } ?>
    </div><br><br>
<?php endif; ?>  

<!---------Feedback Popup ------------>
    
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Feedback</h4>
                </div>
                <form method="POST" action="<?php echo e(url('/guest_user_rating')); ?>" aria-label="<?php echo e(__('Login')); ?>">
                    <div class="modal-body">
                    
                        <?php echo csrf_field(); ?>
                    
                        <input type="hidden" name="redirectUrl" value="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>">

                        <input type="hidden" name="level_id" value="<?php echo e($subLevelId); ?>">
                        <div class="rating">
                            <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Meh"></label>
                            <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Kinda bad"></label>
                            <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Kinda bad"></label>
                            <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Sucks big tim"></label>
                            <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Sucks big time"></label>
                        </div>
                        <div class="form-group">
                            <textarea placeholder="Description" class="form-control" name="description" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!---- End ---->

<script>
    $('.start-fade-out').fadeOut(3000, function() {
        $('.go-start-button').css('display','block');
        $('.with-flash').css('display','none');
    });
    jQuery(document).on('click','.get-rating',function(){
        var val = jQuery(this).attr('data-attr');
        jQuery('.allRating').val(val);
    });
    $("#next-question-level").click(function()
    {
        jQuery(this).attr('disabled',true);
        jQuery(this).html('Processing...');
        var all_prev_ids=$("#all-prev-ids").val();
        var subLevel = $(this).attr('sub-lev');
        var currentQ = $(this).attr('cur');
        var studentId = $('.studentId').val();
        var sessionId = $(this).attr('data-attr');
        if(all_prev_ids == '')
        {
            $("#all-prev-ids").val(currentQ);
        	var all_prev=$("#all-prev-ids").val();
        }
        else
        {
            $("#all-prev-ids").val(all_prev_ids+','+currentQ);
        	var all_prev=$("#all-prev-ids").val();
        }
        
      	$.ajax({
            dataType: "html",
            url: "user/get-questions-next/" +subLevel+'/'+ currentQ+'/?all_prev='+$("#all-prev-ids").val()+'&studentId='+studentId+'&sessionId='+sessionId,
            success:function(data)
            {
                jQuery(this).attr('disabled',false);
                jQuery(this).html('Next');
            	$('#question-data').html(data);
                $("#lets_start").hide();
            }
        });
	});

    jQuery(document).off('click','#prev-question-level-func').on('click','#prev-question-level-func',function()
    {
        jQuery(this).attr('disabled',true);
        jQuery(this).html('Processing...');
        var all_prev_ids=$("#all-prev-ids").val();
        var subLevel = $(this).attr('sub-lev');
        var prevQ = $(this).attr('prev');
        var studentId = $('.studentId').val();
        var sessionId = $(this).attr('data-attr');
        $.ajax({
            dataType: "html",
            url: "user/get-questions-previous/" +subLevel+'/'+ prevQ+'/?allPrevId='+all_prev_ids+'&studentId='+studentId+'&sessionId='+sessionId,
           	success:function(data)
           	{
           	    jQuery(this).attr('disabled',false);
                jQuery(this).html('Previous');
                var allPrevIds = removeValue(all_prev_ids, prevQ);
                var allPrevIds1 = removeValue(all_prev_ids);
                

           		var n = allPrevIds.lastIndexOf(",");
                var s1 = allPrevIds.substring(0, n);
				var s2 = allPrevIds.substring(n + 1);

                $('#question-data').html(data);
           		jQuery('.prev').attr('prev',s2);
                $("#lets_start").hide();
           	}
        });
	});

	function removeValue(list, value) 
	{
	  	return list.replace(new RegExp(",?" + value + ",?"), function(match) 
	  	{
	      	var first_comma = match.charAt(0) === ',',second_comma;

	      	if (first_comma && (second_comma = match.charAt(match.length - 1) === ',')) 
	      	{
		        return ',';
	      	}
	      	return '';
	    });
	};

	
	$(document).ready(function() 
	{
        $('.repeat_audio_btn').click(function()
	    {
			var attrs = jQuery(this).attr('data-attr');
			var audioSrc = jQuery('.'+attrs).val();
			var audioElement = document.getElementById(attrs);
			audioElement.setAttribute('src', audioSrc);
			
			audioElement.addEventListener('ended', function() 
			{
				this.play();
			}, false);
			
			audioElement.addEventListener("canplay",function()
			{
				$("#length").text("Duration:" + audioElement.duration + " seconds");
				$("#source").text("Source:" + audioElement.src);
				$("#status").text("Status: Ready to play").css("color","green");
			});
			
			audioElement.addEventListener("timeupdate",function()
			{
				$("#currentTime").text("Current second:" + audioElement.currentTime);
			});
	    
	        audioElement.currentTime = 0;
			audioElement.play();
	    });
    
	    $('#pause').click(function() 
	    {
	        audioElement.pause();
	        $("#status").text("Status: Paused");
	    });
	    
	    $('#restart').click(function() 
	    {
	        audioElement.currentTime = 0;
	    });
	});

	var classHighlight = 'highlight';
	var $thumbs = $('.thumbnail').click(function(e) {
	    e.preventDefault();
	    $thumbs.removeClass(classHighlight);
	    $(this).addClass(classHighlight);
	});

    jQuery(document).on('click','.repeat_text_btn',function(){
        $('a').removeClass('highlight');
    });

    $('.fade-out').fadeOut(3000, function() {
        $('.go-button').css('display','block');
        $('.next-prev-btn').css('display','block');
    });

    jQuery(document).on('click','.get-question-detail',function(){
        jQuery('.without-flash').css('display','block');
        jQuery('.next-prev-btn').css('display','block');
        jQuery('.with-flash').css('display','none');
    });

    jQuery(document).on('click','.next_three_blanks',function(){
        jQuery('.without-flash').css('display','block');
        jQuery('.next-prev-btn').css('display','block');
        jQuery('.with-flash').css('display','none');
        jQuery('.multi_word').css('display','block');
    });

    jQuery(document).on('click','.repeat-flash-word',function(){
        jQuery('.go-button').css('display','none');
        $('.next-prev-btn').css('display','none');
        $('.fade-out').fadeIn( "slow", function() {
            $('.fade-out').fadeOut(3000, function() {
                $('.go-button').css('display','block');
                $('.next-prev-btn').css('display','block');
            });
        });
    });

    jQuery(document).on('click','.update_blank_space',function(){
        var value = jQuery(this).attr('data-attr');
        var trimVal = value.trim();
        jQuery('.blank_space_val').html(trimVal);
    });

    jQuery(document).on('click','.repeat_blank_space',function(){
        jQuery('.blank_space_val').html('__');
    });

    jQuery(document).on('click','.get-flash-detail',function(){
        jQuery('.with-flash-btn').css('display','none');
        jQuery('.with-flash').css('display','block');
        $('.fade-out-flash').fadeOut(4000, function() {
            $('.go-button').css('display','block');
            $('.next-prev-btn').css('display','block');
        });
    });

    jQuery(document).on('click','.start-fill-blanks',function(){
        jQuery('.ans').css('display','block');
        jQuery('.with-flash').css('display','none');
        jQuery('.multi_word').css('display','block');
        jQuery('.align-items-center').css('display','block');
        jQuery('.next-prev-btn').css('display','block');
    });

    jQuery(document).on('click','#guest_user_feedback',function(){
        jQuery('#myModal').modal('show');
    });

</script>
<style>
	.highlight {
	    font-weight: bold;
	}
</style>
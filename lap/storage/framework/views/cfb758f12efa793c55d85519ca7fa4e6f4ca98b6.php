

<div class="progress details_progressbar">
    <div class="progress-bar" style="width:<?php echo e($totalWidth); ?>%" aria-valuemax="100"></div>
</div>

<input type="hidden" id="allprevids" value="<?php echo e($prevqueId); ?>">
<?php if($subLevelId=='64' || $subLevelId=='65'): ?>
	<div class="with-flash">
		<div class="btn_opt text-center my-4 player go-button">
			<a href="javascript:void(0);" class="get-question-detail">Start</a>
		</div>	
	</div>

<?php else: ?>
	
	<div class="middle_content <?php if($subLevelId == 1 && $ques->session_id != 0): ?> <?php if($ques != ''): ?> middle_content2 <?php endif; ?> <?php endif; ?>" >
		<div class="ques_box">
			<div class="ques_panel">
	        	<div class="overlay"></div>

	        	

		        	<?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
		        		<div class="with-flash">
		        			<div class="ans">
					            <div class="single">
					                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
					                <p class="start-fade-out" style="letter-spacing: 3px;">
					                	<?php echo e($ques->option1); ?>

					                </p>
					                </a>
					            </div>
					        </div>
					    </div>
					    <div class="player btn_opt text-center my-4  go-start-button" style="display:none">
						    <a href="javascript:void(0);" class="get-question-detail">Start</a>
						</div>
		        	<?php endif; ?>



			    	<?php if($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='48'): ?>
				    	<div class="with-flash">
				    		<?php
				    			if($subLevelId=='48')
				    			{
				    				$ans = $ques->option1;
				    			}
				    			else
				    			{
					    			$answer = $ques->answer;
					    			if($answer == 'option1')
					    			{
					    				$ans = $ques->option1;
					    			}
					    			else if($answer == 'option2')
					    			{
					    				$ans = $ques->option2;
					    			}
					    			else if($answer == 'option3')
					    			{
					    				$ans = $ques->option3;
					    			}
					    		}
				    		?>
				    		<div class="ans">
					            <div class="single">
					                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
					                <p class="fade-out" style="letter-spacing: 3px;">
					                	<?php echo e(strtolower($ans)); ?>

					                </p>
					                </a>
					            </div>
					        </div>
					        <?php if($subLevelId=='26' || $subLevelId=='24' || $subLevelId=='48'): ?>
					        	<div class="player btn_opt text-center my-4  go-button" style="display:none">
								    <a href="javascript:void(0);" class="repeat-flash-word">Repeat</a>
								    <?php if($subLevelId=='48'): ?>
								    	<a href="javascript:void(0);" class="next_three_blanks">Next</a>
								    <?php else: ?>
								    	<a href="javascript:void(0);" class="get-question-detail">Next</a>
								    <?php endif; ?>
								    
								</div>
					        <?php else: ?>
					        	<div class="btn_opt text-center my-4 player go-button" style="display:none">
								    <!--<button type="button" class="btn btn-success get-question-detail">Go</button>-->
								    <a href="javascript:void(0);" class="get-question-detail">Go</a>
								</div>	
					        <?php endif; ?>
					        
				    	</div>
				    <?php elseif($subLevelId=='12' || $subLevelId=='31' || $subLevelId=='33' || $subLevelId=='53'): ?>
				    	<div class="with-flash">
				    		<div class="ans">
					            <div class="single">
					                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
					                <p class="fade-out" style="letter-spacing: 3px;">
					                	<?php if($subLevelId=='53'): ?>
		                                    <?php echo e($ques->option1); ?>

		                                <?php else: ?>
		                                    <?php echo e(strtolower($ques->option1)); ?>

		                                <?php endif; ?>
					                </p>
					                </a>
					            </div>
					        </div>
					        <div class="btn_opt text-center my-4 player go-button" style="display:none">
		                        <a href="javascript:void(0);" class="repeat-flash-word">Repeat</a>
		                    </div>   
				    	</div>
				    <?php elseif($subLevelId=='21'): ?>
				    	<div class="with-flash">
				    		<div class="ans">
					            <div class="single">
					                <a href="javascript:void(0)" class="thumbnail" style="Text-Decoration: None !important; ">
					                <p class="cluster_word" style="letter-spacing: 3px;">
					                	<?php echo e($ques->answer); ?>

		                            </p>
		                            <p class="cluster_second_word" style="letter-spacing: 3px;display:none;">
					                	<?php echo e($ques->answer); ?>

		                            </p>
					                </a>
					            </div>
					        </div>
					           
				    	</div>
				    <?php elseif($subLevelId=='19' || $subLevelId=='32' || $subLevelId=='38' || $subLevelId=='54'): ?>
				    	<div class="with-flash">
				    		<div class="btn_opt text-center my-4 player go-button">
				    			<?php if($subLevelId=='54'): ?>
				    				<a href="javascript:void(0);" class="get-question-detail">Next</a>
				    			<?php else: ?>
				    				<a href="javascript:void(0);" class="get-question-detail">Start</a>
				    			<?php endif; ?>
							    
							</div>	
				    	</div>
				    
				    <?php elseif($subLevelId=='26' || $subLevelId=='24'): ?>
				    	<div class="with-flash-btn">
				    		<div class="btn_opt text-center my-4 player go-button">
							    <a href="javascript:void(0);" class="get-flash-detail">Start</a>
							</div>	
				    	</div>
				    	<div class="with-flash" style="display:none">
				    		<?php
				    			$answer = $ques->answer;
				    			if($subLevelId=='26')
				    			{
					    			if($answer == 'option1')
					    			{
					    				$ans = $ques->option1;
					    			}
					    			else if($answer == 'option2')
					    			{
					    				$ans = $ques->option2;
					    			}
					    			else if($answer == 'option3')
					    			{
					    				$ans = $ques->option3;
					    			}
					    		}
					    		else
					    		{
					    			$ans = $ques->option1;
					    		}
				    		?>
				    		<div class="ans">
					            <div class="single">
					                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
					                <p class="fade-out" style="letter-spacing: 3px;">
					                	<?php echo e(strtolower($ans)); ?>

					                </p>
					                </a>
					            </div>
					        </div>
					       
				        	<div class="player btn_opt text-center my-4  go-button" style="display:none">
							    <a href="javascript:void(0);" class="repeat-flash-word">Repeat</a>
							    <a href="javascript:void(0);" class="get-question-detail">Next</a>
							</div>
					    </div>
				    <?php elseif($subLevelId=='25'): ?>
				    	<div class="without-flash">
				    		<div class="ans multi_word mb-4">
					            <div class="single single2">
							        <?php
							        	$explode_ans = explode(" ",$ques->option1);
							        	$startVal = key($explode_ans);
						        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
						        		for($i=$startVal; $i<=$lastVal;$i++){
						        	?>
						        			<a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail level_two" style="Text-Decoration: None !important; ">
						        				<?php if($i != 0): ?>
							                		<p><?php echo e(' '.$explode_ans[$i]); ?></p>
							                	<?php else: ?>
							                		<p><?php echo e($explode_ans[$i]); ?></p>
							                	<?php endif; ?>
								           	</a>
							        <?php } ?>
								</div>
					        </div>
					       	<div class="middle_foot d-flex justify-content-center align-items-center">
								<a href="javascript:void(0);" class="repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>" <?php if($subLevelId == 1): ?> <?php endif; ?>><i class="fas fa-redo-alt"></i> Repeat</a>
							</div>  
				    	</div>
				    <?php endif; ?>

			    	<?php if($subLevelId=='42' || $subLevelId=='43' || $subLevelId=='44' || $subLevelId=='52' || $subLevelId == '50' || $subLevelId == '55' || $subLevelId == '56' || $subLevelId == '62' || $subLevelId == '63' || $subLevelId == '14' || $subLevelId == '15' || $subLevelId == '22' || $subLevelId == '28' || $subLevelId == '29' || $subLevelId == '34' || $subLevelId == '36' || $subLevelId == '37' || $subLevelId == '21'): ?>

			    	<!-- <div class="player btn_opt text-center my-4  go-button rthym-btn">
	                    <a href="javascript:void(0);" data-attr="metronome" class="rythm-task">Metronome beat</a>
	                    <a href="javascript:void(0);" data-attr="pulse" class="rythm-task">Pulse-light-up</a>
	                    <a href="javascript:void(0);" data-attr="off" class="rythm-task">Off</a>
	                </div> -->

			    	<div class="rhythm-task-option">
			    		<input type="hidden" class="rythm-task-type">
			    		<audio src="<?php echo e(url('/public/audio/2SEC.mp3')); ?>" id="my_metronome" loop="loop"></audio> 

				    	<?php if($ques->option1 != ''): ?>
					    	<div class="ans">
					            <div class="single single2">
					                <?php
						        		$explode_ans = explode(" ",$ques->option1);
						        		$startVal = key($explode_ans);
						        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
						        		$j=0;
						        		for($i=$startVal; $i<=$lastVal;$i++){
						        	?>
						        			<a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail  pulse_bolde_<?php echo e($j); ?> multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> <?php if($subLevelId == '52' || $subLevelId == '44' || $subLevelId == '43'): ?> rythm-task  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
						        				<?php if($i != 0): ?>
							                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="letter-spacing: 3px;font-weight:100;font-size: 55px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
							                	<?php else: ?>
							                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="letter-spacing: 3px;font-weight:100;font-size: 55px;"><?php echo e($explode_ans[$i]); ?></p>
							                	<?php endif; ?>
							                </a>
							            
									<?php $j++; } ?>
					            </div>
					        </div>
					    <?php endif; ?>
					   	<?php if($ques->option2 != ''): ?>
					   		<div class="ans">
					            <div class="single single2">
					                <?php
						        		$explode_ans = explode(" ",$ques->option2);
						        		$startVal = key($explode_ans);
						        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
						        		if(count($explode_ans) == 5)
						        		{
						        			$j=5;
						        		}
						        		else
						        		{
						        			$j = 4;
						        		}
						        		
						        		for($i=$startVal; $i<=$lastVal;$i++){
						        	?>
						        			<a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail pulse_bolde_<?php echo e($j); ?> multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> <?php if($subLevelId == '52' || $subLevelId == '44' || $subLevelId == '43'): ?> rythm-task  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
						        				<?php if($i != 0): ?>
							                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="letter-spacing: 3px;font-weight:100;font-size: 55px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
							                	<?php else: ?>
							                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="letter-spacing: 3px;font-weight:100;font-size: 55px;"><?php echo e($explode_ans[$i]); ?></p>
							                	<?php endif; ?>
							                </a>
							            
									<?php $j++; } ?>
					            </div>
					        </div>
					    <?php endif; ?>
					    <?php if($ques->option3 != ''): ?>
					    	<div class="ans">
					            <div class="single single2">
					                <?php
						        		$explode_ans = explode(" ",$ques->option3);
						        		$startVal = key($explode_ans);
						        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
						        		if(count($explode_ans) == 5)
						        		{
						        			$j=10;
						        		}
						        		else
						        		{
						        			$j = 8;
						        		}
						        		for($i=$startVal; $i<=$lastVal;$i++){
						        	?>
						        			<a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail pulse_bolde_<?php echo e($j); ?> multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> <?php if($subLevelId == '52' || $subLevelId == '44' || $subLevelId == '43'): ?> rythm-task  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
						        				<?php if($i != 0): ?>
							                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="letter-spacing: 3px;font-weight:100;font-size: 55px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
							                	<?php else: ?>
							                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="letter-spacing: 3px;font-weight:100;font-size: 55px;"><?php echo e($explode_ans[$i]); ?></p>
							                	<?php endif; ?>
							                </a>
							            
									<?php $j++; } ?>
					            </div>
					        </div>
					    <?php endif; ?>
					    <?php if($ques->option4 != ''): ?>
					    	<div class="ans">
					            <div class="single single2">
					                <?php
						        		$explode_ans = explode(" ",$ques->option4);
						        		$startVal = key($explode_ans);
						        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
						        		if(count($explode_ans) == 5)
						        		{
						        			$j=15;
						        		}
						        		else
						        		{
						        			$j = 12;
						        		}
						        		for($i=$startVal; $i<=$lastVal;$i++){
						        	?>
						        			<a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail pulse_bolde_<?php echo e($j); ?> multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> <?php if($subLevelId == '52' || $subLevelId == '44' || $subLevelId == '43'): ?> rythm-task  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
						        				<?php if($i != 0): ?>
							                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="letter-spacing: 3px;font-weight:100;font-size: 55px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
							                	<?php else: ?>
							                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="letter-spacing: 3px;font-weight:100;font-size: 55px;"><?php echo e($explode_ans[$i]); ?></p>
							                	<?php endif; ?>
							                </a>
							            
									<?php $j++; } ?>
					            </div>
					        </div>
					    <?php endif; ?>
					    <?php if($subLevelId=='21'): ?>
					    	<div class="middle_foot d-flex justify-content-center align-items-center">
					    		<a href="javascript:void(0);" class="repeat_cluster_word" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
					    	</div>
					    <?php endif; ?>
					</div>
				    
							    <?php endif; ?>
							    <div class="without-flash" <?php if(($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='43' || $subLevelId=='44' || $subLevelId=='42' || $subLevelId=='12' || $subLevelId=='26' || $subLevelId=='31' || $subLevelId=='33' || $subLevelId == '50' || $subLevelId=='53' || $subLevelId=='52' | $subLevelId=='55' | $subLevelId=='56' || $subLevelId == '62' || $subLevelId == '63' || $subLevelId == '14' || $subLevelId == '15' || $subLevelId == '22' || $subLevelId == '28' || $subLevelId == '29' || $subLevelId == '34' || $subLevelId == '36' || $subLevelId == '37' || $subLevelId == '19' || $subLevelId == '25' || $subLevelId == '32' || $subLevelId=='38' || $subLevelId=='24' || $subLevelId=='54' || $subLevelId=='48' || $subLevelId=='21' || $subLevelId=='64' || $subLevelId=='65') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?> style="display:none;" <?php endif; ?>>
							    	
							    	<?php if($subLevelId=='59' || $subLevelId=='60' || $subLevelId=='61'): ?>
								    	<div class="ans">
						                    <div class="single">
						                        <p style="color:black"><?php echo e(strtolower($ques->question)); ?></p>
						                        </a>
						                    </div>
							            </div>
							        <?php endif; ?>
							    	<?php if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='20' || $subLevelId=='30' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?>
						                <?php
						                	if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))
						                	{
						                		$explodeblank = explode("_",$ques->option2);
							                    if($explodeblank[0] == '')
							                    {
							                        $whole_word = '<span class="blank_space_val">__</span>'.$explodeblank[1];
							                    }
							                    elseif($explodeblank[1] == '')
							                    {
							                        $whole_word = $explodeblank[0].'<span class="blank_space_val">__</span>';
							                    }
							                    elseif($explodeblank[0] != '' && $explodeblank[1] != '')
							                    {
							                        $whole_word = $explodeblank[0].'<span class="blank_space_val">__</span>'.$explodeblank[1];
							                    }
						                	}
						                	else if($subLevelId=='49' || $subLevelId=='48' || $subLevelId=='23')
					                        {
					                            $whole_word = '<span class="blank_space_val_first">__</span>'.' '.'<span class="blank_space_val_two">__</span>'.' '.'<span class="blank_space_val_third">__</span>';
					                        }
					                        else
					                        {
					                        	if($subLevelId=='16')
					                        	{
					                        		$explodeWord = explode(" ",$ques->option1);

					                        		if(isset($explodeWord[0]) && isset($explodeWord[1]) && isset($explodeWord[2]))
					                        		{
					                        			if($explodeWord[0] != '_' && $explodeWord[1] == '_' && $explodeWord[2] == '_')
						                        		{
						                        			$whole_word = $explodeWord[0].'<span class="blank_space_val_first"> __</span><span class="blank_space_val_two"> __</span>';
						                        		}
						                        		elseif($explodeWord[0] == '_' && $explodeWord[1] == '_' && $explodeWord[2] != '_')
						                        		{
						                        			$whole_word = '<span class="blank_space_val_first"> __</span><span class="blank_space_val_two"> __ </span>'.$explodeWord[2];
						                        		}
						                        		elseif($explodeWord[0] != '_' && $explodeWord[1] == '_' && $explodeWord[2] != '_')
						                        		{
						                        			$whole_word = $explodeWord[0].'<span class="blank_space_val_first"> __ </span>'.$explodeWord[2];
						                        		}
					                        		}
					                        		elseif(isset($explodeWord[0]) && isset($explodeWord[1]) && isset($explodeWord[2]) && isset($explodeWord[3]))
					                        		{
					                        			$whole_word = $explodeWord[0].'<span class="blank_space_val_first"> __</span><span class="blank_space_val_two"> __</span><span class="blank_space_val_third"> __</span>';
					                        		}
					                        		else
					                        		{
														if($explodeWord[0] == '_' && $explodeWord[1] != '_')
					                                    {
					                                        $whole_word = '<span class="blank_space_val_first"> __ </span>'.$explodeWord[1];
					                                    }
						                        		
						                        		elseif($explodeWord[0] != '_' && $explodeWord[1] == '_')
						                        		{
						                        			$whole_word = $explodeWord[0].'<span class="blank_space_val_first"> __</span>';
						                        		}
					                        		}
					                        	}
					                        	else
					                        	{
					                        		$explodeblank = explode("_",$ques->option1);
								                    if($explodeblank[0] == '')
								                    {
								                        $whole_word = '<span class="blank_space_val">__</span>'.$explodeblank[1];
								                    }
								                    elseif($explodeblank[1] == '')
								                    {
								                        $whole_word = $explodeblank[0].'<span class="blank_space_val">__</span>';
								                    }
								                    elseif($explodeblank[0] != '' && $explodeblank[1] != '')
								                    {
								                        $whole_word = $explodeblank[0].'<span class="blank_space_val">__</span>'.$explodeblank[1];
								                    }
					                        	}
							                }
						                ?>
						                <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40'): ?>
							        		<div class="with-flash">
									    		<div class="btn_opt text-center my-4 player go-button">
												    <a href="javascript:void(0);" class="start-fill-blanks">Start</a>
												</div>	
									    	</div>
							        	<?php endif; ?>
						                <div class="ans" <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40'): ?> style="display:none" <?php endif; ?>>
						                    <div class="single">
						                        <a href="javascript:void(0)" style="Text-Decoration: None !important; ">
						                        <p><?php echo strtolower($whole_word);?></p>
						                        </a>
						                    </div>
						                </div>
						            <?php elseif($subLevelId=='24'): ?>
						            	<div class="ans multi_word mb-4">
								            <div class="single single2">
										        <?php
										        	$explode_ans = explode(" ",$ques->answer);
										        	$startVal = key($explode_ans);
									        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
									        		for($i=$startVal; $i<=$lastVal;$i++){
									        	?>
									        			<a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail" style="Text-Decoration: None !important;">
									        				<?php if($i != 0): ?>
										                		<p style="font-size:80px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
										                	<?php else: ?>
										                		<p style="font-size:80px;"><?php echo e($explode_ans[$i]); ?></p>
										                	<?php endif; ?>
											           </a>
										            
												<?php } ?>
											</div>
								        </div>
								    <?php else: ?>
								    	<div class="ans">
								            <div class="single">
								                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
								                <?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
									                <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:110px">
									                	<?php echo e(strtolower($ques->option1)); ?>

									                </p>
									            <?php else: ?>
									            	<?php if($subLevelId == 51): ?>
										            	<p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
										                	<?php echo e($ques->option1); ?>

										                </p>
										            <?php else: ?>
										            	<p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
										                	<?php echo e(strtolower($ques->option1)); ?>

										                </p>
										            <?php endif; ?>
									            <?php endif; ?>

								                </a>
								            </div>
								        </div>
								    <?php endif; ?>
								    <?php if($ques->session_id != 0): ?>
								        <div class="ans">
								            <div class="single">
								                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
								                <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option2)); ?></p>
								                </a>
								            </div>
								        </div>
								    <?php endif; ?>
							        <div class="ans">
							            <div class="single">
							                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
							                <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option3)); ?></p>
							                </a>
							            </div>
							        </div>
							        <div class="ans">
							            <div class="single">
							                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
							                <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option4)); ?></p>
							                </a>
							            </div>
							        </div>
							        <div class="ans">
							            <div class="single">
							                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
							                <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option5)); ?></p>
							                </a>
							            </div>
							        </div>
							        <div class="ans">
							            <div class="single">
							                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
							                <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option6)); ?></p>
							                </a>
							            </div>
							        </div>
							        <?php if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='20' || $subLevelId=='30' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?>
							        	
							        	<div class="ans multi_word mb-4" <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='48' ): ?> style="display:none" <?php endif; ?>>
								            <div class="single single2">
										        <?php
										        	if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='48' || $subLevelId=='16') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)))
										        	{
										        		$explode_ans = explode(",",$ques->answer);
										        	}
										        	else
										        	{
										        		$explode_ans = explode(" ",$ques->answer);
										        	}
													$startVal = key($explode_ans);
									        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
									        		for($i=$startVal; $i<=$lastVal;$i++){
									        	?>

									        	<?php if($subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23'): ?>
									        		<a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="update_three_space" style="Text-Decoration: None !important; ">
									        	<?php else: ?>
								        			<a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="update_blank_space" style="Text-Decoration: None !important; ">
								        		<?php endif; ?>
							        				<?php if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='48' || $subLevelId=='16') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?>
								        			
								        				<?php if($i != 0): ?>
									                		<p><?php echo e(','.$explode_ans[$i]); ?></p>
									                	<?php else: ?>
									                		<p><?php echo e($explode_ans[$i]); ?></p>
									                	<?php endif; ?>
									                <?php else: ?>
									               
									                	<?php if($i != 0): ?>
									                		<p><?php echo e(' '.$explode_ans[$i]); ?></p>
									                	<?php else: ?>
									                		<p><?php echo e($explode_ans[$i]); ?></p>
									                	<?php endif; ?>
									               	<?php endif; ?>
									            </a>
										            
												<?php } ?>
											</div>
								        </div>
							        <?php endif; ?>
							        <?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
										<div class="middle_foot d-flex justify-content-center align-items-center">
											<a href="javascript:void(0);" class="repeat_blank_space" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
										</div>
							        <?php elseif($subLevelId!='1'): ?>
									    <div class="player">
									        <?php if($ques->answer_record != ''): ?>

									        	<!-- Repeat with audio -->

									        	<div class="middle_foot d-flex justify-content-center align-items-center">
													<a href="javascript:void(0);" class="repeat_audio_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>

													<input type="hidden" class="<?php echo 'repeat_'.$ques->id;?>" value="<?php echo e(asset('/answer_record/'.$ques->answer_record)); ?>">

													<audio class="repeat_audio ml-3" id="<?php echo 'repeat_'.$ques->id;?>"  controls>
													  	<source src="<?php echo e(asset('/answer_record/'.$ques->answer_record)); ?>" type="audio/mp3">
													</audio>
												</div>
											<?php else: ?>
									        <?php if($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='20' || $subLevelId=='30' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23' || $subLevelId=='21'): ?>

									        		<!-- Repeat -->
									        		<div class="middle_foot d-flex justify-content-center align-items-center" <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40'): ?> style="display:none !important" <?php endif; ?>>
									        			<?php if($subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23'): ?>
									        				<?php if($subLevelId=='49'): ?>
										        				<?php if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute' ): ?>
											        				<p class="three_blanks sit_blank" style="letter-spacing: 3px;">
													                	<?php echo e($ques->option1); ?>

										                            </p>
										                        <?php endif; ?>
										                     <?php endif; ?>

										                    <?php if($subLevelId=='23'): ?>
										                    	<a href="javascript:void(0);" class="repeat_three_space <?php if($subLevelId=='49'): ?> ml-auto <?php endif; ?>" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Next</a>
										                    <?php else: ?>
										                    	<a href="javascript:void(0);" class="repeat_three_space <?php if($subLevelId=='49'): ?> ml-auto <?php endif; ?>" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
										                    <?php endif; ?>

									        				
									        			<?php else: ?>

															<a href="javascript:void(0);" class="repeat_blank_space" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
														<?php endif; ?>
													</div>

									        	<?php elseif($subLevelId!='17' && $subLevelId!='45' && $subLevelId!='57' && $subLevelId!='13' && $subLevelId!='20' && $subLevelId!='30' && $subLevelId!='35' && $subLevelId!='39' && $subLevelId!='40' && $subLevelId!='49' && $subLevelId!='48' && $subLevelId!='16' && $subLevelId!='23'): ?>

									        		<!-- Repeat -->

									        		<div class="middle_foot d-flex justify-content-center align-items-center">
														<a href="javascript:void(0);" class="repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
													</div>
									         	<?php endif; ?>

									        <?php endif; ?>

									    </div>	
									
							        <?php else: ?>
							        	<?php if($subLevelId!='17'): ?>

							        		<!-- Repeat -->

							        		<div class="middle_foot d-flex justify-content-center align-items-center">
												<a href="javascript:void(0);" class="repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>" <?php if($subLevelId == 1): ?> <?php endif; ?>><i class="fas fa-redo-alt"></i> Repeat</a>
											</div>
						                <?php endif; ?>
							        <?php endif; ?>
							    </div>
					    	</div>
							<br>
						
						</div>
					</div>

				<!-- Next Previous -->
			

				<div class="bottom_btn d-md-flex ques_btn next-prev-btn" <?php if(($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='12' || $subLevelId=='31' || $subLevelId=='26' || $subLevelId=='33' || $subLevelId=='53' || $subLevelId=='19' || $subLevelId == '32' || $subLevelId == '35' || $subLevelId=='38' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='24' || $subLevelId=='54' || $subLevelId=='48') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?> style="display:none !important;" <?php endif; ?>>
					<!-- <a href="javascript:void(0);" class="prev">Previous</a> -->
					<?php if($ques->session_id == 0): ?>
						<a href="<?php echo e(url('sublevel_sublevels/'.encrypt($levels->sub_level_id).'?student='.encrypt($studentId))); ?>" class="ml-auto">Complete Later</a>
					<?php else: ?>
						<a href="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>" class="ml-auto">Complete Later</a>
					<?php endif; ?>
					<a href="javascript:void(0);" class="ml-auto" id="next-question-level" sub-lev='<?php echo e($ques->sub_level_id); ?>' cur='<?php echo e($ques->id); ?>'>Next</a>
				</div>	

<?php endif; ?>
			


	
<script>
	
	 //location.href = 'https:' + window.location.href.substring(window.location.protocol.length);

	
	//console.log('https:' + window.location.href.substring(window.location.protocol.length));
    $("#next-question-level").click(function()
    {
        jQuery(this).attr('disabled',true);
        jQuery(this).html('Processing...');
        var subLevel = $(this).attr('sub-lev');
	    var currentQ = $(this).attr('cur');
	    var studentId = $('.studentId').val();
	    var all_prev_ids = $('#allprevids').val();


	    if(all_prev_ids == '')
        {
        	$("#allprevids").val(currentQ);
        	var all_prev=$("#allprevids").val();
        }
        else
        {

        	$("#allprevids").val(all_prev_ids+','+currentQ);
        	var all_prev=($("#allprevids").val());
        }

        $.ajax({
	        dataType: "html",
	        url: "user/get-questions-next/" +subLevel+'/'+ currentQ+'/?all_prev='+all_prev+'&studentId='+studentId,


	        success:function(data){
	            jQuery(this).attr('disabled',false);
                jQuery(this).html('Next');
	            $('#question-data').html(data);
	            $("#lets_start").hide();
	        }
	    });
	});
	$(document).ready(function() 
	{
    	$('.repeat_audio_btn').click(function() 
    	{
    		$('a').removeClass('highlight');
			var attrs = jQuery(this).attr('data-attr');
			var audioSrc = jQuery('.'+attrs).val();
			var audioElement = document.getElementById(attrs);
			audioElement.setAttribute('src', audioSrc);
			
			audioElement.addEventListener('ended', function() 
			{
				this.play();
			}, false);
			
			audioElement.addEventListener("canplay",function()
			{
				$("#length").text("Duration:" + audioElement.duration + " seconds");
				$("#source").text("Source:" + audioElement.src);
				$("#status").text("Status: Ready to play").css("color","green");
			});
			
			audioElement.addEventListener("timeupdate",function()
			{
				$("#currentTime").text("Current second:" + audioElement.currentTime);
			});
	    
	        audioElement.currentTime = 0;
			audioElement.play();
	    });
    
	    $('#pause').click(function() 
	    {
	        audioElement.pause();
	        $("#status").text("Status: Paused");
    	});
    
	    $('#restart').click(function() 
	    {
	        audioElement.currentTime = 0;
	    });
	});

	
	var classHighlight = 'highlight';
	var $thumbs = $('.thumbnail').click(function(e) 
	{
	    e.preventDefault();
	    $thumbs.removeClass(classHighlight);
	    $(this).addClass(classHighlight);
	});

	jQuery(document).on('click','.repeat_text_btn',function(){
		$('a').removeClass('highlight');
	});

	$('.fade-out').fadeOut(5000, function() {
        $('.go-button').css('display','block');
        $('.next-prev-btn').css('display','block');
    });

    $('.start-fade-out').fadeOut(5000, function() {
        $('.go-start-button').css('display','block');
        $('.with-flash').css('display','none');
    });

	jQuery(document).on('click','.get-question-detail',function(){
		jQuery('.without-flash').css('display','block');
		jQuery('.next-prev-btn').css('display','block');
		jQuery('.with-flash').css('display','none');
		jQuery('.go-start-button').css('display','none');
	});

	jQuery(document).on('click','.next_three_blanks',function(){
		jQuery('.without-flash').css('display','block');
		jQuery('.next-prev-btn').css('display','block');
		jQuery('.with-flash').css('display','none');
		jQuery('.multi_word').css('display','block');
	});

	jQuery(document).on('click','.repeat-flash-word',function(){
		jQuery('.go-button').css('display','none');
		$('.next-prev-btn').css('display','none !important');
		$('.fade-out').fadeIn( "slow", function() {
		    $('.fade-out').fadeOut(3000, function() {
		        $('.go-button').css('display','block');
		        $('.next-prev-btn').css('display','block');
		    });
		});
	});

	jQuery(document).on('click','.update_blank_space',function(){
		var value = jQuery(this).attr('data-attr');
		var trimVal = value.trim();
		jQuery('.blank_space_val').html(trimVal);
	});

/*===============jquery for three blanks============*/

	jQuery(document).on('click','.update_three_space',function(){
		var value = jQuery(this).attr('data-attr');
		var trimVal = value.trim();
		if(jQuery('.blank_space_val_first').html() == '__' || jQuery('.blank_space_val_first').html() == ' __' || jQuery('.blank_space_val_first').html() == ' __ ')
		{
			jQuery('.blank_space_val_first').html(trimVal);
		}
		else if(jQuery('.blank_space_val_two').html() == '__' || jQuery('.blank_space_val_two').html() == ' __' || jQuery('.blank_space_val_two').html() == ' __ ')
		{
			jQuery('.blank_space_val_two').html(trimVal);
		}
		else if(jQuery('.blank_space_val_third').html() == '__' || jQuery('.blank_space_val_third').html() == ' __' || jQuery('.blank_space_val_third').html() == ' __ ')
		{
			jQuery('.blank_space_val_third').html(trimVal);
		}
	});

/*===============Repeat button for three blanks============*/

	jQuery(document).on('click','.repeat_three_space',function(){
		jQuery('.blank_space_val_first').html(' __ ');
		jQuery('.blank_space_val_two').html(' __ ');
		jQuery('.blank_space_val_third').html(' __ ');
	}); 

	jQuery(document).on('click','.repeat_blank_space',function(){
		jQuery('.blank_space_val').html('__');
	});

	

/*================Rhythm task=================*/

<?php if($subLevelId!='64' || $subLevelId!='65'){?>

	function getRandomNumber()
	{
		<?php 

			if($ques->option1 != '')
			{
				$ques1 = $ques->option1;
			}
			if($ques->option2 != '')
			{
				$ques2 = $ques->option2;
			}
			if($ques->option3 != '')
			{
				$ques3 = $ques->option3;
			}

		?>
		ques1str = '<?php echo $ques1 ?>';
		var ques1result = ques1str.split(" ");

		ques2str = '<?php echo $ques1 ?>';
		var ques2result = ques2str.split(" ");

		ques3str = '<?php echo $ques1 ?>';
		var ques3result = ques3str.split(" ");

		ques4str = '<?php echo $ques1 ?>';
		var ques4result = ques4str.split(" ");

		arrayfirst = $.merge( $.merge( [], ques1result ), ques2result);

		arraysecond = $.merge( $.merge( [], arrayfirst ), ques3result);

		$arraysec = $.merge( $.merge( [], arraysecond ), ques4result);

		$totalItems = $arraysec.length;

		var finalkey = [];

		$.each($arraysec, function(key, value) {
		    finalkey.push(key+parseInt(1));
		});

		$randomKey = finalkey[Math.floor(Math.random()*finalkey.length)];
	}
<?php } ?>

	function playAudio($randomKey)
	{
		var timeoutSec = $randomKey*parseInt(2000);
		var audio = document.getElementById('my_metronome');
		for($i=1;$i<=$randomKey;$i++)
		{
			audio.play();
		}
		setTimeout(function(){
	        audio.pause();
	        audio.currentTime = 0;
	    }, timeoutSec);
	}

	setTimeout(function(){
	      		
	    var player = document.getElementById('my_metronome');
		var rhythmType = jQuery('.rythm-task-type').val();
		if(rhythmType != '')
		{
			if(rhythmType == 'metronome')
			{
				getRandomNumber();
				playAudio($randomKey);
			}
			else if(rhythmType == 'pulse')
			{
				jQuery('.pulse_bolde_0').css({'font-weight':800});
					jQuery('.rythm_task_pulse_0').css({'font-weight':''});
				setTimeout(function(){
					jQuery('.pulse_bolde_0').css({'font-weight':''});
					jQuery('.rythm_task_pulse_0').css({'font-weight':'100'});
				},1000);
			}
			
		}
	}, 1000);
	

	/*jQuery(document).on('click','.pulse_task',function(){
		jQuery(this).parent().removeClass('highlight');
		var Text = jQuery(this);
		if(Text.css('font-weight') == '100') 
        {
        	Text.css({'font-weight':200});	
        	Text.css({'font-size':160});
        	Text.parent().css({'position': 'relative'});
			Text.parent().css({'top': '0px'});
			Text.parent().css({'left': '0px'});
			Text.parent().css({'right': '0px'});
			Text.parent().css({'bottom': '0px'});
			Text.parent().css({'background': '#ececec'});
			Text.parent().css({'width': '100%'});
			Text.css({'transition':'all 0.5s ease-in-out'});	
      	 	setTimeout(function(){ Text.css({'font-weight':300})}, 30)
            setTimeout(function(){ Text.css({'font-weight':400})}, 60)
            setTimeout(function(){ Text.css({'font-weight':500})}, 90)
            setTimeout(function(){ Text.css({'font-weight':600})},120)
            setTimeout(function(){ Text.css({'font-weight':700})},150)
            setTimeout(function(){ Text.css({'font-weight':800})},180)
            setTimeout(function(){ Text.css({'font-weight':900})},210)
        }
        setTimeout(function(){ 
        	Text.css({'font-size':55}); 
        	Text.css({'font-weight':100});
        	Text.parent().css({'position': ''});
			Text.parent().css({'top': ''});
			Text.parent().css({'left': ''});
			Text.parent().css({'right': ''});
			Text.parent().css({'bottom': ''});
			Text.parent().css({'background': ''});
			Text.parent().css({'width': ''});
        },2000)
		//jQuery(this).fadeIn();
		/*jQuery(this).css({'font-weight':800});
		setTimeout(function(){ jQuery(this).css({'font-weight':900})}, 60);*/
	/*});*/

	function playRandomAudioAfterOneMin(counter)
	{
		var count = counter;
		getRandomNumber();
		var totalItems = $totalItems-parseInt(1);
		var type = jQuery('.rythm-task-type').val();
	  	if(counter <= totalItems)
	  	{
	  		if(type == 'metronome')
	  		{
		    	setTimeout(function(){
		      		counter++;
		      		playRandomAudioAfterOneMin(counter);
				    getRandomNumber();
					playAudio($randomKey);
		    	}, 20000);
		    }
		    else if(type == 'pulse')
		    {
		    	setTimeout(function(){
		    		jQuery('.pulse_bolde_'+counter).css({'font-weight':800});
					jQuery('.rythm_task_pulse_'+counter).css({'font-weight':''});
					counter++;
					playRandomAudioAfterOneMin(counter);
					setTimeout(function(){
						jQuery('.pulse_bolde_'+count).css({'font-weight':''});
						jQuery('.rythm_task_pulse_'+count).css({'font-weight':'100'});
					},1000);
				},2500);
		    }
	  	}
	}
	jQuery(document).on('click','.get-flash-detail',function(){
		jQuery('.with-flash-btn').css('display','none');
		jQuery('.with-flash').css('display','block');
	});

	jQuery(document).on('click','.start-fill-blanks',function(){
		jQuery('.ans').css('display','block');
		jQuery('.with-flash').css('display','none');
		jQuery('.multi_word').css('display','block');
		jQuery('.align-items-center').css('display','block');
		jQuery('.next-prev-btn').css('display','block');
	});

	jQuery(document).on('click','.clusterItems',function(){
		var type = jQuery(this).attr('data-attr');
		jQuery('.cluster_second_word').css('display','none');
		jQuery('.cluster_word').css('display','block');
		jQuery('.cluster_word').html(type);
	});

	jQuery(document).on('click','.repeat_cluster_word',function(){
		jQuery('.cluster_second_word').css('display','block');
		jQuery('.cluster_word').css('display','none');
	});

	setTimeout(function(){playRandomAudioAfterOneMin(1)},1000);
	//setTimeout(function(){console.log('dsa')},1000);
	
	
</script>
<style>
	.highlight {
	    font-weight: bold;
	}
</style>
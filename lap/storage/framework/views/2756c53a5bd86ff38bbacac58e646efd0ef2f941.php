<?php $__env->startSection('content'); ?>


<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section"> 
        <h1>
            <i class="fa fa-table"></i>User Management<br><small>You can manage your entire registered users!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Users</li>
    <?php if($type == 'institute'): ?>
        <li><a href="<?php echo e(url('/admin/manage-users?type=institute')); ?>">Manage Institutes</a></li>
    <?php elseif($type == 'teacher'): ?>
        <li><a href="<?php echo e(url('/admin/manage-users?type=teacher')); ?>">Manage Teachers</a></li>
    <?php elseif($type == 'student'): ?>
        <li><a href="<?php echo e(url('/admin/manage-users?type=student')); ?>">Manage Students</a></li>
    <?php else: ?>
        <li><a href="<?php echo e(url('/admin/manage-users')); ?>">Manage Users</a></li>
    <?php endif; ?>
</ul>
<!-- END Datatables Header -->

<!-- Datatables Content -->
<div class="block full">
    <div class="block-title">
        <?php if($type == 'institute'): ?>
            <h2><strong>Manage</strong> Institute</h2>
            <a href="<?php echo e(url('/admin/add-user')); ?>"><button type="button" class="btn btn-primary">Add Institute</button></a>
        <?php elseif($type == 'teacher'): ?>
            <h2><strong>Manage</strong> Teacher</h2>
            <a href="<?php echo e(url('/admin/add-teacher')); ?>"><button type="button" class="btn btn-primary">Add Teacher</button></a>
        <?php elseif($type == 'student'): ?>
            <h2><strong>Manage</strong> Student</h2>
            <a href="<?php echo e(url('/admin/add-student')); ?>"><button type="button" class="btn btn-primary">Add Student</button></a>
        <?php else: ?>
            <h2><strong>Manage</strong> users</h2>
        <?php endif; ?>
    </div>

    <?php if(session()->has('success')): ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session()->get('success')); ?>

    </div>
    <?php endif; ?>
    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center"><i class="gi gi-user"></i></th>
                    <th>Name</th>
                    <?php if($type == 'teacher'): ?>
                        <th>Institute</th>
                    <?php elseif($type == 'student'): ?>
                        <th>Teacher/Institute</th>
                    <?php else: ?>
                        <th>Add Teacher/Student</th>
                    <?php endif; ?>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Status</th>
                    <?php if($type == 'institute'): ?>
                        <th></th>
                    <?php endif; ?>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1
                ?>
                <?php $__currentLoopData = $allUsers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $allUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $encodedUserId = encrypt($allUser->id) ;
                    $inst_data=DB::table('users')->where('id',$allUser->institute_id)->first();
                    $stu_data=DB::table('users')->where('id',$allUser->teacher_id)->first();
                ?>
                <tr>
                    <td class="text-center"><?php echo e($i); ?></td>
                    <td class="text-center"><img src="<?php echo e(asset('/public/img/placeholders/avatars/avatar12.jpg')); ?>" alt="avatar" class="img-circle"></td>
                    <td><?php echo e($allUser->name); ?></td>
                    <?php if($allUser->type=='Institute'): ?>
                        <td>
                            <a href="javascript:void(0)">
                                <span title="click to Add Teacher/Student" onclick="window.location.href='<?php echo e(url('admin/add-student-teacher/'.$encodedUserId)); ?>'" class="label label-success">Add Teacher/Student</span>
                            </a>
                        </td>
                    <?php elseif($type == 'teacher' || $type == 'student'): ?>
                         <?php if(isset($inst_data) && !empty($inst_data->name)): ?>
                            <td>
                                <?php echo e(ucwords($inst_data->name)); ?> Institute
                            </td>
                        <?php elseif(isset($stu_data) && !empty($stu_data->name)): ?>
                            <td>
                                <?php echo e(ucwords($stu_data->name)); ?> Teacher
                            </td>
                        <?php else: ?>
                            <td>N/A</td>
                        <?php endif; ?>
                    <?php else: ?>

                        <?php if(isset($inst_data) && !empty($inst_data->name)): ?>
                            <td>
                                <?php echo e(ucwords($allUser->type)); ?> Under <?php echo e(ucwords($inst_data->name)); ?> Institute
                            </td>
                        <?php else: ?>
                            <td>N/A</td>
                        <?php endif; ?>

                    <?php endif; ?>
                    <td><?php echo e($allUser->email); ?></td>
                    <td><?php echo e(ucwords($allUser->type)); ?></td>
                    <td>
                        <a href="javascript:void(0)">
                            <span title="click to change status" onclick="window.location.href='<?php echo e(url('admin/update-users-status/'.$encodedUserId)); ?>'" class="label <?php echo e($allUser->status=='1' ? 'label-success' : 'label-danger'); ?>"><?php echo e($allUser->status=='1' ? 'Click to Deactive' : 'Click to Activate'); ?></span>
                        </a>
                    </td> 
                    <?php if($type == 'institute'): ?>
                        <td>
                            <?php if($allUser->type=='Institute'): ?>

                                <a href="<?php echo e(url('manage-users?teacherId='.encrypt($allUser->id).'&type=teacher')); ?>">
                                    <span title="View Teachers" class="label label-success">View Teachers</span>
                                </a>

                                <a href="<?php echo e(url('manage-users?teacherId='.encrypt($allUser->id).'&type=student')); ?>">
                                    <span title="View Students" class="label label-success">View Students</span>
                                </a>

                            <?php endif; ?>
                        </td> 
                    <?php endif; ?>                  
                    <td class="text-center">
                        
                        <div class="btn-group">
                            
                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/edit-user/'.$encodedUserId)); ?>'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>

                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/delete-user/'.$encodedUserId)); ?>'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                </tr>
                 <?php $i++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
<!-- END Datatables Content -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
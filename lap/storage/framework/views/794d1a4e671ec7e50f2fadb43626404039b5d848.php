<?php $__env->startSection('middle_content'); ?>

  	<div class="layout-content" data-scrollable>

      	<div id="btnContainer" class='pull-right'>
	        <a href="<?php echo e(url('add-students')); ?>"><button class="btn btn-primary">Add Student</button></a>
	        <button class="btn" id="list_view"><i class="fa fa-bars"></i> List</button> 
	        <button class="btn active" id="grid_view"><i class="fa fa-th-large"></i> Grid</button>
      	</div>
      	<br>
		<div class="container-fluid">
	      
	        <div class="card-columns row" id="hide_show_grid"> 

	          	<?php
	            	$i=0;
	          	?>
	   			<?php $__currentLoopData = $student; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	              
	              	<div class="card">
	                	<div class="card-header bg-white ">
		                  <h4 class="card-title"><a href="take-course.html"><?php echo e($stu->name); ?></a></h4>
		                  <small class="text-muted">Name: <?php echo e($stu->name); ?></small>
		                </div>
						<div class="card-footer bg-white">
		                  <a href="<?php echo e(url('/front')); ?>" class="btn btn-primary btn-sm"> Courses <i class="material-icons">play_circle_outline</i></a>
		                </div>
	            	</div>

					<?php
		              	$i++;
		            ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

	      	</div>
	    </div>
  	</div>
  
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
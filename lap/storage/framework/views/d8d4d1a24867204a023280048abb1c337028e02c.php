<?php $__env->startSection('middle_content'); ?>


  <!-- Content -->
  <div class="layout-content" data-scrollable>

    <div id="btnContainer" class='pull-right'>
        <button class="btn" id="list_view"><i class="fa fa-bars"></i> List</button> 
        <button class="btn active" id="grid_view"><i class="fa fa-th-large"></i> Grid</button>
      </div>
      <br>
    <div class="container-fluid">

      <div class="card-columns row" id="hide_show_grid">
        <?php
          $i=0;
        ?>
        <?php $__currentLoopData = $levelSubLevel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $levSubLev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php
                if($i == 0)
                {
                  $disabled = "";
                }
                else
                {
                  $disabled = "disabled";
                }
            ?>
            
            <div class="card">
              <div class="card-header bg-white ">
                <h4 class="card-title"><a href="take-course.html"><?php echo e($levSubLev->title); ?></a></h4>
                <small class="text-muted">Questions: <?php echo e(count($levSubLev->subLevelsQues)); ?></small>
              </div>

              <progress class="progress progress-primary progress-striped m-b-0" value="40" max="100">40%</progress>
              
              <div class="card-footer bg-white">
                <a href="<?php echo e(url('/level_questions_/'.encrypt($levSubLev->id))); ?>">
                  <input type="button" class="btn btn-primary btn-sm" value="Continue"> 
                </a>
              </div>
            </div>

            <?php
              $i++;
            ?>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
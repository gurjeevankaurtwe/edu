<?php $__env->startSection('content'); ?>
<!-- Dashboard 2 Header -->
<div class="content-header">
    <ul class="nav-horizontal text-left">
        <li class="active">
            <a href="javascript:void(0)"><i class="fa fa-user"></i> Users</a>
        </li>

    </ul>
</div>
<!-- END Dashboard 2 Header -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
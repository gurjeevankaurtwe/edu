<?php $__env->startSection('title', 'Level Questions'); ?>
<?php $__env->startSection('middle_content'); ?>

  <!-- Content -->
  <div class="layout-content" data-scrollable>
      <div class="container-fluid">
	
		    <div class="question_wrap">
		
    			<div class="ques_head">
    			    <div class="row">
    			        <div class="col-lg-8 col-md-12">
    			            <div class="intro_leftbx">
                                <h3><?php echo e($levelDetail->title); ?></h3>
                                <p><?php echo e($levelDetail->description); ?>

                                </p>
                            <a href="javascript:void();" id="lets_start" level-id="<?php echo e($subLevelId); ?>" class="btn btn-info btn-md">Let's Start</a>
    			            </div>
    			        </div>
    			        <div class="col-lg-4 d_none">
    			            <div class="intro_rightimg">
    			                <img src="<?php echo e(asset('img/styudy-boy1.jpg')); ?>" alt="image">
    			            </div>
    			        </div>
    			    </div>
    			</div>

          <input type="hidden" id="all-prev-ids" value="<?php echo e($ques->id); ?>">
          
          <div id="question-data"></div>
              
                <?php if(Auth::user()->type == 'student'): ?>
                
                  <audio autoplay controlsList="nodownload" loop="" id="vid">
                      <source src="<?php echo e(asset('/audio/AtPeace.wav')); ?>" type="audio/wav">
                  </audio>
                <?php else: ?>
                  <div class="audio_player" >
                    <audio controls autoplay controlsList="nodownload" loop="" id="vid">
                        <source src="<?php echo e(asset('/audio/AtPeace.wav')); ?>" type="audio/wav">
                    </audio>
                  </div>
                <?php endif; ?>
              
		    </div>
      </div>
  </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!DOCTYPE html>
<html class="bootstrap-layout">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Dashboard</title>

  <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
  <meta name="robots" content="noindex">

  <!-- Material Design Icons  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Roboto Web Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- App CSS -->
  <link type="text/css" href="<?php echo e(asset('/frontend/assets/css/style.min.css')); ?>" rel="stylesheet">
  <link type="text/css" href="<?php echo e(asset('/frontend/assets/css/style.css')); ?>" rel="stylesheet">
  <link type="text/css" href="<?php echo e(asset('/frontend/assets/css/main.css')); ?>" rel="stylesheet">
  
</head>

  <body class="layout-container ls-top-navbar si-l3-md-up">

  <!-- Navbar -->
  <nav class="navbar navbar-dark bg-primary navbar-full navbar-fixed-top">

    <!-- Toggle sidebar -->
    <button class="navbar-toggler pull-xs-left" type="button" data-toggle="sidebar" data-target="#sidebarLeft">
        <span class="material-icons">menu</span>
    </button>

    <!-- Brand -->
    <a href="<?php echo e(url('/front')); ?>" class="navbar-brand">
        <!--<i class="material-icons">school</i>--> 
        <img src="<?php echo e(asset('image/ltlogo2.png')); ?>" height="60" width="150"></a>

    <!-- Search -->
    <!-- <form class="form-inline pull-xs-left hidden-sm-down">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search">
        <span class="input-group-btn"><button class="btn" type="button"><i class="material-icons">search</i></button></span>
      </div>
    </form> -->
    <!-- // END Search -->

    <ul class="nav navbar-nav hidden-sm-down">

      <!-- Menu -->
    <!--   <li class="nav-item">
        <a class="nav-link" href="forum.html">Forum</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="get-help.html">Get Help</a>
      </li> -->
    </ul>

    <!-- Menu -->
    <ul class="nav navbar-nav pull-xs-right">

      <!-- <li class="nav-item">
        <a href="cart.html" class="nav-link">
          <i class="material-icons">shopping_cart</i>
        </a>
      </li> -->

      <!-- User dropdown -->
      
      
      <li class="nav-item dropdown data_drop">
        <a class="nav-link active dropdown-toggle p-a-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false">
          <img src="<?php echo e(asset('/img/avatar-1577909_960_720.png')); ?>" alt="Avatar" class="img-circle" width="40">
        </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-list" style="display: none;" aria-labelledby="Preview">
          <!-- <a class="dropdown-item" href="account-edit.html"><i class="material-icons md-18">lock</i> <span class="icon-text">Edit Account</span></a>
          <a class="dropdown-item" href="profile.html"><i class="material-icons md-18">person</i> <span class="icon-text">Public Profile</span></a> -->
          <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
          <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
        </div>
      </li>
      <!-- // END User dropdown -->

    </ul>
    <!-- // END Menu -->

  </nav>
  <!-- // END Navbar -->
  <!-- Sidebar -->
  <div class="sidebar sidebar-left sidebar-light sidebar-visible-md-up si-si-3 ls-top-navbar-xs-up sidebar-transparent-md" id="sidebarLeft" data-scrollable>
    
    
    <div class="sidebar-heading">Student</div>
    <ul class="sidebar-menu">
      
      <!-- <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?php echo e(url('/front')); ?>">
          <i class="sidebar-menu-icon material-icons">school</i> My Courses
        </a>
      </li> -->
      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?php echo e(url('/students')); ?>">
          <i class="sidebar-menu-icon material-icons">school</i> Students
        </a>
      </li>
    </ul>
    <!-- Components menu -->
    
    <!-- // END Components Menu -->
  </div>
  <!-- // END Sidebar -->

    <?php echo $__env->yieldContent("middle_content"); ?>

    <script src="<?php echo e(asset('/frontend/assets/vendor/jquery.min.js')); ?>"></script>

    <script src="<?php echo e(asset('/frontend/assets/vendor/tether.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/frontend/assets/vendor/bootstrap.min.js')); ?>"></script>

    <script src="<?php echo e(asset('/frontend/assets/vendor/adminplus.js')); ?>"></script>

    <script src="<?php echo e(asset('/frontend/assets/js/main.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/frontend/assets/js/edu.js')); ?>"></script>
    <script src="<?php echo e(asset('/frontend/assets/js/frontend.js')); ?>"></script>
    <script>
       $(document).ready(function(){
        $(".data_drop").click(function(){
           if ($('.dropdown-menu-list').css('display') == 'block'){
                $(".dropdown-menu-list").hide();
            }else{
                $(".dropdown-menu-list").show();
            }
            
          
        });
      });
    </script>
</body>

</html>
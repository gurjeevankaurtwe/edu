<?php $__env->startSection('content'); ?>

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Session Management<br><small>You can manage your entire registered sessions!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Users</li>
    <li><a href="<?php echo e(url('manage-session')); ?>">Manage Sessions</a></li>
</ul>
<!-- END Datatables Header -->

<!-- Datatables Content -->
<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> sessions</h2>
    </div>

    <?php if(session()->has('success')): ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session()->get('success')); ?>

    </div>
    <?php endif; ?>
    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center"><i class="gi gi-user"></i></th>
                    <th>Name</th>
                    <th>Teacher</th>
                    <th>Student</th>
                    <th>Subject</th>
                    <th>Session Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <?php if(Auth::user()->type!='teacher'): ?>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                    <?php endif; ?>
                    
                    <?php if(Auth::user()->type=='teacher'): ?>
                    <th>View</th>
                    <th class="text-center">Add Questions</th>
                    <?php endif; ?>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1
                ?>
                <?php $__currentLoopData = $all_sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $allUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $encodedUserId = encrypt($allUser->id) ;
                $teacher_data=DB::table('users')->where('id',$allUser->teacher_id)->first();
                $student_data=DB::table('users')->where('id',$allUser->student_id)->first();
                $subject_data=DB::table('subjects')->where('id',$allUser->subject_id)->first();
                $session_days=DB::table('session_days')->where('session_id',$allUser->id)->get();
                ?>
                <tr>
                    <td class="text-center"><?php echo e($i); ?></td>
                    <td class="text-center"><img src="<?php echo e(asset('img/placeholders/avatars/avatar12.jpg')); ?>" alt="avatar" class="img-circle"></td>
                    <td><a href="javascript:void(0)"><?php echo e($allUser->name); ?></a></td>
                    <td><?php echo e(ucwords($teacher_data->name)); ?></td>
                    <td><?php echo e(ucwords($student_data->name)); ?></td>
                    <td><?php echo e(ucwords($subject_data->name)); ?></td>
                    <td><?php echo e($allUser->session_date); ?></td>
                    <td><?php echo e($allUser->session_start_time); ?></td>
                    <td><?php echo e($allUser->session_end_time); ?></td>
                    <?php if(Auth::user()->type!='teacher'): ?>
                    <td>
                        <span title="click to change status" onclick="window.location.href='<?php echo e(url('admin/update-session-status/'.$encodedUserId)); ?>'" class="label <?php echo e($allUser->status=='1' ? 'label-success' : 'label-danger'); ?>"><?php echo e($allUser->status=='1' ? 'Click to Deactive' : 'Click to Activate'); ?></span>
                    </td>  
                    <td class="text-center">  
                        <div class="btn-group">
                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/edit-session/'.$encodedUserId)); ?>'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/delete-session/'.$encodedUserId)); ?>'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                    <?php endif; ?>
                    
                    <?php if(Auth::user()->type=='teacher'): ?>
                    <td>
                        <span title="click to view" onclick="window.location.href='<?php echo e(url('admin/manage-questions/'.$encodedUserId)); ?>'" class="label label-success">View Questions</span>
                    </td>  
                    <td class="text-center">  
                        <span title="click to add question" onclick="window.location.href='<?php echo e(url('admin/add-questions/'.$encodedUserId)); ?>'" class="label label-success">Add Questions</span>
                    </td>
                    <?php endif; ?>
                </tr>
                 <?php $i++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
<!-- END Datatables Content -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
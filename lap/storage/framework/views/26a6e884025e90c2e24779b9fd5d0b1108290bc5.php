<?php $__env->startSection('middle_content'); ?>

		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Edit Trainer
					</h3>
				</div>
				<div class="course_wrap">
					<div id="btnContainer" class='text-right my-3'>
	        <a href="<?php echo e(url('/teachers')); ?>"><button class="btn btn-primary">Back</button></a>
	    </div>
			<form action="<?php echo e(url('/add-teacher')); ?>" method="post" autocomplete="off">
	        	<?php echo csrf_field(); ?>

	        	<input type="hidden" value="<?php echo e($teaDetail->id); ?>" name="id">
	        	
			  	<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputPassword4">Email</label>
				      	<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo e($teaDetail->email); ?>" required>
				      	<span class="text-danger"><?php echo e($errors->first('email')); ?></span>
				    </div>
				    
				    <div class="form-group col-md-6">
					    <label for="inputAddress2">Password</label>
					    <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo e($teaDetail->password_hint); ?>" min="5" required>
					    <span class="text-danger"><?php echo e($errors->first('password')); ?></span>
				  	</div>
				  	
			  	</div>
			  	<div class="form-row">
				    <div class="form-group col-md-6">
					    <label for="inputAddress">First Name</label>
					    <input type="text" class="form-control" name="name" placeholder="First Name" value="<?php echo e($teaDetail->name); ?>" required>
					    <span class="text-danger"><?php echo e($errors->first('name')); ?></span>
				  	</div>
				  	

				  	<div class="form-group col-md-6">
					    <label for="inputAddress">Last Name</label>
					    <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="<?php echo e($teaDetail->last_name); ?>" required>
					    <span class="text-danger"><?php echo e($errors->first('last_name')); ?></span>
				  	</div>
				  	
				    
			  	</div>
			  	<div class="form-row">
			  		<div class="form-group col-md-6">
					    <label for="inputAddress2">Date of Birth</label>
					    <input type="text" class="form-control trainer-datepicker" name="dob" value="<?php echo e($teaDetail->dob); ?>" placeholder="Date of Birth" required>
					    <span class="text-danger"><?php echo e($errors->first('dob')); ?></span>
				  	</div>
				</div>
				<div class="text-center my-3">
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>									
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
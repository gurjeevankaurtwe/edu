<?php $__env->startSection('middle_content'); ?>

		<div class="layout-content" data-scrollable>

      	<div id="btnContainer" class='pull-right'>
	        <a href="<?php echo e(url('add-students')); ?>"><button class="btn btn-primary">Back</button></a>
	    </div>
      	<br>
		<div class="container-fluid">
	      
	        <form action="<?php echo e(url('/add-students')); ?>" method="post" autocomplete="off">
	        	<?php echo csrf_field(); ?>
			  	<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputEmail4">Student Refrence Code</label>
				      	<input type="text" class="form-control" name="reference_code" placeholder="Reference Code" required>
				    </div>
				    <div class="form-group col-md-6">
				      	<label for="inputPassword4">Email</label>
				      	<input type="email" class="form-control" name="email" placeholder="Email" required>
				    </div>
			  	</div>
			  	<div class="form-row">
				    <div class="form-group col-md-6">
					    <label for="inputAddress">Name</label>
					    <input type="text" class="form-control" name="name" placeholder="Name" required>
				  	</div>
				    <div class="form-group col-md-6">
					    <label for="inputAddress2">Age</label>
					    <input type="text" class="form-control" name="age" placeholder="Age" required>
				  	</div>
			  	</div>
			  	<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputCity">Grade or N/A</label>
				      	<input type="text" class="form-control" name="grade" placeholder="Grade or N/A" required>
				    </div>
				    <div class="form-group col-md-4">
				      	<label for="inputState">Gender</label>
				      	<select name="gender" class="form-control" name="gender" required> 
					        <option value="">Select Gender...</option>
					        <option value="Female">Female</option>
					        <option value="male">Male</option>
				      	</select>
				    </div>
				    <div class="form-group col-md-2">
				      <label for="inputZip">Setup Timing</label>
				      <input type="text" class="form-control" name="timing" placeholder="Timing" required>
				    </div>
			  	</div>
			  	<button type="submit" class="btn btn-primary">Add</button>
			</form>
	    </div>
  	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
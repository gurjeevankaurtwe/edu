<?php $__env->startSection('title', 'Guest User'); ?>
<?php $__env->startSection('middle_content'); ?>

	<div class="login_wrapper">
	
		<div class="login_banner">
			<div class="carousel slide" data-ride="carousel" data-interval="4000">
				<div class="carousel-inner">
					<div class="carousel-item active" style="background-image: url('<?php echo url('/public/login-img/11.jpg');?>')"></div>
					<div class="carousel-item" style="background-image: url('<?php echo url('/public/login-img/13.jpg');?>')"></div>
					<div class="carousel-item" style="background-image: url('<?php echo url('/public/login-img/14.jpg');?>')"></div>
				</div>
			</div>
		</div>


		
		<div class="login_box">
                    <p><img src="<?php echo e(url('/public/image/ltlogo2.png')); ?>" height="150" width="200"></p>
                    <?php if(session()->has('success')): ?>
			    <div class="alert alert-success">
			        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			        <?php echo e(session()->get('success')); ?>

			    </div>
		    <?php endif; ?>
			<form method="POST" action="<?php echo e(url('/demo')); ?>" aria-label="<?php echo e(__('Login')); ?>">
                <?php echo csrf_field(); ?>
                <div class="form-group">
					<select class="form-control" name="title" autofocus> 
				        <option value="">Select Title...</option>
				        <option value="Dr.">Dr.</option>
				        <option value="Mr.">Mr.</option>
				        <option value="Mrs.">Mrs.</option>
				        <option value="Miss.">Miss.</option>
				    </select>
				     <?php if($errors->has('title')): ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($errors->first('title')); ?></strong>
                        </span>
                    <?php endif; ?>
				</div>
				<div class="form-group">
					<input id="name" type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" placeholder="First Name" value="<?php echo e(old('name')); ?>" required autofocus>

                    <?php if($errors->has('name')): ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($errors->first('name')); ?></strong>
                        </span>
                    <?php endif; ?>
				</div>
				<div class="form-group">
					<input id="last_name" type="text" class="form-control<?php echo e($errors->has('last_name') ? ' is-invalid' : ''); ?>" name="last_name" placeholder="Last Name" value="<?php echo e(old('last_name')); ?>" autofocus>

                    <?php if($errors->has('last_name')): ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($errors->first('last_name')); ?></strong>
                        </span>
                    <?php endif; ?>
				</div>
				<div class="form-group">
					<input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" placeholder="Email" value="<?php echo e(old('email')); ?>" required autofocus>

                    <?php if($errors->has('email')): ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
				</div>
				<div class="form-group">
					<input id="company_name" type="text" class="form-control<?php echo e($errors->has('company_name') ? ' is-invalid' : ''); ?>" name="company_name" placeholder="Company Name" value="<?php echo e(old('company_name')); ?>" autofocus>

                    <?php if($errors->has('company_name')): ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($errors->first('company_name')); ?></strong>
                        </span>
                    <?php endif; ?>
				</div>

				<div class="form-group">
					<input id="phone" type="text" class="form-control<?php echo e($errors->has('phone') ? ' is-invalid' : ''); ?>" name="phone" placeholder="Phone" value="<?php echo e(old('phone')); ?>" autofocus>

                    <?php if($errors->has('phone')): ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($errors->first('phone')); ?></strong>
                        </span>
                    <?php endif; ?>
				</div>
				<div class="text-center btn_optn">
					<button type="submit" class="btn btn-info btn-md btn-block btn_design">Submit</button>
				</div>
			</form>
		</div>
	
	</div>
	
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.include.login_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
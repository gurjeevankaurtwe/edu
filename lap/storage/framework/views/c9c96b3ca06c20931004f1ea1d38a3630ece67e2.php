<div class="ques_box">
	<?php if($ques != ''): ?>
        <div class="ques_panel">
            <?php if($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='26'): ?>
                <div class="with-flash">
                    <?php
                        $answer = $ques->answer;
                        if($answer == 'option1')
                        {
                            $ans = $ques->option1;
                        }
                        else if($answer == 'option2')
                        {
                            $ans = $ques->option2;
                        }
                        else if($answer == 'option3')
                        {
                            $ans = $ques->option3;
                        }
                    ?>
                    <div class="ans">
                        <div class="single">
                            <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                            <p class="fade-out"><?php echo e($ans); ?></p>
                            </a>
                        </div>
                    </div>
                    <?php if($subLevelId=='26'): ?>
                        <div class="player go-button" style="display:none">
                            <button type="button" class="btn btn-success repeat-flash-word">Repeat</button>
                            <button type="button" class="btn btn-success get-question-detail">Next</button>
                        </div>
                    <?php else: ?>
                        <div class="player go-button" style="display:none">
                            <button type="button" class="btn btn-success get-question-detail">Go</button>
                        </div>  
                    <?php endif; ?>
                </div>
            <?php elseif($subLevelId=='12' || $subLevelId=='31' || $subLevelId=='33' || $subLevelId=='53'): ?>
            <div class="with-flash">
                <div class="ans">
                    <div class="single">
                        <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                        <p class="fade-out"><?php echo e($ques->option1); ?></p>
                        </a>
                    </div>
                </div>
                <div class="player go-button" style="display:none">
                    <button type="button" class="btn btn-success repeat-flash-word">Repeat</button>
                </div>  
            </div>
            <?php endif; ?>
            <div class="without-flash" <?php if($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='12' || $subLevelId=='26' || $subLevelId=='31' || $subLevelId=='33' || $subLevelId=='53'): ?> style="display:none;" <?php endif; ?>>
                <?php if($subLevelId=='45' || $subLevelId=='13'): ?>
                    <?php
                        $explodeblank = explode("_",$ques->option1);
                        if($explodeblank[0] == '')
                        {
                            $whole_word = '<span class="blank_space_val">_</span>'.$explodeblank[1];
                        }
                        elseif($explodeblank[1] == '')
                        {
                            $whole_word = $explodeblank[0].'<span class="blank_space_val">_</span>';
                        }
                        elseif($explodeblank[0] != '' && $explodeblank[1] != '')
                        {
                            $whole_word = $explodeblank[0].'<span class="blank_space_val">_</span>'.$explodeblank[1];
                        }
                    ?>
                    <div class="ans">
                        <div class="single">
                            <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                            <p><?php echo $whole_word;?></p>
                            </a>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="ans">
                        <div class="single">
                            <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                            <p><?php echo e($ques->option1); ?></p>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="ans">
                    <div class="single">
                        <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                        <p><?php echo e($ques->option2); ?></p>
                        </a>
                    </div>
                </div>
                <div class="ans">
                    <div class="single">
                        <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                        <p><?php echo e($ques->option3); ?></p>
                        </a>
                    </div>
                </div>
                <div class="ans">
                    <div class="single">
                        <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
                        <p><?php echo e($ques->option4); ?></p>
                        </a>
                    </div>
                </div>
                <?php if($subLevelId=='45' || $subLevelId=='13'): ?>
                    <div class="ans">
                        <div class="single">
                            <?php
                                $explode_ans = explode(",",$ques->answer);
                                $startVal = key($explode_ans);
                                $lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
                                for($i=$startVal; $i<=$lastVal;$i++){
                            ?>
                                    <a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="update_blank_space" style="Text-Decoration: None !important; ">
                                        <?php if($i != 0): ?>
                                            <p><?php echo e(','.$explode_ans[$i]); ?></p>
                                        <?php else: ?>
                                            <p><?php echo e($explode_ans[$i]); ?></p>
                                        <?php endif; ?>
                                    </a>
                                
                            <?php } ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if($subLevelId!='1'): ?>
                    <div class="player">
                        <?php if($ques->answer_record != ''): ?>
    						<button type="button" class="btn btn-success repeat_audio_btn pull-right" data-attr="<?php echo 'repeat_'.$ques->id;?>">Repeat</button>
            				<input type="hidden" class="<?php echo 'repeat_'.$ques->id;?>" value="<?php echo e(asset('/answer_record/'.$ques->answer_record)); ?>">
                            <audio class="repeat_audio" id="<?php echo 'repeat_'.$ques->id;?>"  controls>
                                <source src="<?php echo e(asset('/answer_record/'.$ques->answer_record)); ?>" type="audio/mp3">
                            </audio>
                        <?php else: ?>
                            <?php if($subLevelId=='45' || $subLevelId=='13'): ?>
                                <button type="button" class="btn btn-success repeat_blank_space" data-attr="<?php echo 'repeat_'.$ques->id;?>">Repeat</button>
                            <?php elseif($subLevelId!='17' && $subLevelId!='45' && $subLevelId!='57' && $subLevelId!='13'): ?>
                                <button type="button" class="btn btn-success repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>">Repeat</button>
                            <?php endif; ?>

                        <?php endif; ?>
                    </div>					
                <?php else: ?>
                    <?php if($subLevelId!='17'): ?>
                        <button type="button" class="btn btn-success repeat_text_btn pull-right" data-attr="<?php echo 'repeat_'.$ques->id;?>">Repeat</button>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="ques_btn next-prev-btn" <?php if($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='12' || $subLevelId=='31' || $subLevelId=='26' || $subLevelId=='33' || $subLevelId=='53'): ?> style="display:none;" <?php endif; ?>>
            <a href="#" class="prev" id="prev-question-level-func" sub-lev='<?php echo e($ques->sub_level_id); ?>' prev='<?php echo e($prevqueId); ?>'>Previous</a>
            <a href="#" id="next-question-level" class="next pull-right" sub-lev='<?php echo e($ques->sub_level_id); ?>' cur='<?php echo e($ques->id); ?>'>Next</a>
        </div>
        <br>
    <?php else: ?>
        <div class="ques_panel">
            <h3><span>Congrats!</span>Your level has been completed</h3>
        </div>
        <div class="ques_btn">

            <a href="<?php echo e(url('level_questions_/'.encrypt($subLevelId))); ?>" class="next pull-right">Redo</a>
            <a href="<?php echo e(url('front')); ?>" id="next-question-level2" class="next pull-right">Completed</a>
        </div><br><br>
    <?php endif; ?>
</div>

<script>
    $("#next-question-level").click(function()
    {
        var all_prev_ids=$("#all-prev-ids").val();
        var subLevel = $(this).attr('sub-lev');
        var currentQ = $(this).attr('cur');
        if(all_prev_ids == '')
        {
        	var all_prev=$("#all-prev-ids").val(currentQ);
        }
        else
        {
        	var all_prev=$("#all-prev-ids").val(all_prev_ids+','+currentQ);
        }
        
      	$.ajax({
            dataType: "html",
            url: "user/get-questions-next/" +subLevel+'/'+ currentQ+'/?all_prev='+$("#all-prev-ids").val(),
            success:function(data)
            {
            	$('#question-data').html(data);
                $("#lets_start").hide();
            }
        });
	});

    jQuery(document).off('click','#prev-question-level-func').on('click','#prev-question-level-func',function()
    {
        var all_prev_ids=$("#all-prev-ids").val();
        var subLevel = $(this).attr('sub-lev');
        var prevQ = $(this).attr('prev');
        $.ajax({
            dataType: "html",
            url: "user/get-questions-previous/" +subLevel+'/'+ prevQ+'/?allPrevId='+all_prev_ids,
           	success:function(data)
           	{
           		var allPrevIds = removeValue(all_prev_ids, prevQ);
           		var n = allPrevIds.lastIndexOf(",");

				var s1 = allPrevIds.substring(0, n);
				var s2 = allPrevIds.substring(n + 1);

				$("#all-prev-ids").val(allPrevIds);
           		$('#question-data').html(data);
           		jQuery('.prev').attr('prev',s2);
                $("#lets_start").hide();
           	}
        });
	});

	function removeValue(list, value) 
	{
	  	return list.replace(new RegExp(",?" + value + ",?"), function(match) 
	  	{
	      	var first_comma = match.charAt(0) === ',',second_comma;

	      	if (first_comma && (second_comma = match.charAt(match.length - 1) === ',')) 
	      	{
		        return ',';
	      	}
	      	return '';
	    });
	};

	
	$(document).ready(function() 
	{
        $('.repeat_audio_btn').click(function()
	    {
			var attrs = jQuery(this).attr('data-attr');
			var audioSrc = jQuery('.'+attrs).val();
			var audioElement = document.getElementById(attrs);
			audioElement.setAttribute('src', audioSrc);
			
			audioElement.addEventListener('ended', function() 
			{
				this.play();
			}, false);
			
			audioElement.addEventListener("canplay",function()
			{
				$("#length").text("Duration:" + audioElement.duration + " seconds");
				$("#source").text("Source:" + audioElement.src);
				$("#status").text("Status: Ready to play").css("color","green");
			});
			
			audioElement.addEventListener("timeupdate",function()
			{
				$("#currentTime").text("Current second:" + audioElement.currentTime);
			});
	    
	        audioElement.currentTime = 0;
			audioElement.play();
	    });
    
	    $('#pause').click(function() 
	    {
	        audioElement.pause();
	        $("#status").text("Status: Paused");
	    });
	    
	    $('#restart').click(function() 
	    {
	        audioElement.currentTime = 0;
	    });
	});

	var classHighlight = 'highlight';
	var $thumbs = $('.thumbnail').click(function(e) {
	    e.preventDefault();
	    $thumbs.removeClass(classHighlight);
	    $(this).addClass(classHighlight);
	});

    jQuery(document).on('click','.repeat_text_btn',function(){
        $('a').removeClass('highlight');
    });

    $('.fade-out').fadeOut(2000, function() {
        $('.go-button').css('display','block');
    });

    jQuery(document).on('click','.get-question-detail',function(){
        jQuery('.without-flash').css('display','block');
        jQuery('.next-prev-btn').css('display','block');
        jQuery('.with-flash').css('display','none');
    });

    jQuery(document).on('click','.repeat-flash-word',function(){
        jQuery('.go-button').css('display','none');
        $('.next-prev-btn').css('display','none');
        $('.fade-out').fadeIn( "slow", function() {
            $('.fade-out').fadeOut(2000, function() {
                $('.go-button').css('display','block');
                $('.next-prev-btn').css('display','block');
            });
        });
    });

    jQuery(document).on('click','.update_blank_space',function(){
        var value = jQuery(this).attr('data-attr');
        var trimVal = value.trim();
        jQuery('.blank_space_val').html(trimVal);
    });

    jQuery(document).on('click','.repeat_blank_space',function(){
        jQuery('.blank_space_val').html('_');
    });

</script>
<style>
	.highlight {
	    font-weight: bold;
	}
</style>
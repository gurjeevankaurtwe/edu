<?php $__env->startSection('title', 'Login'); ?>
<?php $__env->startSection('middle_content'); ?>
	
	<div class="login_wrapper">
	
		<div class="login_banner">
			<div class="carousel slide" data-ride="carousel" data-interval="4000">
				<div class="carousel-inner">
					<div class="carousel-item active" style="background-image: url('<?php echo url('/public/login-img/11.jpg');?>')"></div>
					<div class="carousel-item" style="background-image: url('<?php echo url('/public/login-img/13.jpg');?>')"></div>
					<div class="carousel-item" style="background-image: url('<?php echo url('/public/login-img/14.jpg');?>')"></div>
				</div>
			</div>
		</div>


		
		<div class="login_box">
                    <p><img src="<?php echo e(url('/public/image/ltlogo2.png')); ?>" height="150" width="200"></p>
                    <?php if(session()->has('success')): ?>
			    <div class="alert alert-success">
			        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			        <?php echo e(session()->get('success')); ?>

			    </div>
		    <?php endif; ?>
			<form method="POST" action="<?php echo e(route('login')); ?>" aria-label="<?php echo e(__('Login')); ?>">
                <?php echo csrf_field(); ?>
				<div class="form-group">
					<input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" placeholder="Email" value="<?php echo e(old('email')); ?>" required autofocus>

                    <?php if($errors->has('email')): ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
				</div>
				<div class="form-group">
					<input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" placeholder="Password" name="password" required>

                    <?php if($errors->has('password')): ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
				</div>
				<div class="text-left">
					<a href="javascript:void();" class="btn_link">Forgot Password?</a>
				</div>
				<div class="text-center btn_optn">
					<button type="submit" class="btn btn-info btn-md btn-block btn_design">Login</button>
				</div>
				<!-- <div class="text-center extra_optn">
					<p>New To <span>Edu-Theraputics</span></p>
					<p><a href="javascript:void();" class="btn_link">Sign Up</a></p>
				</div> -->
			</form>
		</div>
	
	</div>
	
	
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.include.login_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
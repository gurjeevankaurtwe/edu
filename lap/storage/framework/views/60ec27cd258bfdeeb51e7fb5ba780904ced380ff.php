<?php $__env->startSection('content'); ?>

<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Guest User Rating Management<br><small>You can manage your entire guest user rating!</small>
        </h1>
    </div>
</div>

<ul class="breadcrumb breadcrumb-top">
    <li>Guest User Rating</li>
    <li>
    	<a href="<?php echo e(url('admin/guest_users')); ?>">Manage Guest User Rating</a>
    </li>
</ul>

<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> guest user rating</h2>
    </div>

    <?php if(session()->has('success')): ?>
    
    	<div class="alert alert-success">
	        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        <?php echo e(session()->get('success')); ?>

	    </div>

    <?php endif; ?>

    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Level</th>
                    <th>Rating</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
            	<?php
                	$i = 1
                ?>
                <?php if(count($userRating) > 0): ?>
                    <?php $__currentLoopData = $userRating; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    	<tr>
    	                    <td class="text-center"><?php echo e($i); ?></td>
                            <td class="text-center"><?php echo e($val->level->title); ?></td>
    	                    <td class="text-center"><?php echo e($val->rating); ?></td>
    	                    <td class="text-center"><?php echo e(ucfirst($val->description)); ?></td>
                        </tr>
                     	<?php $i++; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
               
            </tbody>
        </table>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
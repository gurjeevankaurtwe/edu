<?php $__env->startSection('content'); ?>

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Question Management<br><small>You can add new question from here to submit this form!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Session</li>
    <li><a href="<?php echo e(url('admin/add-questions/'.encrypt($session_id))); ?>">Question Management</a></li>
</ul>
<!-- END Datatables Header -->
<div class="col-md-12">
    <!-- Form Validation Example Block -->
    <div class="block">
        <!-- Form Validation Example Title -->
        <div class="block-title">
            <h2><strong>Add</strong> Question</h2>
        </div>
        <!-- END Form Validation Example Title -->
        <?php if(session()->has('error')): ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo e(session()->get('error')); ?>

        </div>
        <?php endif; ?>
        <!-- Form Validation Example Content -->
        <form id="form-validation" action="" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
            <input type="hidden" name="url" value="<?php echo e(URL::previous()); ?>">
            <?php echo e(csrf_field()); ?>

            <fieldset>
                <legend><i class="fa fa-angle-right"></i> Question Info</legend>
                <div class="input_fields_wrap"> 
                    <?php if($session_id=='1'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[0][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Audio File <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[0][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[1][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[1][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[2][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[2][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[3][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[3][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[4][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[4][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    <?php endif; ?>
                    
                    <?php if($session_id=='2'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[0][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[0][option2_img]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[0][option3_img]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option4]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[0][option4_img]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[0][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Audio File <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[0][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[1][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[1][option2_img]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[1][option3_img]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option4]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[1][option4_img]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[1][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[1][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[2][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[2][option2_img]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[2][option3_img]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option4]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[2][option4_img]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[2][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[2][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[3][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[3][option2_img]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[3][option3_img]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option4]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[3][option4_img]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[3][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[3][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[4][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[4][option2_img]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[4][option3_img]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option4]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[4][option4_img]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[4][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[4][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    <?php endif; ?>
                    
                    <?php if($session_id=='3'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[0][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Audio File <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[0][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[1][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[1][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[2][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[2][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[3][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[3][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="data[4][option1_img]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[4][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    <?php endif; ?>

                    <?php if($session_id=='5'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if($session_id=='6'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[0][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Audio File <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[0][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[1][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[1][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[2][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[2][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[3][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[3][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[4][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[4][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    <?php endif; ?>
                    <?php if($session_id=='7'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[0][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Audio File <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[0][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[1][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[1][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[2][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[2][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[3][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[3][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[4][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[4][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    <?php endif; ?>

                <!-- Level 8 -->

                    <?php if($session_id=='8'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[0][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[1][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[2][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[3][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[4][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                    <option value="option4">Option 4</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                    </div>
                    <?php endif; ?>

                    <!-- Level 1.10 -->

                    <?php if($session_id=='10'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[0][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Audio File <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[0][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[1][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[1][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[2][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[2][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[3][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[3][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[4][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="data[4][answer_record]" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>

                    </div>
                    <?php endif; ?>

                    <!-- Level 11 -->

                    <?php if($session_id=='11'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[0][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[1][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[2][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[3][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[4][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                    </div>
                    <?php endif; ?>

                    <!--- Level 1.12 or Level 1.13 or Level 1.14---->

                    <?php if($session_id=='42' || $session_id == '43' || $session_id == '44' || $session_id == '52' || $session_id == '50' || $session_id == '55' || $session_id == '56' || $session_id == '62' || $session_id == '63' || $session_id == '14' || $session_id == '15' || $session_id == '22' || $session_id == '28' || $session_id == '29' || $session_id == '34' || $session_id == '36' || $session_id == '37' || $session_id == '19' || $session_id == '25'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option4]" class="form-control" placeholder="option 4">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                <?php endif; ?>
                    
                   <!-- Level 1.15 , Level 2.9 , Level 2.18-->

                    <?php if($session_id=='45' || $session_id=='20' || $session_id=='30' || $session_id=='35' || $session_id=='39'): ?>
                        <div>
                            <h2>Question 1</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                           
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 2</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 3</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 4</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 5</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                    <?php endif; ?>

                     <!-- Level 1.17 -->

                    <?php if($session_id=='47'): ?>
                        <div>
                            <h2>Question 1</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <select  id="val_username" name="data[0][answer]" class="form-control">
                                        <option value="">-- Please select answer--</option>
                                        <option value="option1">Option 1</option>
                                        <option value="option2">Option 2</option>
                                        <option value="option3">Option 3</option>
                                    </select>
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Audio File <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="file" id="val_username" name="data[0][answer_record]" class="form-control">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>

                        </div>
                        
                        <div>
                            <h2>Question 2</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                           
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option3]" class="form-control" placeholder="option 3">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <select  id="val_username" name="data[1][answer]" class="form-control">
                                        <option value="">-- Please select answer--</option>
                                        <option value="option1">Option 1</option>
                                        <option value="option2">Option 2</option>
                                        <option value="option3">Option 3</option>
                                    </select>
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="file" id="val_username" name="data[1][answer_record]" class="form-control">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>

                        </div>
                        
                        <div>
                            <h2>Question 3</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option3]" class="form-control" placeholder="option 3">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <select  id="val_username" name="data[2][answer]" class="form-control">
                                        <option value="">-- Please select answer--</option>
                                        <option value="option1">Option 1</option>
                                        <option value="option2">Option 2</option>
                                        <option value="option3">Option 3</option>
                                    </select>
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="file" id="val_username" name="data[2][answer_record]" class="form-control">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>

                        </div>
                        
                        <div>
                            <h2>Question 4</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                           
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option3]" class="form-control" placeholder="option 3">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <select  id="val_username" name="data[3][answer]" class="form-control">
                                        <option value="">-- Please select answer--</option>
                                        <option value="option1">Option 1</option>
                                        <option value="option2">Option 2</option>
                                        <option value="option3">Option 3</option>
                                    </select>
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="file" id="val_username" name="data[3][answer_record]" class="form-control">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>

                        </div>
                        
                        <div>
                            <h2>Question 5</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option3]" class="form-control" placeholder="option 3">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <select  id="val_username" name="data[4][answer]" class="form-control">
                                        <option value="">-- Please select answer--</option>
                                        <option value="option1">Option 1</option>
                                        <option value="option2">Option 2</option>
                                        <option value="option3">Option 3</option>
                                    </select>
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer MP3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="file" id="val_username" name="data[4][answer_record]" class="form-control">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>

                        </div>
                    <?php endif; ?>

                    <!-- Level 1.23 -->

                    <?php if($session_id=='53'): ?>
                        <div>
                            <h2>Question 1</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 2</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 3</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 4</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 5</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- Level 1.27 -->

                    <?php if($session_id=='57'): ?>
                        <div>
                            <h2>Question 1</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 2</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 3</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 4</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 5</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!--- Level 2.1 --->

                    <?php if($session_id=='12' || $session_id=='32'): ?>
                        <div>
                            <h2>Question 1</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 2</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 3</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 4</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 5</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- Level 1.2 -->

                    <?php if($session_id=='13'): ?>
                        <div>
                            <h2>Question 1</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                           
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 2</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 3</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 4</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 5</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][answer]" class="form-control" placeholder="Answer">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!--- Level 2.6 --->

                    <?php if($session_id=='17' || $session_id=='38'): ?>
                        <div>
                            <h2>Question 1</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 2</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 3</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 4</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 5</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- Level 2.7 -->

                    <?php if($session_id=='18'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                       
                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option3]" class="form-control" placeholder="option 3">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    <?php endif; ?>

                    <!-- Level 2.15 -->

                    <?php if($session_id=='26'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[0][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[1][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[2][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                       
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[3][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select  id="val_username" name="data[4][answer]" class="form-control">
                                    <option value="">-- Please select answer--</option>
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                </select>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <!--- Level 2.19 --->

                    <?php if($session_id=='31'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <!--- Level 2.21 --->

                    <?php if($session_id=='33'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <!--- Level 2.29 --->

                    <?php if($session_id=='41'): ?>
                    <div>
                        <h2>Question 1</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 2</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 3</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 4</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    <div>
                        <h2>Question 5</h2>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <!-- Level 3.1 -->

                    <?php if($session_id=='59' || $session_id=='60' || $session_id=='61'): ?>
                        <div>
                            <h2>Question 1</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[0][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[0][option3]" class="form-control" placeholder="option 3">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[0][option4]" class="form-control" placeholder="option 4">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 5 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[0][option5]" class="form-control" placeholder="option 5">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 6 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[0][option6]" class="form-control" placeholder="option 6">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div>
                            <h2>Question 2</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                           
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[1][option3]" class="form-control" placeholder="option 3">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[1][option4]" class="form-control" placeholder="option 4">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                          
                           <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 5 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[1][option5]" class="form-control" placeholder="option 5">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 6 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[1][option6]" class="form-control" placeholder="option 6">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        <div>
                            <h2>Question 3</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[2][option3]" class="form-control" placeholder="option 3">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[2][option4]" class="form-control" placeholder="option 4">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                          
                           <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 5 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[2][option5]" class="form-control" placeholder="option 5">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 6 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[2][option6]" class="form-control" placeholder="option 6">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>

                           

                        </div>
                        
                        <div>
                            <h2>Question 4</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                           
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[3][option3]" class="form-control" placeholder="option 3">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[3][option4]" class="form-control" placeholder="option 4">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                          
                           <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 5 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[3][option5]" class="form-control" placeholder="option 5">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 6 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[3][option6]" class="form-control" placeholder="option 6">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>


                        </div>
                        
                        <div>
                            <h2>Question 5</h2>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][question]" class="form-control" placeholder="question">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option1]" class="form-control" placeholder="option 1">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option2]" class="form-control" placeholder="option 2">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            
                            <div class="form-group">
                            <label class="col-md-4 control-label" for="val_username">Option 3 <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" id="val_username" value="" name="data[4][option3]" class="form-control" placeholder="option 3">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 4 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[4][option4]" class="form-control" placeholder="option 4">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                          
                           <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 5 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[4][option5]" class="form-control" placeholder="option 5">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="val_username">Option 6 <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="val_username" value="" name="data[4][option6]" class="form-control" placeholder="option 6">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>


                        </div>
                    <?php endif; ?>
                </div>
                
                
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button id="submit_session" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                </div>
            </fieldset>

        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
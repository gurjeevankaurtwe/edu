<style>
	.middle_content{position: relative;}
	  .level_detail_wrap .middle_content{width:80%;margin:20px auto 20px;}
	.arrow-left.left {
	    position: absolute;
	    left: -50px;
	    font-size: 24px;
	    top: 45%;
	    color: #274f59;transition: all 0.3s ease-in;
	    text-align: center;
	    width: 40px;
	    line-height: 40px;
	    height: 40px;
	    border-radius: 50%;
	    border:2px solid #274f59;z-index: 100;
    cursor: pointer;
	}
.level_detail_wrap .middle_content .ans.multi_word p {
    font-size: 40px;
}
.level_detail_wrap .middle_content .ans p.fill_blank_cs{
	font-size: 40px;
	font-weight: bold;
}
.level_detail_wrap .middle_content .ans p.rythm_task_pulse {
    font-size: 40px;
}
.level_detail_wrap .middle_content .ans p {
    font-size: 80px ;
    letter-spacing: 3px;
}
.arrow-left.right {
    position: absolute;
    right: -50px;
    font-size: 24px;z-index: 100;
    cursor: pointer;
    top: 45%;
    color: #274f59;transition: all 0.3s ease-in;
    text-align: center;
    width: 40px;
    line-height: 40px;
    height: 40px;
    border-radius: 50%;
    border:2px solid  #274f59;
}.arrow-left.right:hover, .arrow-left.left:hover{background:#274f59; color:#fff;}
.middle_content2 .arrow-left.left {
	left: -50px;
	}
	.middle_content2 .arrow-left.right  {
	right: -50px;
	}
.level_detail_wrap .middle_content2{
  position: relative;
    width: 400px;
    height: 400px;
    min-height: 100%;
    margin: 20px auto;
  background:#fff !important;
    background-image: url('../public/frontend/images/others/round6.png') !important;
    background-repeat: no-repeat !important;
    background-size: contain !important;
    background-position: center;
    border:none !important;
}
.level_detail_wrap .middle_content2 .ques_box{text-align: center;
    position: relative;
    top: 15%;}
.msg.ques_panel{
margin-top:10px;
    }
.middle_content2 .msg.ques_panel{
margin-top:50px;text-align: center;
    }
.middle_content2 .msg.ques_panel img{max-width:70px;}
    .level_detail_wrap .middle_content2 .ans p, .level_detail_wrap .middle_content2 .middle_foot a{  color: #264e58; }
}
.level_detail_wrap .bottom_btn{margin-top:0px;}
.level_detail_wrap .middle_content .ans p{}.level_detail_wrap .middle_content2 .ans p{font-size: 110px;}.level_detail_wrap{padding:20px 30px;}
@media (max-width:1240px){
	.level_detail_wrap .middle_content .ans p.levl-sm{font-size: 30px !important;    word-break: break-all;}
	.level_detail_wrap .middle_content .ans.multi_word p{font-size: 22px;}
	.level_detail_wrap .middle_content .ans p.fill_blank_cs, .level_detail_wrap .middle_content .ans p.rythm_task_pulse {font-size: 20px;    font-weight: bold ;}
	.highlight_first a, .highlight_first p{letter-spacing: 4px !important;}
	.content, .content_right {width:175px !important;height:175px !important;}
}


@media (max-width:992px) and (min-width:767px){
    .level_detail_wrap .middle_content .ans .single2{
        width:100%;
    }.middle_content .single p.sm-font {
    font-size: 28px !important;line-height: 30px;
}
.level_detail_wrap .middle_content .ans p.levl-sm{font-size: 30px !important;    word-break: break-all;}
.level_detail_wrap .middle_content .ans.multi_word p{font-size: 23px;}
.level_detail_wrap .middle_content .ans p.fill_blank_cs, .level_detail_wrap .middle_content .ans p.rythm_task_pulse {font-size: 26px !important;    font-weight: bold ;}
.update_blank_space p{font-size: 26px !important;}  .highlight_first a, .highlight_first p{letter-spacing: 4px !important;}
.content, .content_right {width:175px !important;height:175px !important;}


}

@media (max-width:690px){
	.level_detail_wrap .middle_content .ans p, .level_detail_wrap .middle_content2 .middle_foot a {
    color: #264e58;
    font-size: 14px !important;
}
.level_detail_wrap .middle_content .ans .single2 a.rythmtask {
    margin-right: 8px;
}

}

@media (max-width:475px){
	.level_detail_wrap .middle_content .ans .single2 a.rythmtask {
    margin-right: 2px;
    }
    .level_detail_wrap .middle_content .ans p, .level_detail_wrap .middle_content2 .middle_foot a {
    color: #264e58;
    font-size: 12px !important;
    letter-spacing: 1px;
}
}
@media (max-width:400px){
	.level_detail_wrap .middle_content .ans .single2 a.rythmtask {
    margin-right: 2px;
    }
    .level_detail_wrap .middle_content .ans p, .level_detail_wrap .middle_content2 .middle_foot a {
    color: #264e58;
    font-size: 12px !important;
    letter-spacing: 1px;
}
.level_detail_wrap .middle_content {
    width: 100%;
    margin: 20px auto 20px;
}
.arrow-left.right {
    position: absolute;
    right: -43px;
    border: 0px solid #274f59;
}
}


@media (max-width:767px){
  .level_detail_wrap .middle_content2{
  width: 320px;
    height: 320px;
}
.level_detail_wrap .middle_content2 .bottom_btn{
	margin-top:30px;
}
.level_detail_wrap .middle_content2 .ans p{
	font-size:80px !important;
}
.level_detail_wrap .middle_content .ans p, 
.level_detail_wrap .middle_content2 .middle_foot a {
    color: #264e58;
    font-size: 19px;
}
.middle_content2 .arrow-left.right{right:-40px;}
.middle_content2 .arrow-left.left {
    left: -40px;
}.level_detail_wrap .middle_content .ans p, .level_detail_wrap .middle_content2 .middle_foot a {
    color: #264e58;
}.middle_content .single p{}
.middle_content2 .msg.ques_panel {
    margin-top: 50px;
}.level_detail_wrap .bottom_btn a{padding:6px 8px;margin: 0px 5px;}
.level_detail_wrap .middle_content .ans p.levl-sm{font-size: 30px !important;    word-break: break-all;}
.middle_content .single p.sm-font {
    font-size: 28px !important;line-height: 30px;
}.highlight_first a, .highlight_first p{letter-spacing: 4px !important;}
}
@media (max-width:420px){
  .level_detail_wrap .middle_content2{
   width: 180px;
    height: 180px;
}
.level_detail_wrap .middle_content2 .bottom_btn{
	margin-top:15px;
}
.level_detail_wrap .middle_content2 .ans p{
	font-size:60px !important;
}  
.level_detail_wrap .middle_content2 .ques_box{top:0%;}
.middle_content2 .msg.ques_panel{margin-top:20px;}
.msg.ques_panel h3{font-size:20px;}
.middle_content .single p.sm-font {
    font-size: 16px !important;
}.level_detail_wrap .bottom_btn{margin-top:0px;}
.level_detail_wrap .middle_content .ans p.levl-sm{font-size: 30px !important;    word-break: break-all;}
}
</style>
<?php 
	if($subLevelId == 64 || $subLevelId == 65 ||  $subLevelId == 66 ||  $subLevelId == 67 ||  $subLevelId == 68 ||  $subLevelId == 69 ||  $subLevelId == 70){
?>
		<div class="progress details_progressbar" style="display:none;">
		    <div class="progress-bar" style="width:<?php echo e($totalWidth); ?>%" aria-valuemax="100"></div>
		</div>
<?php }else{ ?>

		<div class="progress details_progressbar">
		    <div class="progress-bar" style="width:<?php echo e($totalWidth); ?>%" aria-valuemax="100"></div>
		</div>

<?php } ?>

<input type="hidden" id="allprevids" value="<?php echo e($prevqueId); ?>">

<?php

	if($subLevelId == 64 || $subLevelId == 65 ||  $subLevelId == 66)
	{
?>
		<div class="middle_content <?php if($subLevelId == 1 && $ques->session_id != 0): ?> <?php if($ques != ''): ?> middle_content2 <?php endif; ?> <?php endif; ?>" >
			
			<div class="ques_box">
				<div class="ques_panel">
	        		<div class="overlay"></div>
						<div>
		        			<div class="ans">
					            <div class="single"></div>
					        </div>
					    </div>
					    <div class="player btn_opt text-center my-4  go-start-button">
						    <a href="javascript:void(0);" class="repeat-audio">Repeat</a>
						</div>
				</div>
			</div>
		</div>
		<div class="bottom_btn d-flex ques_btn">
        	
       		
	        <?php
	            

	                $latest_sub_level = DB::select("SELECT * FROM sub_levels WHERE id = $subLevelId");
	               
	        ?>
            		<a href="<?php echo e(url('levels_sub_levels/'.encrypt($latest_sub_level[0]->level_id).'?student='.encrypt($studentId))); ?>" id="next-question-level2" class="ml-auto">Go To Next Level</a>
       		
       		

       		<a href="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>" id="next-question-level2" class="ml-auto" style="display:none">Done</a>
      	
      	</div><br><br>
<?php 
	} else if($subLevelId == 67 ||  $subLevelId == 68 ||  $subLevelId == 69){
		
?>
		<div class="middle_content <?php if($subLevelId == 1 && $ques->session_id != 0): ?> <?php if($ques != ''): ?> middle_content2 <?php endif; ?> <?php endif; ?>" >
			
			<div class="ques_box">
				<div class="ques_panel">
	        		<div class="overlay"></div>
						<div>
							<div class="ans">

					            <input type="hidden" class="stuId" value="<?php echo e($studentId); ?>">
					            <input type="hidden" class="subLevelId" value="<?php echo e($subLevelId); ?>">
					            <?php if($subLevelId == 67): ?>
					            	<input type="hidden" class="earType" value="right">
					            	<input type="hidden" class="key" value="m">
					            	<audio src="<?php echo e(url('/public/right.wav')); ?>" id="my_metronome" loop="loop"></audio>

					            <?php endif; ?>
					            <?php if($subLevelId == 68): ?>
					            	<input type="hidden" class="earType" value="left">
					            	<input type="hidden" class="key" value="z">
					            	<audio src="<?php echo e(url('/public/left.wav')); ?>" id="my_metronome" loop="loop"></audio>
					            <?php endif; ?>
					            <?php if($subLevelId == 69): ?>
					            	<input type="hidden" class="earType" value="">
					            	<input type="hidden" class="key" value="">
					            	<audio src="" id="my_metronome" loop="loop"></audio>
					            <?php endif; ?>
					            
					            <input type="hidden" class="site_url" value="<?php echo e(url('/')); ?>">
					            
					            <div class="totalScore single" style="display:none">
									<h5>Total No Response 	:  <span class="no_response">3</span></h3>
									<h5>Total Anticipation  :  <span class="anti">3</span></h3>
									<h5>Total Response 		:  <span class="response">3</span></h3>
									<h5>Average Response Time 		:  <span class="average_score">3</span></h3>
								</div>
					        </div>
					    </div>
					   
						<br><br><br>
						
						
				</div>
			</div>
		</div>
		<div class="bottom_btn d-flex ques_btn">
        	
       		
	        <?php
	           

	                $latest_sub_level = DB::select("SELECT * FROM sub_levels WHERE id = $subLevelId");
	               
	        ?>
            		
       		
       		<a href="javascript:void(0)" id="don_tempo_trainer" class="ml-auto" style="display:none">Done</a>
       		<a href="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>" class="ml-auto">Back</a>
      	
      	</div><br><br>
		
		        		

<?php }elseif($subLevelId == 70){?>

	<style>
		.outer {
		    border: 3px solid #264E58;
		    display: flow-root;
		}
		.content {
		    width: 200px;
		    height: 200px;
		    border-radius: 50%;
		    float: left;
		    border: 2px solid #000;
		    margin: 8px 30px;
		}
		.content_right {
		    width: 200px;
		    height: 200px;
		    border-radius: 50%;
		    float: right;
		    border: 2px solid #000;
		    margin: 8px 30px;
		}
	</style>
	
			
	<input type="hidden" class="stuId" value="<?php echo e($studentId); ?>">
    <input type="hidden" class="subLevelId" value="<?php echo e($subLevelId); ?>">
    <input type="hidden" class="key" value="">
    <input type="hidden" class="site_url" value="<?php echo e(url('/')); ?>">

	<div class="outer col-md-12">
	    <div class="content left">
	       
	    </div>
		<div class="content_right right">
	       
	    </div>
	    <div class="totalScore single" style="display:none">
			<h5>Total No Response 	:  <span class="no_response">3</span></h3>
			<h5>Total Anticipation  :  <span class="anti">3</span></h3>
			<h5>Total Response 		:  <span class="response">3</span></h3>
			<h5>Average Response Time 		:  <span class="average_score">3</span></h3>
		</div>
	</div>

	<div class="bottom_btn d-flex ques_btn">
        	
       		
        <?php
           

                $latest_sub_level = DB::select("SELECT * FROM sub_levels WHERE id = $subLevelId");
               
        ?>
        		
   		
   		<a href="javascript:void(0)" id="don_tempo_trainer" class="ml-auto" style="display:none">Done</a>&nbsp;&nbsp;
   		
   		<a class="ml-auto" href="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>">Back</a>
  	
  	</div><br><br>


<?php }else{ ?>
	
		<div class="middle_content <?php if($subLevelId == 1 && $ques->session_id != 0): ?> <?php if($ques != ''): ?> middle_content2 <?php endif; ?> <?php endif; ?>" >

			<!-- <div class="arrow-left left">
			    <i class="fa fa-angle-left"></i>
			</div> -->
			
			<!-- <?php if($subLevelId=='1'): ?>

				<div class="arrow-left right" id="next-question-level" data-attr="<?php echo e($ques->session_id); ?>" sub-lev='<?php echo e($ques->sub_level_id); ?>' cur='<?php echo e($ques->id); ?>'>
			    	<i class="fa fa-angle-right"></i>
				</div>

			<?php endif; ?> -->
			<!-- Next Previous -->

				
			

					<div class="bottom_btn d-md-flex ques_btn next-prev-btn" <?php if(($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='12' || $subLevelId=='31' || $subLevelId=='26' || $subLevelId=='33' || $subLevelId=='53' || $subLevelId=='19' || $subLevelId == '32' || $subLevelId == '35' || $subLevelId=='38' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='24' || $subLevelId=='54' || $subLevelId=='48') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?> style="display:none !important;" <?php endif; ?>>

						<?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>

							<div class="arrow-left right" id="next-question-level" data-attr="<?php echo e($ques->session_id); ?>" sub-lev='<?php echo e($ques->level_sub_level_id); ?>' cur='<?php echo e($ques->id); ?>'>
						    	<i class="fa fa-angle-right"></i>
							</div>

						<?php else: ?>

							<div class="arrow-left right" id="next-question-level" data-attr="<?php echo e($ques->session_id); ?>" sub-lev='<?php echo e($ques->sub_level_id); ?>' cur='<?php echo e($ques->id); ?>'>
						    	<i class="fa fa-angle-right"></i>
							</div>
						<?php endif; ?>
					</div>
				
				
			<div class="ques_box">
				<div class="ques_panel">
	        		<div class="overlay"></div>

	        			<?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
			        		<div class="with-flash">
			        			<div class="ans">
						            <div class="single">
        					                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
						                <p class="start-fade-out" style="letter-spacing: 3px;">
						                	<?php echo e($ques->option1); ?>

						                </p>
						                </a>
						            </div>
						        </div>
						    </div>
						    <div class="player btn_opt text-center my-4  go-start-button" style="display:none">
							    <a href="javascript:void(0);" class="get-question-detail">Start</a>
							</div>
		        		<?php endif; ?>



			    		<?php if(($subLevelId=='8' && $ques->session_id != 0) || ($subLevelId=='11' && $ques->session_id != 0) || $subLevelId=='48'): ?>
					    	<div class="with-flash">
					    		<?php
					    			if($subLevelId=='48')
					    			{
					    				$ans = $ques->option1;
					    			}
					    			else
					    			{
						    			$answer = $ques->answer;
						    			if($answer == 'option1')
						    			{
						    				$ans = $ques->option1;
						    			}
						    			else if($answer == 'option2')
						    			{
						    				$ans = $ques->option2;
						    			}
						    			else if($answer == 'option3')
						    			{
						    				$ans = $ques->option3;
						    			}
						    		}
					    		?>
					    		<div class="ans">
						            <div class="single">
						                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
						                <p class="fade-out" style="letter-spacing: 3px;">
						                	<?php echo e(strtolower($ans)); ?>

						                </p>
						                </a>
						            </div>
						        </div>
						        <?php if($subLevelId=='26' || $subLevelId=='24' || $subLevelId=='48'): ?>
						        	<div class="player btn_opt text-right my-4  go-button" style="display:none">
									    <a href="javascript:void(0);" class="repeat-flash-word">Repeat</a>
									    <?php if($subLevelId=='48'): ?>
									    	<a href="javascript:void(0);" class="next_three_blanks">Next</a>
									    <?php else: ?>
									    	<a href="javascript:void(0);" class="get-question-detail">Next</a>
									    <?php endif; ?>
									    
									</div>
						        <?php else: ?>
						        	<div class="btn_opt text-right my-4 player go-button" style="display:none">
									    <a href="javascript:void(0);" class="get-question-detail">Go</a>
									</div>	
						        <?php endif; ?>
						        
					    	</div>
				    	<?php elseif($subLevelId=='12' || $subLevelId=='31' || $subLevelId=='33' || $subLevelId=='53'): ?>
					    	<div class="with-flash">
					    		<div class="ans">
						            <div class="single">
						                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
						                <p class="fade-out" style="letter-spacing: 3px;">
						                	<?php if($subLevelId=='53'): ?>
			                                    <?php echo e($ques->option1); ?>

			                                <?php elseif($subLevelId=='12'): ?>
			                                	<?php echo e(ucfirst($ques->option1)); ?>

			                                <?php elseif($subLevelId=='33'): ?>
			                                	<?php echo e($ques->option1); ?>

			                                <?php else: ?>
			                                    <?php echo e(strtolower($ques->option1)); ?>

			                                <?php endif; ?>
						                </p>
						                </a>
						            </div>
						        </div>
						        <div class="btn_opt text-right my-4 player go-button" style="display:none">
			                        <a href="javascript:void(0);" class="repeat-flash-word">Repeat</a>
			                    </div>   
					    	</div>
				    	<?php elseif($subLevelId=='21'): ?>
					    	<div class="with-flash">
					    		<div class="ans">
						            <div class="single">
						                <a href="javascript:void(0)" class="thumbnail" style="Text-Decoration: None !important; ">
						                <p class="cluster_word" style="letter-spacing: 3px;">
						                	<?php echo e($ques->answer); ?>

			                            </p>
			                            <p class="cluster_second_word" style="letter-spacing: 3px;display:none;">
						                	<?php echo e($ques->answer); ?>

			                            </p>
						                </a>
						            </div>
						        </div>
						    </div>
					    <?php elseif($subLevelId=='19' || $subLevelId=='32' || $subLevelId=='38' || $subLevelId=='54'): ?>
					    	<div class="with-flash">
					    		<div class="btn_opt text-right my-4 player go-button">
					    			<?php if($subLevelId=='54'): ?>
					    				<a href="javascript:void(0);" class="get-question-detail">Next</a>
					    			<?php else: ?>
					    				<a href="javascript:void(0);" class="get-question-detail">Start</a>
					    			<?php endif; ?>
								    
								</div>	
					    	</div>
				    
			   		 	<?php elseif($subLevelId=='26' || $subLevelId=='24'): ?>
					    	<div class="with-flash-btn">
					    		<div class="btn_opt text-right my-4 player go-button">
								    <a href="javascript:void(0);" class="get-flash-detail">Start</a>
								</div>	
					    	</div>
					    	<div class="with-flash" style="display:none">
					    		<?php
					    			$answer = $ques->answer;
					    			if($subLevelId=='26')
					    			{
						    			if($answer == 'option1')
						    			{
						    				$ans = $ques->option1;
						    			}
						    			else if($answer == 'option2')
						    			{
						    				$ans = $ques->option2;
						    			}
						    			else if($answer == 'option3')
						    			{
						    				$ans = $ques->option3;
						    			}
						    		}
						    		else
						    		{
						    			$ans = $ques->option1;
						    		}
					    		?>
					    		<div class="ans">
						            <div class="single">
						                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">

						                <p class="fade-out-flash" style="letter-spacing: 3px;">
						                	<?php echo e(strtolower($ans)); ?>

						                </p>
						                </a>
						            </div>
						        </div>
						       
					        	<div class="player btn_opt text-right my-4  go-button" style="display:none">
								    <a href="javascript:void(0);" class="repeat-flash-word">Repeat</a>
								    <a href="javascript:void(0);" class="get-question-detail">Next</a>
								</div>
						    </div>
				    	<?php elseif($subLevelId=='25'): ?>
					    	<div class="without-flash">
					    		<div class="ans multi_word mb-4">
						            <div class="single single2">
								        <?php
								        	$explode_ans = explode(" ",$ques->option1);
								        	$startVal = key($explode_ans);
							        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
							        		for($i=$startVal; $i<=$lastVal;$i++){
							        	?>
							        			<a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail level_two" style="Text-Decoration: None !important; ">
							        				<?php if($i != 0): ?>
								                		<p><?php echo e(' '.$explode_ans[$i]); ?></p>
								                	<?php else: ?>
								                		<p><?php echo e($explode_ans[$i]); ?></p>
								                	<?php endif; ?>
									           	</a>
								        <?php } ?>
									</div>
						        </div>
						       	<div class="middle_foot d-flex justify-content-center align-items-center">
									<a href="javascript:void(0);" class="repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>" <?php if($subLevelId == 1): ?> <?php endif; ?>><i class="fas fa-redo-alt"></i> Repeat</a>
								</div>  
					    	</div>
				   	 	<?php endif; ?>

			    		<?php if($subLevelId=='42' || $subLevelId=='43' || $subLevelId=='44' || $subLevelId=='52' || $subLevelId == '50' || $subLevelId == '55' || $subLevelId == '56' || $subLevelId == '62' || $subLevelId == '63' || $subLevelId == '14' || $subLevelId == '15' || $subLevelId == '22' || $subLevelId == '28' || $subLevelId == '29' || $subLevelId == '34' || $subLevelId == '36' || $subLevelId == '37' || $subLevelId == '21'): ?>

				    		<div class="rhythm-task-option">
				    			<input type="hidden" class="rythm-task-type">
				    			
				    			<!-- <audio  controlsList="nodownload" loop="" id="my_metronome" style="width: 100%;">
			                      	<source src="<?php echo e(url('/public/audio/2SEC.mp3')); ?>" type="audio/wav">
			                  	</audio> -->
				    			<?php if($ques->option1 != ''): ?>
							    	<div class="ans">
							            <div class="single single2">
							                <?php
								        		$explode_ans = explode(" ",$ques->option1);
								        		$startVal = key($explode_ans);
								        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
								        		$j=0;
								        		for($i=$startVal; $i<=$lastVal;$i++){
								        	?>
								        			<a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail  pulse_bolde_<?php echo e($j); ?> multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> <?php if($subLevelId == '52' || $subLevelId == '44' || $subLevelId == '43'): ?> rythm-task  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
								        				<?php if($i != 0): ?>
									                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="font-weight:100;"><?php echo e(' '.$explode_ans[$i]); ?></p>
									                	<?php else: ?>
									                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="font-weight:100;"><?php echo e($explode_ans[$i]); ?></p>
									                	<?php endif; ?>
									                </a>
									            
											<?php $j++; } ?>
							            </div>
							            
							        </div>
							    <?php endif; ?>
							   	<?php if($ques->option2 != ''): ?>
							   		<div class="ans">
							            <div class="single single2">
							                <?php
								        		$explode_ans = explode(" ",$ques->option2);
								        		$startVal = key($explode_ans);
								        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
								        		if($subLevelId == '34' || $subLevelId == '37')
								        		{
								        			$j=3;
								        		}
								        		else
								        		{
								        			if(count($explode_ans) == 5)
									        		{
									        			$j=5;
									        		}
									        		else
									        		{
									        			$j = 4;
									        		}
								        		}
								        		
								        		
								        		
								        		for($i=$startVal; $i<=$lastVal;$i++){
								        	?>
								        			<a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail pulse_bolde_<?php echo e($j); ?> multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> <?php if($subLevelId == '52' || $subLevelId == '44' || $subLevelId == '43'): ?> rythm-task  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
								        				<?php if($i != 0): ?>
									                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="font-weight:100;"><?php echo e(' '.$explode_ans[$i]); ?></p>
									                	<?php else: ?>
									                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="font-weight:100;"><?php echo e($explode_ans[$i]); ?></p>
									                	<?php endif; ?>
									                </a>
									            
											<?php $j++; } ?>
							            </div>
							        </div>
							    <?php endif; ?>
							    <?php if($ques->option3 != ''): ?>
							    	<div class="ans">
							            <div class="single single2">
							                <?php
								        		$explode_ans = explode(" ",$ques->option3);
								        		$startVal = key($explode_ans);
								        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
								        		if($subLevelId == '34' || $subLevelId == '37')
								        		{
								        			$j=6;
								        		}
								        		else
								        		{
								        			if(count($explode_ans) == 5)
									        		{
									        			$j=10;
									        		}
									        		else
									        		{
									        			$j = 8;
									        		}
								        		}
								        		
								        		for($i=$startVal; $i<=$lastVal;$i++){
								        	?>
								        			<a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail pulse_bolde_<?php echo e($j); ?> multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> <?php if($subLevelId == '52' || $subLevelId == '44' || $subLevelId == '43'): ?> rythm-task  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
								        				<?php if($i != 0): ?>
									                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="font-weight:100;"><?php echo e(' '.$explode_ans[$i]); ?></p>
									                	<?php else: ?>
									                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="font-weight:100;"><?php echo e($explode_ans[$i]); ?></p>
									                	<?php endif; ?>
									                </a>
									            
											<?php $j++; } ?>
							            </div>
							        </div>
							    <?php endif; ?>
							    <?php if($ques->option4 != ''): ?>
							    	<div class="ans">
							            <div class="single single2">
							                <?php
								        		$explode_ans = explode(" ",$ques->option4);
								        		$startVal = key($explode_ans);
								        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
								        		if($subLevelId == '34' || $subLevelId == '37')
								        		{
								        			$j=9;
								        		}
								        		else
								        		{
								        			if(count($explode_ans) == 5)
									        		{
									        			$j=15;
									        		}
									        		else
									        		{
									        			$j = 12;
									        		}
								        		}
								        		
								        		for($i=$startVal; $i<=$lastVal;$i++){
								        	?>
								        			<a href="#" data-attr="<?php echo e($explode_ans[$i]); ?>" class="thumbnail pulse_bolde_<?php echo e($j); ?> multi_word <?php if($subLevelId == '21'): ?> clusterItems <?php endif; ?> <?php if($subLevelId != '21'): ?> rythmtask  <?php endif; ?> <?php if($subLevelId == '52' || $subLevelId == '44' || $subLevelId == '43'): ?> rythm-task  <?php endif; ?> mb-4" style="Text-Decoration: None !important;">
								        				<?php if($i != 0): ?>
									                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="font-weight:100;"><?php echo e(' '.$explode_ans[$i]); ?></p>
									                	<?php else: ?>
									                		<p class="rythm_task_pulse rythm_task_pulse_<?php echo e($j); ?>" style="font-weight:100;"><?php echo e($explode_ans[$i]); ?></p>
									                	<?php endif; ?>
									                </a>
									            
											<?php $j++; } ?>
							            </div>
							        </div>
							    <?php endif; ?>
							    <?php if($subLevelId=='21'): ?>
							    	<div class="middle_foot d-flex justify-content-center align-items-center">
							    		<a href="javascript:void(0);" class="repeat_cluster_word" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
							    	</div>
							    <?php endif; ?>
							</div>
				    	<?php endif; ?>
					    <div class="without-flash" <?php if(($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='43' || $subLevelId=='44' || $subLevelId=='42' || $subLevelId=='12' || $subLevelId=='26' || $subLevelId=='31' || $subLevelId=='33' || $subLevelId == '50' || $subLevelId=='53' || $subLevelId=='52' | $subLevelId=='55' | $subLevelId=='56' || $subLevelId == '62' || $subLevelId == '63' || $subLevelId == '14' || $subLevelId == '15' || $subLevelId == '22' || $subLevelId == '28' || $subLevelId == '29' || $subLevelId == '34' || $subLevelId == '36' || $subLevelId == '37' || $subLevelId == '19' || $subLevelId == '25' || $subLevelId == '32' || $subLevelId=='38' || $subLevelId=='24' || $subLevelId=='54' || $subLevelId=='48' || $subLevelId=='21' || $subLevelId=='64' || $subLevelId=='65') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?> style="display:none;" <?php endif; ?>>
							    	
					    	<?php if($subLevelId=='59' || $subLevelId=='60' || $subLevelId=='61'): ?>
						    	<div class="ans">
				                    <div class="single">
				                        <p class="levl-sm" style="color:black"><?php echo e($ques->question); ?></p>
				                        </a>
				                    </div>
					            </div>
					        <?php endif; ?>
							    	<?php if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='20' || $subLevelId=='30' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?>
						                <?php
						                	if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))
						                	{
						                		$explodeblank = explode("_",$ques->option2);
							                    if($explodeblank[0] == '')
							                    {
							                        $whole_word = '<span class="blank_space_val">_</span>'.$explodeblank[1];
							                    }
							                    elseif($explodeblank[1] == '')
							                    {
							                        $whole_word = $explodeblank[0].'<span class="blank_space_val">_</span>';
							                    }
							                    elseif($explodeblank[0] != '' && $explodeblank[1] != '')
							                    {
							                        $whole_word = $explodeblank[0].'<span class="blank_space_val">_</span>'.$explodeblank[1];
							                    }
						                	}
						                	else if($subLevelId=='49' || $subLevelId=='48' || $subLevelId=='23')
					                        {
					                            $whole_word = '<span class="blank_space_val_first">_</span>'.' '.'<span class="blank_space_val_two">_</span>'.' '.'<span class="blank_space_val_third">_</span>';
					                        }
					                        else
					                        {
					                        	if($subLevelId=='16')
					                        	{

					                        		$explodeWord = explode(" ",$ques->option1);

					                        		if(isset($explodeWord[0]) && isset($explodeWord[1]) && isset($explodeWord[2]) && !isset($explodeWord[3]))
					                        		{
					                        			if($explodeWord[0] != '_' && $explodeWord[1] == '_' && $explodeWord[2] == '_')
						                        		{
						                        			$whole_word = $explodeWord[0].'<span class="blank_space_val_first"> _</span><span class="blank_space_val_two"> _</span>';
						                        		}
						                        		if($explodeWord[0] != '_' && $explodeWord[1] == '_' && $explodeWord[2] == '_')
						                        		{
						                        			$whole_word = $explodeWord[0].'<span class="blank_space_val_first"> _</span><span class="blank_space_val_two"> _</span>';
						                        		}
						                        		elseif($explodeWord[0] == '_' && $explodeWord[1] == '_' && $explodeWord[2] != '_')
						                        		{
						                        			$whole_word = '<span class="blank_space_val_first"> _</span><span class="blank_space_val_two"> _ </span>'.$explodeWord[2];
						                        		}
						                        		elseif($explodeWord[0] != '_' && $explodeWord[1] == '_' && $explodeWord[2] != '_')
						                        		{
						                        			$whole_word = $explodeWord[0].'<span class="blank_space_val_first"> _ </span>'.$explodeWord[2];
						                        		}
					                        		}
					                        		elseif(isset($explodeWord[0]) && isset($explodeWord[1]) && isset($explodeWord[2]) && isset($explodeWord[3]))
					                        		{
					                        			$whole_word = $explodeWord[0].'<span class="blank_space_val_first"> _</span><span class="blank_space_val_two"> _</span><span class="blank_space_val_third"> _</span>';
					                        		}
					                        		else
					                        		{
														if($explodeWord[0] == '_' && $explodeWord[1] != '_')
					                                    {
					                                        $whole_word = '<span class="blank_space_val_first"> _ </span>'.$explodeWord[1];
					                                    }
						                        		
						                        		elseif($explodeWord[0] != '_' && $explodeWord[1] == '_')
						                        		{
						                        			$whole_word = $explodeWord[0].'<span class="blank_space_val_first"> _</span>';
						                        		}
					                        		}
					                        	}
					                        	else
					                        	{
					                        		$explodeblank = explode("_",$ques->option1);
								                    if($explodeblank[0] == '')
								                    {
								                        $whole_word = '<span class="blank_space_val"  style="letter-spacing: 3px;">_</span>'.$explodeblank[1];
								                    }
								                    elseif($explodeblank[1] == '')
								                    {
								                        $whole_word = $explodeblank[0].'<span class="blank_space_val" style="letter-spacing: 3px;">_</span>';
								                    }
								                    elseif($explodeblank[0] != '' && $explodeblank[1] != '')
								                    {
								                        $whole_word = $explodeblank[0].'<span class="blank_space_val"  style="letter-spacing: 3px;">_</span>'.$explodeblank[1];
								                    }
					                        	}
							                }
						                ?>
						                <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40'): ?>
							        		<div class="with-flash">
									    		<div class="btn_opt text-right my-4 player go-button">
												    <a href="javascript:void(0);" class="start-fill-blanks">Start</a>
												</div>	
									    	</div>
							        	<?php endif; ?>
						                <div class="ans" <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40'): ?> style="display:none" <?php endif; ?>>
						                    <div class="single">
						                        <a href="javascript:void(0)" style="Text-Decoration: None !important; ">
							                        <?php if($subLevelId=='16' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='45'): ?>
							                        	<p class="fill_blank_cs"><?php echo $whole_word;?></p>
							                        <?php else: ?>
						                        		<p class="fill_blank_cs"><?php echo ucfirst($whole_word);?></p>
						                        	<?php endif; ?>
												</a>
						                    </div>
						                </div>
						            <?php elseif($subLevelId=='24'): ?>
						            	<div class="ans multi_word mb-4">
						            		<div class="single">
						            			<a href="javascript:void(0)" style="Text-Decoration: None !important; ">
						                        <p class="levl-sm"><?php echo '<span class="blank_space_val">_</span>'.$ques->option1;?></p>
						                        </a>
						                    </div><br>
								            <div class="single single2">
								            	<?php
										        	$explode_ans = explode(" ",$ques->answer);
										        	$startVal = key($explode_ans);
									        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
									        		for($i=$startVal; $i<=$lastVal;$i++){
									        	?>
									        			<a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="update_blank_space" style="Text-Decoration: None !important;">
									        				<?php if($i != 0): ?>
										                		<p style="font-size:80px;"><?php echo e(' '.$explode_ans[$i]); ?></p>
										                	<?php else: ?>
										                		<p style="font-size:80px;"><?php echo e($explode_ans[$i]); ?></p>
										                	<?php endif; ?>
											           </a>
										            
												<?php } ?>
											</div>
								        </div>
								    <?php else: ?>
								    	<div class="ans">
								            <div class="single">
								            	<?php if($subLevelId=='18'): ?>
								            		<a href="javascript:void(0)" class="highlight_first" data-attr="0" style="Text-Decoration: None !important; ">
									               
										            	<p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
										                	<?php echo e(strtolower($ques->option1)); ?>

										                </p>
											           

									                </a>
								            	<?php else: ?>
									                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
									                <?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
										                <p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:110px">
										                	<?php echo e(strtolower($ques->option1)); ?>

										                </p>
										            <?php else: ?>
										            	<?php if($subLevelId == 51): ?>
											            	<p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
											                	<?php echo e($ques->option1); ?>

											                </p>
														<?php elseif($subLevelId == 1 && $ques->session_id != 0): ?>
											            	<p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
											                	<?php echo e($ques->option1); ?>

											                </p>
											            <?php elseif($subLevelId == 54): ?>
											            	<p class="levl-sm" <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
											                	<?php echo e($ques->option1); ?>

											                </p>
											            <?php elseif($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
											            	<p class="sm-font" <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:38px;">
											                	<?php echo e($ques->option1); ?>

											                </p>
											            <?php else: ?>
											            	<p class="levl-sm" <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
											                	<?php echo e(strtolower($ques->option1)); ?>

											                </p>
											            <?php endif; ?>
										            <?php endif; ?>

									                </a>
									            <?php endif; ?>
								            </div>
								        </div>
								    <?php endif; ?>
								    <?php if($ques->session_id != 0): ?>
								    	<?php if($subLevelId=='18'): ?>
								    		<div class="ans">
									            <div class="single">
								            		<a href="javascript:void(0)" class="highlight_first" data-attr="0" style="Text-Decoration: None !important; ">
									               
										            	<p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
										                	<?php echo e(strtolower($ques->option2)); ?>

										                </p>
											           

									                </a>
									            </div>
									        </div>
						            	<?php else: ?>
									        <div class="ans">
									            <div class="single">
									                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
									                	<?php if($subLevelId == 54): ?>
											            	<p class="levl-sm" style="letter-spacing: 3px;"><?php echo e($ques->option2); ?></p>
										            	<?php elseif($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
											            	<p class="sm-font" style="letter-spacing: 3px;font-size:38px;">
											                	<?php echo e($ques->option2); ?>

											                </p>
											            <?php else: ?>
											            	<p class="levl-sm" style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option2)); ?></p>
											            <?php endif; ?>
									                </a>
									            </div>
									        </div>
									    <?php endif; ?>
								    <?php endif; ?>
							        <div class="ans">
							        	<?php if($subLevelId=='18'): ?>
							        		<div class="ans">
									            <div class="single">
								            		<a href="javascript:void(0)" class="highlight_first" data-attr="0" style="Text-Decoration: None !important; ">
									               
										            	<p <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;">
										                	<?php echo e(strtolower($ques->option3)); ?>

										                </p>
											        </a>
											    </div>
											</div>
						            	<?php else: ?>
								            <div class="single">
								                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
								                <?php if($subLevelId == 54): ?>
									            	<p class="levl-sm" style="letter-spacing: 3px;"><?php echo e($ques->option3); ?></p>
									            <?php elseif($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
									            	<p class="sm-font" style="letter-spacing: 3px;font-size:38px;">
									                	<?php echo e($ques->option3); ?>

									                </p>
									            <?php else: ?>
									            	<?php if(($subLevelId == 1 && $ques->session_id == 0) || ($subLevelId == 2 && $ques->session_id == 0) || ($subLevelId == 3 && $ques->session_id == 0) || ($subLevelId == 4 && $ques->session_id == 0) || ($subLevelId == 8 && $ques->session_id == 0) || ($subLevelId == 7 && $ques->session_id == 0) || ($subLevelId == 9 && $ques->session_id == 0) || ($subLevelId == 10 && $ques->session_id == 0) || ($subLevelId == 11 && $ques->session_id == 0)): ?>
									            		<p class="levl-sm" style="letter-spacing: 3px;"></p>
									            	<?php else: ?>
									            		<p class="levl-sm" style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option3)); ?></p>
									            	<?php endif; ?>
									            <?php endif; ?>
								                </a>
								            </div>
								        <?php endif; ?>
							        </div>
							        <div class="ans">
							            <div class="single">
							                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
							                <?php if($subLevelId == 54): ?>
								            	<p class="levl-sm" style="letter-spacing: 3px;"><?php echo e($ques->option4); ?></p>
								            <?php elseif($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
								            	<p class="sm-font" style="letter-spacing: 3px;font-size:38px;">
								                	<?php echo e($ques->option4); ?>

								                </p>
								            <?php else: ?>
								            	<p class="levl-sm" style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option4)); ?></p>
								            <?php endif; ?>
							                </a>
							            </div>

							        </div>
							        <div class="ans">
							            <div class="single">
							                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
						                	<?php if($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
								            	<p class="sm-font" style="letter-spacing: 3px;font-size:38px;">
								                	<?php echo e($ques->option5); ?>

								                </p>
								            <?php else: ?>
							                	<p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option5)); ?></p>
							                <?php endif; ?>
							                </a>
							            </div>
							        </div>
							        <!-- <div class="ans">
							            <div class="single">
							                <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
							                <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option6)); ?></p>
							                </a>
							            </div>
							        </div> -->
							        <div class="ans">
					                    <div class="single">
					                        <a href="#" class="thumbnail" style="Text-Decoration: None !important; ">
					                        <?php if($subLevelId == 59 || $subLevelId == 60 || $subLevelId == 61): ?>
					                            <p class="sm-font" <?php if($subLevelId == 1): ?> <?php endif; ?> style="letter-spacing: 3px;font-size:38px;">
					                                <?php echo e($ques->option6); ?>

					                            </p>
					                        <?php else: ?>
					                            <p style="letter-spacing: 3px;"><?php echo e(strtolower($ques->option6)); ?></p>
					                        <?php endif; ?>
					                        </a>
					                    </div>
					                </div>
							        <?php if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='20' || $subLevelId=='30' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?>
							        	
							        	<div class="ans multi_word mb-4" <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='48' ): ?> style="display:none" <?php endif; ?>>
								            <div class="single single2">
										        <?php
										        	if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='48' || $subLevelId=='16'))
										        	{
										        		$explode_ans = explode(",",$ques->answer);
										        	}
										        	elseif(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))
										        	{
									                	$explode_ans = explode(",",$ques->answer);
									                }
										        	else
										        	{
										        		$explode_ans = explode(" ",$ques->answer);
										        	}

													$startVal = key($explode_ans);
									        		$lastVal = key( array_slice( $explode_ans, -1, 1, TRUE ) );
									        		for($i=$startVal; $i<=$lastVal;$i++){
									        	?>

									        	<?php if($subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23'): ?>
									        		<a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="update_three_space" style="Text-Decoration: None !important; ">
									        	<?php else: ?>
								        			<a href="javascript:void(0)" data-attr="<?php echo e($explode_ans[$i]); ?>" class="update_blank_space" style="Text-Decoration: None !important; ">
								        		<?php endif; ?>
							        				<?php if(($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='48' || $subLevelId=='16')): ?>
								        				
								        				<?php if($i != 0): ?>
									                		<p><?php echo e(','.$explode_ans[$i]); ?></p>
									                	<?php else: ?>
									                		<p><?php echo e($explode_ans[$i]); ?></p>
									                	<?php endif; ?>

									                <?php else: ?>

									                	<?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
											        	
										                	<?php if($i != 0): ?>
										                		<p><?php echo e(','.$explode_ans[$i]); ?></p>
										                	<?php else: ?>
										                		<p><?php echo e($explode_ans[$i]); ?></p>
										                	<?php endif; ?>
										                
											        	<?php else: ?>
											        	
											        		<?php if($i != 0): ?>
										                		<p><?php echo e(' '.$explode_ans[$i]); ?></p>
										                	<?php else: ?>
										                		<p><?php echo e($explode_ans[$i]); ?></p>
										                	<?php endif; ?>
											        	<?php endif; ?>
									               
									                	
									               	<?php endif; ?>
									            </a>
										            
												<?php } ?>
											</div>
								        </div>
							        <?php endif; ?>
							        <?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>
										<div class="middle_foot d-flex justify-content-center align-items-center">
											<a href="javascript:void(0);" class="repeat_blank_space" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
										</div>
							        <?php elseif($subLevelId!='1'): ?>
									    <div class="player">
									        <?php if($ques->answer_record != ''): ?>

									        	<!-- Repeat with audio -->

									        	<div class="middle_foot d-flex justify-content-center align-items-center">
													<a href="javascript:void(0);" class="repeat_audio_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>

													<input type="hidden" class="<?php echo 'repeat_'.$ques->id;?>" value="<?php echo e(asset('/answer_record/'.$ques->answer_record)); ?>">

													<audio class="repeat_audio ml-3" id="<?php echo 'repeat_'.$ques->id;?>"  controls>
													  	<source src="<?php echo e(asset('/answer_record/'.$ques->answer_record)); ?>" type="audio/mp3">
													</audio>
												</div>
											<?php else: ?>
									        <?php if($subLevelId=='45' || $subLevelId=='13' || $subLevelId=='20' || $subLevelId=='30' || $subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23' || $subLevelId=='21' || $subLevelId=='24'): ?>

									        		<!-- Repeat -->
									        		<div class="middle_foot d-flex justify-content-center align-items-center" <?php if($subLevelId=='35' || $subLevelId=='39' || $subLevelId=='40'): ?> style="display:none !important" <?php endif; ?>>
									        			<?php if($subLevelId=='49' || $subLevelId=='48' || $subLevelId=='16' || $subLevelId=='23'): ?>
									        				<!-- <?php if($subLevelId=='49'): ?>
										        				<?php if(Auth::user()->type == 'teacher' || Auth::user()->type == 'Institute' ): ?>
											        				<p class="three_blanks sit_blank" style="letter-spacing: 3px;">
													                	<?php echo e($ques->option1); ?>

										                            </p>
										                        <?php endif; ?>
										                     <?php endif; ?> -->

										                    <?php if($subLevelId=='23'): ?>
										                    	<a href="javascript:void(0);" class="repeat_three_space <?php if($subLevelId=='49'): ?> ml-auto <?php endif; ?>" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Next</a>
										                    <?php else: ?>
										                    	<a href="javascript:void(0);" class="repeat_three_space <?php if($subLevelId=='49'): ?> ml-auto <?php endif; ?>" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
										                    <?php endif; ?>

									        				
									        			<?php else: ?>

															<a href="javascript:void(0);" class="repeat_blank_space" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
														<?php endif; ?>
													</div>

									        	<?php elseif($subLevelId!='17' && $subLevelId!='45' && $subLevelId!='57' && $subLevelId!='13' && $subLevelId!='20' && $subLevelId!='30' && $subLevelId!='35' && $subLevelId!='39' && $subLevelId!='40' && $subLevelId!='49' && $subLevelId!='48' && $subLevelId!='16' && $subLevelId!='23' && $subLevelId!='24'): ?>

									        		<!-- Repeat -->

									        		<div class="middle_foot d-flex justify-content-center align-items-center">
														<a href="javascript:void(0);" class="repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>"><i class="fas fa-redo-alt"></i> Repeat</a>
													</div>
									         	<?php endif; ?>

									        <?php endif; ?>

									    </div>	
									
							        <?php else: ?>
							        	<?php if($subLevelId!='17' && ($subLevelId!='1' && $ques->session_id != 0)): ?>

							        		<!-- Repeat -->

							        		<div class="middle_foot d-flex justify-content-center align-items-center">
												<a href="javascript:void(0);" class="repeat_text_btn" data-attr="<?php echo 'repeat_'.$ques->id;?>" <?php if($subLevelId == 1): ?> <?php endif; ?>><i class="fas fa-redo-alt"></i> Repeat</a>
											</div>
						                <?php endif; ?>
							        <?php endif; ?>
							    </div>
					    	</div>
							<br>
						
						</div>
					</div>

				<!-- Next Previous -->

				<?php if($subLevelId!='1'): ?>
			

					<div class="bottom_btn d-flex ques_btn next-prev-btn" <?php if(($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='12' || $subLevelId=='31' || $subLevelId=='26' || $subLevelId=='33' || $subLevelId=='53' || $subLevelId=='19' || $subLevelId == '32' || $subLevelId == '35' || $subLevelId=='38' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='24' || $subLevelId=='54' || $subLevelId=='48') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?> style="display:none !important;" <?php endif; ?>>
						<!-- <a href="javascript:void(0);" class="prev">Previous</a> -->
						<?php if($ques->session_id == 0): ?>
							<a href="<?php echo e(url('sublevel_sublevels/'.encrypt($levels->sub_level_id).'?student='.encrypt($studentId))); ?>" class="ml-auto">Complete Later</a>
						<?php else: ?>
							<a href="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>" class="ml-auto">Complete Later</a>
						<?php endif; ?>

						<!-- <?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)): ?>

							<a href="javascript:void(0);" class="ml-auto" id="next-question-level" data-attr="<?php echo e($ques->session_id); ?>" sub-lev='<?php echo e($ques->level_sub_level_id); ?>' cur='<?php echo e($ques->id); ?>'>Next</a>

						<?php else: ?>
							<a href="javascript:void(0);" class="ml-auto" id="next-question-level" data-attr="<?php echo e($ques->session_id); ?>" sub-lev='<?php echo e($ques->sub_level_id); ?>' cur='<?php echo e($ques->id); ?>'>Next</a>
						<?php endif; ?> -->
					</div>
				<?php else: ?>
					<div class="bottom_btn d-flex ques_btn next-prev-btn" <?php if(($subLevelId=='8' || $subLevelId=='11' || $subLevelId=='12' || $subLevelId=='31' || $subLevelId=='26' || $subLevelId=='33' || $subLevelId=='53' || $subLevelId=='19' || $subLevelId == '32' || $subLevelId == '35' || $subLevelId=='38' || $subLevelId=='39' || $subLevelId=='40' || $subLevelId=='24' || $subLevelId=='54' || $subLevelId=='48') || (($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0))): ?> style="display:none !important;" <?php endif; ?>>
						<!-- <a href="javascript:void(0);" class="prev">Previous</a> -->
						<?php if($ques->session_id == 0): ?>
							<a href="<?php echo e(url('sublevel_sublevels/'.encrypt($levels->sub_level_id).'?student='.encrypt($studentId))); ?>" class="ml-auto">Complete Later</a>
						<?php else: ?>
							<a href="<?php echo e(url('levels_sub_levels/'.encrypt($levels->level_id).'?student='.encrypt($studentId))); ?>" class="ml-auto">Complete Later</a>
						<?php endif; ?>

						
					</div>
				<?php endif; ?>	
<?php } ?>

<script>
	
	$("#next-question-level").click(function()
    {
        jQuery(this).attr('disabled',true);
        jQuery(this).html('...');
        var subLevel = $(this).attr('sub-lev');
	    var currentQ = $(this).attr('cur');
	    var studentId = $('.studentId').val();
	    var all_prev_ids = $('#allprevids').val();
	    var sessionId = $(this).attr('data-attr');


	    if(all_prev_ids == '')
        {
        	$("#allprevids").val(currentQ);
        	var all_prev=$("#allprevids").val();
        }
        else
        {

        	$("#allprevids").val(all_prev_ids+','+currentQ);
        	var all_prev=($("#allprevids").val());
        }

        $.ajax({
	        dataType: "html",
	        url: "user/get-questions-next/" +subLevel+'/'+ currentQ+'/?all_prev='+all_prev+'&studentId='+studentId+'&sessionId='+sessionId,


	        success:function(data){
	        	jQuery(this).html('>');
	            jQuery(this).attr('disabled',false);
                jQuery(this).html('Next');
	            $('#question-data').html(data);
	            $("#lets_start").hide();
	        }
	    });
	});
	$(document).ready(function() 
	{
    	$('.repeat_audio_btn').click(function() 
    	{
    		$('a').removeClass('highlight');
			var attrs = jQuery(this).attr('data-attr');
			var audioSrc = jQuery('.'+attrs).val();
			var audioElement = document.getElementById(attrs);
			audioElement.setAttribute('src', audioSrc);
			
			audioElement.addEventListener('ended', function() 
			{
				this.play();
			}, false);
			
			audioElement.addEventListener("canplay",function()
			{
				$("#length").text("Duration:" + audioElement.duration + " seconds");
				$("#source").text("Source:" + audioElement.src);
				$("#status").text("Status: Ready to play").css("color","green");
			});
			
			audioElement.addEventListener("timeupdate",function()
			{
				$("#currentTime").text("Current second:" + audioElement.currentTime);
			});
	    
	        audioElement.currentTime = 0;
			audioElement.play();
	    });
    
	    $('#pause').click(function() 
	    {
	        audioElement.pause();
	        $("#status").text("Status: Paused");
    	});
    
	    $('#restart').click(function() 
	    {
	        audioElement.currentTime = 0;
	    });
	});

	jQuery(document).on('click','.highlight_first',function(){
		var attr = jQuery(this).attr('data-attr');
		if(attr == 0)
		{
			jQuery('.highlight_first').attr('data-attr',1);
			$(this).addClass(classHighlight);
		}
		if(attr == 1)
		{
			jQuery('.highlight_first').attr('data-attr',2);
			$(this).addClass(classHighlight);
		}
	});
	var classHighlight = 'highlight';
	var $thumbs = $('.thumbnail').click(function(e) 
	{
	    e.preventDefault();
	    $thumbs.removeClass(classHighlight);
	    $(this).addClass(classHighlight);
	});

	jQuery(document).on('click','.repeat_text_btn',function(){
		$('a').removeClass('highlight');
		jQuery('.highlight_first').attr('data-attr',0);
	});

	$('.fade-out').fadeOut(3000, function() {
        $('.go-button').css('display','block');
        $('.next-prev-btn').css('display','block');
    });

/*==============Level 9 fadeout option 1 like 1 ================*/

	function playMetro(counter,totalBeats)
	{
		var total  = counter+parseInt(1);
		if(total <= totalBeats)
		{
			setTimeout(function(){
			      		
	      		playMetro(total,totalBeats);
			    
	    	}, 1000);
			var audioSrc = '<?php echo url('/public/audio/2SEC.mp3'); ?>';

			var audioElement = document.getElementById('vid');
			audioElement.setAttribute('src', audioSrc);
			var playPromise = audioElement.play();
			if (playPromise !== undefined) 
			{
			    playPromise.then(_ => {
			    })
			    .catch(error => {
			    	
			    });
		  	}
		}

		var totals = parseInt(totalBeats)+parseInt(1);

		if(total == totals)
		{
			jQuery('.get-question-detail').prop('disabled',false);
			var audioElement = document.getElementById('vid');
			audioElement.pause();
		}
	}
	jQuery(document).ready(function(){

		$('.start-fade-out').fadeOut(3000, function() 
	    {
	        $('.go-start-button').css('display','block');
	        $('.with-flash').css('display','none');
	        <?php 
	        	if(($subLevelId == 2 && $ques->session_id == 0) || ($subLevelId == 4 && $ques->session_id == 0) || ($subLevelId == 8 && $ques->session_id == 0) || ($subLevelId == 11 && $ques->session_id == 0))
	        	{
			?>
					jQuery('.get-question-detail').prop('disabled',true);
					totalBeats = '<?php echo $ques->answer_record; ?>';
					counter = 0;
					setTimeout(function(){
			      		
			      		playMetro(counter,totalBeats);
					    
			    	}, 1000);
			<?php }?>
		});

	});

    

	jQuery(document).on('click','.get-question-detail',function(){
		jQuery('.without-flash').css('display','block');
		jQuery('.next-prev-btn').css('display','block');
		jQuery('.with-flash').css('display','none');
		jQuery('.go-start-button').css('display','none');
	});

	jQuery(document).on('click','.next_three_blanks',function(){
		jQuery('.without-flash').css('display','block');
		jQuery('.next-prev-btn').css('display','block');
		jQuery('.with-flash').css('display','none');
		jQuery('.multi_word').css('display','block');
	});

	jQuery(document).on('click','.repeat-flash-word',function(){
		jQuery('.go-button').css('display','none');
		$('.next-prev-btn').css('display','none !important');
		//jQuery('.fade-out-flash').css('display','block');
		$('.fade-out').fadeIn( "slow", function() {
		    $('.fade-out').fadeOut(3000, function() {
		    	$('.go-button').css('display','block');
		        $('.next-prev-btn').css('display','block');
		    });
		});
		$('.fade-out-flash').fadeIn( "slow", function() {
		    $('.fade-out-flash').fadeOut(3000, function() {
		    	$('.go-button').css('display','block');
		        $('.next-prev-btn').css('display','block');
		    });
		});
	});

	jQuery(document).on('click','.update_blank_space',function(){
		var value = jQuery(this).attr('data-attr');
		var trimVal = value.trim();
		<?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)){ 
		?>

			var answer = '<?php echo $ques->option3; ?>';

			if(value != answer)
			{
				jQuery(this).children('p').css('color','red');
			}
			else
			{
				jQuery(this).children('p').css('color','green');
				jQuery('.blank_space_val').html(trimVal);
			}

		<?php } else{ ?>
			jQuery('.blank_space_val').html(trimVal);
		<?php } ?>
	});

/*===============jquery for three blanks============*/

	jQuery(document).on('click','.update_three_space',function(){
		var value = jQuery(this).attr('data-attr');
		var trimVal = value.trim();
		jQuery('.blank_space_val_first').html(trimVal);
		jQuery('.update_three_space').addClass('update_sec_space');
		jQuery('.update_sec_space').removeClass('update_three_space');
	});

	jQuery(document).on('click','.update_sec_space',function(){
		var value = jQuery(this).attr('data-attr');
		var trimVal = value.trim();
		jQuery('.blank_space_val_two').html(trimVal);
		jQuery('.update_sec_space').addClass('update_third_space');
		jQuery('.update_third_space').removeClass('update_sec_space');
	});

	jQuery(document).on('click','.update_third_space',function(){
		var value = jQuery(this).attr('data-attr');
		var trimVal = value.trim();
		jQuery('.blank_space_val_third').html(trimVal);
		jQuery('.update_third_space').addClass('three_space_class');
		jQuery('.three_space_class').removeClass('update_third_space');
	});

/*===============Repeat button for three blanks============*/

	jQuery(document).on('click','.repeat_three_space',function(){
		jQuery('.blank_space_val_first').html(' _ ');
		jQuery('.blank_space_val_two').html(' _ ');
		jQuery('.blank_space_val_third').html(' _ ');
		jQuery('.three_space_class').addClass('update_three_space');
		jQuery('.update_sec_space').addClass('update_three_space');
		jQuery('.update_third_space').addClass('update_three_space');
		jQuery('.update_three_space').addClass('three_space_class');
		jQuery('.update_three_space').addClass('three_space_class');
	}); 

	jQuery(document).on('click','.repeat_blank_space',function()
	{
		<?php if(($subLevelId=='1' && $ques->session_id == 0) || ($subLevelId=='2' && $ques->session_id == 0) || ($subLevelId=='3' && $ques->session_id == 0) || ($subLevelId=='4' && $ques->session_id == 0) || ($subLevelId=='7' && $ques->session_id == 0) || ($subLevelId=='8' && $ques->session_id == 0) || ($subLevelId=='9' && $ques->session_id == 0) || ($subLevelId=='10' && $ques->session_id == 0) || ($subLevelId=='11' && $ques->session_id == 0)){ 
        ?>

        	jQuery('.update_blank_space').children('p').css('color','');

        	jQuery('.blank_space_val').html('_');

    	<?php }else{ ?>

			jQuery('.blank_space_val').html('_');

		<?php } ?>
	});

	

/*================Rhythm task=================*/

<?php 
	if($subLevelId == 64 || $subLevelId == 65 || $subLevelId == 66 ||  $subLevelId == 67 ||  $subLevelId == 68 ||  $subLevelId == 69 ||  $subLevelId == 70){
?>
		console.log('fdsa');
<?php }else{ ?>

	function getRandomNumber()
	{
		<?php 

			if($ques->option1 != '')
			{
				$ques1 = $ques->option1;
			}
			if($ques->option2 != '')
			{
				$ques2 = $ques->option2;
			}
			if($ques->option3 != '')
			{
				$ques3 = $ques->option3;
			}

		?>
		ques1str = '<?php echo $ques1 ?>';
		var ques1result = ques1str.split(" ");

		ques2str = '<?php echo $ques1 ?>';
		var ques2result = ques2str.split(" ");

		ques3str = '<?php echo $ques1 ?>';
		var ques3result = ques3str.split(" ");

		ques4str = '<?php echo $ques1 ?>';
		var ques4result = ques4str.split(" ");

		arrayfirst = $.merge( $.merge( [], ques1result ), ques2result);

		arraysecond = $.merge( $.merge( [], arrayfirst ), ques3result);

		$arraysec = $.merge( $.merge( [], arraysecond ), ques4result);

		$totalItems = $arraysec.length;

		var finalkey = [];

		$.each($arraysec, function(key, value) {
		    finalkey.push(key+parseInt(1));
		});

		$randomKey = finalkey[Math.floor(Math.random()*finalkey.length)];	}


	function playAudio($randomKey)
	{
		/*var timeoutSec = $randomKey*parseInt(2000);
		/*var playaudio = document.getElementById('vid');
		playaudio.play();*/
		/*var audioSrc = '<?php //echo url('/public/audio/2SEC.mp3'); ?>';

		var audioElement = document.getElementById('vid');
		audioElement.setAttribute('src', audioSrc);
		audioElement.play();
        var isPlaying = false;

        audioElement.onplaying = function() {
          isPlaying = true;
        };
        audioElement.onpause = function() {
          isPlaying = false;
        };
		
		audioElement.addEventListener('ended', function() 
		{
			this.play();
		}, false)
		playaudio.pause();
		var audio = document.getElementById('my_metronome');
		var playaudio = document.getElementById('vid');
		playaudio.pause();
		for($i=1;$i<=$randomKey;$i++)
		{
			var isPlaying = false;
			audio.play();
            
		}*/
	}

	setTimeout(function(){
	      		
	    var player = document.getElementById('my_metronome');
		var rhythmType = jQuery('.rythm-task-type').val();
		if(rhythmType != '')
		{
			if(rhythmType == 'metronome')
			{

				getRandomNumber();
				playAudio($randomKey);
			}
			else if(rhythmType == 'pulse')
			{
				var playaudio = document.getElementById('vid');
				playaudio.pause();
				jQuery('.pulse_bolde_0').css({'font-weight':800});
					jQuery('.rythm_task_pulse_0').css({'font-weight':''});
				setTimeout(function(){
					jQuery('.pulse_bolde_0').css({'font-weight':''});
					jQuery('.rythm_task_pulse_0').css({'font-weight':'100'});
				},500);
			}
		}
	}, 500);
	
	function playRandomAudioAfterOneMin(counter)
	{
		var count = counter;
		console.log(count);
		
		getRandomNumber();
		var totalItems = $totalItems-parseInt(1);
		console.log(totalItems);
		var type = jQuery('.rythm-task-type').val();
	  	if(counter <= totalItems)
	  	{
	  		if(type == 'metronome')
	  		{
		    	/*setTimeout(function(){
		      		counter++;
		      		playRandomAudioAfterOneMin(counter);
				    getRandomNumber();
					playAudio($randomKey);
		    	}, 4000);*/
		    }
		    if(type == 'pulse')
		    {
		    	setTimeout(function(){
		    		jQuery('.pulse_bolde_'+counter).css({'font-weight':800});
					jQuery('.rythm_task_pulse_'+counter).css({'font-weight':''});
					counter++;
					playRandomAudioAfterOneMin(counter);
					setTimeout(function(){
						jQuery('.pulse_bolde_'+count).css({'font-weight':''});
						jQuery('.rythm_task_pulse_'+count).css({'font-weight':'100'});
					},500);
				},1000);
		    }
	  	}
	}
	setTimeout(function(){playRandomAudioAfterOneMin(1)},500);
	
<?php } ?>
	
	jQuery(document).on('click','.repeat-audio',function(){
		var audio = document.getElementById('vid');
		audio.play();
        audio.currentTime = 0;
	});
	jQuery(document).on('click','.get-flash-detail',function(){
		jQuery('.with-flash-btn').css('display','none');
		jQuery('.with-flash').css('display','block');
		$('.fade-out-flash').fadeOut(4000, function() {
	        $('.go-button').css('display','block');
	        $('.next-prev-btn').css('display','block');
	    });
	});

	jQuery(document).on('click','.start-fill-blanks',function(){
		jQuery('.ans').css('display','block');
		jQuery('.with-flash').css('display','none');
		jQuery('.multi_word').css('display','block');
		jQuery('.align-items-center').css('display','block');
		jQuery('.next-prev-btn').css('display','block');
	});

	jQuery(document).on('click','.clusterItems',function(){
		var type = jQuery(this).attr('data-attr');
		jQuery('.cluster_second_word').css('display','none');
		jQuery('.cluster_word').css('display','block');
		jQuery('.cluster_word').html(type);
	});

	jQuery(document).on('click','.repeat_cluster_word',function(){
		jQuery('.cluster_second_word').css('display','block');
		jQuery('.cluster_word').css('display','none');
	});

/*===========Temo Trainer(Level 4)=============*/

	/*=======On press key m of rightear check or save record in database========*/

	<?php if($subLevelId == 67 || $subLevelId == 68 || $subLevelId == 69 ||  $subLevelId == 70){ ?>

		var audio = document.getElementById('vid');
		if(audio)
		{
			audio.pause();
        	audio.currentTime = 0;
		}
		
		function backspace_emolator()
		{
		  var e = jQuery.Event("keydown", { keyCode: 77 }); // you can specify your desired key code 
		  
		}

	<?php if($subLevelId == 67 || $subLevelId == 68){ ?>

		jQuery(document).off('keyup').on('keyup',function(ev){
			var eartype = jQuery('.earType').val();
			if(eartype == 'right')
			{
				if (ev.which === 77)
				{   // press m key
		           addKeystrockTime(eartype);
		        } 
			}
			if(eartype == 'left')
			{

				if (ev.which === 90)
				{   // press z key
		           addKeystrockTime(eartype);
		        } 
			}
		});

	<?php } ?>

	    var i = 1;

	    $(document).ready(function() 
		{
			if(i == 1)
			{
				setTimeout(function(){playBeep(1)},1000);
				i++;
			}
		});
	    

	    function playBeep(counter)
		{
			if(counter <= 25)
			{
				<?php if($subLevelId == 67 || $subLevelId == 68){ ?>
					var eartype = jQuery('.earType').val();
					addBeepTime(counter,eartype);
					var player = document.getElementById('my_metronome');
					player.play();
					setTimeout(function(){
				        player.pause();
				        player.currentTime = 0;
				    }, 1000);

				<?php }else if($subLevelId == 69){ ?>

					var randomNum = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
		           	var randomNums = randomNum[Math.floor(Math.random()*randomNum.length)];

					if ( randomNums % 2 == 0 )
					{
						addBeepTime(counter,'right');
						var src = '<?php echo url('/public/right.wav');?>';
						jQuery("#my_metronome").attr("src", src);
						var player = document.getElementById('my_metronome');
						player.play();
						jQuery(document).off('keyup').on('keyup',function(ev){
							if (ev.which === 77)
							{   // press m key
					           addKeystrockTime('right');
					        } 
					    });
    					
					}
					else
					{
						addBeepTime(counter,'left');
						var src = '<?php echo url('/public/left.wav');?>';
						jQuery("#my_metronome").attr("src", src);
						var player = document.getElementById('my_metronome');
						player.play();
						jQuery(document).off('keyup').on('keyup',function(ev){
							if (ev.which === 90)
							{   // press z key
					           addKeystrockTime('left');
					        }
					    }); 
					}
					setTimeout(function(){
				        player.pause();
				        player.currentTime = 0;
				    }, 1000);

				<?php }else if($subLevelId == 70){ ?>

					var randomNum = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
		           	var randomNums = randomNum[Math.floor(Math.random()*randomNum.length)];
		           	console.log(randomNums);

					if ( randomNums % 2 == 0 )
					{
						addBeepTime(counter,'right');
						jQuery('.right').css('border','7px solid #000');
						jQuery('.right').css('box-shadow', '-1px 9px 13px #080808cf');
						jQuery('.left').css('border','2px solid #000');
						jQuery('.left').css('box-shadow', '');
						setTimeout(function(){
					        jQuery('.right').css('border','2px solid #000');
							jQuery('.right').css('box-shadow', '');
					    }, 2000);
						jQuery('.without-shadow').css('display','none');
						
						jQuery(document).off('keyup').on('keyup',function(ev){
							if (ev.which === 77)
							{   // press m key
					           addKeystrockTime('right');
					        } 
					    });
    					
					}
					else
					{
						addBeepTime(counter,'left');
						jQuery('.right').css('border','2px solid #000');
						jQuery('.right').css('box-shadow', '');
						jQuery('.left').css('border','7px solid #000');
						jQuery('.left').css('box-shadow', '-1px 9px 13px #080808cf');
						setTimeout(function(){
					        jQuery('.left').css('border','2px solid #000');
							jQuery('.left').css('box-shadow', '');
					    }, 2000);
						jQuery(document).off('keyup').on('keyup',function(ev){
							if (ev.which === 90)
							{   // press z key
					           addKeystrockTime('left');
					        }
					    }); 
					}
					

				<?php } ?>
				
				
			    if(counter == 25)
				{
					var stuId = jQuery('.stuId').val();
					var subLevelId = jQuery('.subLevelId').val();
					var eartype = jQuery('.earType').val();
					var site_url = jQuery('.site_url').val();
					jQuery('.totalScore').css('display','block');
					jQuery('#don_tempo_trainer').css('display','block');
					jQuery('.left').css('display','none');
					jQuery('.right').css('display','none');
					$.ajax({
				        dataType: "html",
				        url: site_url+"/student_average_score" +'?stuId='+stuId+'&subLevelId='+subLevelId+'&eartype='+eartype,
				        dataType:"json",
						success:function(data)
						{
							jQuery('.no_response').html(data.no);
							jQuery('.anti').html(data.anti);
							jQuery('.response').html(data.res);
							jQuery('.average_score').html(data.ave);
							var count = counter+parseInt(1);
							<?php if($subLevelId == 70){ ?>
				            	var random = [4000,6000];
				            <?php }else{ ?>
				            	var random = [5000,7000,9000];
				            <?php } ?>
							var randomSec = random[Math.floor(Math.random()*random.length)];
							setTimeout(function(){
								playBeep(count);
						    }, randomSec);
				        }
				    });
				}
			}
		}	

		/*===========add beep time============*/

		function addBeepTime(counter,type)
		{
			var stuId = jQuery('.stuId').val();
			var subLevelId = jQuery('.subLevelId').val();
			var eartype = type;
			var site_url = jQuery('.site_url').val();
			$.ajax({
		        dataType: "html",
		        url: site_url+"/add_beep_time" +'?stuId='+stuId+'&subLevelId='+subLevelId+'&eartype='+eartype,
		        dataType:"json",
				success:function(data)
				{
					var count = counter+parseInt(1);
		            <?php if($subLevelId == 70){ ?>
		            	var random = [4000,6000];
		            <?php }else{ ?>
		            	var random = [5000,7000,9000];
		            <?php } ?>
					var randomSec = random[Math.floor(Math.random()*random.length)];
					setTimeout(function(){
						playBeep(count);
				    }, randomSec);
		        }
		    });
		}

		/*===========add keystrock time if beep time is avaible for this============*/

		function addKeystrockTime(eartype)
		{
			var stuId = jQuery('.stuId').val();
			var subLevelId = jQuery('.subLevelId').val();
			var eartype = eartype;
			var site_url = jQuery('.site_url').val();
			$.ajax({
		        dataType: "html",
		        url: site_url+"/student_temo_trainer" +'?stuId='+stuId+'&subLevelId='+subLevelId+'&eartype='+eartype,
		        dataType:"json",
				success:function(data){
		            
		        }
		    });
		}

		/*=============On click tempo trainer done button delete all records============*/

		jQuery(document).on('click','#don_tempo_trainer',function(){
			var stuId = jQuery('.stuId').val();
			var subLevelId = jQuery('.subLevelId').val();
			var eartype = jQuery('.earType').val();
			var site_url = jQuery('.site_url').val();
			$.ajax({
		        dataType: "html",
		        url: site_url+"/done_temo_trainer" +'?stuId='+stuId+'&subLevelId='+subLevelId+'&eartype='+eartype,
		        dataType:"json",
				success:function(data)
				{
		            window.location.href = data.redirectUrl; 
		        }
		    });
		});

	<?php } ?>

	
	
	
</script>
<style>
	.highlight {
	    font-weight: bold;
	}
</style>
		


	

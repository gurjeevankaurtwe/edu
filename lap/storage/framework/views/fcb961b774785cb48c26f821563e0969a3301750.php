<?php $__env->startSection('middle_content'); ?>
		
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Student List
					</h3>
					<a href="<?php echo e(url('/add-students')); ?>"><button type="button" class="btn btn-primary">Add Student</button></a>
				</div>
				<div class="course_wrap">
					<div class="row">
						<?php if(Auth::user()->type == 'teacher'): ?>
							<?php $__currentLoopData = $student; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="col-xl-4 col-lg-6 col-md-6 col-12">
									<div class="course_card color_1">
										<div class="overlay">
											<h2><?php echo e(ucfirst($stu->name)); ?></h2>
											<h6>Email: <?php echo e($stu->email); ?></h6>
											<a href="<?php echo e(url('/home?student='.encrypt($stu->id))); ?>">Courses</a>
										</div>								
									</div>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php elseif(Auth::user()->type == 'Institute'): ?>
							<?php $__currentLoopData = $student; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

								<?php if($stu->type == 'student'): ?>
									<div class="col-xl-4 col-lg-6 col-md-6 col-12">
										<div class="course_card color_1">											
											<div class="overlay">
												<div class="custom_btn text-right">
													<a title="Edit student detail" href="<?php echo e(url('/edit-student/'.encrypt($stu->id))); ?>" style="color:#4BA04A;"><i class="fas fa-edit"></i></a>
													<a title="View student detail" href="<?php echo e(url('/view-student-detail/'.encrypt($stu->id))); ?>" style="color:#3D83C1;"><i class="fas fa-eye"></i></a>
													<a title="Delete student" href="<?php echo e(url('/delete-student/'.encrypt($stu->id))); ?>" onclick="return confirm('Are you sure you want to delete this item?');" class="pull-right" style="color:#C63531;"><i class="fas fa-trash-alt"></i></a>
												</div>
												<h2><?php echo e(ucfirst($stu->name)); ?></h2>
												<h6>Email: <?php echo e($stu->email); ?></h6>
												<div class="btn_opt">
													<a href="<?php echo e(url('/home?student='.encrypt($stu->id))); ?>">Courses</a>																							
													<?php if($stu->status == 1): ?>
														<a title="Click to Deactive" href="<?php echo e(url('/update-student-status/'.encrypt($stu->id).'/0')); ?>">Activate</a>
													<?php else: ?>
														<a title="Click to Activate" href="<?php echo e(url('/update-student-status/'.encrypt($stu->id).'/1')); ?>" >Deactivate</a>
													<?php endif; ?>
												</div>																							
											</div>								
										</div>
									</div>
								<?php elseif($stu->type == 'teacher'): ?>
									<?php $__currentLoopData = $stu->students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stud): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										
										<div class="col-xl-4 col-lg-6 col-md-6 col-12">
											<div class="course_card color_1">
												<div class="overlay">
													<div class="custom_btn text-right">
														<a title="Edit student detail" href="<?php echo e(url('/edit-student/'.encrypt($stud->id))); ?>" style="color:#4BA04A;"><i class="fas fa-edit"></i></a>
														<a title="View student detail" href="<?php echo e(url('/view-student-detail/'.encrypt($stud->id))); ?>" style="color:#3D83C1;"><i class="fas fa-eye"></i></a>
														<a title="Delete student" href="<?php echo e(url('/delete-student/'.encrypt($stud->id))); ?>" onclick="return confirm('Are you sure you want to delete this item?');" class="pull-right" style="color:#C63531;"><i class="fas fa-trash-alt"></i></a>
													</div>
													<h2><?php echo e(ucfirst($stud->name)); ?></h2>
													<h6>Email: <?php echo e($stud->email); ?></h6>
													<a href="<?php echo e(url('/home?student='.encrypt($stud->id))); ?>">Courses</a>
													<?php if($stud->status == 1): ?>
														<a title="Click to Deactive" href="<?php echo e(url('/update-student-status/'.encrypt($stud->id).'/0')); ?>">Activate</a>
													<?php else: ?>
														<a title="Click to Activate" href="<?php echo e(url('/update-student-status/'.encrypt($stud->id).'/1')); ?>" >Deactivate</a>
													<?php endif; ?>
												</div>								
											</div>
									</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>
					</div>
				</div>									
			</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
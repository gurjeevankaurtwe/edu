<?php $__env->startSection('content'); ?>

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Questions Management<br><small>You can manage your entire registered questions!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Users</li>
    <li><a href="<?php echo e(url('admin/manage-session')); ?>">Manage Sessions</a></li>
</ul>
<!-- END Datatables Header -->

<!-- Datatables Content -->
<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> questions</h2>
    </div>

    <?php if(session()->has('success')): ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session()->get('success')); ?>

    </div>
    <?php endif; ?>
    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Session Name</th>
                    <th>Question</th>
                    <th>Option 1</th>
                    <?php if($session_id=='1' || $session_id=='3'): ?>
                    <th>Option1 Image</th>
                    <?php endif; ?>
                    <?php if($session_id!='1'): ?>
                    <th>Option 2</th>
                    <th>Option 3</th>
                    <th>Option 4</th>
                    <?php endif; ?>
                    <th>Answer</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1
                ?>
                <?php $__currentLoopData = $all_questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $allUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $encodedUserId = encrypt($allUser->id) ;
               
                ?>
                <tr>
                    <td class="text-center"><?php echo e($i); ?></td>
                    <td><?php echo e($allUser->session_name); ?></td>
                    <td><?php echo e($allUser->question); ?></td>
                    <td><?php echo e($allUser->option1); ?></td>
                    <?php if($session_id=='1' || $session_id=='3'): ?>
                    <td><img src="<?php echo e(asset('question_images/'.$allUser->option1_img)); ?>" width="50" height="50"></td>
                    <?php endif; ?>
                    <?php if($session_id!='1'): ?>
                    <td><?php echo e($allUser->option2); ?></td>
                    <td><?php echo e($allUser->option3); ?></td>
                    <td><?php echo e($allUser->option4); ?></td>
                    
                    <?php endif; ?>
                    <td><?php echo e($allUser->answer); ?></td>
                    
                    
                    <td>
                        <span title="click to change status" onclick="window.location.href='<?php echo e(url('admin/update-questions-status/'.$encodedUserId)); ?>'" class="label <?php echo e($allUser->status=='1' ? 'label-success' : 'label-danger'); ?>"><?php echo e($allUser->status=='1' ? 'Click to Deactive' : 'Click to Activate'); ?></span>
                    </td>  
                    <td class="text-center">  
                        <div class="btn-group">
                            <?php if($session_id=='1' || $session_id=='3'): ?>
                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/edit-questions/'.$encodedUserId)); ?>'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                            <?php endif; ?>
                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/delete-questions/'.$encodedUserId)); ?>'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                    
                    
                </tr>
                 <?php $i++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
<!-- END Datatables Content -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
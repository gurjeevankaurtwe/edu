<?php $__env->startSection('middle_content'); ?>


		
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Sub Level
					</h3>
				</div>
				<div class="course_wrap">
					<div class="sub_level_wrap">
						<div class="row">
							<?php
					          $i=0;
					        ?>
					        <?php $__currentLoopData = $levelSubLevel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $levSubLev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

					            <?php
					                if($i == 0)
					                {
					                  $disabled = "";
					                }
					                else
					                {
					                  $disabled = "disabled";
					                }
					            ?>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="sublev_box">								
										<div class="overlay">
											<h3><?php echo e($levSubLev->title); ?></h3>
											<div class="progress">
												
												<div class="progress-bar" style="" aria-valuemax="100"></div>
											</div>									
											<div class="d-flex align-items-center">
												
													<h6>Questions: <?php echo e(count($levSubLev->subLevelsQues)); ?></h6>
													<a href="<?php echo e(url('/level_questions_/'.encrypt($levSubLev->id).'?student='.encrypt($studentId).'&subLevel=1')); ?>" class="ml-auto">Continue</a>
												
												
											</div>																		
										</div>									
									</div>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>													
					</div>
				</div>									
			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('middle_content'); ?>


		
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						Sub Level
					</h3>
				</div>
				<div class="course_wrap">
					<div class="sub_level_wrap">
						<div class="row">
							<?php
					          $i=0;
					        ?>
					        <?php $__currentLoopData = $levelSubLevel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $levSubLev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

					            <?php
					                if($i == 0)
					                {
					                  $disabled = "";
					                }
					                else
					                {
					                  $disabled = "disabled";
					                }
					            ?>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="sublev_box">
										<?php
											if(Auth::user()->type == 'teacher')
									        {
									            $teacherId = Auth::user()->id;
									            $studentIds = $studentId;
									            $compSubLevels = DB::table('student_level_details')->where('student_id',$studentIds)->whereNull('session_id')->where('teacher_id',$teacherId)->where('sublevel_id',$levSubLev->id)->get();
									        }
									        if(Auth::user()->type == 'student' || Auth::user()->type == 'Institute')
									        {
									            $teacherId = '';
									            if(Auth::user()->type == 'student')
									            {
									            	$studentIds = Auth::user()->id;
									            }
									            elseif(Auth::user()->type == 'Institute')
									            {
									            	$studentIds = $studentId;
									            }
									            $compSubLevels = DB::table('student_level_details')->where('student_id',$studentIds)->whereNull('session_id')->where('sublevel_id',$levSubLev->id)->get();
									        }
									        $getTotalQues = DB::table('questions')->where('sub_level_id',$levSubLev->id)->where('session_id','!=',0)->where('status',1)->get();

									        
									        if(count($getTotalQues) > 0)
									        {

									        	$totalWidth =  (count($compSubLevels)*100)/count($getTotalQues);
									        }
									        else
									        {
									        	$totalWidth = 0;
									        }

									        $nextLevel = '';

									        
								        	$preLevQuesDetail = array();
								        	$getPrevTotalQues = array();
								        	$prevLevel = DB::table('sub_levels')->where('level_id',1)->where('id', '<', $levSubLev->id)->orderBy('id','DESC')->first();
											if($prevLevel != '')
								        	{
								        		$preLevQuesDetail = DB::table('student_level_details')->where('student_id',$studentIds)->where('sublevel_id',$prevLevel->id)->get();
								        		
								        		$getPrevTotalQues = DB::table('questions')->where('sub_level_id',$prevLevel->id)->where('status',1)->get();
								        	}

								        	if(!empty($preLevQuesDetail))
								        	{
								        		if(count($preLevQuesDetail) == count($getPrevTotalQues))
									        	{
									        		$lock = '';
									        		$onClick = '';
									        	}
									        	else
									        	{
									        		$lock = '<i class="fa fa-lock" aria-hidden="true"></i>';
									        		$onClick = 'onclick="return false"';
									        	}
								        	}
								        	else
								        	{
								        		$lock = '<i class="fa fa-lock" aria-hidden="true"></i>';
								        		$onClick = 'onclick="return false"';
								        	}

									   	?>

										<div class="overlay">
											<h3><?php echo e($levSubLev->title); ?></h3>
											<?php if($levSubLev->id != 64 && $levSubLev->id != 65 && $levSubLev->id != 66 && $levSubLev->id != 67 && $levSubLev->id != 68 && $levSubLev->id != 69 && $levSubLev->id != 70): ?>
											<div class="progress">

											    
													<div class="progress-bar" style="width:<?php echo e($totalWidth.'%'); ?>" aria-valuemax="100"></div>
												
											</div>	
											<?php endif; ?>								
											<div class="d-flex align-items-center">
												
													<?php if(count($levSubLev->levelSubLevel) > 0): ?>
														<h6>SubLevels: <?php echo e(count($levSubLev->levelSubLevel)); ?></h6>
														<a href="<?php echo e(url('/sublevel_sublevels/'.encrypt($levSubLev->id).'?student='.encrypt($studentId))); ?>" class="ml-auto">Continue</a>
													<?php else: ?>
														<?php if($levSubLev->id != 64 && $levSubLev->id != 65 && $levSubLev->id != 66 && $levSubLev->id != 67 && $levSubLev->id != 68 && $levSubLev->id != 69 && $levSubLev->id != 70): ?>
															<h6>Questions: <?php echo e(count($levSubLev->subLevelsQues)); ?></h6>
														<?php endif; ?>
														<a href="<?php echo e(url('/level_questions_/'.encrypt($levSubLev->id).'?student='.encrypt($studentId))); ?>" class="ml-auto">Continue</a>
													<?php endif; ?>
												
												
											</div>																		
										</div>									
									</div>
								</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>													
					</div>
				</div>									
			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('middle_content'); ?>

		<div class="main-panel">
			<div class="content-wrapper">
				<div class="page-header">
					<h3 class="page-title">
						View Student
					</h3>
				</div>
				<div class="course_wrap">
					<div id="btnContainer" class='text-right my-3'>
	        <a href="<?php echo e(url('/students')); ?>"><button class="btn btn-primary">Back</button></a>
	    </div>
			<form action="<?php echo e(url('/add-students')); ?>" method="post" autocomplete="off">
	        	<?php echo csrf_field(); ?>
	        	<input type="hidden" name="id" value="<?php echo e($stuDetail->id); ?>">
			  	<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputEmail4">Student Refrence Code</label>
				      	<input type="text" class="form-control" name="reference_code" placeholder="Reference Code" value="<?php if($stuFurDetail != ''): ?> <?php echo e($stuFurDetail->reference_code); ?> <?php endif; ?>" readonly>
				    </div>
				    <div class="form-group col-md-6">
				      	<label for="inputPassword4">Email</label>
				      	<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo e($stuDetail->email); ?>" readonly>
				    </div>
			  	</div>
			  	<div class="form-row">
				    <div class="form-group col-md-6">
					    <label for="inputAddress">First Name</label>
					    <input type="text" class="form-control" name="name" value="<?php echo e($stuDetail->name); ?>" placeholder="First Name" readonly>
				  	</div>

				  	<div class="form-group col-md-6">
					    <label for="inputAddress">Last Name</label>
					    <input type="text" class="form-control" name="last_name" value="<?php echo e($stuDetail->last_name); ?>" placeholder="Last Name" readonly>
				  	</div>
				    
			  	</div>
			  	<div class="form-row">
			  		<div class="form-group col-md-6">
					    <label for="inputAddress2">Date of Birth</label>
					    <input type="text" class="form-control datepicker" name="dob" value="<?php echo e($stuDetail->dob); ?>" placeholder="Date of Birth" readonly>
				  	</div>
				  	<div class="form-group col-md-6">
					    <label for="inputAddress2">Password</label>
					    <input type="text" class="form-control" name="password" value="<?php echo e($stuDetail->password_hint); ?>" placeholder="Password" min="5" readonly>
				  	</div>
				</div>
				<div class="form-row">
				    <div class="form-group col-md-6">
				      	<label for="inputCity">Grade or N/A</label>
				      	<input type="text" class="form-control" name="grade" value="<?php if($stuFurDetail != ''): ?> <?php echo e($stuFurDetail->grade); ?> <?php endif; ?>" placeholder="Grade or N/A" readonly>
				    </div>
				    <div class="form-group col-md-4">
				      	<label for="inputState">Gender</label>
				      	<select name="gender" class="form-control" name="gender" disabled> 
					        <option value="">Select Gender...</option>
					        <option value="Female" <?php if($stuFurDetail != ''){ if($stuFurDetail->gender == 'Female'){ echo 'selected="selected"';}}?>>Female</option>
					        <option value="male" <?php if($stuFurDetail != ''){ if($stuFurDetail->gender == 'male'){ echo 'selected="selected"';}}?>>Male</option>
				      	</select>
				    </div>
				    <div class="form-group col-md-2">
				      <label for="inputZip">Setup Timing</label>
				      <input type="text" class="form-control" name="timing" value="<?php if($stuFurDetail != ''): ?> <?php echo e($stuFurDetail->timings); ?> <?php endif; ?>" placeholder="Timing" readonly>
				    </div>
			  	</div>
			  	
			</form>
				</div>									
			</div>

		
  	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
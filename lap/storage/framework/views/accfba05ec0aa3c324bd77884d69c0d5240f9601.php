<?php $__env->startSection('content'); ?>

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Question Management<br><small>You can add new question from here to submit this form!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Session</li>
    <li><a href="<?php echo e(url('admin/manage-session')); ?>">Manage</a></li>
</ul>
<!-- END Datatables Header -->
<div class="col-md-12">
    <!-- Form Validation Example Block -->
    <div class="block">
        <!-- Form Validation Example Title -->
        <div class="block-title">
            <h2><strong>Edit</strong> Question</h2>
        </div>
        <!-- END Form Validation Example Title -->
        <?php if(session()->has('error')): ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo e(session()->get('error')); ?>

        </div>
        <?php endif; ?>
        <!-- Form Validation Example Content -->
        <form id="form-validation" action="" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <fieldset>
                <legend><i class="fa fa-angle-right"></i> Question Info</legend>
                <div class="input_fields_wrap"> 
                    
                    <div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Question <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" value="<?php echo e($userData->question); ?>" name="question" required="" class="form-control" placeholder="question">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" value="<?php echo e($userData->option1); ?>" id="val_username" value="" name="option1" required="" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 1 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="option1_img" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                
                            </div><br>
                            <img src="<?php echo e(asset('question_images/'.$userData->option1_img)); ?>" width="50" height="50">
                        </div>
                        </div>
                        <?php if($userData->sub_level_id!='1'): ?>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" id="val_username" name="option2" class="form-control" placeholder="option 2" value="<?php echo e($userData->option2); ?>" required="">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                
                            </div><br>
                            <img src="<?php echo e(asset('question_images/'.$userData->option2_img)); ?>" width="50" height="50">
                        </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Option 2 Image<span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" value="" name="option2_img" class="form-control" placeholder="option 1">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                            </div>
                        </div>
                        </div>
                        <?php endif; ?>
<!--                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Answer <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="">
                                <input type="radio" <?php if($userData->answer=='option1'): ?> checked="" <?php endif; ?> id="val_username" value="option1" name="answer" >option 1
                                <input type="radio" <?php if($userData->answer=='option2'): ?> checked="" <?php endif; ?> id="val_username" value="option2" name="answer" >option 2
                                
                            </div>
                        </div>
                        </div>-->
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Audio File <span class="text-danger">*</span></label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="file" id="val_username" name="answer_record" class="form-control">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                
                            </div><br> 
                        </div>
                        </div>
                        <audio controls style="margin-left:375px;">
                            <source src="<?php echo e(asset('answer_record/'.$userData->answer_record)); ?>" type="audio/mpeg">
                        </audio>
                        
                    </div>
                </div>
                
                
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button id="submit_session" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                    </div>
                </div>
            </fieldset>

        </form>
        <!-- END Form Validation Example Content -->


    </div>
    <!-- END Validation Block -->
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
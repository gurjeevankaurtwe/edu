<?php $__env->startSection('content'); ?>

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>User Management<br><small>You can add new student from here to submit this form!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Users</li>
    <li><a href="<?php echo e(url('admin/add-student')); ?>">Add Student</a></li>
</ul>
<div class="col-md-12">
    <div class="block">
        <div class="block-title">
            <h2><strong>Add</strong> Student</h2>
        </div>
        
        <form id="form-validation" action="<?php echo e(url('admin/add-student')); ?>" method="post" class="form-horizontal form-bordered" autocomplete="off">
            <?php echo e(csrf_field()); ?>

            <fieldset>


                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_ref">Student Reference Code<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="reference_code" value="<?php echo e(old('reference_code')); ?>" name="reference_code" class="form-control" placeholder="Student Reference Code">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('reference_code')); ?></span>
                     </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">First Name <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_username" value="<?php echo e(old('first_name')); ?>" name="first_name" class="form-control" placeholder="Your First Name.." required>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('first_name')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_lastname">Last Name <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_lastname" value="<?php echo e(old('last_name')); ?>" name="last_name" class="form-control" placeholder="Your Last Name.." required>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('last_name')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_dob">Date of birth<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_dob" name="dob" value="<?php echo e(old('dob')); ?>" class="form-control datepicker" placeholder="Your Date of birth.." required>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('dob')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">Email <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_email" name="email" value="<?php echo e(old('email')); ?>" class="form-control" placeholder="Your Email" required>
                            <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">Password <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="password" id="val_email" required="" name="password" value="<?php echo e(old('password')); ?>" class="form-control" placeholder="Your Password">
                            <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('password')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_grade">Grade or N/A<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="grade"  name="grade" value="<?php echo e(old('grade')); ?>" class="form-control" placeholder="Grade or N/A">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('grade')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_gender">Gender<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <select name="gender" id="gender" value="<?php echo e(old('gender')); ?>" class="form-control">
                                <option value="">--Select Gender</option>
                               
                                    <option value="Female">Female</option>
                                    <option value="male">Male</option>
                                    <option value="prefernottotell">Prefer not to tell</option>
                               
                            </select>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('gender')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_timing">SetUp Timings<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="timing" name="timing" value="<?php echo e(old('timing')); ?>" class="form-control" placeholder="Your SetUp Timings..">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('timing')); ?></span>
                    </div>
                </div>
                
                <input type="hidden" name="user_type" value="student">
                
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                </div>
            </fieldset>

        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>

<!-- Datatables Header -->
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Sub Levels Management  For : <?php echo e($userData->title); ?><br><small>You can manage your entire registered levels!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Levels</li>
    <li><a href="<?php echo e(url('/admin/sub-levels/'.$id)); ?>">Manage Sub Levels</a></li> 
</ul>
<!-- END Datatables Header -->

<!-- Datatables Content -->
<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> Sub levels</h2>
    </div>

    <?php if(session()->has('success')): ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session()->get('success')); ?>

    </div>
    <?php endif; ?>
    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1
                ?>
                <?php $__currentLoopData = $allUserTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $allUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $encodedUserId = encrypt($allUser->id) ?>
                <tr>
                    <td class="text-center"><?php echo e($i); ?></td> 
                    <td><?php echo e($allUser->title); ?></td>
                    <td>
                        <a href="javascript:void(0)">
                            <span title="click to change status" onclick="window.location.href='<?php echo e(url('admin/update-sub-level-status/'.$encodedUserId)); ?>'" class="label <?php echo e($allUser->status=='1' ? 'label-success' : 'label-danger'); ?>"><?php echo e($allUser->status=='1' ? 'Click to Deactive' : 'Click to Activate'); ?></span>
                        </a>
                    </td>                    
                    <td class="text-center"> 
                        <div class="btn-group">
                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/edit-sub-level/'.$allUser->id)); ?>'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>&nbsp;
                            <a href="javascript:void(0)">
                                <span href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/manage-questions/'.$encodedUserId)); ?>'" class="label label-success">Questions</span>&nbsp; 
                            </a>
                            <a href="javascript:void(0)">
                                <span href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/add-questions/'.$encodedUserId)); ?>'" class="label label-danger">Add Question</span>
                            </a>
<!--                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/delete-level/'.$encodedUserId)); ?>'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>-->
                        </div>
                    </td>
                </tr>
                 <?php $i++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
<!-- END Datatables Content -->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
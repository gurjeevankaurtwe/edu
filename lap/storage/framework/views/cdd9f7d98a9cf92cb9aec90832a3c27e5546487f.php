<?php $__env->startSection('content'); ?>

<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Level Management<br><small>You can update level from here to submit this form!</small>
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>User Types</li>
    <li><a href="<?php echo e(url('admin/levels')); ?>">Level</a></li>
</ul>
<div class="col-md-12">
    <div class="block">
        <div class="block-title">
            <h2><strong>Edit</strong> Level</h2>
        </div>
        <form id="form-validation" action="" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <fieldset>
                <legend><i class="fa fa-angle-right"></i> Level Info</legend>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Title <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" id="val_username" value="<?php echo e($userData->title); ?>" name="title" class="form-control" placeholder="Your title..">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Description <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <textarea id="val_username" name="add_description" class="form-control" required><?php echo e($userData->description); ?></textarea>
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Audio <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            
                       <input type="file" id="val_username"  class="form-control" name="audio" required>
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('audio')); ?></span>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                </div>
            </fieldset>

        </form>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
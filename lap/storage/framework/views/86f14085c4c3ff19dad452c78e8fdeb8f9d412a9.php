<?php $__env->startSection('content'); ?>

<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Package Management<br><small>You can manage your entire pacakges!</small>
        </h1>
    </div>
</div>

<ul class="breadcrumb breadcrumb-top">
    <li>Packages</li>
    <li>
    	<a href="<?php echo e(url('admin/packages')); ?>">Manage Packages</a>
    </li>
</ul>

<div class="block full">
    <div class="block-title">
        <h2><strong>Manage</strong> users</h2>
    </div>

    <?php if(session()->has('success')): ?>
    
    	<div class="alert alert-success">
	        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        <?php echo e(session()->get('success')); ?>

	    </div>

    <?php endif; ?>

    <div class="table-responsive">
        <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            	<?php
                	$i = 1
                ?>
                <?php $__currentLoopData = $packageDetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $package): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                	<?php 
                		$encodedUserId = encrypt($package->id) ;
                	?>
                	<tr>
	                    <td class="text-center"><?php echo e($i); ?></td>
	                    <td class="text-center"><?php echo e(ucfirst($package->title)); ?></td>
	                    <td class="text-center"><?php echo e($package->price); ?></td>
	                    <td class="text-center"><?php echo e($package->sale_price); ?></td>
	                 	<td>
	                 		<span title="click to change status" onclick="window.location.href='<?php echo e(url('admin/update-package-status/'.$encodedUserId)); ?>'" class="label <?php echo e($package->status=='Active' ? 'label-success' : 'label-danger'); ?>"><?php echo e($package->status=='Active' ? 'Click to Deactive' : 'Click to Activate'); ?></span>
	                 	</td>                    
	                 	<td class="text-center">
	                        <div class="btn-group">
	                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/edit-package/'.$encodedUserId)); ?>'" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
	                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo e(url('admin/delete-package/'.$encodedUserId)); ?>'" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
	                        </div>
	                    </td>
	                </tr>
                 	<?php $i++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               
            </tbody>
        </table>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
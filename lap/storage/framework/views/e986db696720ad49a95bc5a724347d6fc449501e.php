<?php $__env->startSection('middle_content'); ?>

  <div class="layout-content" data-scrollable>

      <div id="btnContainer" class='pull-right'>
        <button class="btn" id="list_view"><i class="fa fa-bars"></i> List</button> 
        <button class="btn active" id="grid_view"><i class="fa fa-th-large"></i> Grid</button>
      </div>
      <br>

      <div class="container-fluid">
      
        <div class="card-columns row" id="hide_show_grid"> 

          <?php
            $i=0;
          ?>
   
          <?php $__currentLoopData = $level; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              
              <?php
                if($i == 0)
                {
                    $icon = 'fa-unlock';
                }
                else
                {
                    $icon = 'fa-lock';
                }
              ?>

              <div class="card">
                <div class="card-button-wrapper">
                  <i class="fa <?php echo e($icon); ?>"></i>
                </div>
                <div class="card-header bg-white ">
                  <h4 class="card-title"><a href="take-course.html"><?php echo e($lev->title); ?></a></h4>
                  <small class="text-muted">Sub-Levels: <?php echo e(count($lev->levelSubLevels)); ?></small>
                </div>

                <div class="card-footer bg-white">
                  <a href="<?php echo e(url('/levels_sub_levels/'.encrypt($lev->id))); ?>" class="btn btn-primary btn-sm"> Continue <i class="material-icons">play_circle_outline</i></a>
                </div>
            </div>

            <?php
              $i++;
            ?>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

      </div>
    </div>
  </div>
  
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.include.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
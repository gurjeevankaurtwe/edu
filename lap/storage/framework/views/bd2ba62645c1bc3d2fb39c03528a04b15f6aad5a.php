<?php $__env->startSection('content'); ?>

<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i>Package Management<br><small>You can edit package from here to submit this form!</small>
        </h1>
    </div>
</div>

<ul class="breadcrumb breadcrumb-top">
    <li>Packages</li>
    <li>
        <a href="<?php echo e(url('admin/add-user')); ?>">Edit Package</a>
    </li>
</ul>

<div class="col-md-12">
    <div class="block">
        <div class="block-title">
            <h2><strong>Edit</strong> Package</h2>
        </div>
        <?php if(session()->has('error')): ?>
    
    	<div class="alert alert-danger">
	        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        <?php echo e(session()->get('error')); ?>

	    </div>

    <?php endif; ?>
        <form id="form-validation" action="" method="post" class="form-horizontal form-bordered" autocomplete="off">
            <?php echo e(csrf_field()); ?>

            <fieldset>
                <legend><i class="fa fa-angle-right"></i> Package Info</legend>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Title <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" value="<?php echo e($packageData->title); ?>" required="" name="title" class="form-control" placeholder="Title..">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                       <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_email">Description <span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <textarea placeholder="Description.." name="edit_description"><?php echo e($packageData->description); ?></textarea>
                            <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Price<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text"  value="<?php echo e($packageData->price); ?>" required="" name="price" class="form-control number" placeholder="Price..">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('price')); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">Sale Price</label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" value="<?php echo e($packageData->sale_price); ?>" name="sale_price" class="form-control number" placeholder="Sale Price..">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="val_username">No. of Concurrent Session<span class="text-danger">*</span></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="number" value="<?php echo e($packageData->concurrent_session); ?>" required="" name="concurrent_session" class="form-control" placeholder="Concurrent Session..">
                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                        </div>
                        <span class="text-danger"><?php echo e($errors->first('concurrent_session')); ?></span>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                    </div>
                </div>
            </fieldset>

        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin_dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
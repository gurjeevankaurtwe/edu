<div class="eltdf-banner-simple-holder <?php echo esc_attr( $holder_classes ); ?>" <?php echo esmarts_elated_get_inline_style( $holder_styles ); ?>>
	<?php if ( ! empty( $custom_icon ) ) { ?>
		<div class="eltdf-bs-icon">
			<?php echo wp_get_attachment_image( $custom_icon, 'full' ); ?>
		</div>
	<?php } ?>
	<?php if ( ! empty( $title ) ) { ?>
		<span class="eltdf-banner-title" <?php echo esmarts_elated_get_inline_style( $title_styles ); ?>><?php echo esc_html( $title ); ?></span>
	<?php } ?>
	<?php if ( ! empty( $link ) ) { ?>
		<a itemprop="url" class="eltdf-bs-link" href="<?php echo esc_url( $link ); ?>" target="<?php echo esc_attr( $target ); ?>"></a>
	<?php } ?>
</div>
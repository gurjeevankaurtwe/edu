<div class="eltdf-banner-holder <?php echo esc_attr($holder_classes); ?>">
	<?php if (!empty($image)) { ?>
	    <div class="eltdf-banner-image">
	        <?php echo wp_get_attachment_image($image, 'full'); ?>
	    </div>
	<?php } ?>
    <div class="eltdf-banner-text-holder" <?php echo esmarts_elated_get_inline_style($content_styles); ?>>
	    <div class="eltdf-banner-text-inner">
		    <?php if (!empty($content_icon)) { ?>
			    <div class="eltdf-banner-icon">
				    <?php echo wp_get_attachment_image($content_icon, 'full'); ?>
			    </div>
		    <?php } ?>
		    <div class="eltdf-banner-content">
		        <?php if(!empty($title)) { ?>
		            <<?php echo esc_attr($title_tag); ?> class="eltdf-banner-title"><?php echo esc_html($title); ?></<?php echo esc_attr($title_tag); ?>>
		        <?php } ?>
		        <?php if(!empty($text)) { ?>
			        <p class="eltdf-banner-text"><?php echo esc_html($text); ?></p>
		        <?php } ?>
				<?php if(!empty($link) && !empty($link_text)) { ?>
					<div class="eltdf-banner-link">
						<?php echo esmarts_elated_get_button_html( array(
							'link'   => $link,
							'target' => $target,
							'text'   => $link_text,
							'type'   => 'solid',
                            'skin'                   => 'light',
                            'hover_animation'        => 'yes',
						) ); ?>
					</div>
		        <?php } ?>
	        </div>
		</div>
	</div>
</div>
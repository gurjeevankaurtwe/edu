<div class="eltdf-section-title-holder <?php echo esc_attr($holder_classes); ?>" <?php echo esmarts_elated_get_inline_style($holder_styles); ?>>
	<div class="eltdf-st-inner">
		<?php if ( ! empty( $watermark ) ) { ?>
			<span class="eltdf-st-watermark" <?php echo esmarts_elated_get_inline_style( $watermark_styles ); ?>>
				<?php echo esc_html( $watermark ); ?>
			</span>
		<?php } ?>
		<?php if ( ! empty( $tagline ) ) { ?>
			<<?php echo esc_attr( $tagline_tag ); ?> class="eltdf-st-tagline" <?php echo esmarts_elated_get_inline_style( $tagline_styles ); ?>>
				<?php echo wp_kses( $tagline, array( 'br' => true ) ); ?>
			</<?php echo esc_attr( $tagline_tag ); ?>>
		<?php } ?>
		<?php if ( ! empty( $title ) ) { ?>
			<<?php echo esc_attr( $title_tag ); ?> class="eltdf-st-title" <?php echo esmarts_elated_get_inline_style( $title_styles ); ?>>
				<?php echo wp_kses( $title, array( 'br' => true ) ); ?>
			</<?php echo esc_attr( $title_tag ); ?>>
		<?php } ?>
		<?php if ( ! empty( $text ) ) { ?>
			<<?php echo esc_attr( $text_tag ); ?> class="eltdf-st-text" <?php echo esmarts_elated_get_inline_style( $text_styles ); ?>>
				<?php echo wp_kses( $text, array( 'br' => true ) ); ?>
			</<?php echo esc_attr( $text_tag ); ?>>
		<?php } ?>
	</div>
</div>
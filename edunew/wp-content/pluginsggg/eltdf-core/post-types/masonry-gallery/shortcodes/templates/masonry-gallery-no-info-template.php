<article class="eltdf-item-space <?php echo esc_attr( $item_classes ) ?>">
	<div class="eltdf-mg-content">
		<?php if ( has_post_thumbnail() ) { ?>
			<div class="eltdf-mg-image">
				<div class="eltdf-mg-image-overlay" style="background-image:url(<?php echo esc_url( $background_image_url ); ?>)"></div>
			</div>
		<?php } ?>
		<?php if ( ! empty( $item_link ) ) { ?>
			<a itemprop="url" class="eltdf-mg-item-link" href="<?php echo esc_url( $item_link ); ?>" target="<?php echo esc_attr( $item_link_target ); ?>"></a>
		<?php } ?>
	</div>
</article>

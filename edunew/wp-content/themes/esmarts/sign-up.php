
<?php
/*
Template Name: Sign Up
*/

$eltdf_sidebar_layout = esmarts_elated_sidebar_layout();

get_header();
esmarts_elated_get_title();
get_template_part( 'slider' );

?>
<style>
	.container_outer {
    width: 40%;
    margin: 0px auto;
    
}
.outer_form{
	margin-top: 50px;
    margin-bottom: 50px;
    box-shadow: 0 0 10px black;
    padding:15px;

}
.button {
	text-align: right;
	padding-top: 10px;
	padding-bottom: 10px;
}
.custom {
    background-color: #4582ff;
    color: #fff;
    padding: 10px 17px;
    border: none;
    border-radius: 5px;
}
.custom:hover{
	background-color: #396bd0;
}
.new_form label{ 
	color: #000;
}
.input_1 {
    border: 1px solid #b5b0b0 !important;
    height: 40px !important;
    width: 100%;
}
</style>
<div class="container_outer">
	<div class="outer_form">
     	<div class="col-md-6">
			<form class="new_form" id="formdata" autocomplete="off">
			  	<div class="form-group ">
			    	<label for="first_name">First Name:</label>
			    	<input type="text" class="form-control input_1" name="first_name" placeholder="First Name" required>
			  	</div>
			  	<div class="form-group ">
			    	<label for="last_name">Last Name:</label>
			    	<input type="text" class="form-control input_1" name="last_name" placeholder="Last Name" required>
			  	</div>
			  	<div class="form-group ">
			    	<label for="email">Email:</label>
			    	<input type="email" class="form-control input_1" name="email" placeholder="Email" required>
			  	</div>
			  	<div class="form-group ">
			    	<label for="email">Phone:</label>
			    	<input type="text" class="form-control input_1" name="phone" placeholder="Phone" required>
			  	</div>
			  	<div class="form-group">
			    	<label for="usertype">UserType:</label><br>
			    	<select class="form-control input_1" name="usertype" required>
			    		<option value="">Select UserType...</option>
			    		<option value="teacher">Individual</option>
			    		<option value="Institute">Institute</option>
			    	</select>
			    </div>
			    <br>
			  	<div class="form-group">
			    	<label for="pwd">Password:</label>
			    	<input type="password" class="form-control input_1" placeholder="Password" name="password" required>
			  	</div>
			  	<!-- <div class="checkbox">
			    	<label><input type="checkbox"> Remember me</label>
			  	</div> -->

			  	<div class="button text-right pt-4">
			  		<span style="color:black;float: left;">Already have an account <a style="color:#4582FF" target="_blank" href="http://edu-therapeutics.com/lap/login">Login</a></span>
			  		<button type="button" class="custom lap_save_data">Submit</button>
			  	</div>
			</form>
	</div>
	</div>
</div>
<?php get_footer(); ?>
<?php

namespace ElatedCore\CPT\Shortcodes\MasonryElementsHolder;

use ElatedCore\Lib;

class MasonryElementsHolder implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'eltdf_masonry_elements_holder';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'      => esc_html__( 'Elated Masonry Elements Holder', 'eltdf-core' ),
					'base'      => $this->base,
					'icon'      => 'icon-wpb-masonry-elements-holder extended-custom-icon',
					'category'  => esc_html__( 'by ELATED', 'eltdf-core' ),
					'as_parent' => array( 'only' => 'eltdf_masonry_elements_holder_item' ),
					'js_view'   => 'VcColumnView',
					'params'    => array(
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Columns', 'eltdf-core' ),
							'param_name'  => 'columns',
							'value'       => array(
								esc_html__( 'Two', 'eltdf-core' )   => 'two',
								esc_html__( 'Three', 'eltdf-core' ) => 'three',
								esc_html__( 'Four', 'eltdf-core' )  => 'four'
							),
							'save_always' => true
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'columns' => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['holder_classes'] = $this->getClasses( $params );
		$params['content']        = $content;
		
		$html = eltdf_core_get_shortcode_module_template_part( 'templates/masonry-elements-holder-template', 'masonry-elements-holder', '', $params );
		
		return $html;
	}
	
	private function getClasses( $params ) {
		$classes = array(
			'eltdf-masonry-elements-holder'
		);
		
		if ( $params['columns'] !== '' ) {
			$classes[] = 'eltdf-masonry-' . esc_attr( $params['columns'] ) . '-columns';
		}
		
		return $classes;
	}
}
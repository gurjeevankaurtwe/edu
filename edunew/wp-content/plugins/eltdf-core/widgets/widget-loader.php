<?php

if ( ! function_exists( 'esmarts_elated_register_widgets' ) ) {
	function esmarts_elated_register_widgets() {
		$widgets = apply_filters( 'esmarts_elated_filter_register_widgets', $widgets = array() );
		
		foreach ( $widgets as $widget ) {
			register_widget( $widget );
		}
	}
	
	add_action( 'widgets_init', 'esmarts_elated_register_widgets' );
}
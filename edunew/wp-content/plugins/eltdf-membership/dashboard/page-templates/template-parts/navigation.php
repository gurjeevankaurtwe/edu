<ul class="eltdf-membership-dashboard-nav clearfix">
	<?php
	$nav_items = eltdf_membership_get_dashboard_navigation_items();
	$user_action = isset($_GET['user-action']) ? $_GET['user-action'] : 'profile';
	foreach ( $nav_items as $nav_item ) { ?>
		<li <?php if($user_action == $nav_item['user_action']){ echo 'class="eltdf-active-dash"'; } ?>>
			<a href="<?php echo esmarts_elated_get_module_part($nav_item['url']); ?>">
                <?php if(isset($nav_item['icon'])){ ?>
                    <span class="eltdf-dash-icon">
						<?php echo esmarts_elated_get_module_part($nav_item['icon']); ?>
					</span>
                <?php } ?>
                <span class="eltdf-dash-label">
				    <?php echo esmarts_elated_get_module_part($nav_item['text']); ?>
                </span>
			</a>
		</li>
	<?php } ?>
	<li>
		<a href="<?php echo wp_logout_url( home_url( '/' ) ); ?>">
             <span class="eltdf-dash-icon">
                <i class="lnr lnr-arrow-right-circle"></i>
            </span>
			<?php esc_html_e( 'Log out', 'eltdf-membership' ); ?>
		</a>
	</li>
</ul>
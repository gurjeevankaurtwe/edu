<?php

class ElatedfMembershipLoginRegister extends WP_Widget {
	protected $params;
	
	public function __construct() {
		parent::__construct(
			'eltdf_login_register_widget', // Base ID
			'Elated Login',
			array( 'description' => esc_html__( 'Login and register wordpress widget', 'eltdf-membership' ), )
		);
	}
	
	public function widget( $args, $instance ) {
		$additional_class = '';
		if ( is_user_logged_in() ) {
			$additional_class .= 'eltdf-user-logged-in';
		} else {
			$additional_class .= 'eltdf-user-not-logged-in';
		}
		
		echo '<div class="widget eltdf-login-register-widget ' . esc_attr( $additional_class ) . '">';
		if ( ! is_user_logged_in() ) {
			echo '<a href="#" class="eltdf-modal-opener eltdf-login-opener" data-modal="login">' . esc_html__( 'Login', 'eltdf-membership' ) . '</a>';
			echo '<a href="#" class="eltdf-modal-opener eltdf-register-opener" data-modal="register">' . esc_html__( 'Register', 'eltdf-membership' ) . '</a>';
			
			add_action( 'wp_footer', array( $this, 'eltdf_membership_render_login_form' ) );
			add_action( 'wp_footer', array( $this, 'eltdf_membership_render_register_form' ) );
			add_action( 'wp_footer', array( $this, 'eltdf_membership_render_password_form' ) );
		} else {
			echo eltdf_membership_get_widget_template_part( 'login-widget', 'login-widget-template' );
		}
		echo '</div>';
	}

	public function eltdf_membership_render_login_form() {
		
		//Render modal with login and register forms
		echo eltdf_membership_get_widget_template_part( 'login-widget', 'login-modal-template' );
	}
	
	public function eltdf_membership_render_register_form() {
		
		//Render modal with login and register forms
		echo eltdf_membership_get_widget_template_part( 'login-widget', 'register-modal-template' );
	}
	
	public function eltdf_membership_render_password_form() {
		
		//Render modal with login and register forms
		echo eltdf_membership_get_widget_template_part( 'login-widget', 'password-modal-template' );
	}
}

function eltdf_membership_login_widget_load() {
	register_widget( 'ElatedfMembershipLoginRegister' );
}

add_action( 'widgets_init', 'eltdf_membership_login_widget_load' );
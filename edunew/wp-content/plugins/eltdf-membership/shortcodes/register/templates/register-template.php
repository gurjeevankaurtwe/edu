<div class="eltdf-social-register-holder">
	<form method="post" class="eltdf-register-form">
		<fieldset>
			<div>
				<label class="eltdf-username-label"><?php esc_html_e( 'User Name', 'eltdf-membership' ) ?></label>
				<input type="text" name="user_register_name" id="user_register_name" value="" required pattern=".{3,}" title="<?php esc_attr_e( 'Three or more characters', 'eltdf-membership' ); ?>"/>
			</div>
			<div>
				<label class="eltdf-username-label"><?php esc_html_e( 'Email', 'eltdf-membership' ) ?></label>
				<input type="email" name="user_register_email" id="user_register_email" value="" required />
			</div>
            <div>
	            <label class="eltdf-username-label"><?php esc_html_e( 'Password', 'eltdf-membership' ) ?></label>
                <input type="password" name="user_register_password" id="user_register_password" value="" required />
            </div>
            <div>
	            <label class="eltdf-username-label"><?php esc_html_e( 'Repeat Password', 'eltdf-membership' ) ?></label>
                <input type="password" name="user_register_confirm_password" id="user_register_confirm_password" value="" required />
            </div>
			<div class="eltdf-register-button-holder">
				<?php
				if ( eltdf_membership_theme_installed() ) {
					echo esmarts_elated_get_button_html( array(
						'html_type'         => 'button',
						'text'              => esc_html__( 'REGISTER', 'eltdf-membership' ),
						'type'              => 'solid',
                        'skin'              => 'light',
                        'hover_animation'   => 'yes',
						'size'              => 'large'
					) );
				} else {
					echo '<button type="submit">' . esc_html__( 'REGISTER', 'eltdf-membership' ) . '</button>';
				}
				wp_nonce_field( 'eltdf-ajax-register-nonce', 'eltdf-register-security' ); ?>
			</div>
		</fieldset>
	</form>
	<?php do_action( 'eltdf_membership_action_login_ajax_response' ); ?>
</div>
<div class="eltdf-price-table eltdf-item-space <?php echo esc_attr($holder_classes); ?>">
	<div class="eltdf-pt-inner">
		<ul>
			<?php if(!empty($star_rating)) { ?>
				<li class="eltdf-pt-star-holder">
					<?php for ($i = 0; $i < intval($star_rating); $i++) { ?>
						<span class="eltdf-pt-star icon_star"></span>
					<?php } ?>
				</li>
			<?php } ?>
			<?php if(!empty($title)) { ?>
				<li class="eltdf-pt-title-holder">
					<h4 class="eltdf-pt-title"><?php echo esc_html($title); ?></h4>
				</li>
			<?php } ?>
			<li class="eltdf-pt-prices">
				<span class="eltdf-pt-price"><?php echo esc_html($price); ?></span>
				<span class="eltdf-pt-mark"><?php echo esc_html($price_period); ?></span>
			</li>
			<li class="eltdf-pt-content">
				<?php echo do_shortcode($content); ?>
			</li>
			<?php 
			if(!empty($button_text)) { ?>
				<li class="eltdf-pt-button">
					<?php echo esmarts_elated_get_button_html(array(
						'link'              => $link,
						'text'              => $button_text,
						'type'              => 'solid',
                        'size'              => 'medium',
                        'hover_animation'   => 'yes',
					)); ?>
				</li>				
			<?php } ?>
		</ul>
	</div>
</div>
<div <?php esmarts_elated_class_attribute( $holder_classes ); ?>>
	<div class="eltdf-masonry-elements-grid-sizer"></div>
	<?php echo do_shortcode( $content ); ?>
</div>
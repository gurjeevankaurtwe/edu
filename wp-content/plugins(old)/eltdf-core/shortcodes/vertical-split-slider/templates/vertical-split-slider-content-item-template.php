<div class="eltdf-vss-ms-section <?php echo esc_attr($holder_classes); ?>" <?php echo esmarts_elated_get_inline_attrs($content_data); ?> <?php esmarts_elated_inline_style($content_style);?>>
	<?php echo do_shortcode($content); ?>
</div>
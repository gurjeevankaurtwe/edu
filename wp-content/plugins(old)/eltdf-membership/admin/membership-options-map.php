<?php
/**
 * Options map file
 */

if ( ! function_exists( 'eltdf_membership_membership_options_map' ) ) {
	function eltdf_membership_membership_options_map( $page ) {
		
		if ( eltdf_membership_theme_installed() ) {
			
			$panel_social_login = esmarts_elated_add_admin_panel(
				array(
					'page'  => $page,
					'name'  => 'panel_social_login',
					'title' => esc_html__( 'Enable Social Login', 'eltdf-membership' )
				)
			);
			
			esmarts_elated_add_admin_field(
				array(
					'type'          => 'yesno',
					'name'          => 'enable_social_login',
					'default_value' => 'no',
					'label'         => esc_html__( 'Enable Social Login', 'eltdf-membership' ),
					'description'   => esc_html__( 'Enabling this option will allow login from social networks of your choice', 'eltdf-membership' ),
					'args'          => array(
						'dependence'             => true,
						'dependence_hide_on_yes' => '',
						'dependence_show_on_yes' => '#eltdf_panel_enable_social_login'
					),
					'parent'        => $panel_social_login
				)
			);
			
			$panel_enable_social_login = esmarts_elated_add_admin_panel(
				array(
					'page'            => $page,
					'name'            => 'panel_enable_social_login',
					'title'           => esc_html__( 'Enable Login via', 'eltdf-membership' ),
					'hidden_property' => 'enable_social_login',
					'hidden_value'    => 'no'
				)
			);
			
			esmarts_elated_add_admin_field(
				array(
					'type'          => 'yesno',
					'name'          => 'enable_facebook_social_login',
					'default_value' => 'no',
					'label'         => esc_html__( 'Facebook', 'eltdf-membership' ),
					'description'   => esc_html__( 'Enabling this option will allow login via Facebook', 'eltdf-membership' ),
					'args'          => array(
						'dependence'             => true,
						'dependence_hide_on_yes' => '',
						'dependence_show_on_yes' => '#eltdf_enable_facebook_social_login_container'
					),
					'parent'        => $panel_enable_social_login
				)
			);
			
			$enable_facebook_social_login_container = esmarts_elated_add_admin_container(
				array(
					'name'            => 'enable_facebook_social_login_container',
					'hidden_property' => 'enable_facebook_social_login',
					'hidden_value'    => 'no',
					'parent'          => $panel_enable_social_login
				)
			);
			
			esmarts_elated_add_admin_field(
				array(
					'type'          => 'text',
					'name'          => 'enable_facebook_login_fbapp_id',
					'default_value' => '',
					'label'         => esc_html__( 'Facebook App ID', 'eltdf-membership' ),
					'description'   => esc_html__( 'Copy your application ID form created Facebook Application', 'eltdf-membership' ),
					'parent'        => $enable_facebook_social_login_container
				)
			);
			
			esmarts_elated_add_admin_field(
				array(
					'type'          => 'yesno',
					'name'          => 'enable_google_social_login',
					'default_value' => 'no',
					'label'         => esc_html__( 'Google+', 'eltdf-membership' ),
					'description'   => esc_html__( 'Enabling this option will allow login via Google+', 'eltdf-membership' ),
					'args'          => array(
						'dependence'             => true,
						'dependence_hide_on_yes' => '',
						'dependence_show_on_yes' => '#eltdf_enable_google_social_login_container'
					),
					'parent'        => $panel_enable_social_login
				)
			);
			
			$enable_google_social_login_container = esmarts_elated_add_admin_container(
				array(
					'name'            => 'enable_google_social_login_container',
					'hidden_property' => 'enable_google_social_login',
					'hidden_value'    => 'no',
					'parent'          => $panel_enable_social_login
				)
			);
			
			esmarts_elated_add_admin_field(
				array(
					'type'          => 'text',
					'name'          => 'enable_google_login_client_id',
					'default_value' => '',
					'label'         => esc_html__( 'Client ID', 'eltdf-membership' ),
					'description'   => esc_html__( 'Copy your Client ID form created Google Application', 'eltdf-membership' ),
					'parent'        => $enable_google_social_login_container
				)
			);
		}
	}
	
	add_action( 'esmarts_elated_action_social_options', 'eltdf_membership_membership_options_map' );
}

<?php /**
* Template Name: sign-up
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
<style>
@media only screen and (max-width: 500px) {
	  .outer_form {
    width: 100% !important;
  }
  .link.pt-2 {
    text-align: left !important;
}
.as span{
	text-align:left !important;
}
.outer_form {
    margin: 14px 14px !important ;
}
  }
  
  @media only screen and (max-width: 1024px) {
	  .outer_form {
    width: 100% !important;
  }
  }
  @media only screen and (max-width: 320px) {
	  .outer_form {
    margin: 14px 0px !important ;
}
  }
.outer_form {
    box-shadow: 0px 1px 10px #b9b6b6;
    padding: 15px;
    width: 60%;
    margin: 9px auto;
    border-radius: 5px;
}

.custom {
    background-color: #4582ff;
    color: #fff;
    padding: 10px 17px;
    border: none;
    border-radius: 5px;
}
.custom:hover{
	background-color: #396bd0;
}
.new_form label{ 
	color: #000;
}
.input_1{
	 border-radius: 20px !important;
}
.input_1 {
    border: 1px solid #cac6c6 !important;
}
 .btn_new {
    background: #ebad3c;
    color: #fff;
    padding: 9px 24px;
    font-size: 16px;
    border-radius: 51px;
}
.btn_new:hover{
	color:#fff;
}
.link a {
	color:#000;
	text-decoration:none;
}
.link a :hover{
	color:#ebad3c;
}
	.outer_form h2 {
    border-bottom: 2px solid #ccc6c6; 
	font-size:30px !important;
    
    padding-bottom: 6px;
}
.as span{
	text-align:right;
}
.input_select{
	height:43px;
}
@media (max-width:576px){
.space_over {
    padding-top: 8px;
    padding-bottom: 8px;
}
}

</style>
<?php
$eltdf_sidebar_layout = esmarts_elated_sidebar_layout();

get_header();
esmarts_elated_get_title();
get_template_part( 'slider' );
do_action('esmarts_elated_action_before_main_content');
?>


<div class="eltdf-container eltdf-default-page-template">
	<?php do_action( 'esmarts_elated_action_after_container_open' ); ?>
	
	<div class="eltdf-container-inner clearfix">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="eltdf-grid-row">
				<div <?php echo esmarts_elated_get_content_sidebar_class(); ?>>
					<?php
						the_content();
						do_action( 'esmarts_elated_action_page_after_content' );
					?>
				</div>
				<?php if ( $eltdf_sidebar_layout !== 'no-sidebar' ) { ?>
					<div <?php echo esmarts_elated_get_sidebar_holder_class(); ?>>
						<?php get_sidebar(); ?>
					</div>
				<?php } ?>
			</div>
		<?php endwhile; endif; ?>
	</div>
	
	<?php do_action( 'esmarts_elated_action_before_container_close' ); ?>
</div>
<div class="container">
 <div class="row" style="margin-top:px; margin-bottom:50px; ">
	<div class=" outer_form" >
	<div class="col-md-4">
	<h2>Sign Up</h2>
	</div>
			<form class="new_form row pt-4" id="formdata" autocomplete="off">
			<div class="col-md-6">
			  	<div class="form-group ">
			    	<label for="first_name">First Name:</label>
			    	<input type="text" class="form-control input_1" name="first_name" placeholder="First Name" required>
			  	</div>
				</div>
				<div class="col-md-6">
			  	<div class="form-group ">
			    	<label for="last_name">Last Name:</label>
			    	<input type="text" class="form-control input_1" name="last_name" placeholder="Last Name" required>
			  	</div>
				</div>
				<div class="col-md-6">
			  	<div class="form-group ">
			    	<label for="email">Email:</label>
			    	<input type="email" class="form-control input_1" name="email" placeholder="Email" required>
			  	</div>
				</div>
				<div class="col-md-6">
			  	<div class="form-group ">
			    	<label for="email">Phone:</label>
			    	<input type="text" class="form-control input_1" name="phone" placeholder="Phone" required>
			  	</div>
				</div>
				<div class="col-md-6">
			  	<div class="form-group space_over">
			    	<label for="usertype">UserType:</label><br>
			    	<select class="form-control input_select input_1" name="usertype" required>
			    		<option value="">Select UserType...</option>
			    		<option value="teacher">Individual</option>
			    		<option value="Institute">Institute</option>
			    	</select>
			    </div>
				</div>
			    <div class="col-md-6">
			  	<div class="form-group">
			    	<label for="pwd">Password:</label>
			    	<input type="password" class="form-control input_1" placeholder="Password" name="password" required>
			  	</div>
				</div>

			  	<!-- <div class="checkbox">
			    	<label><input type="checkbox"> Remember me</label>
			  	</div> -->

			  	<div class="col-md-6 text-left ">
			  		<div class="g-recaptcha" data-sitekey="6LewRcUUAAAAACv3hVnj7mGFuvBjQYYG84PHriXS"></div>
				
			  		
					</div>
					<div class="col-md-6 as ">
					<span class="link" style="">Already have an account ?

					<a target="_blank" href="http://edu-therapeutics.com/lap/login">Login</a>
					</span>
						
					</div>
					<div class="col-md-12 pt-4 text-center">
					
						<button type="button" class="custom lap_save_data btn_new">Submit</button>
					</div>
			</form>
	
	</div>
	</div>
</div>

<?php get_footer(); ?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <style>

  @media only screen and (max-width: 500px) {
	  .log_card {
    width: 100% !important;
  }
  .link.pt-2 {
    text-align: left !important;
}
  }
  
  @media only screen and (max-width: 1024px) {
	  .log_card {
    width: 100% !important;
  }
  }
  @media only screen and (max-width: 320px){
  .link {
    padding: 0px !important;
    margin-left: -4px;
  }
  .eltdf-mobile-header .eltdf-mobile-nav .eltdf-grid>ul {
    padding-bottom: 22px;
    background-color: #fff !important;
}
  .log_card{
	  box-shadow:none !important;
  }
}
  .btn_new {
    background: #ebad3c;
    color: #fff;
    padding: 9px 24px;
    font-size: 16px;
    border-radius: 51px;
}
.btn_new:hover{
	color:#fff;
}

.input_new {
    border-radius: 20px !important;
}
.log_card {
    width: 50%;
    margin: auto;
    background: #fff;
    box-shadow: 0 0 11px black;
    padding: 19px;
    border-radius: 6px;
}
.link a{
	
	color:#ebad3c;
	text-decoration:none;
}
.link a:hover{
	color:#000;
}
.log_card h2 {
    border-bottom: 2px solid #ccc6c6;
    width: 83px;
	font-size:30px !important;
    padding-bottom: 6px;
}
.right_border{
	width:100%;
}
  </style>

<div class="woocommerce"><div class="woocommerce-notices-wrapper"></div>
<div class="row">
<div class="right_border ">
<div class="log_card">
		<h2>Login</h2>

		<form action="https://edu-therapeutics.com/my-account/" class="woocommerce-form woocommerce-form-login login" id="formdata" method="post">

			
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide ">
				<label for="username">Email address&nbsp;<span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text input_1 form-control input_new" name="username" id="username" autocomplete="username" placeholder="Username or email address" value="" required>			
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide ">
				<label for="password">Password&nbsp;<span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text input_1 form-control input_new " type="password" name="password" id="password" placeholder="Password" autocomplete="current-password" required>
			</div>

			
			<div class="form-row">
				<!---<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
					<a href="https://edu-therapeutics.com/sign-up">Create an account</a>
				</label>--->
				<div class=" col-md-8 col-sm-12 col-xs-12 text-left pt-2 link">
					<div class="g-recaptcha" data-sitekey="6LewRcUUAAAAACv3hVnj7mGFuvBjQYYG84PHriXS"></div>
					
					<a href="https://edu-therapeutics.com/my-account/lost-password/"> Lost your password?</a>
				</div> 
				
				
				<div class="col-md-4 col-sm-12 col-xs-12 text-right link pt-2 ">		
					<a href="https://edu-therapeutics.com/sign-up">Create an account?</a> &nbsp 
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 text-center pt-4">
				<button type="button" class="woocommerce-button button woocommerce-form-login__submit lap_login btn btn-warning btn_new" name="login" value="Log in">Log in</button>
				</div>
				
			</div>
			
			
</div>
			
		</form>
</div>
<!--<div class="col-md-6">
<div class="log_card">
<h2>Sign Up</h2>
<form action="https://edu-therapeutics.com/my-account/" class="woocommerce-form woocommerce-form-login login" id="formdata" method="post">
            <div class="form-group ">
			<label for="first_name">First Name:</label>
			    	<input type="text" class="form-control  input_new" name="first_name" placeholder="First Name" required>
			</div>
			<div class="form-group ">
			    <label for="last_name">Last Name:</label>
			    	<input type="text" class="form-control input_1" name="last_name" placeholder="Last Name" required>
            </div>
			<div class="form-group ">
			<label for="email">Email:</label>
			    	<input type="email" class="form-control input_1" name="email" placeholder="Email" required>
				</div>
                <div class="form-group ">				
					<label for="email">Phone:</label>
			    	<input type="text" class="form-control input_1" name="phone" placeholder="Phone" required>
					</div>
					<div class="form-group ">
					<label for="usertype">UserType:</label><br>
			    	<select class="form-control input_1" name="usertype" required>
			    		<option value="">Select UserType...</option>
			    		<option value="teacher">Individual</option>
			    		<option value="Institute">Institute</option>
			    	</select>
					</div>
					<div class="form-group ">
					<label for="pwd">Password:</label>
			    	<input type="password" class="form-control input_1" placeholder="Password" name="password" required>
					</div>
					<div class="button text-right pt-4">
			  		<!--<span style="color:black;float: left;">Already have an account <a style="color:#4582FF" target="_blank" href="http://edu-therapeutics.com/lap/login">Login</a></span>-->
			  		
			<!--<p class="form-row">
			
				<input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="fcb69f31ee"><input type="hidden" name="_wp_http_referer" value="/my-account">				
				<button type="button" class="woocommerce-button button woocommerce-form-login__submit lap_login btn btn-warning" name="login" value="Log in">Log in</button>
			</p>-->
			<!--<p class="woocommerce-LostPassword lost_password">
				<a href="https://edu-therapeutics.com/my-account/lost-password/">Lost your password?</a>
			</p>--

			
		</form>
		</div>
</div>--->
</div>
</div>
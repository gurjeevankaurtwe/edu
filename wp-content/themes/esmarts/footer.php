<?php do_action( 'esmarts_elated_get_footer_template' ); ?>
<style>
	.lap_button {
	    width: 209px;
	}
	.lap_login_button {
	    width: 209px;
	}
	h2.eltdf-single-product-title {
	    font-size: 26px;
	}
	span.woocommerce-Price-amount.amount {
	    font-size: 17px !important;
	}
	h4.eltdf-product-list-title {
	    font-size: 13px;
	}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/></style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>

	var pathname = window.location.pathname;
	if(pathname == '/cart/')
	{
		(function()
		{
		  if( window.localStorage )
		  {
		    if( !localStorage.getItem('firstLoad') )
		    {
		      localStorage['firstLoad'] = true;
		      window.location.href = "https://edu-therapeutics.com/cart/";
		    }  
		    else
		      localStorage.removeItem('firstLoad');
		  }
		})();
		
	}

	<?php if ( is_user_logged_in() ) { ?>
		jQuery('.lap_button').css('display','none');
		jQuery('.lap_login_button').css('display','block');  
	<?php } else{ ?>
		jQuery('.lap_button').css('display','block');
		jQuery('.lap_login_button').css('display','none');
	<?php } ?>
	
	jQuery(document).on('click','.add_to_cart_button',function(){
		window.location.href = "https://edu-therapeutics.com/cart/";
	});
	jQuery(document).on('click','.lap_save_data ',function(){
		var reqlength = $('.input_1').length;
	    var value = $('.input_1').filter(function () 
	    {
	        return this.value != '';
	    });

	    if (value.length>=0 && (value.length !== reqlength)) 
	    {
	        toastr.error('Please fill out all required fields.');
	    } 
	    else 
	    {
	        var form = $('#formdata').serialize();
			jQuery('.lap_save_data').attr('disabled',true);
			jQuery('.lap_save_data').html('Processing...');
			jQuery.ajax({
		        type: "POST",
		        url: "/wp-admin/admin-ajax.php",
		        data: {
		            action: 'folder_contents',
		            variable: form 
		        },
		        dataType : "json",
		        success: function (data) 
		        {
		        	jQuery('.lap_save_data').attr('disabled',false);
					jQuery('.lap_save_data').html('Submit');
		        	if(data.success == true)
		        	{
		        		toastr.success(data.msg);
		        		window.location.href = data.url; 
		        	}
		        	else
		        	{
		        		toastr.error(data.msg);
		        	}
		           
		        }
		    });
	    }
	});
	jQuery(document).on('click','.lap_login ',function(){
		var reqlength = $('.input_1').length;
	    var value = $('.input_1').filter(function () 
	    {
	        return this.value != '';
	    });

	    if (value.length>=0 && (value.length !== reqlength)) 
	    {
	        toastr.error('Please fill out all required fields.');
	    } 
	    else 
	    {
	        var form = $('#formdata').serialize();
	        jQuery('.lap_login').attr('disabled',true);
			jQuery('.lap_login').html('Processing...');
			jQuery.ajax({
		        type: "POST",
		        url: "/wp-admin/admin-ajax.php",
		        data: {
		            action: 'login_contents',
		            variable: form 
		        },
		        dataType : "json",
		        success: function (data) 
		        {
		        	jQuery('.lap_login').attr('disabled',false);
					jQuery('.lap_login').html('Submit');
		        	if(data.success == true)
		        	{
		        		toastr.success(data.msg);
		        		window.location.href = data.url; 
		        	}
		        	else
		        	{
		        		toastr.error(data.msg);
		        	}
		           
		        }
		    });
	    }
	});
	</script>
<?php
$price = eltdf_lms_calculate_course_price( get_the_ID() );
$currency_postition = get_option( 'woocommerce_currency_pos' );
?>
<div class="eltdf-ci-price-holder">
	<?php if ( $price == 0 ) { ?>
		<span class="eltdf-ci-price-free">
        <?php esc_html_e( 'Free', 'eltdf-lms' ); ?>
      </span>
	<?php } else { ?>
		<span class="eltdf-ci-price-value">
		<?php
			switch ( $currency_postition ) {
				case 'left':
					echo get_woocommerce_currency_symbol() . esc_html( $price );
					break;
				case 'right':
					echo esc_html( $price ) . get_woocommerce_currency_symbol();
					break;
				case 'left_space':
					echo get_woocommerce_currency_symbol() . '  ' . esc_html( $price ) ;
					break;
				case 'right_space':
					echo esc_html( $price ) . '  ' . get_woocommerce_currency_symbol();
					break;
			}
		   ?>
      </span>
	<?php } ?>
</div>

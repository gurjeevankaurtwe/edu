<?php

if ( ! function_exists( 'eltdf_core_map_masonry_gallery_meta' ) ) {
	function eltdf_core_map_masonry_gallery_meta() {
		
		$masonry_gallery_meta_box = esmarts_elated_create_meta_box(
			array(
				'scope' => array( 'masonry-gallery' ),
				'title' => esc_html__( 'Masonry Gallery General', 'eltdf-core' ),
				'name'  => 'masonry_gallery_meta'
			)
		);
		
		esmarts_elated_create_meta_box_field(
			array(
				'name'          => 'eltdf_masonry_gallery_item_title_tag',
				'type'          => 'select',
				'default_value' => 'h4',
				'label'         => esc_html__( 'Title Tag', 'eltdf-core' ),
				'parent'        => $masonry_gallery_meta_box,
				'options'       => esmarts_elated_get_title_tag()
			)
		);
		
		esmarts_elated_create_meta_box_field(
			array(
				'name'   => 'eltdf_masonry_gallery_item_text',
				'type'   => 'text',
				'label'  => esc_html__( 'Text', 'eltdf-core' ),
				'parent' => $masonry_gallery_meta_box
			)
		);
		
		esmarts_elated_create_meta_box_field(
			array(
				'name'   => 'eltdf_masonry_gallery_item_image',
				'type'   => 'image',
				'label'  => esc_html__( 'Custom Item Icon', 'eltdf-core' ),
				'parent' => $masonry_gallery_meta_box
			)
		);
		
		esmarts_elated_create_meta_box_field(
			array(
				'name'   => 'eltdf_masonry_gallery_item_link',
				'type'   => 'text',
				'label'  => esc_html__( 'Link', 'eltdf-core' ),
				'parent' => $masonry_gallery_meta_box
			)
		);

		esmarts_elated_create_meta_box_field(
			array(
				'name'          => 'eltdf_masonry_gallery_item_link_target',
				'type'          => 'select',
				'default_value' => '_self',
				'label'         => esc_html__( 'Link Target', 'eltdf-core' ),
				'options'       => esmarts_elated_get_link_target_array(),
				'parent'        => $masonry_gallery_meta_box
			)
		);
		
		esmarts_elated_create_meta_box_field(
			array(
				'name'   => 'eltdf_masonry_gallery_item_link_label',
				'type'   => 'text',
				'label'  => esc_html__( 'Link Label', 'eltdf-core' ),
				'parent' => $masonry_gallery_meta_box
			)
		);
		
		esmarts_elated_add_admin_section_title(
			array(
				'name'   => 'eltdf_section_style_title',
				'parent' => $masonry_gallery_meta_box,
				'title'  => esc_html__( 'Masonry Gallery Item Style', 'eltdf-core' )
			)
		);
		
		esmarts_elated_create_meta_box_field(
			array(
				'name'          => 'eltdf_masonry_gallery_item_size',
				'type'          => 'select',
				'default_value' => 'square-small',
				'label'         => esc_html__( 'Size', 'eltdf-core' ),
				'parent'        => $masonry_gallery_meta_box,
				'options'       => array(
					'square-small'        => esc_html__( 'Square Small', 'eltdf-core' ),
					'square-big'          => esc_html__( 'Square Big', 'eltdf-core' ),
					'rectangle-portrait'  => esc_html__( 'Rectangle Portrait', 'eltdf-core' ),
					'rectangle-landscape' => esc_html__( 'Rectangle Landscape', 'eltdf-core' )
				)
			)
		);
		
		esmarts_elated_create_meta_box_field(
			array(
				'name'          => 'eltdf_masonry_gallery_item_type',
				'type'          => 'select',
				'default_value' => 'standard',
				'label'         => esc_html__( 'Type', 'eltdf-core' ),
				'parent'        => $masonry_gallery_meta_box,
				'options'       => array(
					'standard' => esc_html__( 'Standard', 'eltdf-core' ),
					'simple'   => esc_html__( 'Simple', 'eltdf-core' ),
					'no-info'  => esc_html__( 'No Info', 'eltdf-core' )
				)
			)
		);
		
		esmarts_elated_create_meta_box_field(
			array(
				'name'          => 'eltdf_masonry_gallery_item_skin',
				'type'          => 'select',
				'label'         => esc_html__( 'Content Info Skin', 'eltdf-core' ),
				'parent'        => $masonry_gallery_meta_box,
				'options'       => array(
					''      => esc_html__( 'Default', 'eltdf-core' ),
					'light' => esc_html__( 'Light', 'eltdf-core' )
				)
			)
		);
	}
	
	add_action( 'esmarts_elated_action_meta_boxes_map', 'eltdf_core_map_masonry_gallery_meta', 45 );
}
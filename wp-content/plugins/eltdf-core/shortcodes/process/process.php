<?php
namespace ElatedCore\CPT\Shortcodes\Process;

use ElatedCore\Lib;

class Process implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'eltdf_process';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'            => esc_html__( 'Elated Process', 'eltdf-core' ),
					'base'            => $this->base,
					'icon'            => 'icon-wpb-process extended-custom-icon',
					'category'        => esc_html__( 'by ELATED', 'eltdf-core' ),
					'as_parent'       => array( 'only' => 'eltdf_process_item' ),
					'content_element' => true,
					'js_view'         => 'VcColumnView',
					'params'          => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'custom_class',
							'heading'     => esc_html__( 'Custom CSS Class', 'eltdf-core' ),
							'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'number_of_columns',
							'heading'     => esc_html__( 'Number Of Columns', 'eltdf-core' ),
							'value'       => array(
								esc_html__( '2 Columns', 'eltdf-core' ) => 'two',
								esc_html__( '3 Columns', 'eltdf-core' ) => 'three',
								esc_html__( '4 Columns', 'eltdf-core' ) => 'four'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'switch_to_one_column',
							'heading'     => esc_html__( 'Switch to One Column', 'eltdf-core' ),
							'value'       => array(
								esc_html__( 'Default None', 'eltdf-core' ) => '',
								esc_html__( 'Below 1280px', 'eltdf-core' ) => '1280',
								esc_html__( 'Below 1024px', 'eltdf-core' ) => '1024',
								esc_html__( 'Below 768px', 'eltdf-core' )  => '768',
								esc_html__( 'Below 680px', 'eltdf-core' )  => '680',
								esc_html__( 'Below 480px', 'eltdf-core' )  => '480'
							),
							'description' => esc_html__( 'Choose on which stage item will be in one column', 'eltdf-core' ),
							'save_always' => true
						),
						array(
							'type'        => 'attach_image',
							'param_name'  => 'process_background_image',
							'heading'     => esc_html__( 'Process Background Image', 'eltdf-core' ),
							'description' => esc_html__( 'Select image from media library', 'eltdf-core' )
						),
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'custom_class'             => '',
			'number_of_columns'        => 'three',
			'switch_to_one_column'     => '',
			'process_background_image' => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['holder_classes']    = $this->getHolderClasses( $params, $args );
		$params['number_of_items']   = $this->getNumberOfItems( $params['number_of_columns'] );
		$params['background_styles'] = $this->getProcessBackgroundStyles( $params['process_background_image'] );
		$params['content']           = $content;
		
		$html = eltdf_core_get_shortcode_module_template_part( 'templates/process-template', 'process', '', $params );
		
		return $html;
	}
	
	private function getHolderClasses( $params, $args ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['custom_class'] ) ? esc_attr( $params['custom_class'] ) : '';
		$holderClasses[] = ! empty( $params['number_of_columns'] ) ? 'eltdf-' . $params['number_of_columns'] . '-columns' : 'eltdf-' . $args['number_of_columns'] . '-columns';
		$holderClasses[] = ! empty( $params['switch_to_one_column'] ) ? 'eltdf-responsive-' . $params['switch_to_one_column'] : '';
		
		return implode( ' ', $holderClasses );
	}
	
	private function getNumberOfItems( $params ) {
		$number = 3;
		
		switch ($params) {
			case 'two':
				$number = 2;
				break;
			case 'three':
				$number = 3;
				break;
			case 'four':
				$number = 4;
				break;
		}
		
		return $number;
	}
	
	private function getProcessBackgroundStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['process_background_image'] ) ) {
			$styles[] = 'background-image: url(' . wp_get_attachment_url( $params['process_background_image'] ) . ')';
		}
		
		return implode( ';', $styles );
	}
}

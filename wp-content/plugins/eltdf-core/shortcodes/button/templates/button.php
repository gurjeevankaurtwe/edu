<button type="submit" <?php esmarts_elated_inline_style($button_styles); ?> <?php esmarts_elated_class_attribute($button_classes); ?> <?php echo esmarts_elated_get_inline_attrs($button_data); ?> <?php echo esmarts_elated_get_inline_attrs($button_custom_attrs); ?>>
    <span class="eltdf-btn-text"><?php echo esc_html($text); ?></span>
    <?php echo esmarts_elated_icon_collections()->renderIcon($icon, $icon_pack); ?>
    <?php if ($hover_animation === 'yes') { ?>
        <span class="eltdf-btn-hover-item"></span>
    <?php } ?>
</button>
(function ($) {
	'use strict';
	
	var testimonials = {};
	eltdf.modules.testimonials = testimonials;
	
	testimonials.eltdfInitElegantTestimonials = eltdfInitElegantTestimonials;
	
	
	testimonials.eltdfOnWindowLoad = eltdfOnWindowLoad;
	
	$(window).load(eltdfOnWindowLoad);
	
	/*
	 All functions to be called on $(window).load() should be in this function
	 */
	function eltdfOnWindowLoad() {
		eltdfInitElegantTestimonials();
	}
	
	/**
	 * Init testimonials shortcode elegant type
	 */
	function eltdfInitElegantTestimonials(){
		var testimonial = $('.eltdf-testimonials-holder.eltdf-testimonials-elegant');
		
		if(testimonial.length){
			testimonial.each(function(){
				var thisTestimonials = $(this),
					mainTestimonialsSlider = thisTestimonials.children('.eltdf-testimonials'),
					imagePagSlider = thisTestimonials.children('.eltdf-testimonial-image-nav'),
					authorPagSlider = thisTestimonials.children('.eltdf-testimonial-author-nav'),
					loop = true,
					autoplay = true,
					sliderSpeed = 5000,
					sliderSpeedAnimation = 600;
				
				if (mainTestimonialsSlider.data('enable-loop') === 'no') {
					loop = false;
				}
				if (mainTestimonialsSlider.data('enable-autoplay') === 'no') {
					autoplay = false;
				}
				if (typeof mainTestimonialsSlider.data('slider-speed') !== 'undefined' && mainTestimonialsSlider.data('slider-speed') !== false) {
					sliderSpeed = mainTestimonialsSlider.data('slider-speed');
				}
				if (typeof mainTestimonialsSlider.data('slider-speed-animation') !== 'undefined' && mainTestimonialsSlider.data('slider-speed-animation') !== false) {
					sliderSpeedAnimation = mainTestimonialsSlider.data('slider-speed-animation');
				}
				
				if (mainTestimonialsSlider.length && imagePagSlider.length && authorPagSlider.length) {
					var text = mainTestimonialsSlider.owlCarousel({
						items: 1,
						loop: loop,
						autoplay: autoplay,
						autoplayTimeout: sliderSpeed,
						smartSpeed: sliderSpeedAnimation,
						autoplayHoverPause: false,
						dots: false,
						nav: false,
						mouseDrag: false,
						touchDrag: false,
						onInitialize: function () {
							mainTestimonialsSlider.css('visibility', 'visible');
						}
					});
					
					var image = imagePagSlider.owlCarousel({
						loop: loop,
						autoplay: autoplay,
						autoplayTimeout: sliderSpeed,
						smartSpeed: sliderSpeedAnimation,
						autoplayHoverPause: false,
						center: true,
						dots: false,
						nav: false,
						mouseDrag: false,
						touchDrag: false,
						responsive: {
							1025: {
								items: 5
							},
							681: {
								items: 3
							},
							0: {
								items: 1
							}
						},
						onInitialize: function () {
							imagePagSlider.css('visibility', 'visible');
						}
					});
					
					var author = authorPagSlider.owlCarousel({
						items: 1,
						loop: loop,
						autoplay: autoplay,
						autoplayTimeout: sliderSpeed,
						smartSpeed: sliderSpeedAnimation,
						autoplayHoverPause: false,
						center: true,
						dots: false,
						nav: false,
						mouseDrag: false,
						touchDrag: false,
						animateInClass: 'fadeIn',
						animateOut: 'fadeOut',
						onInitialize: function () {
							authorPagSlider.css('visibility', 'visible');
						}
					});
					
					thisTestimonials.css('opacity', '1');
					
					imagePagSlider.find('.owl-item').on('click', function(e){
						e.preventDefault();
						
						var thisItem = $(this),
							itemIndex = thisItem.index(),
							numberOfClones = imagePagSlider.find('.owl-item.cloned').length,
							modifiedItems = itemIndex - numberOfClones / 2 >= 0 ? itemIndex - numberOfClones / 2 : itemIndex;
						
						console.log(modifiedItems);
						
						image.trigger('to.owl.carousel', modifiedItems);
						text.trigger('to.owl.carousel', modifiedItems);
						author.trigger('to.owl.carousel', modifiedItems);
					});
				}
			});
		}
	}
	
})(jQuery);
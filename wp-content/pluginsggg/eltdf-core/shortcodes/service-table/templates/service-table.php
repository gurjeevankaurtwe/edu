<thead>
<tr>
	<?php
	for ( $i = 0; $i < sizeof( $table_titles ); $i ++ ) { ?>
		<th>
			<?php if ( ! empty( $table_star_ratings[ $i ] ) ) { ?>
				<span class="eltdf-st-star-holder">
					<?php for ( $j = 0; $j < intval( $table_star_ratings[ $i ] ); $j ++ ) { ?>
						<span class="eltdf-st-star icon_star"></span>
					<?php } ?>
				</span>
			<?php } ?>
			<?php if ( ! empty( $table_titles[ $i ] ) ) { ?>
				<h4 class="eltdf-st-item-title"><?php echo esmarts_elated_get_module_part( $table_titles[ $i ] ); ?></h4>
			<?php } ?>
			<?php if ( ! empty( $table_prices[ $i ] ) ) { ?>
				<span class="eltdf-st-item-price"><?php echo esc_html( $table_prices[ $i ] ); ?></span>
			<?php } ?>
			<?php if ( ! empty( $table_price_periods[ $i ] ) ) { ?>
				<span class="eltdf-st-item-interval"><?php echo esc_html( $table_price_periods[ $i ] ); ?></span>
			<?php } ?>
		</th>
	<?php } ?>
</tr>
</thead>

<tbody>
<?php
$k = $l = 1;
?>
<?php foreach ( $table_rows as $row ) { ?>
	<tr>
		<td class="eltdf-service-table-feature-title">
			<p><?php echo esc_attr( $row['title'] ); ?></p>
		</td>
		<?php foreach ( $row['features_enabled'] as $feature ) { ?>
			<td>
				<?php if ( $feature == 'yes' ) { ?>
					<span class="eltdf-st-item-mark">
						<span class="eltdf-mark eltdf-checked icon_check"></span>
					</span>
				<?php } else if ( $feature == 'text' ) { ?>
					<span class="eltdf-st-item-mark">
						<span class="eltdf-mark-text"><?php echo esc_html( $params[ 'feature_' . $k . '_' . $l++ . '_custom_text' ] ); ?></span>
					</span>
				<?php } else { ?>
					<span class="eltdf-st-item-mark">
						<span class="eltdf-mark icon_close"></span>
					</span>
				<?php } ?>
			</td>
		<?php } ?>
	</tr>
<?php $k++; $l = 1; } ?>

	<tr class="eltdf-st-item-button">
		<?php
		for ( $i = 0; $i < sizeof( $table_button_texts ); $i ++ ) { ?>
			<td>
				<?php if ( $table_button_texts[ $i ] !== '' ) {
					echo esmarts_elated_get_button_html( array(
						'link' => $table_button_links[ $i ],
						'text' => $table_button_texts[ $i ],
						'type' => 'solid',
						'size' => 'small',
                        'hover_animation'   => 'yes',
					) );
				} ?>
			</td>
		<?php } ?>
	</tr>
</tbody>
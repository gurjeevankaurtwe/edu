<div class="eltdf-membership-dashboard-page">
	<div class="eltdf-membership-dashboard-page-content">
		<div class="eltdf-profile-image">
            <?php echo eltdf_membership_kses_img( $profile_image ); ?>
        </div>
		<p>
			<span><?php esc_html_e( 'First Name', 'eltdf-membership' ); ?>:</span>
			<?php echo esmarts_elated_get_module_part($first_name); ?>
		</p>
		<p>
			<span><?php esc_html_e( 'Last Name', 'eltdf-membership' ); ?>:</span>
			<?php echo esmarts_elated_get_module_part($last_name); ?>
		</p>
		<p>
			<span><?php esc_html_e( 'Email', 'eltdf-membership' ); ?>:</span>
			<?php echo esmarts_elated_get_module_part($email); ?>
		</p>
		<p>
			<span><?php esc_html_e( 'Desription', 'eltdf-membership' ); ?>:</span>
			<?php echo esmarts_elated_get_module_part($description); ?>
		</p>
		<p>
			<span><?php esc_html_e( 'Website', 'eltdf-membership' ); ?>:</span>
			<a href="<?php echo esc_url( $website ); ?>" target="_blank"><?php echo esmarts_elated_get_module_part($website); ?></a>
		</p>
	</div>
</div>

<?php
/**
 * Plugin Name: Elated Membership
 * Description: Plugin that adds social login and user dashboard page
 * Author: Elated Themes
 * Version: 1.1.1
 */

require_once 'load.php';

if ( ! function_exists( 'eltdf_membership_text_domain' ) ) {
	/**
	 * Loads plugin text domain so it can be used in translation
	 */
	function eltdf_membership_text_domain() {
		load_plugin_textdomain( 'eltdf-membership', false, ELATED_MEMBERSHIP_REL_PATH . '/languages' );
	}
	
	add_action( 'plugins_loaded', 'eltdf_membership_text_domain' );
}

if ( ! function_exists( 'eltdf_membership_scripts' ) ) {
	/**
	 * Loads plugin scripts
	 */
	function eltdf_membership_scripts() {
		
		wp_enqueue_style( 'eltdf-membership-style', plugins_url( ELATED_MEMBERSHIP_REL_PATH . '/assets/css/membership.min.css' ) );
        if(esmarts_elated_is_responsive_on()) {
            wp_enqueue_style('eltdf-membership-responsive-style', plugins_url(ELATED_MEMBERSHIP_REL_PATH . '/assets/css/membership-responsive.min.css'));
        }
		
		//include google+ api
		wp_enqueue_script( 'eltdf-membership-google-plus-api', 'https://apis.google.com/js/platform.js', array(), null, false );
		
		$array_deps = array(
			'underscore',
			'jquery-ui-tabs'
		);
		if ( eltdf_membership_theme_installed() ) {
			$array_deps[] = 'esmarts-elated-js-modules';
		}
		
		wp_enqueue_script( 'eltdf-membership-script', plugins_url( ELATED_MEMBERSHIP_REL_PATH . '/assets/js/membership.min.js' ), $array_deps, false, true );
	}
	
	add_action( 'wp_enqueue_scripts', 'eltdf_membership_scripts' );
}

if ( ! function_exists( 'eltdf_membership_style_dynamics_deps' ) ) {
    function eltdf_membership_style_dynamics_deps( $deps ) {
        $style_dynamic_deps_array = array();
        $style_dynamic_deps_array[] = 'eltdf-membership-style';
        if(esmarts_elated_is_responsive_on()) {
            $style_dynamic_deps_array[] = 'eltdf-membership-responsive-style';
        }

        return array_merge($deps, $style_dynamic_deps_array);
    }

    add_filter('esmarts_elated_filter_style_dynamic_deps','eltdf_membership_style_dynamics_deps');
}
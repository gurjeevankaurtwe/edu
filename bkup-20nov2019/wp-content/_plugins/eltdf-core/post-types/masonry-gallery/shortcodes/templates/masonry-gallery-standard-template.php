<article class="eltdf-item-space <?php echo esc_attr( $item_classes ) ?>">
	<div class="eltdf-mg-content">
        <?php if ( ! empty( $item_link ) ) { ?>
            <a itemprop="url" class="eltdf-mg-item-link" href="<?php echo esc_url( $item_link ); ?>" target="<?php echo esc_attr( $item_link_target ); ?>"></a>
        <?php } ?>
		<?php if ( has_post_thumbnail() ) { ?>
			<div class="eltdf-mg-image">
				<div class="eltdf-mg-image-overlay" style="background-image:url(<?php echo esc_url( $background_image_url ); ?>)"></div>
				<?php the_post_thumbnail(); ?>
			</div>
		<?php } ?>
		<div class="eltdf-mg-item-outer">
			<div class="eltdf-mg-item-inner">
				<div class="eltdf-mg-item-content">
					<?php if ( ! empty( $item_image ) ) { ?>
						<img itemprop="image" class="eltdf-mg-item-icon" src="<?php echo esc_url( $item_image['url'] ) ?>" alt="<?php echo esc_attr( $item_image['alt'] ); ?>"/>
					<?php } ?>
					<?php if ( ! empty( $item_title ) ) { ?>
						<<?php echo esc_attr( $item_title_tag ); ?> itemprop="name" class="eltdf-mg-item-title entry-title">
							<?php echo esc_html( $item_title ); ?>
						</<?php echo esc_attr( $item_title_tag ); ?>>
					<?php } ?>
					<?php if ( ! empty( $item_text ) ) { ?>
						<p class="eltdf-mg-item-text"><?php echo esc_html( $item_text ); ?></p>
					<?php } ?>
					<?php if ( ! empty( $item_link ) && ! empty( $item_link_label ) ) { ?>
						<a itemprop="url" class="eltdf-mg-item-button eltdf-btn eltdf-btn-solid eltdf-btn-light eltdf-hover-animation" href="<?php echo esc_url( $item_link ); ?>" target="<?php echo esc_attr( $item_link_target ); ?>">
							<span class="eltdf-btn-text"><?php echo esc_html( $item_link_label ); ?></span>
                            <span class="eltdf-btn-hover-item"></span>
						</a>
					<?php } ?>
				</div>
			</div>
		</div>

	</div>
</article>

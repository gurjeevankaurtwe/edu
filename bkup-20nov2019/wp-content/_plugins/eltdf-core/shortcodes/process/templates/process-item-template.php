<div class="eltdf-process-item <?php echo esc_attr( $holder_classes ); ?>">
	<div class="eltdf-pi-image">
		<?php if ( ! empty( $link_attributes ) ) { ?>
			<a <?php echo implode( ' ', $link_attributes ); ?>>
		<?php } ?>
		<?php echo wp_get_attachment_image($image, 'full'); ?>
		<?php if ( ! empty( $link_attributes ) ) { ?>
			</a>
		<?php } ?>
	</div>
	<?php if(!empty($title)) { ?>
		<<?php echo esc_attr($title_tag); ?> class="eltdf-pi-title" <?php echo esmarts_elated_get_inline_style($title_styles); ?>><?php echo esc_html($title); ?></<?php echo esc_attr($title_tag); ?>>
	<?php } ?>
	<?php if(!empty($text)) { ?>
		<p class="eltdf-pi-text" <?php echo esmarts_elated_get_inline_style($text_styles); ?>><?php echo esc_html($text); ?></p>
	<?php } ?>
</div>
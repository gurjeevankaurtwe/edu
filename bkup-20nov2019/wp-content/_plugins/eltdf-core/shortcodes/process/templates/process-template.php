<div class="eltdf-process-holder <?php echo esc_attr( $holder_classes ); ?>">
	<div class="eltdf-process-background" <?php echo esmarts_elated_get_inline_style($background_styles); ?>></div>
	<div class="eltdf-process-inner">
		<?php echo do_shortcode( $content ); ?>
	</div>
</div>
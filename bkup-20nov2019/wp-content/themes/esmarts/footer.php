<?php do_action( 'esmarts_elated_get_footer_template' ); ?>
<style>
	.lap_button {
	    width: 209px;
	}
	.lap_login_button {
	    width: 160px;
	}
	h2.eltdf-single-product-title {
	    font-size: 26px;
	}
	span.woocommerce-Price-amount.amount {
	    font-size: 17px !important;
	}
	h4.eltdf-product-list-title {
	    font-size: 13px;
	}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/></style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>

	<?php if ( is_user_logged_in() ) { ?>
		jQuery('.lap_button').css('display','none');
		jQuery('.lap_login_button').css('display','block');  
	<?php } else{ ?>
		jQuery('.lap_button').css('display','block');
		jQuery('.lap_login_button').css('display','none');
	<?php } ?>
	
	/*jQuery(document).on('click','.ajax_add_to_cart',function(){
		alert('test')
		jQuery(this).attr('href','#')
		$.ajax({
	        type: "POST",
	        url: "/wp-admin/admin-ajax.php",
	        data: {action : 'remove_item_from_cart','product_id' : '4'},
	        success: function (res) {
	            if (res) {
	                alert('Removed Successfully');
	            }
	        }
	    });
	});*/
	jQuery(document).on('click','.lap_save_data ',function(){
		var reqlength = $('.input_1').length;
	    var value = $('.input_1').filter(function () 
	    {
	        return this.value != '';
	    });

	    if (value.length>=0 && (value.length !== reqlength)) 
	    {
	        toastr.error('Please fill out all required fields.');
	    } 
	    else 
	    {
	        var form = $('#formdata').serialize();
			jQuery('.lap_save_data').attr('disabled',true);
			jQuery('.lap_save_data').html('Processing...');
			jQuery.ajax({
		        type: "POST",
		        url: "/edunew/wp-admin/admin-ajax.php",
		        data: {
		            action: 'folder_contents',
		            variable: form 
		        },
		        dataType : "json",
		        success: function (data) 
		        {
		        	jQuery('.lap_save_data').attr('disabled',false);
					jQuery('.lap_save_data').html('Submit');
		        	/*if(data.success == true)
		        	{
		        		toastr.success(data.msg);
		        		window.location.href = data.url; 
		        	}
		        	else
		        	{
		        		toastr.error(data.msg);
		        	}*/
		           
		        }
		    });
	    }
	});
	</script>
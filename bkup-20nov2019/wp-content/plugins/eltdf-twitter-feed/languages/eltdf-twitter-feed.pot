# Copyright (C) 2019 Elated Themes
# This file is distributed under the same license as the Elated Twitter Feed plugin.
msgid ""
msgstr ""
"Project-Id-Version: Elated Twitter Feed 1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/eltdf-twitter-feed\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2019-07-16T15:16:42+02:00\n"
"PO-Revision-Date: 2019-07-16T15:16:42+02:00\n"
"X-Generator: WP-CLI 2.0.1\n"
"X-Domain: eltdf-twitter-feed\n"

#. Plugin Name of the plugin
msgid "Elated Twitter Feed"
msgstr ""

#. Description of the plugin
msgid "Plugin that adds Twitter feed functionality to our theme"
msgstr ""

#. Author of the plugin
msgid "Elated Themes"
msgstr ""

#: \shortcodes\twitter-list\holder.php:20
msgid "It seams that you haven't connected with your Twitter account"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:24
msgid "Elated Twitter List"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:26
msgid "by ELATED"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:33
#: \widgets\eltdf-twitter-widget.php:40
msgid "User ID"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:39
msgid "Number of Columns"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:41
msgid "One"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:42
msgid "Two"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:43
msgid "Three"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:44
msgid "Four"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:45
msgid "Five"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:52
msgid "Space Between Columns"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:58
#: \widgets\eltdf-twitter-widget.php:45
msgid "Number of Tweets"
msgstr ""

#: \shortcodes\twitter-list\twitter-list.php:63
#: \widgets\eltdf-twitter-widget.php:68
msgid "Tweets Cache Time"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:12
msgid "Elated Twitter Widget"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:14
msgid "Display your Twitter feed"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:26
msgid "Title"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:31
msgid "Type"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:33
msgid "Standard"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:34
msgid "Slider"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:50
msgid "Show Tweet Icon"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:52
#: \widgets\eltdf-twitter-widget.php:62
msgid "Yes"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:53
#: \widgets\eltdf-twitter-widget.php:61
msgid "No"
msgstr ""

#: \widgets\eltdf-twitter-widget.php:59
msgid "Show Tweet Time"
msgstr ""

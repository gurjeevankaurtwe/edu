<div class="eltdf-register-notice">
	<p class="eltdf-register-notice-title"><?php echo esc_html($message); ?></p>
	<button href="#" class="eltdf-login-action-btn eltdf-modal-opener eltdf-btn eltdf-btn-large eltdf-btn-solid eltdf-hover-animation" data-modal="login" data-title="<?php esc_attr_e('LOGIN', 'eltdf-membership'); ?>">
        <span class="eltdf-log-in-text"><?php esc_html_e('LOGIN', 'eltdf-membership'); ?></span>
    </button>
</div>
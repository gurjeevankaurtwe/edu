<?php
namespace ElatedCore\CPT\Shortcodes\SectionTitle;

use ElatedCore\Lib;

class SectionTitle implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'eltdf_section_title';
		
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Elated Section Title', 'eltdf-core' ),
					'base'                      => $this->base,
					'category'                  => esc_html__( 'by ELATED', 'eltdf-core' ),
					'icon'                      => 'icon-wpb-section-title extended-custom-icon',
					'allowed_container_element' => 'vc_row',
					'params'                    => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'custom_class',
							'heading'     => esc_html__( 'Custom CSS Class', 'eltdf-core' ),
							'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'eltdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'content_padding',
							'heading'    => esc_html__( 'Content Side Padding (px or %)', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'content_alignment',
							'heading'     => esc_html__( 'Content Alignment', 'eltdf-core' ),
							'value'       => array(
								esc_html__( 'Default', 'eltdf-core' ) => '',
								esc_html__( 'Left', 'eltdf-core' )    => 'left',
								esc_html__( 'Center', 'eltdf-core' )  => 'center',
								esc_html__( 'Right', 'eltdf-core' )   => 'right'
							),
							'save_always' => true
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'eltdf-core' ),
							'admin_label' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'title_tag',
							'heading'     => esc_html__( 'Title Tag', 'eltdf-core' ),
							'value'       => array_flip( esmarts_elated_get_title_tag( true, array( 'span' => esc_html__( 'Custom Heading', 'eltdf-core' ) ) ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'title', 'not_empty' => true ),
							'group'       => esc_html__( 'Title Style', 'eltdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'title_color',
							'heading'    => esc_html__( 'Title Color', 'eltdf-core' ),
							'dependency' => array( 'element' => 'title', 'not_empty' => true ),
							'group'      => esc_html__( 'Title Style', 'eltdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'watermark',
							'heading'    => esc_html__( 'Title Watermark', 'eltdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'watermark_color',
							'heading'    => esc_html__( 'Title Watermark Color', 'eltdf-core' ),
							'dependency' => array( 'element' => 'watermark', 'not_empty' => true ),
							'group'      => esc_html__( 'Title Style', 'eltdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'watermark_font_size',
							'heading'    => esc_html__( 'Title Watermark Font Size (px)', 'eltdf-core' ),
							'dependency' => array( 'element' => 'watermark', 'not_empty' => true ),
							'group'      => esc_html__( 'Title Style', 'eltdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'tagline',
							'heading'    => esc_html__( 'Tagline', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'tagline_tag',
							'heading'     => esc_html__( 'Tagline Tag', 'eltdf-core' ),
							'value'       => array_flip( esmarts_elated_get_title_tag( true, array( 'p' => 'p', esc_html__( 'Custom', 'eltdf-core' ) => 'span' ) ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'tagline', 'not_empty' => true ),
							'group'       => esc_html__( 'Tagline Style', 'eltdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'tagline_color',
							'heading'    => esc_html__( 'Tagline Color', 'eltdf-core' ),
							'dependency' => array( 'element' => 'tagline', 'not_empty' => true ),
							'group'      => esc_html__( 'Tagline Style', 'eltdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'tagline_margin',
							'heading'    => esc_html__( 'Tagline Bottom Margin (px)', 'eltdf-core' ),
							'dependency' => array( 'element' => 'tagline', 'not_empty' => true ),
							'group'      => esc_html__( 'Tagline Style', 'eltdf-core' )
						),
						array(
							'type'       => 'textarea',
							'param_name' => 'text',
							'heading'    => esc_html__( 'Text', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'text_tag',
							'heading'     => esc_html__( 'Text Tag', 'eltdf-core' ),
							'value'       => array_flip( esmarts_elated_get_title_tag( true, array( 'p' => 'p', esc_html__( 'Custom', 'eltdf-core' ) => 'span' ) ) ),
							'save_always' => true,
							'dependency'  => array( 'element' => 'text', 'not_empty' => true ),
							'group'       => esc_html__( 'Text Style', 'eltdf-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'text_color',
							'heading'    => esc_html__( 'Text Color', 'eltdf-core' ),
							'dependency' => array( 'element' => 'text', 'not_empty' => true ),
							'group'      => esc_html__( 'Text Style', 'eltdf-core' )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'text_margin',
							'heading'    => esc_html__( 'Text Top Margin (px)', 'eltdf-core' ),
							'dependency' => array( 'element' => 'text', 'not_empty' => true ),
							'group'      => esc_html__( 'Text Style', 'eltdf-core' )
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args = array(
			'custom_class'        => '',
			'content_padding'     => '',
			'content_alignment'   => '',
			'title'               => '',
			'title_tag'           => 'h2',
			'title_color'         => '',
			'watermark'           => '',
			'watermark_color'     => '',
			'watermark_font_size' => '',
			'tagline'             => '',
			'tagline_tag'         => 'span',
			'tagline_color'       => '',
			'tagline_margin'      => '',
			'text'                => '',
			'text_tag'            => 'span',
			'text_color'          => '',
			'text_margin'         => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['holder_classes']   = $this->getHolderClasses( $params, $args );
		$params['holder_styles']    = $this->getHolderStyles( $params );
		$params['title_tag']        = ! empty( $params['title_tag'] ) ? $params['title_tag'] : $args['title_tag'];
		$params['title_styles']     = $this->getTitleStyles( $params );
		$params['watermark_styles'] = $this->getWatermarkStyles( $params );
		$params['tagline_tag']      = ! empty( $params['tagline_tag'] ) ? $params['tagline_tag'] : $args['tagline_tag'];
		$params['tagline_styles']   = $this->getTaglineStyles( $params );
		$params['text_tag']         = ! empty( $params['text_tag'] ) ? $params['text_tag'] : $args['text_tag'];
		$params['text_styles']      = $this->getTextStyles( $params );
		
		$html = eltdf_core_get_shortcode_module_template_part( 'templates/section-title', 'section-title', '', $params );
		
		return $html;
	}
	
	private function getHolderClasses( $params, $args ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['custom_class'] ) ? esc_attr( $params['custom_class'] ) : '';
		$holderClasses[] = ! empty( $params['watermark'] ) ? 'eltdf-st-has-watermark' : '';
		
		return implode( ' ', $holderClasses );
	}
	
	private function getHolderStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['content_padding'] ) ) {
			$styles[] = 'padding: 0 ' . $params['content_padding'];
		}
		
		if ( ! empty( $params['content_alignment'] ) ) {
			$styles[] = 'text-align: ' . $params['content_alignment'];
		}
		
		return implode( ';', $styles );
	}
	
	private function getTitleStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['title_color'] ) ) {
			$styles[] = 'color: ' . $params['title_color'];
		}
		
		return implode( ';', $styles );
	}
	
	private function getWatermarkStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['watermark_color'] ) ) {
			$styles[] = 'color: ' . $params['watermark_color'];
		}
		
		if ( $params['watermark_font_size'] !== '' ) {
			$styles[] = 'font-size: ' . esmarts_elated_filter_px( $params['watermark_font_size'] ) . 'px';
		}
		
		return implode( ';', $styles );
	}
	
	private function getTaglineStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['tagline_color'] ) ) {
			$styles[] = 'color: ' . $params['tagline_color'];
		}
		
		if ( $params['tagline_margin'] !== '' ) {
			$styles[] = 'margin-bottom: ' . esmarts_elated_filter_px( $params['tagline_margin'] ) . 'px';
		}
		
		return implode( ';', $styles );
	}
	
	private function getTextStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['text_color'] ) ) {
			$styles[] = 'color: ' . $params['text_color'];
		}
		
		if ( $params['text_margin'] !== '' ) {
			$styles[] = 'margin-top: ' . esmarts_elated_filter_px( $params['text_margin'] ) . 'px';
		}
		
		return implode( ';', $styles );
	}
}
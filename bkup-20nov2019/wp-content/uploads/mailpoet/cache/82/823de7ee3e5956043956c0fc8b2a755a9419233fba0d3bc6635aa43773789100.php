<?php

use MailPoetVendor\Twig\Environment;
use MailPoetVendor\Twig\Error\LoaderError;
use MailPoetVendor\Twig\Error\RuntimeError;
use MailPoetVendor\Twig\Markup;
use MailPoetVendor\Twig\Sandbox\SecurityError;
use MailPoetVendor\Twig\Sandbox\SecurityNotAllowedTagError;
use MailPoetVendor\Twig\Sandbox\SecurityNotAllowedFilterError;
use MailPoetVendor\Twig\Sandbox\SecurityNotAllowedFunctionError;
use MailPoetVendor\Twig\Source;
use MailPoetVendor\Twig\Template;

/* settings/woocommerce.html */
class __TwigTemplate_74a9fbd0da19b954c1fd50d9b570262f07db6d5061c493c9b9b59bc66776753e extends \MailPoetVendor\Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<table class=\"form-table\">
  ";
        // line 2
        if (($context["display_woocommerce_editor"] ?? null)) {
            // line 3
            echo "  <tr>
    <th scope=\"row\">
      <label for=\"settings[woocommerce_use_mailpoet_editor]\">
        ";
            // line 6
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Use MailPoet to customize WooCommerce emails", "Setting for using our editor for WooCommerce email");
            echo "
      </label>
      <p class=\"description\">
        ";
            // line 9
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("You can use the MailPoet editor to customize the template used to send WooCommerce emails (notification for order processing, completed, ...).", "Setting for using our editor for WooCommerce email");
            echo "
      </p>
    </th>
    <td>
      <p>
        <input
        data-toggle=\"mailpoet_woocommerce_use_mailpoet_editor\"
        type=\"checkbox\"
        value=\"1\"
        id=\"settings[woocommerce_use_mailpoet_editor]\"
        name=\"woocommerce[use_mailpoet_editor]\"
        ";
            // line 20
            if ($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "woocommerce", []), "use_mailpoet_editor", [])) {
                echo "checked=\"checked\"";
            }
            // line 21
            echo "        >
        <br>
        <a class=\"button-secondary mailpoet_woocommerce_editor_button\" href=\"?page=mailpoet-newsletter-editor&id=";
            // line 23
            echo \MailPoetVendor\twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "woocommerce", []), "transactional_email_id", []), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Open template editor", "Settings button to go to WooCommerce email editor");
            echo "</a>
        <a class=\"button-secondary mailpoet_woocommerce_editor_button_disabled\" disabled=\"disabled\">";
            // line 24
            echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Open template editor", "Settings button to go to WooCommerce email editor");
            echo "</a>
      </p>
      <input
        type=\"hidden\"
        value=\"";
            // line 28
            echo \MailPoetVendor\twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["settings"] ?? null), "woocommerce", []), "transactional_email_id", []), "html", null, true);
            echo "\"
        name=\"woocommerce[transactional_email_id]\"
      >
    </td>
  </tr>
  ";
        }
        // line 34
        echo "  <tr>
    <th scope=\"row\">
      <label for=\"settings[woocommerce_optin_on_checkout]\">
        ";
        // line 37
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Opt-in on checkout", "settings area: add an email opt-in checkbox on the checkout page (e-commerce websites)");
        echo "
      </label>
      <p class=\"description\">
        ";
        // line 40
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Customers can subscribe to the \"WooCommerce Customers\" list via a checkbox on the checkout page.");
        echo "
      </p>
    </th>
    <td>
      <p>
        <input
        data-toggle=\"mailpoet_woocommerce_optin_on_checkout\"
        type=\"checkbox\"
        value=\"1\"
        id=\"settings[woocommerce_optin_on_checkout]\"
        name=\"woocommerce[optin_on_checkout][enabled]\"
        ";
        // line 51
        if ($this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "woocommerce", []), "optin_on_checkout", []), "enabled", [])) {
            echo "checked=\"checked\"";
        }
        // line 52
        echo "        >
      </p>
    </td>
  </tr>
  <tr id=\"mailpoet_woocommerce_optin_on_checkout\">
    <th scope=\"row\">
      <label for=\"settings[woocommerce_checkbox_optin_message]\">
        ";
        // line 59
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Checkbox opt-in message", "settings area: set the email opt-in message on the checkout page (e-commerce websites)");
        echo "
      </label>
      <p class=\"description\">
        ";
        // line 62
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("This is the checkbox message your customers will see on your WooCommerce checkout page to subscribe to the \"WooCommerce Customers\" list.");
        echo "
      </p>
    </th>
    <td>
      <p>
        <input type=\"text\"
          id=\"woocommerce_checkbox_optin_message\"
          name=\"woocommerce[optin_on_checkout][message]\"
          value=\"";
        // line 70
        echo \MailPoetVendor\twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "woocommerce", []), "optin_on_checkout", []), "message", []), "html", null, true);
        echo "\"
          placeholder=\"";
        // line 71
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Checkbox opt-in message", "placeholder text for the WooCommerce checkout opt-in message");
        echo "\"
          class=\"regular-text\" />
          <br>
          <div id=\"settings_woocommerce_optin_on_checkout_error\" class=\"mailpoet_error_item mailpoet_error\">
            ";
        // line 75
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("The checkbox opt-in message cannot be empty.");
        echo "
          </div>
      </p>
    </td>
  </tr>
  <tr>
    <th scope=\"row\">
      <label for=\"settings[mailpoet_subscribe_old_woocommerce_customers]\">
        ";
        // line 83
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Subscribe old WooCommerce customers");
        echo "
      </label>
      <p class=\"description\">
        ";
        // line 86
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Subscribe all my past customers to this list because they agreed to receive marketing emails from me.");
        echo "
      </p>
    </th>
    <td>
      <p>
        <input
        type=\"checkbox\"
        value=\"1\"
        id=\"settings[mailpoet_subscribe_old_woocommerce_customers]\"
        name=\"mailpoet_subscribe_old_woocommerce_customers[enabled]\"
        ";
        // line 96
        if ($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "mailpoet_subscribe_old_woocommerce_customers", []), "enabled", [])) {
            echo "checked=\"checked\"";
        }
        // line 97
        echo "        >
        ";
        // line 107
        echo "        <input
          type=\"hidden\"
          value=\"1\"
          name=\"mailpoet_subscribe_old_woocommerce_customers[dummy]\"
        >
      </p>
    </td>
  </tr>
  <tr>
    <th scope=\"row\">
      <label for=\"settings[mailpoet_accept_cookie_revenue_tracking]\">
        ";
        // line 118
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("Enable browser cookies", "Option in settings page: the user can accept or forbid MailPoet to use browser cookies");
        echo "
      </label>
      <p class=\"description\">
        ";
        // line 121
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translateWithContext("If you enable this option, MailPoet will use browser cookies for more precise WooCommerce tracking. This is practical for abandoned cart emails for example.", "Browser cookies are data created by websites and stored in visitors web browser");
        echo "
      </p>
    </th>
    <td>
      <p>
        <input
          type=\"checkbox\"
          value=\"1\"
          id=\"settings[mailpoet_accept_cookie_revenue_tracking]\"
          data-automation-id=\"accept_cookie_revenue_tracking\"
          name=\"woocommerce[accept_cookie_revenue_tracking][enabled]\"
          ";
        // line 132
        if (($this->getAttribute($this->getAttribute($this->getAttribute(($context["settings"] ?? null), "woocommerce", []), "accept_cookie_revenue_tracking", []), "enabled", []) == "1")) {
            echo "checked=\"checked\"";
        }
        // line 133
        echo "        >
        <input
          type=\"hidden\"
          value=\"1\"
          name=\"woocommerce[accept_cookie_revenue_tracking][set]\"
        >
      </p>
    </td>
  </tr>
</table>
";
    }

    public function getTemplateName()
    {
        return "settings/woocommerce.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 133,  226 => 132,  212 => 121,  206 => 118,  193 => 107,  190 => 97,  186 => 96,  173 => 86,  167 => 83,  156 => 75,  149 => 71,  145 => 70,  134 => 62,  128 => 59,  119 => 52,  115 => 51,  101 => 40,  95 => 37,  90 => 34,  81 => 28,  74 => 24,  68 => 23,  64 => 21,  60 => 20,  46 => 9,  40 => 6,  35 => 3,  33 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "settings/woocommerce.html", "/home/developer/public_html/wp-content/plugins/mailpoet/views/settings/woocommerce.html");
    }
}

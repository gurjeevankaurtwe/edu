<div class="eltdf-testimonial-content" id="eltdf-testimonials-<?php echo esc_attr( $current_id ) ?>">
	<div class="eltdf-testimonial-content-inner">
		<?php if ( ! empty( $text ) ) { ?>
			<p class="eltdf-testimonial-text"><?php echo esc_html( $text ); ?></p>
		<?php } ?>
	</div>
</div>
<div <?php esmarts_elated_class_attribute( $holder_classes ); ?>>
	<div class="eltdf-masonry-elements-item-inner <?php echo esc_attr( $item_class ); ?>" <?php esmarts_elated_inline_style( $holder_style ); ?> <?php echo esmarts_elated_get_inline_attrs( $item_data ); ?>>
		<span class="eltdf-masonry-elements-item-background" <?php esmarts_elated_inline_style( $holder_background ); ?>></span>
		<div class="eltdf-masonry-elements-item-inner-helper">
			<div class="eltdf-masonry-elements-item-inner-tb">
				<div class="eltdf-masonry-elements-item-inner-tc" <?php esmarts_elated_inline_style( $inner_style ); ?>>
					<?php echo do_shortcode( $content ); ?>
				</div>
			</div>
		</div>
	</div>
</div>
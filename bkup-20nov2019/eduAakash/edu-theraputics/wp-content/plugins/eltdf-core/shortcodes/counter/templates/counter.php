<div class="eltdf-counter-holder <?php echo esc_attr($holder_classes); ?>">
	<div class="eltdf-counter-inner">
		<?php if (!empty($image)) { ?>
			<div class="eltdf-counter-image">
				<div class="eltdf-counter-image-inner">
					<?php if(is_array($image_size) && count($image_size)) : ?>
						<?php echo esmarts_elated_generate_thumbnail($image, null, $image_size[0], $image_size[1]); ?>
					<?php else: ?>
						<?php echo wp_get_attachment_image($image, $image_size); ?>
					<?php endif; ?>
				</div>
			</div>
		<?php } ?>
		<?php if(!empty($digit)) { ?>
			<span class="eltdf-counter <?php echo esc_attr($type) ?>" <?php echo esmarts_elated_get_inline_style($counter_styles); ?>><?php echo esc_html($digit); ?></span>
		<?php } ?>
		<?php if(!empty($title)) { ?>
			<<?php echo esc_attr($title_tag); ?> class="eltdf-counter-title" <?php echo esmarts_elated_get_inline_style($counter_title_styles); ?>>
				<?php echo esc_html($title); ?>
			</<?php echo esc_attr($title_tag); ?>>
		<?php } ?>
	</div>
</div>
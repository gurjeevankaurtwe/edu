<?php
namespace ElatedCore\CPT\Shortcodes\PricingTable;

use ElatedCore\Lib;

class PricingTableItem implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'eltdf_pricing_table_item';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Elated Pricing Table Item', 'eltdf-core' ),
					'base'                      => $this->base,
					'icon'                      => 'icon-wpb-pricing-table-item extended-custom-icon',
					'category'                  => esc_html__( 'by ELATED', 'eltdf-core' ),
					'allowed_container_element' => 'vc_row',
					'as_child'                  => array( 'only' => 'eltdf_pricing_table' ),
					'params'                    => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'custom_class',
							'heading'     => esc_html__( 'Custom CSS Class', 'eltdf-core' ),
							'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'skin',
							'heading'     => esc_html__( 'Skin', 'eltdf-core' ),
							'value'       => array(
								esc_html__( 'Green', 'eltdf-core' )  => 'green',
								esc_html__( 'Blue', 'eltdf-core' )   => 'blue',
								esc_html__( 'Orange', 'eltdf-core' ) => 'orange'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'star_rating',
							'heading'     => esc_html__( 'Star Rating', 'eltdf-core' ),
							'value'       => array(
								esc_html__( 'Default None', 'eltdf-core' )   => '',
								esc_html__( 'One Star', 'eltdf-core' ) => '1',
								esc_html__( 'Two Stars', 'eltdf-core' ) => '2',
								esc_html__( 'Three Stars', 'eltdf-core' ) => '3',
								esc_html__( 'Four Stars', 'eltdf-core' ) => '4',
								esc_html__( 'Five Stars', 'eltdf-core' ) => '5'
							),
							'save_always' => true
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'title',
							'heading'     => esc_html__( 'Title', 'eltdf-core' ),
							'value'       => esc_html__( 'Basic Plan', 'eltdf-core' ),
							'save_always' => true
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'price',
							'heading'     => esc_html__( 'Price', 'eltdf-core' ),
							'description' => esc_html__( 'Example value is $100', 'eltdf-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'price_period',
							'heading'     => esc_html__( 'Price Period', 'eltdf-core' ),
							'description' => esc_html__( 'Example label is monthly', 'eltdf-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'button_text',
							'heading'     => esc_html__( 'Button Text', 'eltdf-core' ),
							'value'       => esc_html__( 'Purchase', 'eltdf-core' ),
							'save_always' => true
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'link',
							'heading'    => esc_html__( 'Button Link', 'eltdf-core' ),
							'dependency' => array( 'element' => 'button_text', 'not_empty' => true )
						),
						array(
							'type'       => 'textarea_html',
							'param_name' => 'content',
							'heading'    => esc_html__( 'Content', 'eltdf-core' ),
							'value'      => '<li>content content content</li><li>content content content</li><li>content content content</li>'
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'custom_class' => '',
			'skin'         => 'green',
			'star_rating'  => '',
			'title'        => '',
			'price'        => '',
			'price_period' => '',
			'button_text'  => '',
			'link'         => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['content']        = preg_replace( '#^<\/p>|<p>$#', '', $content ); // delete p tag before and after content
		$params['holder_classes'] = $this->getHolderClasses( $params, $args );
		
		$html = eltdf_core_get_shortcode_module_template_part( 'templates/pricing-table-template', 'pricing-table', '', $params );
		
		return $html;
	}
	
	private function getHolderClasses( $params, $args ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['custom_class'] ) ? esc_attr( $params['custom_class'] ) : '';
		$holderClasses[] = ! empty( $params['skin'] ) ? 'eltdf-pt-skin-' . esc_attr( $params['skin'] ) : $args['skin'];
		
		return implode( ' ', $holderClasses );
	}
}
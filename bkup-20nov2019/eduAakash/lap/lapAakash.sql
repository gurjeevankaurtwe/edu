-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2019 at 04:38 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lap`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$lRC6aITo8XOPyOtQgKdze.E866bVknAbmDLEuPYx.lies5L4Xv.8e', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `institute_id` int(11) NOT NULL,
  `class` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `institute_id`, `class`, `status`, `created_at`, `updated_at`) VALUES
(1, 30, 'Class A', '1', '2019-09-03 05:17:54', '0000-00-00 00:00:00'),
(2, 30, 'Class B', '1', '2019-09-02 06:30:13', '0000-00-00 00:00:00'),
(3, 30, 'Class C', '1', '2019-09-03 05:12:11', '2019-09-03 05:12:11'),
(5, 2, 'Pre1', '1', '2019-09-10 12:44:22', '2019-09-10 12:37:46');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'mon', '1', '2019-09-03 06:03:29', '0000-00-00 00:00:00'),
(2, 'tue', '1', '2019-09-03 06:03:29', '0000-00-00 00:00:00'),
(3, 'wed', '1', '2019-09-03 06:03:29', '0000-00-00 00:00:00'),
(4, 'thu', '1', '2019-09-03 06:03:29', '0000-00-00 00:00:00'),
(5, 'fri', '1', '2019-09-03 06:03:29', '0000-00-00 00:00:00'),
(6, 'sat', '1', '2019-09-03 06:03:45', '0000-00-00 00:00:00'),
(7, 'sun', '1', '2019-09-03 06:03:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `guest_users`
--

CREATE TABLE `guest_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` text,
  `description` text NOT NULL,
  `level_id` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest_users`
--

INSERT INTO `guest_users` (`id`, `user_id`, `rating`, `description`, `level_id`, `created_at`, `updated_at`) VALUES
(1, 118, '3', 'test', '42', '2019-10-23 17:36:14', '2019-10-23 12:55:48'),
(2, 118, '3', 'test', '42', '2019-10-23 17:36:56', '2019-10-23 12:55:54'),
(3, 118, '2', 'sddf', '50', '2019-10-23 17:37:21', '2019-10-23 12:55:56'),
(4, 118, '2', 'fdf', '44', '2019-10-23 17:37:49', '2019-10-23 12:55:59'),
(5, 118, '2', 'fgfgfg', '50', '2019-10-23 17:48:23', '2019-10-23 12:56:02'),
(6, 118, '2', 'test', '1', '2019-10-23 18:11:25', '2019-10-23 12:56:04'),
(7, 125, '4', 'test rating', '42', '2019-10-23 19:35:43', '2019-10-23 19:35:43'),
(8, 133, '5', 'Very good initiative.', '63', '2019-10-24 21:20:52', '2019-10-24 21:20:52'),
(9, 156, '5', 'Very Nice', '1', '2019-10-25 07:14:14', '2019-10-25 07:14:14'),
(10, 168, NULL, 'wer', '1', '2019-10-25 07:45:23', '2019-10-25 07:45:23'),
(11, 168, NULL, 'jbhj', '5', '2019-10-25 07:46:01', '2019-10-25 07:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext,
  `status` enum('1','0') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Level 1', 'These tasks introduce new information – sound/symbol, sound clusters/ syllables, and words. After the instruction screen each task has a letter, syllable, or word that appears singly on the screen. The Teacher voice says the sound, syllable or word while the student sees it on the screen. Student repeats the sound and Teacher scores by touching REPEAT (for error) or NEXT (for correct) button. Teacher may have Student repeat the total task multiple times. Teacher will have Student do the task without the voice prompt to check if Student can read the symbol/cluster or word without a prompt.\r\n', '1', '2019-10-23 05:19:58', '0000-00-00 00:00:00'),
(2, 'Level 2', 'These tasks introduce new information – sound/symbol, sound clusters/ syllables, and words. After the instruction screen each task has a letter, syllable, or word that appears singly on the screen. The Teacher voice says the sound, syllable or word while the student sees it on the screen. Student repeats the sound and Teacher scores by touching REPEAT (for error) or NEXT (for correct) button. Teacher may have Student repeat the total task multiple times. Teacher will have Student do the task without the voice prompt to check if Student can read the symbol/cluster or word without a prompt.\r\n', '1', '2019-10-08 04:47:35', '0000-00-00 00:00:00'),
(3, 'Level 3', 'These tasks introduce new information – sound/symbol, sound clusters/ syllables, and words. After the instruction screen each task has a letter, syllable, or word that appears singly on the screen. The Teacher voice says the sound, syllable or word while the student sees it on the screen. Student repeats the sound and Teacher scores by touching REPEAT (for error) or NEXT (for correct) button. Teacher may have Student repeat the total task multiple times. Teacher will have Student do the task without the voice prompt to check if Student can read the symbol/cluster or word without a prompt.\r\n', '1', '2019-10-08 04:47:42', '0000-00-00 00:00:00'),
(4, 'Level 4', 'These tasks introduce new information – sound/symbol, sound clusters/ syllables, and words. After the instruction screen each task has a letter, syllable, or word that appears singly on the screen. The Teacher voice says the sound, syllable or word while the student sees it on the screen. Student repeats the sound and Teacher scores by touching REPEAT (for error) or NEXT (for correct) button. Teacher may have Student repeat the total task multiple times. Teacher will have Student do the task without the voice prompt to check if Student can read the symbol/cluster or word without a prompt.\r\n', '1', '2019-11-07 06:12:37', '0000-00-00 00:00:00'),
(11, 'Level 5', 'These tasks introduce new information – sound/symbol, sound clusters/ syllables, and words. After the instruction screen each task has a letter, syllable, or word that appears singly on the screen. The Teacher voice says the sound, syllable or word while the student sees it on the screen. Student repeats the sound and Teacher scores by touching REPEAT (for error) or NEXT (for correct) button. Teacher may have Student repeat the total task multiple times. Teacher will have Student do the task without the voice prompt to check if Student can read the symbol/cluster or word without a prompt.\r\n', '1', '2019-10-31 05:15:34', '2019-10-16 07:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `level_sub_levels`
--

CREATE TABLE `level_sub_levels` (
  `id` int(11) NOT NULL,
  `level_id` int(11) DEFAULT NULL,
  `sub_level_id` int(11) DEFAULT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `audio` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level_sub_levels`
--

INSERT INTO `level_sub_levels` (`id`, `level_id`, `sub_level_id`, `title`, `description`, `audio`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 9, 'Level 1.9.1 Attention Task - Flash', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. Flash for this task.</p>\r\n\r\n<p>2. Numeral or letter flashes on screen for 2 seconds and S repeats what he saw or heard.</p>\r\n\r\n<p>3. Trainer presses START and boxes will flash-appear on the screen for 2 seconds each one at a time ending with a ___ (line) and a series of item choices at the bottom of the screen.</p>\r\n\r\n<p>4. Student says numeral or letter that is next number or letter in sequence when line appears and chooses the number/letter to place on the line.</p>\r\n\r\n<p>5. Trainer indicates &ndash;NEXT to go to next task or REPEAT. Item may be repeated at trainer discretion and Trainer chooses DONE when task is complete</p>', '1571912087_Asilomar .wav', 1, '2019-10-24 08:01:51', '2019-10-25 05:45:46'),
(2, 1, 9, 'Level 1.9.2 –Attention Task – metronome beat', '<p>1. Trainer reviews Instructional Screen and presses GO to begin</p>\r\n\r\n<p>2. Trainer says number or letter to blank screen. Student&nbsp;repeats number or letter.</p>\r\n\r\n<p>3. Trainer presses START and beat sounds to blank screen ending with appearance of a ___ (line) and item choices at the bottom of the screen.</p>\r\n\r\n<p>4. Student&nbsp;counts beats and says next number or letter in sequence. Line ___ appears and Student&nbsp;chooses the correct number/letter from the choices at the bottom of the screen.</p>\r\n\r\n<p>5. T indicates &ndash;NEXT or REPEAT and DONE when task is complete.</p>', '1571912280_At Peace.wav', 1, '2019-10-24 08:01:53', '2019-10-24 10:18:00'),
(3, 1, 9, 'Level 1.9.3 Attention Task - Flash', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. Flash for this task.</p>\r\n\r\n<p>2. Numeral or letter flashes on screen for 2 seconds and Student repeats what he saw or heard.</p>\r\n\r\n<p>3. Trainer presses START and boxes will flash-appear on the screen for 2 seconds each one at a time ending with a ___ (line) and a series of item choices at the bottom of the screen.</p>\r\n\r\n<p>4. Student says numeral or letter that is next number or letter in sequence when line appears and chooses the number/letter to place on the line.</p>\r\n\r\n<p>5. Trainer indicates &ndash;NEXT to go to next task or REPEAT. Item may be repeated at trainer discretion and Trainer chooses DONE when task is complete</p>', '1571912465_High Tide.wav', 1, '2019-10-24 09:10:48', '2019-10-24 10:21:05'),
(4, 1, 9, 'Level 1.9.4 Attention Task – Metronome Beat', '<p>1. Trainer&nbsp;reviews Instructional Screen and presses GO to begin</p>\r\n\r\n<p>2. Trainer&nbsp;says number or letter to blank screen. Student repeats number or letter.</p>\r\n\r\n<p>3. Trainer&nbsp;presses START and beat sounds to blank screen ending with appearance of a ___ (line) and item choices at the bottom of the screen.</p>\r\n\r\n<p>4. Student counts beats and says next number or letter in sequence. Line ___ appears and Student chooses the correct number/letter from the choices at the bottom of the screen.</p>\r\n\r\n<p>5. Trainer&nbsp;indicates &ndash;NEXT or REPEAT and DONE when task is complete.</p>', '1571981129_Midnight Imagination.wav', 1, '2019-10-24 09:10:50', '2019-10-25 05:25:29'),
(8, 1, 9, 'Level 1.9.6 Attention Task – Metronome Beat', '<p>1. Trainer reviews Instructional Screen and presses GO to begin</p>\r\n\r\n<p>2. Trainer says number or letter to blank screen. Student repeats number or letter.</p>\r\n\r\n<p>3. Trainer presses START and beat sounds to blank screen ending with appearance of a ___ (line) and item choices at the bottom of the screen.</p>\r\n\r\n<p>4. Student counts beats and says next number or letter in sequence. Line ___ appears and S chooses the correct number/letter from the choices at the bottom of the screen.</p>\r\n\r\n<p>5. Trainer indicates &ndash;NEXT or REPEAT and DONE when task is complete</p>', '1571982358_My Joyful Soul.wav', 1, '2019-10-25 05:36:10', '2019-10-25 05:45:58'),
(7, 1, 9, 'Level 1.9.5 Attention Task - Flash', '<p>1. Trainer&nbsp;reviews Instructional Screen and presses GO to begin. Flash for this task. &nbsp;</p>\r\n\r\n<p>2. Letter flashes on screen for 2 seconds and Student repeats what he saw or heard.</p>\r\n\r\n<p>3. Trainer&nbsp;presses START and boxes will flash-appear on the screen for 2 seconds each one at a time ending with a ___ (line) and a series of item choices at the bottom of the screen.</p>\r\n\r\n<p>4. Student says numeral or letter that is next number or letter in sequence when line appears and chooses the number/letter to place on the line.</p>\r\n\r\n<p>5. Trainer&nbsp;indicates &ndash;NEXT to go to next task or REPEAT. Item may be repeated at trainer discretion and Trainer&nbsp;chooses DONE when task is complete</p>', '1571982170_My Guru.wav', 1, '2019-10-25 05:36:08', '2019-10-25 05:42:50'),
(9, 1, 9, 'Level 1.9.7 Attention Task - Flash', '<p>1. Trainer reviews Instructional Screen and presses GO to begin</p>\r\n\r\n<p>2. Trainer says number or letter to blank screen. Student repeats number or letter.</p>\r\n\r\n<p>3. Trainer presses START and beat sounds to blank screen ending with appearance of a ___ (line) and item choices at the bottom of the screen.</p>\r\n\r\n<p>4. Student counts beats and says next number or letter in sequence. Line ___ appears and S chooses the correct number/letter from the choices at the bottom of the screen.</p>\r\n\r\n<p>5. Trainer indicates &ndash;NEXT or REPEAT and DONE when task is complete.</p>', '1571982611_Not All Is Lost.wav', 1, '2019-10-25 05:45:02', '2019-10-25 05:50:11'),
(10, 1, 9, 'Level 1.9.8 Attention task -Metronome Beat', '<p>1. Trainer reviews Instructional Screen and presses GO to begin</p>\r\n\r\n<p>2. Trainer says number or letter to blank screen. Student repeats number or letter.</p>\r\n\r\n<p>3. Trainer presses START and beat sounds to blank screen ending with appearance of a ___ (line) and item choices at the bottom of the screen.</p>\r\n\r\n<p>4. Student counts beats and says next number or letter in sequence. Line ___ appears and Student chooses the correct number/letter from the choices at the bottom of the screen.</p>\r\n\r\n<p>5. Trainer indicates &ndash;NEXT or REPEAT and DONE when task is complete.</p>', '1571982796_Ocean Meditation.wav', 1, '2019-10-25 05:45:04', '2019-10-25 05:53:17'),
(11, 1, 9, 'Level 1.9.9 Attention Task - Flash', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. Flash for this task.</p>\r\n\r\n<p>2. Numeral or letter flashes on screen for 2 seconds and Student repeats what he saw or heard.</p>\r\n\r\n<p>3. Trainer presses START and boxes will flash-appear on the screen for 2 seconds each one at a time ending with a ___ (line) and a series of item choices at the bottom of the screen.</p>\r\n\r\n<p>4. Student says numeral or letter that is next number or letter in sequence when line appears and chooses the number/letter to place on the line.</p>\r\n\r\n<p>5. Trainer indicates &ndash;NEXT to go to next task or REPEAT. Item may be repeated at trainer discretion and Trainer chooses DONE when task is complete</p>', '1571983049_Sedona Whisper.wav', 1, '2019-10-25 05:52:13', '2019-10-25 05:57:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package_managements`
--

CREATE TABLE `package_managements` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` longtext NOT NULL,
  `price` text NOT NULL,
  `sale_price` text NOT NULL,
  `concurrent_session` text NOT NULL,
  `status` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package_managements`
--

INSERT INTO `package_managements` (`id`, `title`, `description`, `price`, `sale_price`, `concurrent_session`, `status`, `created_at`, `updated_at`) VALUES
(2, 'test', '<p>Some features in CKEditor are configured through JavaScript files by default. Since all files are loaded from remote CDN, there is no way to modify them directly. You can however instruct CKEditor to load local files instead, if you need to:</p>\r\n\r\n<ul>\r\n	<li><a href=\"https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html#cfg-customConfig\"><code>config.customConfig</code></a>&nbsp;- The path to CKEditor configuration file. For more details, see&nbsp;<a href=\"https://ckeditor.com/docs/ckeditor4/latest/guide/dev_configuration.html\">Setting CKEditor Configuration</a>.</li>\r\n	<li><a href=\"https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html#cfg-contentsCss\"><code>config.contentsCss</code></a>&nbsp;- The CSS file(s) to be used to apply style to the content.</li>\r\n	<li><a href=\"https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html#cfg-stylesSet\"><code>config.stylesSet</code></a>&nbsp;- The &quot;styles definition set&quot; to use in the styles dropdown list. For more details, see&nbsp;<a href=\"https://ckeditor.com/docs/ckeditor4/latest/guide/dev_styles.html\">Setting Styles</a>.</li>\r\n	<li><a href=\"https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html#cfg-templates_files\"><code>config.templates_files</code></a>&nbsp;- The list of template files to use in the templates dialog window.</li>\r\n</ul>\r\n\r\n<p>In case of setting any of the options above, it is highly recommended to&nbsp;<a href=\"http://ckeditor.com/download\">download</a>&nbsp;locally the same CKEditor package that is loaded from CDN in order to copy included configuration files and use them as base files for adding additional changes.</p>', '322', '322', '3213', 'Active', '2019-09-19 07:48:12', '2019-10-25 09:54:04'),
(3, 'Basic Plan', '<p>Some features in CKEditor are configured through JavaScript files by default. Since all files are loaded from remote CDN, there is no way to modify them directly. You can however instruct CKEditor to load local files instead, if you need to:</p>\r\n\r\n<ul>\r\n	<li><a href=\"https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html#cfg-customConfig\"><code>config.customConfig</code></a>&nbsp;- The path to CKEditor configuration file. For more details, see&nbsp;<a href=\"https://ckeditor.com/docs/ckeditor4/latest/guide/dev_configuration.html\">Setting CKEditor Configuration</a>.</li>\r\n	<li><a href=\"https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html#cfg-contentsCss\"><code>config.contentsCss</code></a>&nbsp;- The CSS file(s) to be used to apply style to the content.</li>\r\n	<li><a href=\"https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html#cfg-stylesSet\"><code>config.stylesSet</code></a>&nbsp;- The &quot;styles definition set&quot; to use in the styles dropdown list. For more details, see&nbsp;<a href=\"https://ckeditor.com/docs/ckeditor4/latest/guide/dev_styles.html\">Setting Styles</a>.</li>\r\n	<li><a href=\"https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html#cfg-templates_files\"><code>config.templates_files</code></a>&nbsp;- The list of template files to use in the templates dialog window.</li>\r\n</ul>\r\n\r\n<p>In case of setting any of the options above, it is highly recommended to&nbsp;<a href=\"http://ckeditor.com/download\">download</a>&nbsp;locally the same CKEditor package that is loaded from CDN in order to copy included configuration files and use them as base files for adding additional changes.</p>', '32', '21', '2', 'Active', '2019-09-19 09:46:29', '2019-09-19 09:46:29');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `session_id` int(11) DEFAULT NULL,
  `sub_level_id` int(11) DEFAULT NULL,
  `level_sub_level_id` int(11) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `question_image` varchar(255) DEFAULT NULL,
  `option1` varchar(255) DEFAULT NULL,
  `option1_img` varchar(255) DEFAULT NULL,
  `option2` varchar(255) DEFAULT NULL,
  `option2_img` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `answer_img` varchar(255) DEFAULT NULL,
  `answer_record` varchar(255) DEFAULT NULL,
  `status` enum('1','0') NOT NULL,
  `delete_status` enum('0','1') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `option3` text,
  `option3_img` text,
  `option4` text,
  `option4_img` text,
  `option5` varchar(255) DEFAULT NULL,
  `option6` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `session_id`, `sub_level_id`, `level_sub_level_id`, `question`, `question_image`, `option1`, `option1_img`, `option2`, `option2_img`, `answer`, `answer_img`, `answer_record`, `status`, `delete_status`, `created_at`, `updated_at`, `option3`, `option3_img`, `option4`, `option4_img`, `option5`, `option6`) VALUES
(1, 1, 1, NULL, 'What is the first alphabet ?', NULL, 'a', '1568890314_a.png', NULL, NULL, 'a', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 1, NULL, 'What is the second alphabet ?', NULL, 'm', '1568890314_a.png', NULL, NULL, 'b', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 1, 1, NULL, 'What is the third alphabet ?', NULL, 't', '1568890314_a.png', NULL, NULL, 'c', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, 1, NULL, 'What is the fourth alphabet ?', NULL, 's', '1568896237_letter_d_PNG16.png', NULL, NULL, 'd', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 2, 2, NULL, 'What is the first alphabet ?', NULL, 'b', '', 'a', '', 'option1', NULL, '', '1', '0', '2019-10-15 04:39:19', NULL, NULL, '', NULL, '', NULL, NULL),
(6, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 't', '', 'i', '', 'option3', NULL, '', '1', '0', '2019-10-15 04:39:23', NULL, '', '', '', '', NULL, NULL),
(31, 1, 1, NULL, 'What is the fourth alphabet ?', NULL, 'f', '1568896237_letter_d_PNG16.png', NULL, NULL, 'd', NULL, '', '1', '1', '2019-10-15 04:39:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 1, 1, NULL, 'What is the fourth alphabet ?', NULL, 'i', '1568896237_letter_d_PNG16.png', NULL, NULL, 'd', NULL, '', '1', '1', '2019-10-15 04:39:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 1, 1, NULL, 'What is the fourth alphabet ?', NULL, 'b', '1568896237_letter_d_PNG16.png', NULL, NULL, 'd', NULL, '', '1', '1', '2019-10-09 12:04:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 1, 1, NULL, 'What is the fourth alphabet ?', NULL, 's', '1568896237_letter_d_PNG16.png', NULL, NULL, 'd', NULL, '', '1', '1', '2019-10-15 04:39:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 1, 1, NULL, 'What is the fourth alphabet ?', NULL, 'f', '1568896237_letter_d_PNG16.png', NULL, NULL, 'd', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 1, 1, NULL, 'What is the fourth alphabet ?', NULL, 't', '1568896237_letter_d_PNG16.png', NULL, NULL, 'd', NULL, '', '1', '1', '2019-10-15 04:39:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 3, 4, NULL, 'What we call these both letter?', NULL, 'am', '1568966241_ab.png', 'ab', NULL, 'ab', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ib', NULL, NULL, NULL, NULL, NULL),
(32, 1, 1, NULL, 'What is the fourth alphabet ?', NULL, 'a', '1568896237_letter_d_PNG16.png', NULL, NULL, 'd', NULL, '', '1', '1', '2019-10-09 12:06:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 1, 1, NULL, 'What is the fourth alphabet ?', NULL, 'b', '1568896237_letter_d_PNG16.png', NULL, NULL, 'd', NULL, '', '1', '1', '2019-10-09 12:06:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 's', '', 'b', '', 'option3', NULL, '', '1', '0', '2019-10-08 13:19:50', NULL, NULL, '', NULL, '', NULL, NULL),
(35, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'm', '', 'a', '', 'option3', NULL, '', '1', '0', '2019-10-08 13:19:45', NULL, NULL, '', NULL, '', NULL, NULL),
(36, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'f', '', 'i', '', 'option3', NULL, '', '1', '0', '2019-10-08 13:19:35', NULL, NULL, '', NULL, '', NULL, NULL),
(37, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'a', '', 't', '', 'option3', NULL, '', '1', '0', '2019-10-08 13:19:28', NULL, NULL, '', NULL, '', NULL, NULL),
(38, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'm', '', 'f', '', 'option3', NULL, '', '1', '0', '2019-10-08 13:19:20', NULL, NULL, '', NULL, '', NULL, NULL),
(39, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'i', '', 's', '', 'option3', NULL, '', '1', '0', '2019-10-08 13:19:13', NULL, NULL, '', NULL, '', NULL, NULL),
(40, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 't', '', 'b', '', 'option3', NULL, '', '1', '0', '2019-10-08 13:19:07', NULL, NULL, '', NULL, '', NULL, NULL),
(41, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 's', '', 'f', '', 'option3', NULL, '', '1', '0', '2019-10-08 13:18:59', NULL, NULL, '', NULL, '', NULL, NULL),
(42, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'b', '', 'i', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 't', '', 'a', '', NULL, NULL),
(43, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 's', '', 'm', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 'a', '', 'f', '', NULL, NULL),
(44, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'i', '', 'f', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 's', '', 'a', '', NULL, NULL),
(45, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'm', '', 't', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 'a', '', 'i', '', NULL, NULL),
(46, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 's', '', 'a', '', 'option3', NULL, '', '1', '0', '2019-10-08 10:12:51', NULL, 'f', '', 'm', '', NULL, NULL),
(47, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 't', '', 's', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 'i', '', 'b', '', NULL, NULL),
(48, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'm', '', 't', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 'a', '', 'b', '', NULL, NULL),
(49, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'b', '', 'i', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 't', '', 'a', '', NULL, NULL),
(50, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 's', '', 'm', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 'a', '', 'f', '', NULL, NULL),
(51, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'i', '', 'f', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 's', '', 'a', '', NULL, NULL),
(52, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'm', '', 't', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 'a', '', 'i', '', NULL, NULL),
(53, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 's', '', 'a', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 'f', '', 'm', '', NULL, NULL),
(54, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 't', '', 's', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 'i', '', 'b', '', NULL, NULL),
(55, 2, 2, NULL, 'Which is the third alphabet of english?', NULL, 'm', '', 't', '', 'option3', NULL, '', '1', '0', '2019-09-21 08:35:54', NULL, 'a', '', 'b', '', NULL, NULL),
(56, 3, 4, NULL, 'What we call these both letter?', NULL, 'ab', '1568966241_ab.png', 'im', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'at', NULL, NULL, NULL, NULL, NULL),
(57, 3, 4, NULL, 'What we call these both letter?', NULL, 'af', '1568966241_ab.png', 'if', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'as', NULL, NULL, NULL, NULL, NULL),
(58, 3, 4, NULL, 'What we call these both letter?', NULL, 'is', '1568966241_ab.png', 'at', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'it', NULL, NULL, NULL, NULL, NULL),
(59, 3, 4, NULL, 'What we call these both letter?', NULL, 'ib', '1568966241_ab.png', 'at', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'am', NULL, NULL, NULL, NULL, NULL),
(60, 3, 4, NULL, 'What we call these both letter?', NULL, 'at', '1568966241_ab.png', 'it', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'af', NULL, NULL, NULL, NULL, NULL),
(61, 3, 4, NULL, 'What we call these both letter?', NULL, 'if', '1568966241_ab.png', 'ab', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'im', NULL, NULL, NULL, NULL, NULL),
(62, 3, 4, NULL, 'What we call these both letter?', NULL, 'ib', '1568966241_ab.png', 'ab', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'at', NULL, NULL, NULL, NULL, NULL),
(63, 3, 4, NULL, 'What we call these both letter?', NULL, 'if', '1568966241_ab.png', 'it', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'is', NULL, NULL, NULL, NULL, NULL),
(64, 3, 4, NULL, 'What we call these both letter?', NULL, 'am', '1568966241_ab.png', 'af', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'im', NULL, NULL, NULL, NULL, NULL),
(65, 3, 3, NULL, 'What we call these both letter?', NULL, 'am', '1568966241_ab.png', '', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, '', NULL, NULL, NULL, NULL, NULL),
(66, 3, 3, NULL, 'What we call these both letter?', NULL, 'it', '1568966241_ab.png', '', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, '', NULL, NULL, NULL, NULL, NULL),
(67, 3, 3, NULL, 'What we call these both letter?', NULL, 'ab', '1568966241_ab.png', '', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, '', NULL, NULL, NULL, NULL, NULL),
(68, 3, 3, NULL, 'What we call these both letter?', NULL, 'ib', '1568966241_ab.png', '', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, '', NULL, NULL, NULL, NULL, NULL),
(69, 3, 3, NULL, 'What we call these both letter?', NULL, 'if', '1568966241_ab.png', '', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, '', NULL, NULL, NULL, NULL, NULL),
(70, 3, 3, NULL, 'What we call these both letter?', NULL, 'af', '1568966241_ab.png', '', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, '', NULL, NULL, NULL, NULL, NULL),
(71, 3, 3, NULL, 'What we call these both letter?', NULL, 'is', '1568966241_ab.png', '', NULL, '', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, '', NULL, NULL, NULL, NULL, NULL),
(73, 5, 5, NULL, 'Level 5', NULL, 'p', '', NULL, NULL, 'p', NULL, '', '1', '0', '2019-09-24 09:46:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 5, 5, NULL, 'Level 5', NULL, 'n', '', NULL, NULL, 'n', NULL, '', '1', '0', '2019-09-24 09:46:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 5, 5, NULL, 'Level 5', NULL, 'g', '', NULL, NULL, 'g', NULL, '', '1', '0', '2019-09-24 09:46:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 5, 5, NULL, 'Level 5', NULL, 'p', '', NULL, NULL, 'p', NULL, '', '1', '0', '2019-09-24 09:46:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 5, 5, NULL, 'Level 5', NULL, 'g', '', NULL, NULL, 'g', NULL, '', '1', '0', '2019-09-24 09:46:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 5, 5, NULL, 'Level 5', NULL, 'p', '', NULL, NULL, 'p', NULL, '', '1', '0', '2019-09-24 09:46:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 5, 5, NULL, 'Level 5', NULL, 'n', '', NULL, NULL, 'n', NULL, '', '1', '0', '2019-09-24 09:46:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 5, 5, NULL, 'Level 5', NULL, 'g', '', NULL, NULL, 'g', NULL, '', '1', '0', '2019-09-24 09:46:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 5, 5, NULL, 'Level 5', NULL, 'n', '', NULL, NULL, 'n', NULL, '', '1', '0', '2019-09-24 09:46:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 5, 5, NULL, 'Level 5', NULL, 'p', '', NULL, NULL, 'p', NULL, '', '1', '0', '2019-09-24 09:46:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 6, 6, NULL, 'Level 6', NULL, 'ap', '', 'am', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ip', '', '', '', NULL, NULL),
(84, 6, 6, NULL, 'Level 6', NULL, 'ag', '', 'at', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'im', '', '', '', NULL, NULL),
(85, 6, 6, NULL, 'Level 6', NULL, 'in', '', 'an', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ap', '', '', '', NULL, NULL),
(86, 6, 6, NULL, 'Level 6', NULL, 'is', '', 'ab', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ib', '', '', '', NULL, NULL),
(87, 6, 6, NULL, 'Level 6', NULL, 'an', '', 'am', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ip', '', '', '', NULL, NULL),
(88, 6, 6, NULL, 'Level 6', NULL, 'im', '', 'it', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ab', '', '', '', NULL, NULL),
(89, 6, 6, NULL, 'Level 6', NULL, 'an', '', 'in', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ap', '', '', '', NULL, NULL),
(90, 6, 6, NULL, 'Level 6', NULL, 'am', '', 'is', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'an', '', '', '', NULL, NULL),
(91, 6, 6, NULL, 'Level 6', NULL, 'ap', '', 'in', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ip', '', '', '', NULL, NULL),
(92, 6, 6, NULL, 'Level 6', NULL, 'ag', '', 'ig', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'an', '', '', '', NULL, NULL),
(93, 7, 7, NULL, 'Level 7', NULL, 'asp', '', 'ant', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'int', '', '', '', NULL, NULL),
(112, 8, 8, NULL, 'Level 8', NULL, 'isp', '', 'imp', '', 'option3', NULL, '', '1', '0', '2019-09-25 05:59:18', NULL, 'asp', '', '', '', NULL, NULL),
(95, 7, 7, NULL, 'Level 7', NULL, 'amp', '', 'isp', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'imp', '', '', '', NULL, NULL),
(113, 8, 8, NULL, 'Level 8', NULL, 'asp', '', 'ant', '', 'option2', NULL, '', '1', '0', '2019-09-25 05:59:18', NULL, 'int', '', '', '', NULL, NULL),
(97, 7, 7, NULL, 'Level 7', NULL, 'ist', '', 'ast', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'asp', '', '', '', NULL, NULL),
(98, 7, 7, NULL, 'Level 7', NULL, 'isp', '', 'imp', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'asp', '', '', '', NULL, NULL),
(99, 7, 7, NULL, 'Level 7', NULL, 'ant', '', 'ist', '', 'option2', NULL, '', '1', '0', '2019-09-24 10:18:59', NULL, 'amp', '', '', '', NULL, NULL),
(114, 8, 8, NULL, 'Level 8', NULL, 'amp', '', 'isp', '', 'option2', NULL, '', '1', '0', '2019-09-25 05:59:18', NULL, 'imp', '', '', '', NULL, NULL),
(115, 8, 8, NULL, 'Level 8', NULL, 'ist', '', 'ast', '', 'option2', NULL, '', '1', '0', '2019-09-25 06:01:22', NULL, 'asp', '', '', '', NULL, NULL),
(116, 8, 8, NULL, 'Level 8', NULL, 'ant', '', 'ist', '', 'option3', NULL, '', '1', '0', '2019-09-25 06:02:11', NULL, 'amp', '', '', '', NULL, NULL),
(117, 8, 8, NULL, 'Level 8', NULL, 'asp', '', 'ant', '', 'option3', NULL, '', '1', '0', '2019-09-25 06:08:50', NULL, 'int', '', '', '', NULL, NULL),
(103, 7, 7, NULL, 'Level 7', NULL, 'asp', '', 'ant', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'int', '', '', '', NULL, NULL),
(104, 7, 7, NULL, 'Level 7', NULL, 'amp', '', 'isp', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'imp', '', '', '', NULL, NULL),
(118, 8, 8, NULL, 'Level 8', NULL, 'amp', '', 'imp', '', 'option2', NULL, '', '1', '0', '2019-09-25 06:08:50', NULL, 'isp', '', '', '', NULL, NULL),
(119, 8, 8, NULL, 'Level 8', NULL, 'ist', '', 'ast', '', 'option3', NULL, '', '1', '0', '2019-09-25 06:08:50', NULL, 'asp', '', '', '', NULL, NULL),
(120, 8, 8, NULL, 'Level 8', NULL, 'ist', '', 'imp', '', 'option1', NULL, '', '1', '0', '2019-10-16 05:35:29', NULL, 'asp', '', '', '', NULL, NULL),
(121, 8, 8, NULL, 'Level 8', NULL, 'ant', '', 'ist', '', 'option1', NULL, '', '1', '0', '2019-09-25 06:08:50', NULL, 'amp', '', '', '', NULL, NULL),
(122, 10, 10, NULL, 'Level 10', NULL, 'pam', '', 'pim', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ram', '', '', '', NULL, NULL),
(109, 7, 7, NULL, 'Level 7', NULL, 'ist', '', 'ast', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'asp', '', '', '', NULL, NULL),
(110, 7, 7, NULL, 'Level 7', NULL, 'isp', '', 'imp', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'asp', '', '', '', NULL, NULL),
(111, 7, 7, NULL, 'Level 7', NULL, 'ant', '', 'ist', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'amp', '', '', '', NULL, NULL),
(123, 10, 10, NULL, 'Level 10', NULL, 'fan', '', 'lan', '', 'option1', NULL, '', '1', '0', '2019-10-14 05:24:24', NULL, 'fit', '', '', '', NULL, NULL),
(124, 10, 10, NULL, 'Level 10', NULL, 'ran', '', 'tin', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'tim', '', '', '', NULL, NULL),
(125, 10, 10, NULL, 'Level 10', NULL, 'san', '', 'sim', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'sat', '', '', '', NULL, NULL),
(126, 10, 10, NULL, 'Level 10', NULL, 'gam', '', 'sig', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'sam', '', '', '', NULL, NULL),
(127, 10, 10, NULL, 'Level 10', NULL, 'pan', '', 'bam', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'tab', '', '', '', NULL, NULL),
(128, 10, 10, NULL, 'Level 10', NULL, 'tif', '', 'taf', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'tas', '', '', '', NULL, NULL),
(129, 10, 10, NULL, 'Level 10', NULL, 'fam', '', 'min', '', 'option2', NULL, '', '1', '0', '2019-10-14 05:24:20', NULL, 'fin', '', '', '', NULL, NULL),
(130, 10, 10, NULL, 'Level 10', NULL, 'pas', '', 'las', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'tis', '', '', '', NULL, NULL),
(131, 10, 10, NULL, 'Level 10', NULL, 'bin', '', 'nib', '', 'option3', NULL, '', '1', '0', '2019-10-14 05:30:19', NULL, 'bat', '', '', '', NULL, NULL),
(132, 11, 11, NULL, 'Level 11', NULL, 'pam', '', 'pim', '', 'option2', NULL, '', '1', '0', '2019-09-25 07:42:50', NULL, 'ram', '', '', '', NULL, NULL),
(133, 11, 11, NULL, 'Level 11', NULL, 'fan', '', 'lan', '', 'option2', NULL, '', '1', '0', '2019-10-14 05:24:16', NULL, 'fit', '', '', '', NULL, NULL),
(134, 11, 11, NULL, 'Level 11', NULL, 'ran', '', 'tin', '', 'option3', NULL, '', '1', '0', '2019-09-25 07:42:50', NULL, 'tim', '', '', '', NULL, NULL),
(135, 11, 11, NULL, 'Level 11', NULL, 'san', '', 'sim', '', 'option1', NULL, '', '1', '0', '2019-09-25 07:42:50', NULL, 'sat', '', '', '', NULL, NULL),
(136, 11, 11, NULL, 'Level 11', NULL, 'gam', '', 'sig', '', 'option2', NULL, '', '1', '0', '2019-09-25 07:42:50', NULL, 'sam', '', '', '', NULL, NULL),
(137, 11, 11, NULL, 'Level 11', NULL, 'pan', '', 'bam', '', 'option3', NULL, '', '1', '0', '2019-09-25 07:45:56', NULL, 'tab', '', '', '', NULL, NULL),
(138, 11, 11, NULL, 'Level 11', NULL, 'tif', '', 'taf', '', 'option3', NULL, '', '1', '0', '2019-09-25 07:45:56', NULL, 'tas', '', '', '', NULL, NULL),
(139, 11, 11, NULL, 'Level 11', NULL, 'fam', '', 'min', '', 'option3', NULL, '', '1', '0', '2019-10-14 05:24:10', NULL, 'fin', '', '', '', NULL, NULL),
(140, 11, 11, NULL, 'Level 11', NULL, 'pas', '', 'las', '', 'option1', NULL, '', '1', '0', '2019-09-25 07:45:56', NULL, 'tis', '', '', '', NULL, NULL),
(141, 11, 11, NULL, 'Level 11', NULL, 'bin', '', 'nib', '', 'option2', NULL, '', '1', '0', '2019-10-16 05:39:36', NULL, 'bat', '', '', '', NULL, NULL),
(142, 12, 12, NULL, 'Level2.1', NULL, 'A dish', '', NULL, NULL, 'A dish', NULL, '', '1', '0', '2019-09-26 06:16:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, 12, 12, NULL, 'Level2.1', NULL, 'I can fish.', '', NULL, NULL, 'I can fish.', NULL, '', '1', '0', '2019-09-26 06:16:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, 12, 12, NULL, 'Level2.1', NULL, 'A fish is fat.', '', NULL, NULL, 'A fish is fat.', NULL, '', '1', '0', '2019-09-26 06:16:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, 12, 12, NULL, 'Level2.1', NULL, 'I can wish.', '', NULL, NULL, 'I can wish.', NULL, '', '1', '0', '2019-09-26 06:16:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, 12, 12, NULL, 'Level2.1', NULL, 'I sing.', '', NULL, NULL, 'I sing.', NULL, '', '1', '0', '2019-09-26 06:16:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, 12, 12, NULL, 'Level 2.1', NULL, 'You can run.', '', NULL, NULL, 'You can run.', NULL, '', '1', '0', '2019-09-26 06:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, 12, 12, NULL, 'Level 2.1', NULL, 'I can hit.', '', NULL, NULL, 'I can hit.', NULL, '', '1', '0', '2019-09-26 06:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 12, 12, NULL, 'Level 2.1', NULL, 'Can you bat?', '', NULL, NULL, 'Can you bat?', NULL, '', '1', '0', '2019-09-26 06:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(150, 12, 12, NULL, 'Level 2.1', NULL, 'A ring', '', NULL, NULL, 'A ring', NULL, '', '1', '0', '2019-09-26 06:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, 12, 12, NULL, 'Level 2.1', NULL, 'Can you nap?', '', NULL, NULL, 'Can you nap?', NULL, '', '1', '0', '2019-09-26 06:17:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, 17, 17, NULL, 'Level 2.6', NULL, 'metit', '', NULL, NULL, 'metit', NULL, '', '1', '0', '2019-09-26 06:36:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, 17, 17, NULL, 'Level 2.6', NULL, 'totut', '', NULL, NULL, 'totut', NULL, '', '1', '0', '2019-09-26 06:36:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(154, 17, 17, NULL, 'Level 2.6', NULL, 'sumish', '', NULL, NULL, 'sumish', NULL, '', '1', '0', '2019-09-26 06:36:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, 17, 17, NULL, 'Level 2.6', NULL, 'lebnib', '', NULL, NULL, 'lebnib', NULL, '', '1', '0', '2019-09-26 06:36:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, 17, 17, NULL, 'Level 2.6', NULL, 'nutish', '', NULL, NULL, 'nutish', NULL, '', '1', '0', '2019-09-26 06:36:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, 17, 17, NULL, 'Level 2.6', NULL, 'ripat', '', NULL, NULL, 'ripat', NULL, '', '1', '0', '2019-09-26 06:38:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, 17, 17, NULL, 'Level 2.6', NULL, 'samash', '', NULL, NULL, 'samash', NULL, '', '1', '0', '2019-09-26 06:38:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, 17, 17, NULL, 'Level 2.6', NULL, 'ranmand', '', NULL, NULL, 'ranmand', NULL, '', '1', '0', '2019-09-26 06:38:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, 17, 17, NULL, 'Level 2.6', NULL, 'motall', '', NULL, NULL, 'motall', NULL, '', '1', '0', '2019-09-26 06:38:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(161, 17, 17, NULL, 'Level 2.6', NULL, 'bingich', '', NULL, NULL, 'bingich', NULL, '', '1', '0', '2019-09-26 06:38:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, 17, 17, NULL, 'Level 2.6', NULL, 'tanging', '', NULL, NULL, 'tanging', NULL, '', '1', '0', '2019-09-26 06:38:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(163, 17, 17, NULL, 'Level 2.6', NULL, 'rebog', '', NULL, NULL, 'rebog', NULL, '', '1', '0', '2019-09-26 06:38:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(164, 17, 17, NULL, 'Level 2.6', NULL, 'tishing', '', NULL, NULL, 'tishing', NULL, '', '1', '0', '2019-09-26 06:38:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(165, 17, 17, NULL, 'Level 2.6', NULL, 'deckish', '', NULL, NULL, 'deckish', NULL, '', '1', '0', '2019-09-26 06:38:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(166, 17, 17, NULL, 'Level 2.6', NULL, 'neckush', '', NULL, NULL, 'neckush', NULL, '', '1', '0', '2019-09-26 06:38:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(167, 18, 18, NULL, 'Level 2.7', NULL, 'band', '', 'find', '', '', NULL, '', '1', '0', '2019-09-26 06:58:25', NULL, 'hand', '', '', '', NULL, NULL),
(168, 18, 18, NULL, 'Level 2.7', NULL, 'run', '', 'ran', '', '', NULL, '', '1', '0', '2019-09-26 06:58:25', NULL, 'tan', '', '', '', NULL, NULL),
(169, 18, 18, NULL, 'Level 2.7', NULL, 'fish', '', 'lash', '', '', NULL, '', '1', '0', '2019-09-26 06:58:25', NULL, 'bash', '', '', '', NULL, NULL),
(170, 18, 18, NULL, 'Level 2.7', NULL, 'ram', '', 'sam', '', '', NULL, '', '1', '0', '2019-09-26 06:58:25', NULL, 'tan', '', '', '', NULL, NULL),
(171, 18, 18, NULL, 'Level 2.7', NULL, 'bay', '', 'may', '', '', NULL, '', '1', '0', '2019-09-26 06:58:25', NULL, 'you', '', '', '', NULL, NULL),
(172, 26, 26, NULL, 'Level 2.15', NULL, 'fat', '', 'fate', '', 'option1', NULL, '', '1', '0', '2019-09-26 09:50:51', NULL, '', '', '', '', NULL, NULL),
(173, 26, 26, NULL, 'Level 2.15', NULL, 'bite', '', 'bit', '', 'option2', NULL, '', '1', '0', '2019-09-26 09:50:51', NULL, '', '', '', '', NULL, NULL),
(174, 26, 26, NULL, 'Level 2.15', NULL, 'kite', '', 'kit', '', 'option1', NULL, '', '1', '0', '2019-09-26 09:50:51', NULL, '', '', '', '', NULL, NULL),
(175, 26, 26, NULL, 'Level 2.15', NULL, 'man', '', 'mane', '', 'option2', NULL, '', '1', '0', '2019-09-26 09:50:51', NULL, '', '', '', '', NULL, NULL),
(176, 26, 26, NULL, 'Level 2.15', NULL, 'pipe', '', 'pip', '', 'option2', NULL, '', '1', '0', '2019-09-26 09:50:51', NULL, '', '', '', '', NULL, NULL),
(177, 26, 26, NULL, 'Level 2.15', NULL, 'cope', '', 'cop', '', 'option1', NULL, '', '1', '0', '2019-09-26 09:52:35', NULL, '', '', '', '', NULL, NULL),
(178, 26, 26, NULL, 'Level 2.15', NULL, 'bat', '', 'bate', '', 'option2', NULL, '', '1', '0', '2019-09-26 09:52:35', NULL, '', '', '', '', NULL, NULL),
(179, 26, 26, NULL, 'Level 2.15', NULL, 'dame', '', 'dam', '', 'option1', NULL, '', '1', '0', '2019-09-26 09:52:35', NULL, '', '', '', '', NULL, NULL),
(180, 26, 26, NULL, 'Level 2.15', NULL, 'sit', '', 'site', '', 'option1', NULL, '', '1', '0', '2019-09-26 09:52:35', NULL, '', '', '', '', NULL, NULL),
(181, 26, 26, NULL, 'Level 2.15', NULL, 'fin', '', 'fine', '', 'option2', NULL, '', '1', '0', '2019-09-26 09:52:35', NULL, '', '', '', '', NULL, NULL),
(182, 31, 31, NULL, 'Level 2.19', NULL, 'fat dog', '', NULL, NULL, 'fat dog', NULL, '', '1', '0', '2019-09-26 10:09:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, 31, 31, NULL, 'Level 2.19', NULL, 'big rat', '', NULL, NULL, 'big rat', NULL, '', '1', '0', '2019-09-26 10:09:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(184, 31, 31, NULL, 'Level 2.19', NULL, 'hide pipe', '', NULL, NULL, 'hide pipe', NULL, '', '1', '0', '2019-09-26 10:09:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(185, 31, 31, NULL, 'Level 2.19', NULL, 'tame dog', '', NULL, NULL, 'tame dog', NULL, '', '1', '0', '2019-09-26 10:09:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(186, 31, 31, NULL, 'Level 2.19', NULL, 'dog sat', '', NULL, NULL, 'dam sit', NULL, '', '1', '0', '2019-10-17 05:10:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(187, 31, 31, NULL, 'Level 2.19', NULL, 'is it fun', '', NULL, NULL, 'is it fun', NULL, '', '1', '0', '2019-09-26 10:10:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(188, 31, 31, NULL, 'Level 2.19', NULL, 'can you run', '', NULL, NULL, 'can you run', NULL, '', '1', '0', '2019-09-26 10:10:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(189, 31, 31, NULL, 'Level 2.19', NULL, 'I am fine', '', NULL, NULL, 'I am fine', NULL, '', '1', '0', '2019-09-26 10:10:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(190, 31, 31, NULL, 'Level 2.19', NULL, 'thank the man', '', NULL, NULL, 'thank the man', NULL, '', '1', '0', '2019-09-26 10:10:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(191, 31, 31, NULL, 'Level 2.19', NULL, 'blame the mice', '', NULL, NULL, 'blame the mice', NULL, '', '1', '0', '2019-09-26 10:10:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(192, 31, 31, NULL, 'Level 2.19', NULL, 'the ice is wet', '', NULL, NULL, 'the ice is wet', NULL, '', '1', '0', '2019-09-26 10:10:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(193, 31, 31, NULL, 'Level 2.19', NULL, 'to the bank', '', NULL, NULL, 'to the bank', NULL, '', '1', '0', '2019-09-26 10:10:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(194, 31, 31, NULL, 'Level 2.19', NULL, 'rake the weeds', '', NULL, NULL, 'rake the weeds', NULL, '', '1', '0', '2019-09-26 10:10:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(195, 31, 31, NULL, 'Level 2.19', NULL, 'face the site', '', NULL, NULL, 'face the site', NULL, '', '1', '0', '2019-09-26 10:10:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(196, 31, 31, NULL, 'Level 2.19', NULL, 'a thick mane', '', NULL, NULL, 'a thick mane', NULL, '', '1', '0', '2019-09-26 10:10:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(197, 31, 31, NULL, 'Level 2.19', NULL, 'place the face', '', NULL, NULL, 'place the face', NULL, '', '1', '0', '2019-09-26 10:11:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(198, 31, 31, NULL, 'Level 2.19', NULL, 'hand me the spice', '', NULL, NULL, 'hand me the spice', NULL, '', '1', '0', '2019-09-26 10:11:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(199, 31, 31, NULL, 'Level 2.19', NULL, 'his name is Sam', '', NULL, NULL, 'his name is Sam', NULL, '', '1', '0', '2019-09-26 10:11:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(200, 31, 31, NULL, 'Level 2.19', NULL, 'the game is fun', '', NULL, NULL, 'the game is fun', NULL, '', '1', '0', '2019-09-26 10:11:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(201, 31, 31, NULL, 'Level 2.19', NULL, 'the gun went bang', '', NULL, NULL, 'the gun went bang', NULL, '', '1', '0', '2019-09-26 10:11:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(202, 31, 31, NULL, 'Level 2.19', NULL, 'yum, I like cake', '', NULL, NULL, 'yum, I like cake', NULL, '', '1', '0', '2019-09-26 10:12:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(203, 31, 31, NULL, 'Level 2.19', NULL, 'sing a fun song', '', NULL, NULL, 'sing a fun song', NULL, '', '1', '0', '2019-09-26 10:12:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(204, 31, 31, NULL, 'Level 2.19', NULL, 'the chick got sick', '', NULL, NULL, 'the chick got sick', NULL, '', '1', '0', '2019-09-26 10:12:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(205, 31, 31, NULL, 'Level 2.19', NULL, 'Sam has the gum', '', NULL, NULL, 'Sam has the gum', NULL, '', '1', '0', '2019-09-26 10:12:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(206, 31, 31, NULL, 'Level 2.19', NULL, 'sit in the sun', '', NULL, NULL, 'sit in the sun', NULL, '', '1', '0', '2019-09-26 10:12:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(207, 31, 31, NULL, 'Level 2.19', NULL, 'shake and bake it', '', NULL, NULL, 'shake and bake it', NULL, '', '1', '0', '2019-09-26 10:12:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(208, 33, 33, NULL, 'Level 2.21', NULL, 'my hat', '', NULL, NULL, 'my hat', NULL, '', '1', '0', '2019-09-26 10:32:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(209, 33, 33, NULL, 'Level 2.21', NULL, 'I would', '', NULL, NULL, 'I would', NULL, '', '1', '0', '2019-09-26 10:32:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(210, 33, 33, NULL, 'Level 2.21', NULL, 'little rat', '', NULL, NULL, 'little rat', NULL, '', '1', '0', '2019-09-26 10:32:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(211, 33, 33, NULL, 'Level 2.21', NULL, 'run away', '', NULL, NULL, 'run away', NULL, '', '1', '0', '2019-09-26 10:32:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(212, 33, 33, NULL, 'Level 2.21', NULL, 'where are you', '', NULL, NULL, 'where are you', NULL, '', '1', '0', '2019-09-26 10:32:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(213, 33, 33, NULL, 'Level 2.21', NULL, 'go around', '', NULL, NULL, 'go around', NULL, '', '1', '0', '2019-09-26 10:33:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(214, 33, 33, NULL, 'Level 2.21', NULL, 'have fun', '', NULL, NULL, 'have fun', NULL, '', '1', '0', '2019-09-26 10:33:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(215, 33, 33, NULL, 'Level 2.21', NULL, 'I could', '', NULL, NULL, 'I could', NULL, '', '1', '0', '2019-09-26 10:33:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(216, 33, 33, NULL, 'Level 2.21', NULL, 'about me', '', NULL, NULL, 'about me', NULL, '', '1', '0', '2019-09-26 10:33:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(217, 33, 33, NULL, 'Level 2.21', NULL, 'when is it', '', NULL, NULL, 'when is it', NULL, '', '1', '0', '2019-09-26 10:33:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(218, 33, 33, NULL, 'Level 2.21', NULL, 'he said', '', NULL, NULL, 'he said', NULL, '', '1', '0', '2019-09-26 10:33:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(219, 33, 33, NULL, 'Level 2.21', NULL, 'here we are', '', NULL, NULL, 'here we are', NULL, '', '1', '0', '2019-09-26 10:33:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(220, 33, 33, NULL, 'Level 2.21', NULL, 'what is fun', '', NULL, NULL, 'what is fun', NULL, '', '1', '0', '2019-09-26 10:33:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(221, 41, 41, NULL, 'Level 2.29', NULL, 'no', '', NULL, NULL, 'no', NULL, '', '1', '0', '2019-09-26 10:48:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(222, 41, 41, NULL, 'Level 2.29', NULL, 'yes', '', NULL, NULL, 'yes', NULL, '', '1', '0', '2019-09-26 10:48:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(223, 41, 41, NULL, 'Level 2.29', NULL, 'sing', '', NULL, NULL, 'sing', NULL, '', '1', '0', '2019-09-26 10:48:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(224, 41, 41, NULL, 'Level 2.29', NULL, 'ring', '', NULL, NULL, 'ring', NULL, '', '1', '0', '2019-09-26 10:48:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(225, 41, 41, NULL, 'Level 2.29', NULL, 'backing', '', NULL, NULL, 'backing', NULL, '', '1', '0', '2019-09-26 10:48:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(226, 41, 41, NULL, 'Level 2.29', NULL, 'ticking', '', NULL, NULL, 'ticking', NULL, '', '1', '0', '2019-09-26 10:48:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(227, 41, 41, NULL, 'Level 2.29', NULL, 'running', '', NULL, NULL, 'running', NULL, '', '1', '0', '2019-09-26 10:48:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(228, 41, 41, NULL, 'Level 2.29', NULL, 'No', '', NULL, NULL, 'No', NULL, '', '1', '0', '2019-09-26 10:48:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(229, 41, 41, NULL, 'Level 2.29', NULL, 'on', '', NULL, NULL, 'on', NULL, '', '1', '0', '2019-09-26 10:48:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(230, 41, 41, NULL, 'Level 2.29', NULL, 'Yes', '', NULL, NULL, 'Yes', NULL, '', '1', '0', '2019-09-26 10:48:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(231, 41, 41, NULL, 'Level 2.29', NULL, 'pitching', '', NULL, NULL, 'pitching', NULL, '', '1', '0', '2019-09-26 10:49:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(232, 41, 41, NULL, 'Level 2.29', NULL, 'spring', '', NULL, NULL, 'spring', NULL, '', '1', '0', '2019-09-26 10:49:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(233, 41, 41, NULL, 'Level 2.29', NULL, 'swing', '', NULL, NULL, 'swing', NULL, '', '1', '0', '2019-09-26 10:49:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(234, 41, 41, NULL, 'Level 2.29', NULL, 'swinging', '', NULL, NULL, 'swinging', NULL, '', '1', '0', '2019-09-26 10:49:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(235, 41, 41, NULL, 'Level 2.29', NULL, 'thing', '', NULL, NULL, 'thing', NULL, '', '1', '0', '2019-09-26 10:49:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(236, 41, 41, NULL, 'Level 2.29', NULL, 'sling', '', NULL, NULL, 'sling', NULL, '', '1', '0', '2019-09-26 10:50:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(237, 41, 41, NULL, 'Level 2.29', NULL, 'fling', '', NULL, NULL, 'fling', NULL, '', '1', '0', '2019-09-26 10:50:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(238, 41, 41, NULL, 'Level 2.29', NULL, 'bring', '', NULL, NULL, 'bring', NULL, '', '1', '0', '2019-09-26 10:50:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(239, 41, 41, NULL, 'Level 2.29', NULL, 'no', '', NULL, NULL, 'no', NULL, '', '1', '0', '2019-09-26 10:50:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(240, 41, 41, NULL, 'Level 2.29', NULL, 'yes', '', NULL, NULL, 'yes', NULL, '', '1', '0', '2019-09-26 10:50:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(241, 41, 41, NULL, 'Level 2.21', NULL, 'on', '', NULL, NULL, 'on', NULL, '', '1', '0', '2019-09-26 10:50:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(242, 45, 45, NULL, 'Level 1.156', NULL, '_nt', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 07:55:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(243, 45, 45, NULL, 'Level 1.15', NULL, '_mp', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:48:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(244, 45, 45, NULL, 'Level 1.15', NULL, '_mp', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:28:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(245, 45, 45, NULL, 'Level 1.15', NULL, '_sp', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:28:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(246, 45, 45, NULL, 'Level 1.15', NULL, '_st', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:28:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(247, 45, 45, NULL, 'Level 1.15', NULL, '_nt', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:28:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(249, 45, 45, NULL, 'Level 1.15', NULL, 'an_', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:33:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(250, 45, 45, NULL, 'Level 1.15', NULL, 'am_', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:33:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(251, 45, 45, NULL, 'Level 1.15', NULL, 'is_', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:33:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(252, 45, 45, NULL, 'Level 1.15', NULL, '_am', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:33:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(253, 45, 45, NULL, 'Level 1.15', NULL, '_ab', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:34:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(254, 45, 45, NULL, 'Level 1.15', NULL, '_it', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:34:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(255, 45, 45, NULL, 'Level 1.15', NULL, 'ti_', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:34:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(256, 45, 45, NULL, 'Level 1.15', NULL, 'ni_', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:34:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(257, 45, 45, NULL, 'Level 1.15', NULL, 'ra_', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:34:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(357, 14, 14, NULL, 'Level 2.3', NULL, 'ick eck ick eck', '', 'neck nick nick neck', '', '', NULL, '', '1', '0', '2019-10-21 08:40:22', NULL, 'deck tick sick deck', '', 'lick nick deck neck', '', 'lick nick neck nick lick', NULL),
(259, 45, 45, NULL, 'Level 1.15', NULL, '_am', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:35:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(260, 45, 45, NULL, 'Level 1.15', NULL, '_in', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:35:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(261, 45, 45, NULL, 'Level 1.15', NULL, '_an', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-11-06 09:27:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(262, 45, 45, NULL, 'Level 1.15', NULL, 't_n', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-10-16 05:47:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(263, 45, 45, NULL, 'Level 1.15', NULL, 'f_n', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:36:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(264, 45, 45, NULL, 'Level 1.15', NULL, 'p_n', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:36:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(265, 45, 45, NULL, 'Level 1.15', NULL, 't_p', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:36:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(266, 45, 45, NULL, 'Level 1.15', NULL, 's_p', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:36:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(267, 45, 45, NULL, 'Level 1.15', NULL, 's_p', '', NULL, NULL, 'a, i, f, b, n, s, p, t , m', NULL, '', '1', '0', '2019-09-27 09:36:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(268, 47, 47, NULL, 'Level 1.17', NULL, 'th', '', 'sh', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ch', '', '', '', NULL, NULL),
(269, 47, 47, NULL, 'Level 1.17', NULL, 'ap', '', 'at', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'an', '', '', '', NULL, NULL),
(270, 47, 47, NULL, 'Level 1.17', NULL, 'ep', '', 'ip', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ap', '', '', '', NULL, NULL),
(271, 47, 47, NULL, 'Level 1.17', NULL, 'sh', '', 'am', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'af', '', '', '', NULL, NULL),
(272, 47, 47, NULL, 'Level 1.17', NULL, 'ip', '', 'it', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ick', '', '', '', NULL, NULL),
(273, 47, 47, NULL, 'Level 1.17', NULL, 'is', '', 'up', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ip', '', '', '', NULL, NULL),
(274, 47, 47, NULL, 'Level 1.17', NULL, 'ash', '', 'imp', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'amp', '', '', '', NULL, NULL),
(275, 47, 47, NULL, 'Level 1.17', NULL, 'ick', '', 'ith', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ish', '', '', '', NULL, NULL),
(276, 47, 47, NULL, 'Level 1.17', NULL, 'ash', '', 'ack', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ick', '', '', '', NULL, NULL),
(277, 47, 47, NULL, 'Level 1.17', NULL, 'fan', '', 'san', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'fam', '', '', '', NULL, NULL),
(278, 47, 47, NULL, 'Level 1.17', NULL, 'ump', '', 'imp', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'ent', '', '', '', NULL, NULL),
(279, 47, 47, NULL, 'Level 1.17', NULL, 'tab', '', 'sat', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'pam', '', '', '', NULL, NULL),
(280, 47, 47, NULL, 'Level 1.17', NULL, 'at', '', 'mad', '', 'option3', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'am', '', '', '', NULL, NULL),
(281, 47, 47, NULL, 'Level 1.17', NULL, 'ran', '', 'tip', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'rip', '', '', '', NULL, NULL),
(282, 47, 47, NULL, 'Level 1.17', NULL, 'tan', '', 'fan', '', 'option2', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'man', '', '', '', NULL, NULL),
(283, 47, 47, NULL, 'Level 1.17', NULL, 'fit', '', 'bit', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'lit', '', '', '', NULL, NULL),
(284, 47, 47, NULL, 'Level 1.17', NULL, 'pen', '', 'pan', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'pam', '', '', '', NULL, NULL),
(285, 47, 47, NULL, 'Level 1.17', NULL, 'men', '', 'map', '', 'option1', NULL, '', '1', '0', '2019-10-08 06:33:18', NULL, 'len', '', '', '', NULL, NULL),
(288, 53, 53, NULL, 'Level 1.23', NULL, 'I am', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:14:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(289, 53, 53, NULL, 'Level 1.23', NULL, 'I ran', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:14:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(290, 53, 53, NULL, 'Level 1.23', NULL, 'I can', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:14:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(291, 53, 53, NULL, 'Level 1.23', NULL, 'I am fit', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:14:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(292, 53, 53, NULL, 'Level 1.23', NULL, 'I can bat', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:14:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(293, 53, 53, NULL, 'Level 1.23', NULL, 'I can fan', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:15:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(294, 53, 53, NULL, 'Level 1.23', NULL, 'You can', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:15:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(295, 53, 53, NULL, 'Level 1.23', NULL, 'You ran', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:15:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(296, 53, 53, NULL, 'Level 1.23', NULL, 'I am man', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:15:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(297, 53, 53, NULL, 'Level 1.23', NULL, 'You can bat', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:15:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(298, 53, 53, NULL, 'Level 1.23', NULL, 'You can run', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:16:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(299, 53, 53, NULL, 'Level 1.23', NULL, 'I am Sam', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:16:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(300, 53, 53, NULL, 'Level 1.23', NULL, 'I am Pam', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:16:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(301, 57, 57, NULL, 'Level 1.27', NULL, 'w', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:26:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(302, 57, 57, NULL, 'Level 1.27', NULL, 'c', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:26:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(303, 57, 57, NULL, 'Level 1.27', NULL, 'z', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:26:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(304, 57, 57, NULL, 'Level 1.27', NULL, 'x', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:26:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(305, 57, 57, NULL, 'Level 1.27', NULL, 'are', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:26:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(306, 57, 57, NULL, 'Level 1.27', NULL, 'c', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:26:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(307, 57, 57, NULL, 'Level 1.27', NULL, 'x', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:26:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(308, 57, 57, NULL, 'Level 1.27', NULL, 'w', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:26:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(309, 57, 57, NULL, 'Level 1.27', NULL, 'are', '', NULL, NULL, '', NULL, '', '1', '0', '2019-09-27 10:26:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(310, 13, 13, NULL, 'Level 2.2', NULL, 'You r_n fast.', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:34:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(311, 13, 13, NULL, 'Level 2.2', NULL, 'I like to f_sh', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:53:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(312, 13, 13, NULL, 'Level 2.2', NULL, 'Hand me a d_sh.', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:34:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(313, 13, 13, NULL, 'Level 2.2', NULL, 'Can you h_t a ball?', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:34:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(314, 13, 13, NULL, 'Level 2.2', NULL, 'W_sh for a dog.', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:34:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(315, 13, 13, NULL, 'Level 2.2', NULL, 'J_mp on the log.', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:35:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(316, 13, 13, NULL, 'Level 2.2', NULL, 'The d_g is fun', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:35:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(317, 13, 13, NULL, 'Level 2.2', NULL, 'I h_d a red bat.', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:35:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(318, 13, 13, NULL, 'Level 2.2', NULL, 'The dog h_d the ball.', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:35:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(319, 13, 13, NULL, 'Level 2.2', NULL, 'The kid ran f_st.', '', NULL, NULL, 'u, i, o, a', NULL, '', '1', '0', '2019-09-27 10:35:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(355, 62, 62, NULL, 'Level 1.28', NULL, 'dish fish dish wish', '', 'wish fish dish fish', '', '', NULL, '', '1', '0', '2019-10-21 08:38:00', NULL, 'dish wish fish dish', '', 'fish dish wish fish', '', NULL, NULL),
(353, 55, 55, NULL, 'Level 1.25', NULL, 'ith ich ith ich', '', 'ich ith ith ich', '', '', NULL, '', '1', '0', '2019-10-21 08:31:00', NULL, 'ith ich ich ith', '', 'ich ith ith ich', '', NULL, NULL),
(354, 56, 56, NULL, 'Level 1.26', NULL, 'ing ang ang ing', '', 'ang ing ang ang', '', '', NULL, '', '1', '0', '2019-10-21 08:32:43', NULL, 'ing ang ing ing', '', 'ing ang ing ang', '', NULL, NULL),
(351, 50, 50, NULL, 'Level 1.20', NULL, 'rat mat pat rip', '', 'mip sit pet tan', '', '', NULL, '', '1', '0', '2019-10-21 08:26:24', NULL, 'set rep run man', '', 'fan sun rap pep', '', NULL, NULL),
(327, 59, 59, NULL, 'Ruby', NULL, 'A ruby', '', 'is a rare gem.', '', '', NULL, '', '1', '0', '2019-10-11 12:44:41', NULL, 'It is very hard', '', 'and difficult', '', 'to scratch.', NULL),
(328, 59, 59, NULL, 'Ruby', NULL, 'It is used', '', 'for jewelry.', '', '', NULL, '', '1', '0', '2019-10-11 12:42:05', NULL, 'Because it is', '', 'very rare,', '', 'it costs more', 'than diamonds.'),
(329, 59, 59, NULL, 'Ruby', NULL, 'A gem stone', '', 'does not look important', '', '', NULL, '', '1', '0', '2019-10-11 12:42:05', NULL, 'until it is cut', '', 'and polished.', '', NULL, NULL),
(330, 59, 59, NULL, 'Ruby', NULL, 'After it is polished', '', 'it is very special.', '', '', NULL, '', '1', '0', '2019-10-11 12:42:05', NULL, 'What color', '', 'is a ruby?', '', NULL, NULL),
(332, 60, 60, NULL, 'Moon Rocks', NULL, 'Between 1969 and 1972,', '', 'astronauts collected', '', '', NULL, '', '1', '0', '2019-10-11 12:48:15', NULL, 'many lunar rocks.', '', NULL, '', NULL, NULL),
(333, 60, 60, NULL, 'Moon Rocks', NULL, 'The rocks were collected', '', 'to help us understand', '', '', NULL, '', '1', '0', '2019-10-11 12:48:15', NULL, 'something about the moon.', '', NULL, '', NULL, NULL),
(334, 60, 60, NULL, 'Moon Rocks', NULL, 'The size of the rocks range,', '', 'from the size of a grain of sand', '', '', NULL, '', '1', '0', '2019-10-11 12:48:15', NULL, 'to a basketball.', '', 'There are three kinds of rocks.', '', NULL, NULL),
(335, 60, 60, NULL, 'Moon Rocks', NULL, 'Moon rocks vary in color', '', 'from gray to black to white to green.', '', '', NULL, '', '1', '0', '2019-10-11 12:48:15', NULL, 'Some are glassy,', '', 'some are hard,', '', 'and some are very fragile.', 'Where would you find a moon rock?'),
(337, 61, 61, NULL, 'Thunderstorms', NULL, 'At any time,', '', 'there are nearly 2,000', '', '', NULL, '', '1', '0', '2019-10-11 12:54:11', NULL, 'active thunderstorms in the world.', '', NULL, '', NULL, NULL);
INSERT INTO `questions` (`id`, `session_id`, `sub_level_id`, `level_sub_level_id`, `question`, `question_image`, `option1`, `option1_img`, `option2`, `option2_img`, `answer`, `answer_img`, `answer_record`, `status`, `delete_status`, `created_at`, `updated_at`, `option3`, `option3_img`, `option4`, `option4_img`, `option5`, `option6`) VALUES
(338, 61, 61, NULL, 'Thunderstorms', NULL, 'Most do no harm.', '', 'Many give welcome relief', '', '', NULL, '', '1', '0', '2019-10-11 12:54:11', NULL, 'on a muggy summer day', '', 'and give a special show of lightening.', '', NULL, NULL),
(339, 61, 61, NULL, 'Thunderstorms', NULL, 'Some storms can be a problem.', '', 'They may have hail', '', '', NULL, '', '1', '0', '2019-10-11 12:54:11', NULL, 'as big as golf balls.', '', 'They may have very strong winds', '', 'that wreck homes and barns.', NULL),
(340, 61, 61, NULL, 'Thunderstorms', NULL, 'The average number', '', 'of thunderstorm days in a year', '', '', NULL, '', '1', '0', '2019-10-11 12:54:11', NULL, 'is different', '', 'depending upon where you live.', '', NULL, NULL),
(341, 61, 61, NULL, 'Thunderstorms', NULL, 'On the west coast', '', 'of the United States,', '', '', NULL, '', '1', '0', '2019-10-11 12:54:11', NULL, 'the average thunderstorm days', '', 'in a year is under 10.', '', NULL, NULL),
(342, 61, 61, NULL, 'Thunderstorms', NULL, 'In Florida', '', 'the average number of days', '', '', NULL, '', '1', '0', '2019-10-11 12:55:54', NULL, 'is more than 70!', '', NULL, '', NULL, NULL),
(343, 61, 61, NULL, 'Thunderstorms', NULL, 'The number', '', 'of thunderstorms increases', '', '', NULL, '', '1', '0', '2019-10-11 12:55:54', NULL, 'as you go east and south', '', 'across the United States.', '', NULL, NULL),
(344, 61, 61, NULL, 'Thunderstorms', NULL, 'Do you have', '', 'a lot of thunderstorms?', '', '', NULL, '', '1', '0', '2019-10-11 12:55:54', NULL, NULL, '', NULL, '', NULL, NULL),
(347, 42, 42, NULL, 'Level 1.12', NULL, 'a i a a i', '', 'i a a i a', '', '', NULL, '', '1', '0', '2019-10-16 05:42:07', NULL, 'i a a i a', '', NULL, '', NULL, NULL),
(348, 43, 43, NULL, 'Level 1.13', NULL, 'it at it it', '', 'at it it at', '', '', NULL, '', '1', '0', '2019-10-21 08:23:09', NULL, 'it at it at', '', 'at it at it', '', NULL, NULL),
(349, 44, 44, NULL, 'Level 1.14', NULL, 'is am it am', '', 'it is am is', '', '', NULL, '', '1', '0', '2019-10-21 08:24:31', NULL, 'am it is am', '', 'is it am is', '', NULL, NULL),
(350, 52, 52, NULL, 'Level 1.22', NULL, 'you I is you', '', 'is you I is', '', '', NULL, '', '1', '0', '2019-10-21 08:29:15', NULL, 'I is you I', '', 'is you I you', '', NULL, NULL),
(356, 63, 63, NULL, 'Level 1.29', NULL, 'ot eb og ot', '', 'eb ot ot og', '', '', NULL, '', '1', '0', '2019-10-21 08:35:04', NULL, 'eb ot og eb', '', 'ob eb og eb', '', NULL, NULL),
(358, 15, 15, NULL, 'Level 2.4', NULL, 'tan man ran fan', '', 'can ran sand and', '', '', NULL, '', '1', '0', '2019-10-21 08:42:37', NULL, 'pan fan ban can', '', 'and man tan am', '', NULL, NULL),
(359, 22, 22, NULL, 'Level 2.11', NULL, 'bad bid did dab', '', 'bed bub bud bid', '', '', NULL, '', '1', '0', '2019-10-16 06:31:29', NULL, 'bud bab dud dub', '', NULL, '', NULL, NULL),
(360, 28, 28, NULL, 'Level 2.16', NULL, 'bat bate sit site', '', 'cat cate rat rate', '', '', NULL, '', '1', '0', '2019-10-16 06:35:25', NULL, 'fin fine kit kite', '', 'pip pipe man mane', '', NULL, NULL),
(361, 29, 29, NULL, 'Level 2.17', NULL, 'fat bite fate man', '', 'bit mane pip cope', '', '', NULL, '', '1', '0', '2019-10-21 06:43:09', NULL, 'man cope pipe rip', '', 'fate cop fat ripe', '', NULL, NULL),
(362, 34, 34, NULL, 'Level 2.22', NULL, 'what where about for', '', 'here about would little', '', '', NULL, '', '1', '0', '2019-10-21 08:10:03', NULL, 'said around fun about', '', 'are said when away', '', NULL, NULL),
(363, 36, 36, NULL, 'Level 2.24', NULL, 'tub up dot not', '', 'top mud run fun', '', '', NULL, '', '1', '0', '2019-10-21 08:46:04', NULL, 'rut up sun cup', '', 'hot rug cop pot', '', NULL, NULL),
(364, 37, 37, NULL, 'Level 2.25', NULL, 'thin wish ship with', '', 'bath math shut dash', '', '', NULL, '', '1', '0', '2019-10-21 08:18:36', NULL, 'chap fist cash gasp', '', 'fish chip rich such', '', NULL, NULL),
(365, 19, 19, NULL, 'Level 2.8', NULL, 'rut', '', 'sun', '', '', NULL, '', '1', '0', '2019-10-16 07:14:30', NULL, 'hat', '', NULL, '', NULL, NULL),
(366, 19, 19, NULL, 'Level 2.8', NULL, 'fun', '', 'fan', '', '', NULL, '', '1', '0', '2019-10-16 07:15:04', NULL, 'fin', '', NULL, '', NULL, NULL),
(367, 19, 19, NULL, 'Level 2.8', NULL, 'is', '', 'pan', '', '', NULL, '', '1', '0', '2019-10-16 07:15:37', NULL, 'sun', '', NULL, '', NULL, NULL),
(368, 19, 19, NULL, 'Level 2.8', NULL, 'rip', '', 'pat', '', '', NULL, '', '1', '0', '2019-10-16 07:16:00', NULL, 'fit', '', NULL, '', NULL, NULL),
(369, 19, 19, NULL, 'Level 2.8', NULL, 'lit', '', 'tack', '', '', NULL, '', '1', '0', '2019-10-16 07:16:24', NULL, 'map', '', NULL, '', NULL, NULL),
(370, 19, 19, NULL, 'Level 2.8', NULL, 'sun', '', 'gum', '', '', NULL, '', '1', '0', '2019-10-16 07:16:50', NULL, 'sam', '', NULL, '', NULL, NULL),
(371, 19, 19, NULL, 'Level 2.8', NULL, 'sang', '', 'sing', '', '', NULL, '', '1', '0', '2019-10-16 07:17:12', NULL, 'ring', '', NULL, '', NULL, NULL),
(372, 19, 19, NULL, 'Level 2.8', NULL, 'fit', '', 'dish', '', '', NULL, '', '1', '0', '2019-10-16 07:17:32', NULL, 'rich', '', NULL, '', NULL, NULL),
(373, 19, 19, NULL, 'Level 2.8', NULL, 'tip', '', 'lot', '', '', NULL, '', '1', '0', '2019-10-16 07:17:55', NULL, 'top', '', NULL, '', NULL, NULL),
(374, 19, 19, NULL, 'Level 2.8', NULL, 'song', '', 'sit', '', '', NULL, '', '1', '0', '2019-10-16 07:18:21', NULL, 'ring', '', NULL, '', NULL, NULL),
(375, 20, 20, NULL, 'Level 2.9', NULL, '_nt', '', NULL, NULL, 'b f d m w', NULL, '', '1', '0', '2019-10-22 10:41:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(376, 20, 20, NULL, 'Level 2.9', NULL, '_ing', '', NULL, NULL, 's n r w v', NULL, '', '1', '0', '2019-10-16 08:06:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(377, 20, 20, NULL, 'Level 2.9', NULL, '_ish', '', NULL, NULL, 'd f r s w', NULL, '', '1', '0', '2019-10-16 08:06:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(378, 20, 20, NULL, 'Level 2.9', NULL, '_ip', '', NULL, NULL, 's b d r l', NULL, '', '1', '0', '2019-10-16 08:06:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(379, 20, 20, NULL, 'Level 2.9', NULL, '_ad', '', NULL, NULL, 'n m s l d', NULL, '', '1', '0', '2019-10-16 08:06:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(380, 25, 25, NULL, 'Level 2.14', NULL, 'bit bite', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:24:37', NULL, NULL, '', NULL, '', NULL, NULL),
(381, 25, 25, NULL, 'Level 2.14', NULL, 'kit kite', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:25:00', NULL, NULL, '', NULL, '', NULL, NULL),
(382, 25, 25, NULL, 'Level 2.14', NULL, 'bat bate', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:25:43', NULL, NULL, '', NULL, '', NULL, NULL),
(383, 25, 25, NULL, 'Level 2.14', NULL, 'sit site', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:26:04', NULL, NULL, '', NULL, '', NULL, NULL),
(384, 25, 25, NULL, 'Level 2.14', NULL, 'cot cote', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:26:20', NULL, NULL, '', NULL, '', NULL, NULL),
(385, 25, 25, NULL, 'Level 2.14', NULL, 'win wine', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:26:38', NULL, NULL, '', NULL, '', NULL, NULL),
(386, 25, 25, NULL, 'Level 2.14', NULL, 'fat fate', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:26:52', NULL, NULL, '', NULL, '', NULL, NULL),
(387, 25, 25, NULL, 'Level 2.14', NULL, 'cop cope', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:27:07', NULL, NULL, '', NULL, '', NULL, NULL),
(388, 25, 25, NULL, 'Level 2.14', NULL, 'pip pipe', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:27:19', NULL, NULL, '', NULL, '', NULL, NULL),
(389, 25, 25, NULL, 'Level 2.14', NULL, 'pop Pope', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:27:32', NULL, NULL, '', NULL, '', NULL, NULL),
(390, 25, 25, NULL, 'Level 2.14', NULL, 'shep sheep', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:27:44', NULL, NULL, '', NULL, '', NULL, NULL),
(391, 25, 25, NULL, 'Level 2.14', NULL, 'wed weed', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:27:57', NULL, NULL, '', NULL, '', NULL, NULL),
(392, 25, 25, NULL, 'Level 2.14', NULL, 'fet feet', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:28:11', NULL, NULL, '', NULL, '', NULL, NULL),
(393, 25, 25, NULL, 'Level 2.14', NULL, 'man mane', '', NULL, '', '', NULL, '', '1', '0', '2019-10-16 08:30:55', NULL, NULL, '', NULL, '', NULL, NULL),
(394, 32, 32, NULL, 'Level 2.20', NULL, 'my', '', NULL, NULL, 'my', NULL, '', '1', '0', '2019-10-16 09:08:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(395, 32, 32, NULL, 'Level 2.20', NULL, 'would', '', NULL, NULL, 'would', NULL, '', '1', '0', '2019-10-16 09:08:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(396, 32, 32, NULL, 'Level 2.20', NULL, 'little', '', NULL, NULL, 'little', NULL, '', '1', '0', '2019-10-16 09:08:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(397, 32, 32, NULL, 'Level 2.20', NULL, 'away', '', NULL, NULL, 'away', NULL, '', '1', '0', '2019-10-16 09:08:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(398, 32, 32, NULL, 'Level 2.20', NULL, 'where', '', NULL, NULL, 'where', NULL, '', '1', '0', '2019-10-16 09:08:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(399, 32, 32, NULL, 'Level 2.20', NULL, 'what', '', NULL, NULL, 'what', NULL, '', '1', '0', '2019-10-16 09:09:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(400, 32, 32, NULL, 'Level 2.20', NULL, 'have', '', NULL, NULL, 'have', NULL, '', '1', '0', '2019-10-16 09:09:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(401, 32, 32, NULL, 'Level 2.20', NULL, 'could', '', NULL, NULL, 'could', NULL, '', '1', '0', '2019-10-16 09:09:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(402, 32, 32, NULL, 'Level 2.20', NULL, 'around', '', NULL, NULL, 'around', NULL, '', '1', '0', '2019-10-16 09:09:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(403, 32, 32, NULL, 'Level 2.20', NULL, 'about', '', NULL, NULL, 'about', NULL, '', '1', '0', '2019-10-16 09:09:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(404, 32, 32, NULL, 'Level 2.20', NULL, 'for', '', NULL, NULL, 'for', NULL, '', '1', '0', '2019-10-16 09:09:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(405, 32, 32, NULL, 'Level 2.20', NULL, 'here', '', NULL, NULL, 'here', NULL, '', '1', '0', '2019-10-16 09:09:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(406, 32, 32, NULL, 'Level 2.20', NULL, 'said', '', NULL, NULL, 'said', NULL, '', '1', '0', '2019-10-16 09:09:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(407, 32, 32, NULL, 'Level 2.20', NULL, 'when', '', NULL, NULL, 'when', NULL, '', '1', '0', '2019-10-16 09:09:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(408, 32, 32, NULL, 'Level 2.20', NULL, 'before', '', NULL, NULL, 'before', NULL, '', '1', '0', '2019-10-16 09:09:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(409, 20, 20, NULL, 'Level 2.18', NULL, 'I saw a _ cat', '', NULL, NULL, 'fat fate', NULL, '', '1', '0', '2019-10-16 09:22:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(410, 20, 20, NULL, 'Level 2.18', NULL, 'I can _.', '', NULL, NULL, 'bat bate', NULL, '', '1', '0', '2019-10-16 09:22:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(411, 20, 20, NULL, 'Level 2.18', NULL, 'The man _ a pipe.', '', NULL, NULL, 'hid hide', NULL, '', '1', '0', '2019-10-16 09:22:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(412, 20, 20, NULL, 'Level 2.18', NULL, 'I _  a bite', '', NULL, NULL, 'ate at', NULL, '', '1', '0', '2019-10-16 09:22:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(413, 20, 20, NULL, 'Level 2.18', NULL, 'A dog _ bite.', '', NULL, NULL, 'can cane', NULL, '', '1', '0', '2019-10-16 09:22:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(414, 30, 30, NULL, 'Level 2.18', NULL, 'I saw a _ cat.', '', NULL, NULL, 'fat fate', NULL, '', '1', '0', '2019-10-16 09:26:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(415, 30, 30, NULL, 'Level 2.18', NULL, 'I can _. ', '', NULL, NULL, 'bat bate', NULL, '', '1', '0', '2019-10-16 09:23:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(416, 30, 30, NULL, 'Level 2.18', NULL, 'The man _ a pipe.', '', NULL, NULL, 'hid hide', NULL, '', '1', '0', '2019-10-16 09:23:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(417, 30, 30, NULL, 'Level 2.18', NULL, 'I _ a bite.', '', NULL, NULL, 'ate at', NULL, '', '1', '0', '2019-10-16 09:23:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(418, 30, 30, NULL, 'Level 2.18', NULL, 'A dog _ bite.', '', NULL, NULL, 'can cane', NULL, '', '1', '0', '2019-10-16 09:23:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(420, 35, 35, NULL, 'Level 2.23', NULL, 'I like to run _.', '', NULL, NULL, 'when, said, around', NULL, '', '1', '0', '2019-10-16 09:54:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(421, 35, 35, NULL, 'Level 2.23', NULL, '_ can do we go?', '', NULL, NULL, 'Would, When, Here', NULL, '', '1', '0', '2019-10-16 09:54:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(422, 35, 35, NULL, 'Level 2.23', NULL, 'I _ come here.', '', NULL, NULL, 'for, where, said', NULL, '', '1', '0', '2019-10-16 09:54:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(423, 35, 35, NULL, 'Level 2.23', NULL, '_ is the dog?', '', NULL, NULL, 'About, Where, Find', NULL, '', '1', '0', '2019-10-16 09:54:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(424, 35, 35, NULL, 'Level 2.23', NULL, 'The gift is _ me.', '', NULL, NULL, 'with, what, for', NULL, '', '1', '0', '2019-10-16 09:54:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(425, 35, 35, NULL, 'Level 2.23', NULL, '_ do you want?', '', NULL, NULL, 'Here, What, About', NULL, '', '1', '0', '2019-10-16 09:55:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(426, 38, 38, NULL, 'Level 2.26', NULL, 'och', '', NULL, NULL, 'och', NULL, '', '1', '0', '2019-10-16 10:26:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(427, 38, 38, NULL, 'Level 2.26', NULL, 'ath', '', NULL, NULL, 'ath', NULL, '', '1', '0', '2019-10-16 10:26:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(428, 38, 38, NULL, 'Level 2.26', NULL, 'ush', '', NULL, NULL, 'ush', NULL, '', '1', '0', '2019-10-16 10:26:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(429, 38, 38, NULL, 'Level 2.26', NULL, 'ick', '', NULL, NULL, 'ick', NULL, '', '1', '0', '2019-10-16 10:26:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(430, 38, 38, NULL, 'Level 2.26', NULL, 'ack', '', NULL, NULL, 'ack', NULL, '', '1', '0', '2019-10-16 10:26:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(431, 38, 38, NULL, 'Level 2.26', NULL, 'uck', '', NULL, NULL, 'uck', NULL, '', '1', '0', '2019-10-16 10:27:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(432, 38, 38, NULL, 'Level 2.26', NULL, 'ith', '', NULL, NULL, 'ith', NULL, '', '1', '0', '2019-10-16 10:27:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(433, 38, 38, NULL, 'Level 2.26', NULL, 'ill', '', NULL, NULL, 'ill', NULL, '', '1', '0', '2019-10-16 10:27:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(434, 38, 38, NULL, 'Level 2.26', NULL, 'osh', '', NULL, NULL, 'osh', NULL, '', '1', '0', '2019-10-16 10:27:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(435, 38, 38, NULL, 'Level 2.26', NULL, 'ull', '', NULL, NULL, 'ull', NULL, '', '1', '0', '2019-10-16 10:27:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(436, 38, 38, NULL, 'Level 2.26', NULL, 'ack', '', NULL, NULL, 'ack', NULL, '', '1', '0', '2019-10-16 10:28:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(437, 38, 38, NULL, 'Level 2.26', NULL, 'ick', '', NULL, NULL, 'ick', NULL, '', '1', '0', '2019-10-16 10:28:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(438, 38, 38, NULL, 'Level 2.26', NULL, 'ock', '', NULL, NULL, 'ock', NULL, '', '1', '0', '2019-10-16 10:28:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(439, 39, 39, NULL, 'Level 2.27', NULL, 'b_th', '', NULL, NULL, 'a, e, i,o, u', NULL, '', '1', '0', '2019-10-16 10:41:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(440, 39, 39, NULL, 'Level 2.27', NULL, 's_ck', '', NULL, NULL, 'a, e, i,o, u', NULL, '', '1', '0', '2019-10-16 10:41:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(441, 39, 39, NULL, 'Level 2.27', NULL, 't_ck', '', NULL, NULL, 'a, e, i,o, u', NULL, '', '1', '0', '2019-10-16 10:41:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(442, 39, 39, NULL, 'Level 2.27', NULL, 'p_ll', '', NULL, NULL, 'a, e, i,o, u', NULL, '', '1', '0', '2019-10-16 10:41:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(443, 39, 39, NULL, 'Level 2.27', NULL, 'r_ck', '', NULL, NULL, 'a, e, i,o, u', NULL, '', '1', '0', '2019-10-16 10:41:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(444, 39, 39, NULL, 'Level 2.27', NULL, 'ch_ck', '', NULL, NULL, 'a, e, i, o u', NULL, '', '1', '0', '2019-10-16 10:42:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(445, 39, 39, NULL, 'Level 2.27', NULL, 'f_ll', '', NULL, NULL, 'a, e, i, o u', NULL, '', '1', '0', '2019-10-16 10:42:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(446, 39, 39, NULL, 'Level 2.27', NULL, 'g_sh', '', NULL, NULL, 'a, e, i, o u', NULL, '', '1', '0', '2019-10-16 10:42:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(447, 39, 39, NULL, 'Level 2.27', NULL, 'r_ck', '', NULL, NULL, 'a, e, i, o u', NULL, '', '1', '0', '2019-10-16 10:42:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(448, 39, 39, NULL, 'Level 2.27', NULL, 'w_ck', '', NULL, NULL, 'a, e, i, o u', NULL, '', '1', '0', '2019-10-16 10:42:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(449, 39, 39, NULL, 'Level 2.27', NULL, 'b_ck', '', NULL, NULL, 'a, e, i, o u', NULL, '', '1', '0', '2019-10-16 10:43:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(450, 39, 39, NULL, 'Level 2.27', NULL, 'th_ck', '', NULL, NULL, 'a, e, i, o u', NULL, '', '1', '0', '2019-10-16 10:43:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(451, 40, 40, NULL, 'Level 2.28', NULL, 'lo_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:28:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(452, 40, 40, NULL, 'Level 2.28', NULL, 'pi_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:28:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(453, 40, 40, NULL, 'Level 2.28', NULL, 'ri_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:28:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(454, 40, 40, NULL, 'Level 2.28', NULL, 'ru_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:28:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(455, 40, 40, NULL, 'Level 2.28', NULL, 'sti_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:28:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(456, 40, 40, NULL, 'Level 2.28', NULL, 'la_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:29:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(457, 40, 40, NULL, 'Level 2.28', NULL, 'ru_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:29:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(458, 40, 40, NULL, 'Level 2.28', NULL, 'wi_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:29:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(459, 40, 40, NULL, 'Level 2.28', NULL, 'tra_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:29:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(460, 40, 40, NULL, 'Level 2.28', NULL, 'ri_', '', NULL, NULL, 'ch ck ll sh th', NULL, '', '1', '0', '2019-10-17 09:29:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(461, 24, 24, NULL, 'Level 2.13', NULL, 'ake', '', NULL, NULL, 'm sh e l r t p a k', NULL, '', '1', '0', '2019-10-17 10:40:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(462, 24, 24, NULL, 'Level 2.13', NULL, 'ice', '', NULL, NULL, 'm c n sl b r i e', NULL, '', '1', '0', '2019-10-17 10:40:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(463, 24, 24, NULL, 'Level 2.13', NULL, 'ank', '', NULL, NULL, 'l a b n th t bl k', NULL, '', '1', '0', '2019-10-17 10:40:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(464, 24, 24, NULL, 'Level 2.13', NULL, 'ick', '', NULL, NULL, 'w ck th l ch t i', NULL, '', '1', '0', '2019-10-17 10:40:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(465, 24, 24, NULL, 'Level 2.13', NULL, 'ame', '', NULL, NULL, 'm e n f a bl t', NULL, '', '1', '0', '2019-10-17 10:40:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(466, 24, 24, NULL, 'Level 2.13', NULL, 'ace', '', NULL, NULL, 'c a r e sp pl f', NULL, '', '1', '0', '2019-10-17 10:41:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(467, 51, 51, NULL, 'Level 1.21', NULL, 'I', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:16:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(468, 51, 51, NULL, 'Level 1.21', NULL, 'o', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:16:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(469, 51, 51, NULL, 'Level 1.21', NULL, 'h', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:16:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(470, 51, 51, NULL, 'Level 1.21', NULL, 'j', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:16:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(471, 51, 51, NULL, 'Level 1.21', NULL, 'y', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:16:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(472, 51, 51, NULL, 'Level 1.21', NULL, 'you', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:17:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(473, 51, 51, NULL, 'Level 1.21', NULL, 'd', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:17:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(474, 51, 51, NULL, 'Level 1.21', NULL, 'o', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:17:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(475, 51, 51, NULL, 'Level 1.21', NULL, 'h', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:17:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(476, 51, 51, NULL, 'Level 1.21', NULL, 'I', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:17:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(477, 51, 51, NULL, 'Level 1.21', NULL, 'y', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:18:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(478, 51, 51, NULL, 'Level 1.21', NULL, 'you', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:18:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(479, 51, 51, NULL, 'Level 1.21', NULL, 'd', '', NULL, NULL, '', NULL, '', '1', '0', '2019-10-17 11:18:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(480, 54, 54, NULL, 'Level 1.24', NULL, 'I ran.', '', 'I can.', '', '', NULL, '', '1', '0', '2019-10-17 11:33:47', NULL, 'I am.', '', '', '', NULL, NULL),
(481, 54, 54, NULL, 'Level 1.24', NULL, 'I can run.', '', 'I can fan.', '', '', NULL, '', '1', '0', '2019-10-17 11:33:47', NULL, 'I am man.', '', '', '', NULL, NULL),
(482, 54, 54, NULL, 'Level 1.24', NULL, 'I can bat.', '', 'I can hit.', '', '', NULL, '', '1', '0', '2019-10-17 11:33:47', NULL, 'I can run.', '', '', '', NULL, NULL),
(483, 54, 54, NULL, 'Level 1.24', NULL, 'Can I run?', '', 'Can you bat?', '', '', NULL, '', '1', '0', '2019-10-17 11:33:47', NULL, 'Can I fan?', '', '', '', NULL, NULL),
(484, 54, 54, NULL, 'Level 1.24', NULL, 'I am tan.', '', 'I am Sam.', '', '', NULL, '', '1', '0', '2019-10-17 11:33:47', NULL, 'I am Pam.', '', '', '', NULL, NULL),
(485, 54, 54, NULL, 'Level 1.24', NULL, 'Sam can run.', '', 'Sam can bat.', '', '', NULL, '', '1', '0', '2019-10-17 11:34:59', NULL, 'Sam can hit.', '', '', '', NULL, NULL),
(486, 54, 54, NULL, 'Level 1.24', NULL, 'I am mad.', '', 'I am sad.', '', '', NULL, '', '1', '0', '2019-10-17 11:34:59', NULL, 'I can nap.', '', '', '', NULL, NULL),
(491, 49, 49, NULL, 'Level 1.19', NULL, 'sit', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:20:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(490, 49, 49, NULL, 'Level 1.19', NULL, 'ump', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:20:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(492, 49, 49, NULL, 'Level 1.19', NULL, 'lit', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(493, 49, 49, NULL, 'Level 1.19', NULL, 'rat', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(494, 49, 49, NULL, 'Level 1.19', NULL, 'mip', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(495, 49, 49, NULL, 'Level 1.19', NULL, 'tap', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(496, 49, 49, NULL, 'Level 1.19', NULL, 'man', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(497, 49, 49, NULL, 'Level 1.19', NULL, 'sun', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(498, 49, 49, NULL, 'Level 1.19', NULL, 'pim', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(499, 49, 49, NULL, 'Level 1.19', NULL, 'rip', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(500, 49, 49, NULL, 'Level 1.19', NULL, 'net', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(501, 49, 49, NULL, 'Level 1.19', NULL, 'pet', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(502, 49, 49, NULL, 'Level 1.19', NULL, 'pup', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(503, 49, 49, NULL, 'Level 1.19', NULL, 'let', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(504, 49, 49, NULL, 'Level 1.19', NULL, 'Tap', '', NULL, NULL, 'f r m u i p s t l a n e', NULL, '', '1', '0', '2019-10-18 05:19:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(505, 48, 48, NULL, 'Level 1.18', NULL, 'man', '', NULL, NULL, 'a, i, u, b, m, n, t, f, l', NULL, '', '1', '0', '2019-10-18 07:17:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(506, 48, 48, NULL, 'Level 1.18', NULL, 'bam', '', NULL, NULL, 'a, i, u, b, m, n, t, f, l', NULL, '', '1', '0', '2019-10-18 07:17:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(507, 48, 48, NULL, 'Level 1.18', NULL, 'tim', '', NULL, NULL, 'a, i, u, b, m, n, t, f, l', NULL, '', '1', '0', '2019-10-18 07:17:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(508, 48, 48, NULL, 'Level 1.18', NULL, 'fan', '', NULL, NULL, 'a, i, u, b, m, n, t, f, l', NULL, '', '1', '0', '2019-10-18 07:17:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(509, 48, 48, NULL, 'Level 1.18', NULL, 'tub', '', NULL, NULL, 'a, i, u, b, m, n, t, f, l', NULL, '', '1', '0', '2019-10-18 07:17:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(510, 48, 48, NULL, 'Level 1.18', NULL, 'fit', '', NULL, NULL, 'a, i, u, b, m, n, t, f, l', NULL, '', '1', '0', '2019-10-18 07:18:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(511, 48, 48, NULL, 'Level 1.18', NULL, 'lit', '', NULL, NULL, 'a, i, u, b, m, n, t, f, l', NULL, '', '1', '0', '2019-10-18 07:18:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(512, 48, 48, NULL, 'Level 1.18', NULL, 'ant', '', NULL, NULL, 'a, i, u, b, m, n, t, f, l', NULL, '', '1', '0', '2019-10-18 07:18:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(513, 48, 48, NULL, 'Level 1.18', NULL, 'ran', '', NULL, NULL, 'a, i, u, e, s, t, p, n, r', NULL, '', '1', '0', '2019-10-18 07:18:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(514, 48, 48, NULL, 'Level 1.18', NULL, 'pen', '', NULL, NULL, 'a, i, u, e, s, t, p, n, r', NULL, '', '1', '0', '2019-10-18 07:18:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(515, 48, 48, NULL, 'Level 1.18', NULL, 'rap', '', NULL, NULL, 'a, i, u, e, s, t, p, n, r', NULL, '', '1', '0', '2019-10-18 07:19:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(516, 48, 48, NULL, 'Level 1.18', NULL, 'rip', '', NULL, NULL, 'a, i, u, e, s, t, p, n, r', NULL, '', '1', '0', '2019-10-18 07:19:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(517, 48, 48, NULL, 'Level 1.18', NULL, 'sap', '', NULL, NULL, 'a, i, u, e, s, t, p, n, r', NULL, '', '1', '0', '2019-10-18 07:19:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(518, 48, 48, NULL, 'Level 1.18', NULL, 'nap', '', NULL, NULL, 'a, i, u, e, s, t, p, n, r', NULL, '', '1', '0', '2019-10-18 07:19:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(519, 48, 48, NULL, 'Level 1.18', NULL, 'pan', '', NULL, NULL, 'a, i, u, e, s, t, p, n, r', NULL, '', '1', '0', '2019-10-18 07:19:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(520, 48, 48, NULL, 'Level 1.18', NULL, 'pin', '', NULL, NULL, 'a, i, u, e, s, t, p, n, r', NULL, '', '1', '0', '2019-10-18 07:20:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(521, 48, 48, NULL, 'Level 1.18', NULL, 'rap', '', NULL, NULL, 'a, i, u, e, s, t, p, n, r', NULL, '', '1', '0', '2019-10-18 07:20:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(522, 48, 48, NULL, 'Level 1.18', NULL, 'sit', '', NULL, NULL, 'a, i, u, t, s, m, p, ck', NULL, '', '1', '0', '2019-10-18 07:20:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(523, 48, 48, NULL, 'Level 1.18', NULL, 'ick', '', NULL, NULL, 'a, i, u, t, s, m, p, ck', NULL, '', '1', '0', '2019-10-18 07:20:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(524, 48, 48, NULL, 'Level 1.18', NULL, 'sam', '', NULL, NULL, 'a, i, u, t, s, m, p, ck', NULL, '', '1', '0', '2019-10-18 07:20:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(525, 48, 48, NULL, 'Level 1.18', NULL, 'up', '', NULL, NULL, 'a, i, u, t, s, m, p, ck', NULL, '', '1', '0', '2019-10-18 07:21:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(526, 48, 48, NULL, 'Level 1.18', NULL, 'ack', '', NULL, NULL, 'a, i, u, t, s, m, p, ck', NULL, '', '1', '0', '2019-10-18 07:21:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(527, 48, 48, NULL, 'Level 1.18', NULL, 'ump', '', NULL, NULL, 'a, i, u, t, s, m, p, ck', NULL, '', '1', '0', '2019-10-18 07:21:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(528, 16, 16, NULL, 'Level 2.5', NULL, 'm _ _', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:22:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(529, 16, 16, NULL, 'Level 2.5', NULL, '_ _ t', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:22:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(530, 16, 16, NULL, 'Level 2.5', NULL, '_ _ n', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:22:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(531, 16, 16, NULL, 'Level 2.5', NULL, 't _ _', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:22:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(532, 16, 16, NULL, 'Level 2.5', NULL, '_ s', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:22:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(533, 16, 16, NULL, 'Level 2.5', NULL, 'di _ _', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:23:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(534, 16, 16, NULL, 'Level 2.5', NULL, '_ ing', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:23:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(535, 16, 16, NULL, 'Level 2.5', NULL, '_ _ sh', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:23:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(536, 16, 16, NULL, 'Level 2.5', NULL, 's _ _ _', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:23:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(537, 16, 16, NULL, 'Level 2.5', NULL, 'pe _', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:23:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(538, 16, 16, NULL, 'Level 2.5', NULL, 'n _ t', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:25:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(539, 16, 16, NULL, 'Level 2.5', NULL, 'm _ _', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:25:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(540, 16, 16, NULL, 'Level 2.5', NULL, 'lo _', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:25:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(541, 16, 16, NULL, 'Level 2.5', NULL, '_ ot', '', NULL, NULL, 'a, r, n, t, i, s, h, g, u, e', NULL, '', '1', '0', '2019-10-18 08:25:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(542, 23, 23, NULL, 'Level 2.23', NULL, '_ _ _', '', NULL, NULL, 'r n a d l b', NULL, '', '1', '0', '2019-10-18 10:33:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(543, 21, 21, NULL, 'Level 2.10', NULL, 'cake take make sake', '', 'lake wake rake shake', NULL, '_ake', NULL, '', '1', '0', '2019-10-18 11:14:24', NULL, 'flake bake take make', NULL, NULL, NULL, NULL, NULL),
(544, 21, 21, NULL, 'Level 2.10', NULL, 'tame same lame', '', 'fame dame game', NULL, '_ame', NULL, '', '1', '0', '2019-10-18 11:14:24', NULL, 'came name same', NULL, NULL, NULL, NULL, NULL),
(545, 21, 21, NULL, 'Level 2.10', NULL, 'pace race face', '', 'space place grace', NULL, '_ace', NULL, '', '1', '0', '2019-10-18 11:14:24', NULL, 'lace mace race', NULL, NULL, NULL, NULL, NULL),
(546, 21, 21, NULL, 'Level 2.10', NULL, 'bank sank blank', '', 'thank tank spank', '', '_ank', NULL, '', '1', '0', '2019-10-18 11:21:49', NULL, 'rank blank frank', '', '', '', NULL, NULL),
(547, 21, 21, NULL, 'Level 2.10', NULL, 'mice rice lice', '', 'nice slice price', '', '_ice', NULL, '', '1', '0', '2019-10-18 11:21:38', NULL, 'spice advice dice', '', '', '', NULL, NULL),
(548, 21, 21, NULL, 'Level 2.10', NULL, 'tick wick thick', '', 'chick rick sick', '', '_ick', NULL, '', '1', '0', '2019-10-18 11:21:28', NULL, 'slick chick pick', '', '', '', NULL, NULL),
(552, 0, NULL, 1, 'Level 1.9.1', NULL, '6', '', '1 x x x x _ ', '', 'f, 10, 17, m, 6, 11, 24, v, 49, 15, 19, 47', NULL, '', '1', '0', '2019-10-25 06:58:55', NULL, '', '', '', '', NULL, NULL),
(557, 0, NULL, 1, 'Level 1.9.1', NULL, '11', '', '6 x x x x _', '', 'f, 10, 17, m, 6, 11, 24, v, 49, 15, 19, 47', NULL, '', '1', '0', '2019-10-25 06:58:39', NULL, '', '', '', '', NULL, NULL),
(558, 0, NULL, 1, 'Level 1.9.1', NULL, '17', '', '12 x x x x _', '', 'f, 10, 17, m, 6, 11, 24, v, 49, 15, 19, 47', NULL, '', '1', '0', '2019-10-25 06:58:39', NULL, '', '', '', '', NULL, NULL),
(559, 0, NULL, 1, 'Level 1.9.1', NULL, '24', '', '20 x x x _', '', 'f, 10, 17, m, 6, 11, 24, v, 49, 15, 19, 47', NULL, '', '1', '0', '2019-10-25 06:58:39', NULL, '', '', '', '', NULL, NULL),
(560, 0, NULL, 1, 'Level 1.9.1', NULL, '49', '', '45 x x x _', '', 'f, 10, 17, m, 6, 11, 24, v, 49, 15, 19, 47', NULL, '', '1', '0', '2019-10-25 06:58:39', NULL, '', '', '', '', NULL, NULL),
(561, 0, NULL, 1, 'Level 1.9.1', NULL, '15', '', '9 x x x x x x _', '', 'f, 10, 17, m, 6, 11, 24, v, 49, 15, 19, 47', NULL, '', '1', '0', '2019-10-25 06:58:39', NULL, '', '', '', '', NULL, NULL),
(562, 0, NULL, 1, 'Level 1.9.1', NULL, 'f', '', 'a x x x x _', '', 'f, 10, 17, m, 6, 11, 24, v, 49, 15, 19, 47', NULL, '', '1', '0', '2019-10-25 07:00:45', NULL, '', '', '', '', NULL, NULL),
(563, 0, NULL, 1, 'Level 1.9.1', NULL, 'm', '', 'g x x x x x_', '', 'f, 10, 17, m, 6, 11, 24, v, 49, 15, 19, 47', NULL, '', '1', '0', '2019-10-25 07:00:45', NULL, '', '', '', '', NULL, NULL),
(564, 0, NULL, 1, 'Level 1.9.1', NULL, 'v', '', 't x _', '', 'f, 10, 17, m, 6, 11, 24, v, 49, 15, 19, 47', NULL, '', '1', '0', '2019-10-25 07:00:45', NULL, '', '', '', '', NULL, NULL),
(567, 0, NULL, 3, 'Level 1.9.3', NULL, '7', '', '3 X X X  _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:34:16', NULL, '', '', '', '', NULL, NULL),
(568, 0, NULL, 3, 'Level 1.9.3', NULL, '9', '', '6 X X  _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:34:16', NULL, '', '', '', '', NULL, NULL),
(569, 0, NULL, 3, 'Level 1.9.3', NULL, '12', '', '7 X X X X _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:34:16', NULL, '', '', '', '', NULL, NULL),
(570, 0, NULL, 3, 'Level 1.9.3', NULL, '16', '', '14 X _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:34:16', NULL, '', '', '', '', NULL, NULL),
(571, 0, NULL, 3, 'Level 1.9.3', NULL, '38', '', '33 X X X X  _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:34:16', NULL, '', '', '', '', NULL, NULL),
(572, 0, NULL, 3, 'Level 1.9.3', NULL, 'g', '', 'C X X X  _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:36:21', NULL, '', '', '', '', NULL, NULL),
(573, 0, NULL, 3, 'Level 1.9.3', NULL, 'e', '', 'B X X  _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:36:21', NULL, '', '', '', '', NULL, NULL),
(574, 0, NULL, 3, 'Level 1.9.3', NULL, 'd', '', 'A X X  _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:36:21', NULL, '', '', '', '', NULL, NULL),
(575, 0, NULL, 3, 'Level 1.9.3', NULL, 'h', '', 'D X X X _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:36:21', NULL, '', '', '', '', NULL, NULL),
(576, 0, NULL, 3, 'Level 1.9.3', NULL, 'f', '', 'B X X X _', '', '9, 16, g, f, h, d, e, 36, 7, 12, 38', NULL, '', '1', '0', '2019-10-30 10:36:21', NULL, '', '', '', '', NULL, NULL),
(577, 0, NULL, 7, 'Level 1.9.5', NULL, '7', '', '3 X X X _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:40:29', NULL, '', '', '', '', NULL, NULL),
(578, 0, NULL, 7, 'Level 1.9.5', NULL, '7', '', '2 X X X X _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:40:29', NULL, '', '', '', '', NULL, NULL),
(579, 0, NULL, 7, 'Level 1.9.5', NULL, '7', '', '1 X X X X X _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:40:29', NULL, '', '', '', '', NULL, NULL),
(580, 0, NULL, 7, 'Level 1.9.5', NULL, '10', '', '6 X X X _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:40:29', NULL, '', '', '', '', NULL, NULL),
(581, 0, NULL, 7, 'Level 1.9.5', NULL, '12', '', '9 X X _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:40:29', NULL, '', '', '', '', NULL, NULL),
(582, 0, NULL, 7, 'Level 1.9.5', NULL, 'f', '', 'B X X X _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:42:15', NULL, '', '', '', '', NULL, NULL),
(583, 0, NULL, 7, 'Level 1.9.5', NULL, 'i', '', 'E X X X _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:42:15', NULL, '', '', '', '', NULL, NULL),
(584, 0, NULL, 7, 'Level 1.9.5', NULL, 'k', '', 'H X X _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:42:15', NULL, '', '', '', '', NULL, NULL),
(585, 0, NULL, 7, 'Level 1.9.5', NULL, 'm', '', 'J X X  _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:42:15', NULL, '', '', '', '', NULL, NULL),
(586, 0, NULL, 7, 'Level 1.9.5', NULL, 'e', '', 'A X X X _', '', '10,  f, 7, i, m, 12, j, e, k, n', NULL, '', '1', '0', '2019-10-30 10:42:15', NULL, '', '', '', '', NULL, NULL),
(587, 0, NULL, 9, 'Level 1.9.7', NULL, '6', '', '2 X X X _', '', 'g, 13, p, r, 11, 15, 14,  6, 8, h, l, i', NULL, '', '1', '0', '2019-10-30 11:00:16', NULL, '', '', '', '', NULL, NULL),
(588, 0, NULL, 9, 'Level 1.9.7', NULL, '13', '', '9 X X X _', '', 'g, 13, p, r, 11, 15, 14,  6, 8, h, l, i', NULL, '', '1', '0', '2019-10-30 11:00:16', NULL, '', '', '', '', NULL, NULL),
(589, 0, NULL, 9, 'Level 1.9.7', NULL, '15', '', '11 X X X _', '', 'g, 13, p, r, 11, 15, 14,  6, 8, h, l, i', NULL, '', '1', '0', '2019-10-30 11:00:16', NULL, '', '', '', '', NULL, NULL),
(590, 0, NULL, 9, 'Level 1.9.7', NULL, '15', '', '10 X X X X _', '', 'g, 13, p, r, 11, 15, 14,  6, 8, h, l, i', NULL, '', '1', '0', '2019-10-30 11:00:16', NULL, '', '', '', '', NULL, NULL),
(591, 0, NULL, 9, 'Level 1.9.7', NULL, '11', '', '5 X X X X X _', '', 'g, 13, p, r, 11, 15, 14,  6, 8, h, l, i', NULL, '', '1', '0', '2019-10-30 11:00:16', NULL, '', '', '', '', NULL, NULL),
(592, 0, NULL, 9, 'Level 1.9.7', NULL, 'i', '', 'E X X X _', '', 'g, 13, p, r, 11, 15, 14,  6, 8, h, l, i', NULL, '', '1', '0', '2019-10-30 11:01:55', NULL, '', '', '', '', NULL, NULL),
(593, 0, NULL, 9, 'Level 1.9.7', NULL, 'l', '', 'G X X X X _', '', 'g, 13, p, r, 11, 15, 14,  6, 8, h, l, i', NULL, '', '1', '0', '2019-10-30 11:01:55', NULL, '', '', '', '', NULL, NULL),
(594, 0, NULL, 9, 'Level 1.9.7', NULL, 'p', '', 'N X  _', '', 'g, 13, p, r, 11, 15, 14,  6, 8, h, l, i', NULL, '', '1', '0', '2019-10-30 11:01:55', NULL, '', '', '', '', NULL, NULL),
(595, 0, NULL, 9, 'Level 1.9.7', NULL, 'g', '', 'C X X X  _', '', 'g, 13, p, r, 11, 15, 14,  6, 8, h, l, i', NULL, '', '1', '0', '2019-10-30 11:01:55', NULL, '', '', '', '', NULL, NULL),
(597, 0, NULL, 11, 'Level 1.9.9', NULL, '9', '', '5 X X X _', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:04:27', NULL, '', '', '', '', NULL, NULL),
(598, 0, NULL, 11, 'Level 1.9.9', NULL, '7', '', '2 X X X X _', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:04:27', NULL, '', '', '', '', NULL, NULL),
(599, 0, NULL, 11, 'Level 1.9.9', NULL, '5', '', '1 X X X _', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:04:27', NULL, '', '', '', '', NULL, NULL),
(600, 0, NULL, 11, 'Level 1.9.9', NULL, '11', '', '7 X X X _', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:04:27', NULL, '', '', '', '', NULL, NULL),
(601, 0, NULL, 11, 'Level 1.9.9', NULL, '14', '', '10 X X X _', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:04:27', NULL, '', '', '', '', NULL, NULL),
(602, 0, NULL, 11, 'Level 1.9.9', NULL, '24', '', '20 X X X_', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:06:01', NULL, '', '', '', '', NULL, NULL),
(603, 0, NULL, 11, 'Level 1.9.9', NULL, '19', '', '15 X X X _', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:06:01', NULL, '', '', '', '', NULL, NULL),
(604, 0, NULL, 11, 'Level 1.9.9', NULL, '25', '', '22 X X _', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:06:01', NULL, '', '', '', '', NULL, NULL),
(605, 0, NULL, 11, 'Level 1.9.9', NULL, '34', '', '30 X X X _', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:06:01', NULL, '', '', '', '', NULL, NULL),
(606, 0, NULL, 11, 'Level 1.9.9', NULL, 'e', '', 'A X X X _', '', '7, 9, 11, 5, 24, 25, 19, 34, e, 14', NULL, '', '1', '0', '2019-10-30 11:06:01', NULL, '', '', '', '', NULL, NULL),
(607, 0, NULL, 2, 'Level 1.9.2', NULL, '4', '', '1 X X _', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:26:30', NULL, '', '', '', '', NULL, NULL),
(608, 0, NULL, 2, 'Level 1.9.2', NULL, '5', '', '1 X X X  _', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:26:30', NULL, '', '', '', '', NULL, NULL),
(609, 0, NULL, 2, 'Level 1.9.2', NULL, '3', '', '1 X_', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:26:30', NULL, '', '', '', '', NULL, NULL),
(610, 0, NULL, 2, 'Level 1.9.2', NULL, '5', '', '2 X X  _', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:26:30', NULL, '', '', '', '', NULL, NULL),
(611, 0, NULL, 2, 'Level 1.9.2', NULL, '4', '', '2 X _', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:26:30', NULL, '', '', '', '', NULL, NULL),
(612, 0, NULL, 2, 'Level 1.9.2', NULL, '8', '', '4 X X X _', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:28:15', NULL, '', '', '', '', NULL, NULL),
(613, 0, NULL, 2, 'Level 1.9.2', NULL, '7', '', '3 X X X _', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:28:15', NULL, '', '', '', '', NULL, NULL),
(614, 0, NULL, 2, 'Level 1.9.2', NULL, '8', '', '5 X X  _', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:28:15', NULL, '', '', '', '', NULL, NULL),
(615, 0, NULL, 2, 'Level 1.9.2', NULL, 'd', '', 'A X X  _', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:28:15', NULL, '', '', '', '', NULL, NULL),
(616, 0, NULL, 2, 'Level 1.9.2', NULL, 'e', '', 'C X _', '', '5, 3, 4, 8, d, 7, e, c', NULL, '', '1', '0', '2019-10-30 11:28:15', NULL, '', '', '', '', NULL, NULL),
(617, 0, NULL, 4, 'Level 1.9.4', NULL, '6', '', '2 X X X _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:37:04', NULL, '', '', '', '', NULL, NULL),
(618, 0, NULL, 4, 'Level 1.9.4', NULL, '4', '', '1 X X  _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:37:04', NULL, '', '', '', '', NULL, NULL),
(619, 0, NULL, 4, 'Level 1.9.4', NULL, '9', '', '6 X X _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:37:04', NULL, '', '', '', '', NULL, NULL),
(620, 0, NULL, 4, 'Level 1.9.4', NULL, '9', '', '4 X X X X _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:37:04', NULL, '', '', '', '', NULL, NULL),
(621, 0, NULL, 4, 'Level 1.9.4', NULL, '7', '', '3 X X X _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:37:04', NULL, '', '', '', '', NULL, NULL),
(622, 0, NULL, 4, 'Level 1.9.4', NULL, 'e', '', 'A X X X _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:38:59', NULL, '', '', '', '', NULL, NULL),
(623, 0, NULL, 4, 'Level 1.9.4', NULL, 'g', '', 'C X X X _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:38:59', NULL, '', '', '', '', NULL, NULL),
(624, 0, NULL, 4, 'Level 1.9.4', NULL, 'p', '', 'M X X _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:38:59', NULL, '', '', '', '', NULL, NULL),
(625, 0, NULL, 4, 'Level 1.9.4', NULL, 'j', '', 'F X X X _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:38:59', NULL, '', '', '', '', NULL, NULL),
(626, 0, NULL, 4, 'Level 1.9.4', NULL, 'e', '', 'B X X _', '', '4, 9, 7, g, j, 6, e, p, m, 8', NULL, '', '1', '0', '2019-10-30 11:38:59', NULL, '', '', '', '', NULL, NULL),
(627, 0, NULL, 8, 'Level 1.9.6', NULL, '6', '', '1 X X X X _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:45:41', NULL, '', '', '', '', NULL, NULL),
(628, 0, NULL, 8, 'Level 1.9.6', NULL, '11', '', '6 X X X X _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:45:41', NULL, '', '', '', '', NULL, NULL),
(629, 0, NULL, 8, 'Level 1.9.6', NULL, '17', '', '12 X X X X _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:45:41', NULL, '', '', '', '', NULL, NULL),
(630, 0, NULL, 8, 'Level 1.9.6', NULL, '15', '', '11 X X X _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:45:41', NULL, '', '', '', '', NULL, NULL),
(631, 0, NULL, 8, 'Level 1.9.6', NULL, '9', '', '5 X X X _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:45:41', NULL, '', '', '', '', NULL, NULL),
(632, 0, NULL, 8, 'Level 1.9.6', NULL, '14', '', '9 X X X X   _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:51:07', NULL, '', '', '', '', NULL, NULL),
(633, 0, NULL, 8, 'Level 1.9.6', NULL, 'f', '', 'A X X X X   _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:51:07', NULL, '', '', '', '', NULL, NULL),
(634, 0, NULL, 8, 'Level 1.9.6', NULL, 'm', '', 'G X X X X X _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:51:07', NULL, '', '', '', '', NULL, NULL),
(635, 0, NULL, 8, 'Level 1.9.6', NULL, 'g', '', 'C  X X X _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:51:07', NULL, '', '', '', '', NULL, NULL),
(636, 0, NULL, 8, 'Level 1.9.6', NULL, 'f', '', 'B  X X X _', '', '17, 6, f, g, 11, 9, 16, 15, 13, m, 14', NULL, '', '1', '0', '2019-10-30 11:51:07', NULL, '', '', '', '', NULL, NULL),
(637, 0, NULL, 10, 'Level 1.9.8', NULL, '5', '', '2 X X  _', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:54:08', NULL, '', '', '', '', NULL, NULL),
(638, 0, NULL, 10, 'Level 1.9.8', NULL, '10', '', '6 X X X _', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:54:08', NULL, '', '', '', '', NULL, NULL),
(639, 0, NULL, 10, 'Level 1.9.8', NULL, '6', '', '1 X X X X _', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:54:08', NULL, '', '', '', '', NULL, NULL),
(640, 0, NULL, 10, 'Level 1.9.8', NULL, '8', '', '4 X X X _', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:54:08', NULL, '', '', '', '', NULL, NULL),
(641, 0, NULL, 10, 'Level 1.9.8', NULL, '13', '', '9 X X X_', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:54:08', NULL, '', '', '', '', NULL, NULL),
(642, 0, NULL, 10, 'Level 1.9.8', NULL, '8', '', '3 X X X X _', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:56:00', NULL, '', '', '', '', NULL, NULL),
(643, 0, NULL, 10, 'Level 1.9.8', NULL, 'e', '', 'B X X _', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:56:00', NULL, '', '', '', '', NULL, NULL),
(644, 0, NULL, 10, 'Level 1.9.8', NULL, 'w', '', 'S X X X _', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:56:00', NULL, '', '', '', '', NULL, NULL),
(645, 0, NULL, 10, 'Level 1.9.8', NULL, 'h', '', 'D X X X _', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:56:00', NULL, '', '', '', '', NULL, NULL),
(646, 0, NULL, 10, 'Level 1.9.8', NULL, 'm', '', 'H X X X X _', '', '10,  8,  e,  h,  5,  6, 13, m, w, 12', NULL, '', '1', '0', '2019-10-30 11:56:00', NULL, '', '', '', '', NULL, NULL),
(647, 46, 46, NULL, 'Level 1.16', NULL, 'e', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:01:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(648, 46, 46, NULL, 'Level 1.16', NULL, 'r', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:01:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `questions` (`id`, `session_id`, `sub_level_id`, `level_sub_level_id`, `question`, `question_image`, `option1`, `option1_img`, `option2`, `option2_img`, `answer`, `answer_img`, `answer_record`, `status`, `delete_status`, `created_at`, `updated_at`, `option3`, `option3_img`, `option4`, `option4_img`, `option5`, `option6`) VALUES
(649, 46, 46, NULL, 'Level 1.16', NULL, 'sh', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:01:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(650, 46, 46, NULL, 'Level 1.16', NULL, 'u', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:01:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(651, 46, 46, NULL, 'Level 1.16', NULL, 'd', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:01:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(652, 46, 46, NULL, 'Level 1.16', NULL, 'ch', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:02:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(653, 46, 46, NULL, 'Level 1.16', NULL, 'k', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:02:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(654, 46, 46, NULL, 'Level 1.16', NULL, 'th', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:02:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(655, 46, 46, NULL, 'Level 1.16', NULL, 'l', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:02:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(656, 46, 46, NULL, 'Level 1.16', NULL, 'e', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:02:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(657, 46, 46, NULL, 'Level 1.16', NULL, 'sh', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(658, 46, 46, NULL, 'Level 1.16', NULL, 'ch', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(659, 46, 46, NULL, 'Level 1.16', NULL, 'f', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(660, 46, 46, NULL, 'Level 1.16', NULL, 'u', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(661, 46, 46, NULL, 'Level 1.16', NULL, 'k', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(662, 46, 46, NULL, 'Level 1.16', NULL, 'd', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(663, 46, 46, NULL, 'Level 1.16', NULL, 'r', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(664, 46, 46, NULL, 'Level 1.16', NULL, 'v', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(665, 46, 46, NULL, 'Level 1.16', NULL, 'u', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(666, 46, 46, NULL, 'Level 1.16', NULL, 'e', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:03:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(667, 46, 46, NULL, 'Level 1.16', NULL, 'ch', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:04:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(668, 46, 46, NULL, 'Level 1.16', NULL, 'sh', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:04:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(669, 46, 46, NULL, 'Level 1.16', NULL, 'th', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:04:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(670, 46, 46, NULL, 'Level 1.16', NULL, 'ck', '', NULL, NULL, '', NULL, '', '1', '0', '2019-11-04 05:04:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `institute_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  `class_id` varchar(255) DEFAULT NULL,
  `session_date` date DEFAULT NULL,
  `session_start_time` time DEFAULT NULL,
  `session_end_time` time DEFAULT NULL,
  `status` enum('1','0') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `name`, `institute_id`, `teacher_id`, `student_id`, `subject_id`, `class_id`, `session_date`, `session_start_time`, `session_end_time`, `status`, `created_at`, `updated_at`) VALUES
(1, 'PHP Class', 30, 32, 31, 1, '1', '2019-08-29', '10:00:00', '11:00:00', '1', '2019-09-03 06:16:17', '2019-08-28 10:33:32'),
(3, 'sahil DAS 1', 30, 32, 31, 1, NULL, '2019-08-29', '10:00:00', '11:00:00', '1', '2019-09-03 06:41:41', '2019-09-03 06:28:07'),
(4, 'sahil DAS 2', 30, 32, 31, 1, NULL, '2019-09-03', '10:00:00', '11:00:00', '1', '2019-09-03 06:41:48', '2019-09-03 06:39:56'),
(5, 'sahil DAS 3', 30, 32, 31, 1, NULL, '2019-09-03', '10:00:00', '11:00:00', '1', '2019-09-03 06:41:54', '2019-09-03 06:40:41'),
(6, 'sahil DAS 4', 30, 33, 31, 1, NULL, '2019-09-03', '13:00:00', '14:00:00', '1', '2019-09-14 07:31:58', '2019-09-03 06:41:20');

-- --------------------------------------------------------

--
-- Table structure for table `session_days`
--

CREATE TABLE `session_days` (
  `id` int(11) NOT NULL,
  `session_id` int(11) DEFAULT NULL,
  `day` varchar(255) DEFAULT NULL,
  `status` enum('1','0') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_days`
--

INSERT INTO `session_days` (`id`, `session_id`, `day`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '1', '2019-09-03 06:04:04', NULL),
(2, 1, '2', '1', '2019-09-03 06:04:07', NULL),
(3, 2, '1', '1', '2019-09-03 06:16:00', NULL),
(4, 2, '2', '1', '2019-09-03 06:16:00', NULL),
(5, 3, '3', '1', '2019-09-03 06:28:07', NULL),
(6, 4, '1', '1', '2019-09-03 06:39:56', NULL),
(7, 4, '2', '1', '2019-09-03 06:39:56', NULL),
(8, 5, '3', '1', '2019-09-03 06:40:41', NULL),
(9, 6, '1', '1', '2019-09-03 06:41:20', NULL),
(10, 6, '2', '1', '2019-09-03 06:41:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_details`
--

CREATE TABLE `student_details` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `reference_code` text NOT NULL,
  `gender` text NOT NULL,
  `grade` text NOT NULL,
  `timings` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_details`
--

INSERT INTO `student_details` (`id`, `student_id`, `reference_code`, `gender`, `grade`, `timings`, `created_at`, `updated_at`) VALUES
(1, 31, '12121212', 'male', '3', '5', '2019-10-03 07:38:02', '2019-10-23 20:28:18'),
(2, 49, 'test', 'male', 'a', '30', '2019-10-04 15:17:45', '2019-10-04 15:17:45'),
(3, 52, 'A1', 'male', 'N/A', '1', '2019-10-05 17:15:39', '2019-10-05 17:15:39'),
(4, 55, '142314253421351', 'Female', '43', '4', '2019-10-05 19:18:12', '2019-10-05 19:18:12'),
(5, 62, 'Abc123', 'male', 'Na', '2', '2019-10-09 02:32:30', '2019-10-09 02:32:30'),
(6, 66, 'Xtr222', 'male', 'Na', '2', '2019-10-09 02:43:14', '2019-10-09 02:43:14'),
(7, 73, 'Ttr445', 'Female', 'NA', '2', '2019-10-09 02:49:36', '2019-10-09 02:49:36'),
(8, 74, 'Tr456', 'Female', 'NA', '2', '2019-10-09 02:50:30', '2019-10-09 02:50:30'),
(9, 75, 'Yt4rr', 'Female', 'Na', '2', '2019-10-09 02:51:42', '2019-10-09 02:51:42'),
(10, 81, '11', 'Female', 'A', '2', '2019-10-09 16:05:19', '2019-10-09 16:05:19'),
(11, 84, '43243434', 'Female', '23', '2', '2019-10-09 19:06:45', '2019-10-09 19:06:45'),
(12, 85, '4535435', 'Female', '34', '3', '2019-10-09 19:07:04', '2019-10-09 19:07:04'),
(13, 86, '324324', 'Female', '342', '3', '2019-10-09 19:07:42', '2019-10-09 19:07:42'),
(14, 94, '423423432', 'Female', '342', '3', '2019-10-09 19:55:13', '2019-10-09 19:55:13'),
(15, 97, 'hjhh227', 'male', 'NA', '2', '2019-10-10 02:18:18', '2019-10-10 02:18:18'),
(16, 93, '231323', 'male', '45', '2', '2019-10-10 17:53:18', '2019-10-10 17:55:56'),
(17, 104, '123456', 'Female', '12', '1', '2019-10-10 18:01:26', '2019-10-10 18:01:26'),
(18, 105, '123456', 'Female', '343', '3', '2019-10-10 18:01:56', '2019-10-10 18:01:56'),
(19, 107, '32313231', 'Female', '342', '34', '2019-10-10 18:03:23', '2019-10-10 18:06:30'),
(20, 109, '345342153131', 'Female', '23', '2', '2019-10-11 18:57:33', '2019-10-11 18:57:33'),
(21, 110, '66435424', 'male', '23', '2', '2019-10-11 19:04:05', '2019-10-11 19:04:15'),
(22, 114, '123456789', 'Female', '23', '3', '2019-10-22 12:11:48', '2019-10-22 12:11:48'),
(23, 116, '7355425', 'male', '25', '23', '2019-10-22 12:55:08', '2019-10-22 15:22:09'),
(24, 117, '3432432', 'Female', '23', '23', '2019-10-23 12:39:14', '2019-10-23 12:39:14'),
(25, 122, '12', 'male', 'A', '12', '2019-10-23 17:00:48', '2019-10-23 17:00:48'),
(26, 123, '123456789', 'Female', '23', '2', '2019-10-23 18:07:56', '2019-10-23 18:07:56'),
(27, 129, '11', 'Female', 'A', '22', '2019-10-24 14:06:43', '2019-10-24 14:06:43'),
(28, 130, '12', 'male', 'A', '22', '2019-10-24 19:40:19', '2019-10-24 19:40:19'),
(29, 132, '110101', 'male', 'N/A', '1', '2019-10-24 20:44:43', '2019-10-24 20:44:43');

-- --------------------------------------------------------

--
-- Table structure for table `student_level_details`
--

CREATE TABLE `student_level_details` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `question_id` int(11) NOT NULL,
  `sublevel_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_level_details`
--

INSERT INTO `student_level_details` (`id`, `student_id`, `teacher_id`, `question_id`, `sublevel_id`, `created_at`, `updated_at`) VALUES
(1, 31, NULL, 93, 7, '2019-11-05 16:28:25', '2019-11-05 16:28:25'),
(2, 31, NULL, 95, 7, '2019-11-05 16:28:27', '2019-11-05 16:28:27'),
(3, 31, NULL, 97, 7, '2019-11-05 16:28:32', '2019-11-05 16:28:32'),
(4, 31, NULL, 1, 1, '2019-11-05 16:29:09', '2019-11-05 16:29:09'),
(5, 31, NULL, 2, 1, '2019-11-05 16:29:12', '2019-11-05 16:29:12'),
(6, 31, NULL, 3, 1, '2019-11-05 16:29:18', '2019-11-05 16:29:18'),
(7, 65, NULL, 1, 1, '2019-11-05 17:20:40', '2019-11-05 17:20:40'),
(8, 65, NULL, 2, 1, '2019-11-05 17:20:42', '2019-11-05 17:20:42'),
(9, 65, NULL, 4, 1, '2019-11-05 17:20:55', '2019-11-05 17:20:55'),
(10, 65, NULL, 31, 1, '2019-11-05 17:20:59', '2019-11-05 17:20:59'),
(11, 65, NULL, 29, 1, '2019-11-05 17:21:09', '2019-11-05 17:21:09'),
(12, 65, NULL, 28, 1, '2019-11-05 17:21:11', '2019-11-05 17:21:11'),
(13, 65, NULL, 27, 1, '2019-11-05 17:21:13', '2019-11-05 17:21:13'),
(14, 65, NULL, 5, 2, '2019-11-05 18:25:38', '2019-11-05 18:25:38'),
(15, 65, NULL, 65, 3, '2019-11-05 18:25:53', '2019-11-05 18:25:53'),
(16, 65, NULL, 6, 2, '2019-11-05 18:26:37', '2019-11-05 18:26:37'),
(17, 65, NULL, 34, 2, '2019-11-05 18:26:38', '2019-11-05 18:26:38'),
(18, 65, NULL, 35, 2, '2019-11-05 18:26:40', '2019-11-05 18:26:40'),
(19, 65, NULL, 36, 2, '2019-11-05 18:26:41', '2019-11-05 18:26:41'),
(20, 65, NULL, 37, 2, '2019-11-05 18:26:43', '2019-11-05 18:26:43'),
(21, 65, NULL, 38, 2, '2019-11-05 18:26:45', '2019-11-05 18:26:45'),
(22, 65, NULL, 39, 2, '2019-11-05 18:26:46', '2019-11-05 18:26:46'),
(23, 65, NULL, 40, 2, '2019-11-05 18:26:48', '2019-11-05 18:26:48'),
(24, 65, NULL, 41, 2, '2019-11-05 18:26:49', '2019-11-05 18:26:49'),
(25, 65, NULL, 66, 3, '2019-11-05 18:37:39', '2019-11-05 18:37:39'),
(26, 65, NULL, 30, 1, '2019-11-05 20:24:45', '2019-11-05 20:24:45'),
(27, 66, 64, 348, 43, '2019-11-05 20:29:38', '2019-11-05 20:29:38'),
(28, 66, 64, 1, 1, '2019-11-05 20:38:50', '2019-11-05 20:38:50'),
(29, 66, 64, 2, 1, '2019-11-05 20:38:52', '2019-11-05 20:38:52'),
(30, 66, 64, 5, 2, '2019-11-05 20:40:15', '2019-11-05 20:40:15'),
(31, 66, 64, 6, 2, '2019-11-05 20:40:17', '2019-11-05 20:40:17'),
(32, 66, 64, 34, 2, '2019-11-05 20:40:19', '2019-11-05 20:40:19'),
(33, 66, 64, 35, 2, '2019-11-05 20:40:20', '2019-11-05 20:40:20'),
(34, 66, 64, 36, 2, '2019-11-05 20:40:22', '2019-11-05 20:40:22'),
(35, 66, 64, 37, 2, '2019-11-05 20:40:24', '2019-11-05 20:40:24'),
(36, 66, 64, 38, 2, '2019-11-05 20:40:25', '2019-11-05 20:40:25'),
(37, 66, 64, 39, 2, '2019-11-05 20:40:27', '2019-11-05 20:40:27'),
(38, 66, 64, 40, 2, '2019-11-05 20:40:30', '2019-11-05 20:40:30'),
(39, 66, 64, 41, 2, '2019-11-05 20:41:49', '2019-11-05 20:41:49'),
(40, 66, NULL, 26, 1, '2019-11-06 02:15:11', '2019-11-06 02:15:11'),
(41, 66, NULL, 32, 1, '2019-11-06 02:15:21', '2019-11-06 02:15:21'),
(42, 66, NULL, 33, 1, '2019-11-06 02:15:22', '2019-11-06 02:15:22'),
(43, 66, NULL, 3, 1, '2019-11-06 02:15:47', '2019-11-06 02:15:47'),
(44, 66, NULL, 4, 1, '2019-11-06 02:15:50', '2019-11-06 02:15:50'),
(45, 66, NULL, 31, 1, '2019-11-06 02:15:51', '2019-11-06 02:15:51'),
(46, 66, NULL, 29, 1, '2019-11-06 02:15:54', '2019-11-06 02:15:54'),
(47, 66, NULL, 28, 1, '2019-11-06 02:15:55', '2019-11-06 02:15:55'),
(48, 66, NULL, 27, 1, '2019-11-06 02:15:55', '2019-11-06 02:15:55'),
(49, 66, NULL, 30, 1, '2019-11-06 02:15:58', '2019-11-06 02:15:58'),
(50, 66, NULL, 42, 2, '2019-11-06 02:16:41', '2019-11-06 02:16:41'),
(51, 73, NULL, 1, 1, '2019-11-06 02:17:25', '2019-11-06 02:17:25'),
(52, 73, NULL, 2, 1, '2019-11-06 02:17:26', '2019-11-06 02:17:26'),
(53, 73, NULL, 3, 1, '2019-11-06 02:17:27', '2019-11-06 02:17:27'),
(54, 73, NULL, 4, 1, '2019-11-06 02:17:28', '2019-11-06 02:17:28'),
(55, 73, NULL, 31, 1, '2019-11-06 02:17:28', '2019-11-06 02:17:28'),
(56, 73, NULL, 29, 1, '2019-11-06 02:17:29', '2019-11-06 02:17:29'),
(57, 73, NULL, 28, 1, '2019-11-06 02:17:30', '2019-11-06 02:17:30'),
(58, 73, NULL, 27, 1, '2019-11-06 02:17:31', '2019-11-06 02:17:31'),
(59, 73, NULL, 30, 1, '2019-11-06 02:17:31', '2019-11-06 02:17:31'),
(60, 73, NULL, 26, 1, '2019-11-06 02:17:32', '2019-11-06 02:17:32'),
(61, 73, NULL, 32, 1, '2019-11-06 02:17:33', '2019-11-06 02:17:33'),
(62, 73, NULL, 33, 1, '2019-11-06 02:17:34', '2019-11-06 02:17:34'),
(63, 73, NULL, 5, 2, '2019-11-06 02:17:50', '2019-11-06 02:17:50'),
(64, 73, NULL, 6, 2, '2019-11-06 02:18:09', '2019-11-06 02:18:09'),
(65, 73, NULL, 34, 2, '2019-11-06 02:18:10', '2019-11-06 02:18:10'),
(66, 73, NULL, 35, 2, '2019-11-06 02:18:12', '2019-11-06 02:18:12'),
(67, 73, NULL, 36, 2, '2019-11-06 02:18:14', '2019-11-06 02:18:14'),
(68, 73, NULL, 37, 2, '2019-11-06 02:18:16', '2019-11-06 02:18:16'),
(69, 73, NULL, 38, 2, '2019-11-06 02:18:17', '2019-11-06 02:18:17'),
(70, 73, NULL, 39, 2, '2019-11-06 02:18:19', '2019-11-06 02:18:19'),
(71, 73, NULL, 40, 2, '2019-11-06 02:18:20', '2019-11-06 02:18:20'),
(72, 73, NULL, 41, 2, '2019-11-06 02:18:22', '2019-11-06 02:18:22'),
(73, 73, NULL, 42, 2, '2019-11-06 02:18:26', '2019-11-06 02:18:26'),
(74, 73, NULL, 43, 2, '2019-11-06 02:18:28', '2019-11-06 02:18:28'),
(75, 73, NULL, 44, 2, '2019-11-06 02:18:29', '2019-11-06 02:18:29'),
(76, 73, NULL, 45, 2, '2019-11-06 02:18:33', '2019-11-06 02:18:33'),
(77, 73, NULL, 46, 2, '2019-11-06 02:18:36', '2019-11-06 02:18:36'),
(78, 73, NULL, 47, 2, '2019-11-06 02:18:37', '2019-11-06 02:18:37'),
(79, 73, NULL, 48, 2, '2019-11-06 02:18:39', '2019-11-06 02:18:39'),
(80, 73, NULL, 49, 2, '2019-11-06 02:18:41', '2019-11-06 02:18:41'),
(81, 73, NULL, 50, 2, '2019-11-06 02:18:52', '2019-11-06 02:18:52'),
(82, 73, NULL, 51, 2, '2019-11-06 02:18:56', '2019-11-06 02:18:56'),
(83, 73, NULL, 52, 2, '2019-11-06 02:18:59', '2019-11-06 02:18:59'),
(84, 73, NULL, 53, 2, '2019-11-06 02:19:00', '2019-11-06 02:19:00'),
(85, 73, NULL, 54, 2, '2019-11-06 02:19:03', '2019-11-06 02:19:03'),
(86, 73, NULL, 55, 2, '2019-11-06 02:19:05', '2019-11-06 02:19:05'),
(87, 73, NULL, 65, 3, '2019-11-06 02:37:08', '2019-11-06 02:37:08'),
(88, 73, NULL, 66, 3, '2019-11-06 02:37:14', '2019-11-06 02:37:14'),
(89, 73, NULL, 67, 3, '2019-11-06 02:37:17', '2019-11-06 02:37:17'),
(90, 73, NULL, 68, 3, '2019-11-06 02:37:18', '2019-11-06 02:37:18'),
(91, 73, NULL, 69, 3, '2019-11-06 02:37:19', '2019-11-06 02:37:19'),
(92, 73, NULL, 70, 3, '2019-11-06 02:37:21', '2019-11-06 02:37:21'),
(93, 73, NULL, 71, 3, '2019-11-06 02:37:29', '2019-11-06 02:37:29'),
(94, 73, NULL, 25, 4, '2019-11-06 02:37:51', '2019-11-06 02:37:51'),
(95, 73, NULL, 56, 4, '2019-11-06 02:37:56', '2019-11-06 02:37:56'),
(96, 73, NULL, 57, 4, '2019-11-06 02:37:58', '2019-11-06 02:37:58'),
(97, 73, NULL, 58, 4, '2019-11-06 02:37:59', '2019-11-06 02:37:59'),
(98, 73, NULL, 59, 4, '2019-11-06 02:38:33', '2019-11-06 02:38:33'),
(99, 73, NULL, 60, 4, '2019-11-06 02:38:36', '2019-11-06 02:38:36'),
(100, 73, NULL, 61, 4, '2019-11-06 02:38:39', '2019-11-06 02:38:39'),
(101, 73, NULL, 62, 4, '2019-11-06 02:38:41', '2019-11-06 02:38:41'),
(102, 73, NULL, 63, 4, '2019-11-06 02:38:43', '2019-11-06 02:38:43'),
(103, 73, NULL, 64, 4, '2019-11-06 02:38:45', '2019-11-06 02:38:45'),
(104, 73, NULL, 73, 5, '2019-11-06 02:39:08', '2019-11-06 02:39:08'),
(105, 73, NULL, 74, 5, '2019-11-06 02:39:11', '2019-11-06 02:39:11'),
(106, 73, NULL, 75, 5, '2019-11-06 02:39:13', '2019-11-06 02:39:13'),
(107, 73, NULL, 76, 5, '2019-11-06 02:39:14', '2019-11-06 02:39:14'),
(108, 73, NULL, 77, 5, '2019-11-06 02:39:16', '2019-11-06 02:39:16'),
(109, 73, NULL, 78, 5, '2019-11-06 02:39:19', '2019-11-06 02:39:19'),
(110, 73, NULL, 79, 5, '2019-11-06 02:39:20', '2019-11-06 02:39:20'),
(111, 73, NULL, 80, 5, '2019-11-06 02:39:21', '2019-11-06 02:39:21'),
(112, 73, NULL, 81, 5, '2019-11-06 02:39:22', '2019-11-06 02:39:22'),
(113, 73, NULL, 82, 5, '2019-11-06 02:39:27', '2019-11-06 02:39:27'),
(114, 73, NULL, 83, 6, '2019-11-06 02:40:18', '2019-11-06 02:40:18'),
(115, 73, NULL, 84, 6, '2019-11-06 02:40:24', '2019-11-06 02:40:24'),
(116, 73, NULL, 85, 6, '2019-11-06 02:40:32', '2019-11-06 02:40:32'),
(117, 73, NULL, 86, 6, '2019-11-06 02:40:34', '2019-11-06 02:40:34'),
(118, 73, NULL, 87, 6, '2019-11-06 02:40:35', '2019-11-06 02:40:35'),
(119, 73, NULL, 88, 6, '2019-11-06 02:40:41', '2019-11-06 02:40:41'),
(120, 73, NULL, 89, 6, '2019-11-06 02:40:44', '2019-11-06 02:40:44'),
(121, 73, NULL, 90, 6, '2019-11-06 02:40:46', '2019-11-06 02:40:46'),
(122, 73, NULL, 91, 6, '2019-11-06 02:40:50', '2019-11-06 02:40:50'),
(123, 73, NULL, 92, 6, '2019-11-06 02:40:52', '2019-11-06 02:40:52'),
(124, 73, NULL, 93, 7, '2019-11-06 02:44:43', '2019-11-06 02:44:43'),
(125, 73, NULL, 95, 7, '2019-11-06 02:44:47', '2019-11-06 02:44:47'),
(126, 73, NULL, 97, 7, '2019-11-06 02:44:51', '2019-11-06 02:44:51'),
(127, 73, NULL, 98, 7, '2019-11-06 02:44:54', '2019-11-06 02:44:54'),
(128, 73, NULL, 99, 7, '2019-11-06 02:44:57', '2019-11-06 02:44:57'),
(129, 73, NULL, 103, 7, '2019-11-06 02:44:59', '2019-11-06 02:44:59'),
(130, 73, NULL, 104, 7, '2019-11-06 02:45:02', '2019-11-06 02:45:02'),
(131, 73, NULL, 109, 7, '2019-11-06 02:45:05', '2019-11-06 02:45:05'),
(132, 73, NULL, 110, 7, '2019-11-06 02:45:07', '2019-11-06 02:45:07'),
(133, 73, NULL, 111, 7, '2019-11-06 02:45:10', '2019-11-06 02:45:10'),
(134, 73, NULL, 112, 8, '2019-11-06 02:46:25', '2019-11-06 02:46:25'),
(135, 73, NULL, 113, 8, '2019-11-06 02:46:40', '2019-11-06 02:46:40'),
(136, 73, NULL, 114, 8, '2019-11-06 02:46:51', '2019-11-06 02:46:51'),
(137, 73, NULL, 115, 8, '2019-11-06 02:47:03', '2019-11-06 02:47:03'),
(138, 73, NULL, 116, 8, '2019-11-06 02:47:15', '2019-11-06 02:47:15'),
(139, 73, NULL, 117, 8, '2019-11-06 02:47:26', '2019-11-06 02:47:26'),
(140, 73, NULL, 118, 8, '2019-11-06 02:47:37', '2019-11-06 02:47:37'),
(141, 73, NULL, 119, 8, '2019-11-06 02:47:49', '2019-11-06 02:47:49'),
(142, 73, NULL, 120, 8, '2019-11-06 02:48:01', '2019-11-06 02:48:01'),
(143, 73, NULL, 121, 8, '2019-11-06 02:48:16', '2019-11-06 02:48:16'),
(144, 73, NULL, 122, 10, '2019-11-06 02:54:02', '2019-11-06 02:54:02'),
(145, 73, NULL, 123, 10, '2019-11-06 02:54:07', '2019-11-06 02:54:07'),
(146, 73, NULL, 124, 10, '2019-11-06 02:54:15', '2019-11-06 02:54:15'),
(147, 73, NULL, 125, 10, '2019-11-06 02:54:19', '2019-11-06 02:54:19'),
(148, 73, NULL, 126, 10, '2019-11-06 02:54:25', '2019-11-06 02:54:25'),
(149, 73, NULL, 127, 10, '2019-11-06 02:54:30', '2019-11-06 02:54:30'),
(150, 73, NULL, 128, 10, '2019-11-06 02:54:57', '2019-11-06 02:54:57'),
(151, 73, NULL, 129, 10, '2019-11-06 02:55:06', '2019-11-06 02:55:06'),
(152, 73, NULL, 130, 10, '2019-11-06 02:55:13', '2019-11-06 02:55:13'),
(153, 73, NULL, 131, 10, '2019-11-06 02:55:19', '2019-11-06 02:55:19'),
(154, 73, NULL, 132, 11, '2019-11-06 02:56:00', '2019-11-06 02:56:00'),
(155, 73, NULL, 133, 11, '2019-11-06 02:56:11', '2019-11-06 02:56:11'),
(156, 73, NULL, 134, 11, '2019-11-06 02:56:24', '2019-11-06 02:56:24'),
(157, 73, NULL, 135, 11, '2019-11-06 02:56:38', '2019-11-06 02:56:38'),
(158, 73, NULL, 136, 11, '2019-11-06 02:56:51', '2019-11-06 02:56:51'),
(159, 73, NULL, 137, 11, '2019-11-06 02:57:02', '2019-11-06 02:57:02'),
(160, 73, NULL, 138, 11, '2019-11-06 02:58:17', '2019-11-06 02:58:17'),
(161, 73, NULL, 139, 11, '2019-11-06 02:58:29', '2019-11-06 02:58:29'),
(162, 73, NULL, 140, 11, '2019-11-06 02:58:41', '2019-11-06 02:58:41'),
(163, 73, NULL, 141, 11, '2019-11-06 02:58:54', '2019-11-06 02:58:54'),
(164, 73, NULL, 347, 42, '2019-11-06 03:00:03', '2019-11-06 03:00:03'),
(165, 73, NULL, 348, 43, '2019-11-06 03:03:56', '2019-11-06 03:03:56'),
(166, 73, NULL, 349, 44, '2019-11-06 03:06:09', '2019-11-06 03:06:09'),
(167, 73, NULL, 242, 45, '2019-11-06 03:06:47', '2019-11-06 03:06:47'),
(168, 73, NULL, 243, 45, '2019-11-06 03:06:52', '2019-11-06 03:06:52'),
(169, 73, NULL, 244, 45, '2019-11-06 03:06:57', '2019-11-06 03:06:57'),
(170, 73, NULL, 245, 45, '2019-11-06 03:07:04', '2019-11-06 03:07:04'),
(171, 73, NULL, 246, 45, '2019-11-06 03:07:09', '2019-11-06 03:07:09'),
(172, 73, NULL, 247, 45, '2019-11-06 03:07:14', '2019-11-06 03:07:14'),
(173, 73, NULL, 249, 45, '2019-11-06 03:07:21', '2019-11-06 03:07:21'),
(174, 73, NULL, 250, 45, '2019-11-06 03:07:26', '2019-11-06 03:07:26'),
(175, 73, NULL, 251, 45, '2019-11-06 03:07:29', '2019-11-06 03:07:29'),
(176, 73, NULL, 252, 45, '2019-11-06 03:07:34', '2019-11-06 03:07:34'),
(177, 73, NULL, 253, 45, '2019-11-06 03:07:40', '2019-11-06 03:07:40'),
(178, 73, NULL, 254, 45, '2019-11-06 03:07:45', '2019-11-06 03:07:45'),
(179, 73, NULL, 255, 45, '2019-11-06 03:07:58', '2019-11-06 03:07:58'),
(180, 73, NULL, 256, 45, '2019-11-06 03:08:07', '2019-11-06 03:08:07'),
(181, 73, NULL, 257, 45, '2019-11-06 03:08:11', '2019-11-06 03:08:11'),
(182, 73, NULL, 259, 45, '2019-11-06 03:08:15', '2019-11-06 03:08:15'),
(183, 73, NULL, 260, 45, '2019-11-06 03:08:19', '2019-11-06 03:08:19'),
(184, 73, NULL, 261, 45, '2019-11-06 03:09:13', '2019-11-06 03:09:13'),
(185, 73, NULL, 262, 45, '2019-11-06 03:09:17', '2019-11-06 03:09:17'),
(186, 73, NULL, 263, 45, '2019-11-06 03:09:21', '2019-11-06 03:09:21'),
(187, 73, NULL, 264, 45, '2019-11-06 03:09:26', '2019-11-06 03:09:26'),
(188, 73, NULL, 265, 45, '2019-11-06 03:09:31', '2019-11-06 03:09:31'),
(189, 73, NULL, 266, 45, '2019-11-06 03:09:35', '2019-11-06 03:09:35'),
(190, 73, NULL, 267, 45, '2019-11-06 03:09:40', '2019-11-06 03:09:40'),
(191, 73, NULL, 647, 46, '2019-11-06 03:10:29', '2019-11-06 03:10:29'),
(192, 73, NULL, 648, 46, '2019-11-06 03:10:31', '2019-11-06 03:10:31'),
(193, 73, NULL, 649, 46, '2019-11-06 03:10:33', '2019-11-06 03:10:33'),
(194, 73, NULL, 650, 46, '2019-11-06 03:10:34', '2019-11-06 03:10:34'),
(195, 73, NULL, 651, 46, '2019-11-06 03:10:35', '2019-11-06 03:10:35'),
(196, 73, NULL, 652, 46, '2019-11-06 03:10:37', '2019-11-06 03:10:37'),
(197, 73, NULL, 653, 46, '2019-11-06 03:10:39', '2019-11-06 03:10:39'),
(198, 73, NULL, 654, 46, '2019-11-06 03:10:41', '2019-11-06 03:10:41'),
(199, 73, NULL, 655, 46, '2019-11-06 03:10:42', '2019-11-06 03:10:42'),
(200, 73, NULL, 656, 46, '2019-11-06 03:10:44', '2019-11-06 03:10:44'),
(201, 73, NULL, 657, 46, '2019-11-06 03:10:45', '2019-11-06 03:10:45'),
(202, 73, NULL, 658, 46, '2019-11-06 03:10:47', '2019-11-06 03:10:47'),
(203, 73, NULL, 659, 46, '2019-11-06 03:10:49', '2019-11-06 03:10:49'),
(204, 73, NULL, 660, 46, '2019-11-06 03:10:51', '2019-11-06 03:10:51'),
(205, 73, NULL, 661, 46, '2019-11-06 03:10:52', '2019-11-06 03:10:52'),
(206, 73, NULL, 662, 46, '2019-11-06 03:10:54', '2019-11-06 03:10:54'),
(207, 73, NULL, 663, 46, '2019-11-06 03:10:56', '2019-11-06 03:10:56'),
(208, 73, NULL, 664, 46, '2019-11-06 03:10:58', '2019-11-06 03:10:58'),
(209, 73, NULL, 665, 46, '2019-11-06 03:10:59', '2019-11-06 03:10:59'),
(210, 73, NULL, 666, 46, '2019-11-06 03:11:01', '2019-11-06 03:11:01'),
(211, 73, NULL, 667, 46, '2019-11-06 03:11:02', '2019-11-06 03:11:02'),
(212, 73, NULL, 668, 46, '2019-11-06 03:11:03', '2019-11-06 03:11:03'),
(213, 73, NULL, 669, 46, '2019-11-06 03:11:05', '2019-11-06 03:11:05'),
(214, 73, NULL, 670, 46, '2019-11-06 03:12:08', '2019-11-06 03:12:08'),
(215, 73, NULL, 268, 47, '2019-11-06 03:13:37', '2019-11-06 03:13:37'),
(216, 73, NULL, 269, 47, '2019-11-06 03:13:44', '2019-11-06 03:13:44'),
(217, 73, NULL, 270, 47, '2019-11-06 03:13:48', '2019-11-06 03:13:48'),
(218, 73, NULL, 271, 47, '2019-11-06 03:13:53', '2019-11-06 03:13:53'),
(219, 73, NULL, 272, 47, '2019-11-06 03:13:57', '2019-11-06 03:13:57'),
(220, 73, NULL, 273, 47, '2019-11-06 03:14:01', '2019-11-06 03:14:01'),
(221, 73, NULL, 274, 47, '2019-11-06 03:14:08', '2019-11-06 03:14:08'),
(222, 73, NULL, 275, 47, '2019-11-06 03:14:23', '2019-11-06 03:14:23'),
(223, 73, NULL, 276, 47, '2019-11-06 03:14:31', '2019-11-06 03:14:31'),
(224, 73, NULL, 277, 47, '2019-11-06 03:14:36', '2019-11-06 03:14:36'),
(225, 73, NULL, 278, 47, '2019-11-06 03:14:44', '2019-11-06 03:14:44'),
(226, 73, NULL, 279, 47, '2019-11-06 03:14:49', '2019-11-06 03:14:49'),
(227, 73, NULL, 280, 47, '2019-11-06 03:14:53', '2019-11-06 03:14:53'),
(228, 73, NULL, 281, 47, '2019-11-06 03:14:57', '2019-11-06 03:14:57'),
(229, 73, NULL, 282, 47, '2019-11-06 03:15:05', '2019-11-06 03:15:05'),
(230, 73, NULL, 283, 47, '2019-11-06 03:15:11', '2019-11-06 03:15:11'),
(231, 73, NULL, 284, 47, '2019-11-06 03:15:17', '2019-11-06 03:15:17'),
(232, 73, NULL, 285, 47, '2019-11-06 03:15:22', '2019-11-06 03:15:22'),
(233, 73, NULL, 505, 48, '2019-11-06 03:16:56', '2019-11-06 03:16:56'),
(234, 73, NULL, 506, 48, '2019-11-06 03:17:20', '2019-11-06 03:17:20'),
(235, 73, NULL, 491, 49, '2019-11-06 03:18:53', '2019-11-06 03:18:53'),
(236, 73, NULL, 490, 49, '2019-11-06 03:18:58', '2019-11-06 03:18:58'),
(237, 73, NULL, 492, 49, '2019-11-06 03:18:59', '2019-11-06 03:18:59'),
(238, 73, NULL, 493, 49, '2019-11-06 03:19:01', '2019-11-06 03:19:01'),
(239, 73, NULL, 494, 49, '2019-11-06 03:19:28', '2019-11-06 03:19:28'),
(240, 73, NULL, 495, 49, '2019-11-06 03:19:28', '2019-11-06 03:19:28'),
(241, 73, NULL, 351, 50, '2019-11-06 03:20:51', '2019-11-06 03:20:51'),
(242, 73, NULL, 467, 51, '2019-11-06 03:21:26', '2019-11-06 03:21:26'),
(243, 73, NULL, 468, 51, '2019-11-06 03:21:30', '2019-11-06 03:21:30'),
(244, 73, NULL, 469, 51, '2019-11-06 03:21:31', '2019-11-06 03:21:31'),
(245, 73, NULL, 470, 51, '2019-11-06 03:21:33', '2019-11-06 03:21:33'),
(246, 73, NULL, 471, 51, '2019-11-06 03:21:35', '2019-11-06 03:21:35'),
(247, 73, NULL, 472, 51, '2019-11-06 03:21:37', '2019-11-06 03:21:37'),
(248, 73, NULL, 473, 51, '2019-11-06 03:21:39', '2019-11-06 03:21:39'),
(249, 73, NULL, 474, 51, '2019-11-06 03:21:41', '2019-11-06 03:21:41'),
(250, 73, NULL, 475, 51, '2019-11-06 03:21:42', '2019-11-06 03:21:42'),
(251, 73, NULL, 476, 51, '2019-11-06 03:21:44', '2019-11-06 03:21:44'),
(252, 73, NULL, 477, 51, '2019-11-06 03:21:45', '2019-11-06 03:21:45'),
(253, 73, NULL, 478, 51, '2019-11-06 03:21:48', '2019-11-06 03:21:48'),
(254, 73, NULL, 479, 51, '2019-11-06 03:21:50', '2019-11-06 03:21:50'),
(255, 73, NULL, 350, 52, '2019-11-06 03:23:03', '2019-11-06 03:23:03'),
(256, 73, NULL, 288, 53, '2019-11-06 03:23:44', '2019-11-06 03:23:44'),
(257, 73, NULL, 289, 53, '2019-11-06 03:23:51', '2019-11-06 03:23:51'),
(258, 73, NULL, 290, 53, '2019-11-06 03:24:00', '2019-11-06 03:24:00'),
(259, 73, NULL, 291, 53, '2019-11-06 03:24:06', '2019-11-06 03:24:06'),
(260, 73, NULL, 292, 53, '2019-11-06 03:24:13', '2019-11-06 03:24:13'),
(261, 73, NULL, 293, 53, '2019-11-06 03:24:54', '2019-11-06 03:24:54'),
(262, 73, NULL, 294, 53, '2019-11-06 03:25:00', '2019-11-06 03:25:00'),
(263, 73, NULL, 295, 53, '2019-11-06 03:25:07', '2019-11-06 03:25:07'),
(264, 73, NULL, 296, 53, '2019-11-06 03:25:13', '2019-11-06 03:25:13'),
(265, 73, NULL, 297, 53, '2019-11-06 03:25:20', '2019-11-06 03:25:20'),
(266, 73, NULL, 298, 53, '2019-11-06 03:25:27', '2019-11-06 03:25:27'),
(267, 73, NULL, 299, 53, '2019-11-06 03:25:33', '2019-11-06 03:25:33'),
(268, 73, NULL, 300, 53, '2019-11-06 03:25:39', '2019-11-06 03:25:39'),
(269, 73, NULL, 480, 54, '2019-11-06 03:26:40', '2019-11-06 03:26:40'),
(270, 73, NULL, 481, 54, '2019-11-06 03:26:50', '2019-11-06 03:26:50'),
(271, 73, NULL, 482, 54, '2019-11-06 03:26:56', '2019-11-06 03:26:56'),
(272, 73, NULL, 483, 54, '2019-11-06 03:27:05', '2019-11-06 03:27:05'),
(273, 73, NULL, 484, 54, '2019-11-06 03:27:12', '2019-11-06 03:27:12'),
(274, 73, NULL, 485, 54, '2019-11-06 03:27:52', '2019-11-06 03:27:52'),
(275, 73, NULL, 486, 54, '2019-11-06 03:28:00', '2019-11-06 03:28:00'),
(276, 73, NULL, 354, 56, '2019-11-06 03:30:20', '2019-11-06 03:30:20'),
(277, 73, NULL, 301, 57, '2019-11-06 03:31:20', '2019-11-06 03:31:20'),
(278, 73, NULL, 302, 57, '2019-11-06 03:31:21', '2019-11-06 03:31:21'),
(279, 73, NULL, 303, 57, '2019-11-06 03:31:23', '2019-11-06 03:31:23'),
(280, 73, NULL, 304, 57, '2019-11-06 03:31:25', '2019-11-06 03:31:25'),
(281, 73, NULL, 305, 57, '2019-11-06 03:31:27', '2019-11-06 03:31:27'),
(282, 73, NULL, 306, 57, '2019-11-06 03:31:30', '2019-11-06 03:31:30'),
(283, 73, NULL, 307, 57, '2019-11-06 03:31:31', '2019-11-06 03:31:31'),
(284, 73, NULL, 308, 57, '2019-11-06 03:31:33', '2019-11-06 03:31:33'),
(285, 73, NULL, 309, 57, '2019-11-06 03:31:34', '2019-11-06 03:31:34'),
(286, 73, NULL, 355, 62, '2019-11-06 03:34:21', '2019-11-06 03:34:21'),
(287, 73, NULL, 356, 63, '2019-11-06 03:35:50', '2019-11-06 03:35:50'),
(288, 73, NULL, 142, 12, '2019-11-06 06:13:51', '2019-11-06 06:13:51'),
(289, 73, NULL, 143, 12, '2019-11-06 06:14:05', '2019-11-06 06:14:05'),
(290, 73, NULL, 144, 12, '2019-11-06 06:14:29', '2019-11-06 06:14:29'),
(291, 73, NULL, 145, 12, '2019-11-06 06:14:38', '2019-11-06 06:14:38'),
(292, 73, NULL, 146, 12, '2019-11-06 06:14:44', '2019-11-06 06:14:44'),
(293, 73, NULL, 147, 12, '2019-11-06 06:14:50', '2019-11-06 06:14:50'),
(294, 73, NULL, 148, 12, '2019-11-06 06:15:17', '2019-11-06 06:15:17'),
(295, 73, NULL, 149, 12, '2019-11-06 06:15:24', '2019-11-06 06:15:24'),
(296, 73, NULL, 150, 12, '2019-11-06 06:15:30', '2019-11-06 06:15:30'),
(297, 73, NULL, 151, 12, '2019-11-06 06:15:36', '2019-11-06 06:15:36'),
(298, 73, NULL, 310, 13, '2019-11-06 06:16:19', '2019-11-06 06:16:19'),
(299, 73, NULL, 311, 13, '2019-11-06 06:16:44', '2019-11-06 06:16:44'),
(300, 73, NULL, 312, 13, '2019-11-06 06:16:51', '2019-11-06 06:16:51'),
(301, 73, NULL, 313, 13, '2019-11-06 06:16:57', '2019-11-06 06:16:57'),
(302, 73, NULL, 314, 13, '2019-11-06 06:17:05', '2019-11-06 06:17:05'),
(303, 73, NULL, 315, 13, '2019-11-06 06:17:15', '2019-11-06 06:17:15'),
(304, 73, NULL, 316, 13, '2019-11-06 06:17:19', '2019-11-06 06:17:19'),
(305, 73, NULL, 317, 13, '2019-11-06 06:17:31', '2019-11-06 06:17:31'),
(306, 73, NULL, 318, 13, '2019-11-06 06:17:55', '2019-11-06 06:17:55'),
(307, 73, NULL, 319, 13, '2019-11-06 06:17:59', '2019-11-06 06:17:59'),
(308, 73, NULL, 357, 14, '2019-11-06 06:20:17', '2019-11-06 06:20:17'),
(309, 73, NULL, 358, 15, '2019-11-06 06:22:29', '2019-11-06 06:22:29'),
(310, 73, NULL, 528, 16, '2019-11-06 06:23:16', '2019-11-06 06:23:16'),
(311, 73, NULL, 152, 17, '2019-11-06 06:24:45', '2019-11-06 06:24:45'),
(312, 73, NULL, 153, 17, '2019-11-06 06:24:50', '2019-11-06 06:24:50'),
(313, 73, NULL, 154, 17, '2019-11-06 06:24:52', '2019-11-06 06:24:52'),
(314, 73, NULL, 155, 17, '2019-11-06 06:24:54', '2019-11-06 06:24:54'),
(315, 73, NULL, 156, 17, '2019-11-06 06:24:56', '2019-11-06 06:24:56'),
(316, 73, NULL, 157, 17, '2019-11-06 06:24:58', '2019-11-06 06:24:58'),
(317, 73, NULL, 158, 17, '2019-11-06 06:25:00', '2019-11-06 06:25:00'),
(318, 73, NULL, 159, 17, '2019-11-06 06:25:04', '2019-11-06 06:25:04'),
(319, 73, NULL, 160, 17, '2019-11-06 06:25:08', '2019-11-06 06:25:08'),
(320, 73, NULL, 161, 17, '2019-11-06 06:25:11', '2019-11-06 06:25:11'),
(321, 73, NULL, 162, 17, '2019-11-06 06:25:14', '2019-11-06 06:25:14'),
(322, 73, NULL, 163, 17, '2019-11-06 06:25:15', '2019-11-06 06:25:15'),
(323, 73, NULL, 164, 17, '2019-11-06 06:25:18', '2019-11-06 06:25:18'),
(324, 73, NULL, 165, 17, '2019-11-06 06:25:20', '2019-11-06 06:25:20'),
(325, 31, NULL, 480, 54, '2019-11-06 15:28:25', '2019-11-06 15:28:25'),
(326, 31, NULL, 481, 54, '2019-11-06 15:28:44', '2019-11-06 15:28:44'),
(327, 31, NULL, 482, 54, '2019-11-06 15:28:51', '2019-11-06 15:28:51'),
(328, 31, NULL, 505, 48, '2019-11-06 15:41:28', '2019-11-06 15:41:28'),
(329, 31, NULL, 506, 48, '2019-11-06 15:41:39', '2019-11-06 15:41:39'),
(330, 31, NULL, 507, 48, '2019-11-06 17:22:26', '2019-11-06 17:22:26'),
(331, 31, NULL, 483, 54, '2019-11-06 17:36:44', '2019-11-06 17:36:44'),
(332, 31, NULL, 484, 54, '2019-11-06 17:36:47', '2019-11-06 17:36:47'),
(333, 31, NULL, 485, 54, '2019-11-06 17:36:50', '2019-11-06 17:36:50'),
(334, 31, NULL, 486, 54, '2019-11-06 17:36:54', '2019-11-06 17:36:54'),
(335, 31, NULL, 73, 5, '2019-11-06 17:40:36', '2019-11-06 17:40:36'),
(336, 31, NULL, 552, 1, '2019-11-06 17:57:14', '2019-11-06 17:57:14'),
(337, 31, NULL, 557, 1, '2019-11-06 19:31:04', '2019-11-06 19:31:04'),
(338, 31, NULL, 558, 1, '2019-11-06 20:11:27', '2019-11-06 20:11:27'),
(339, 66, 64, 73, 5, '2019-11-06 21:07:28', '2019-11-06 21:07:28'),
(340, 66, 64, 74, 5, '2019-11-06 21:07:39', '2019-11-06 21:07:39'),
(341, 66, 64, 327, 59, '2019-11-06 21:17:40', '2019-11-06 21:17:40'),
(342, 73, NULL, 166, 17, '2019-11-07 02:50:14', '2019-11-07 02:50:14'),
(343, 73, NULL, 167, 18, '2019-11-07 03:01:39', '2019-11-07 03:01:39'),
(344, 73, NULL, 168, 18, '2019-11-07 03:01:48', '2019-11-07 03:01:48'),
(345, 73, NULL, 169, 18, '2019-11-07 03:01:54', '2019-11-07 03:01:54'),
(346, 73, NULL, 170, 18, '2019-11-07 03:02:01', '2019-11-07 03:02:01'),
(347, 73, NULL, 171, 18, '2019-11-07 03:02:07', '2019-11-07 03:02:07'),
(348, 73, NULL, 365, 19, '2019-11-07 03:03:57', '2019-11-07 03:03:57'),
(349, 73, NULL, 366, 19, '2019-11-07 03:04:05', '2019-11-07 03:04:05'),
(350, 73, NULL, 367, 19, '2019-11-07 03:04:12', '2019-11-07 03:04:12'),
(351, 73, NULL, 368, 19, '2019-11-07 03:04:22', '2019-11-07 03:04:22'),
(352, 73, NULL, 369, 19, '2019-11-07 03:04:26', '2019-11-07 03:04:26'),
(353, 73, NULL, 370, 19, '2019-11-07 03:04:35', '2019-11-07 03:04:35'),
(354, 73, NULL, 371, 19, '2019-11-07 03:04:44', '2019-11-07 03:04:44'),
(355, 73, NULL, 372, 19, '2019-11-07 03:04:50', '2019-11-07 03:04:50'),
(356, 73, NULL, 373, 19, '2019-11-07 03:04:59', '2019-11-07 03:04:59'),
(357, 73, NULL, 374, 19, '2019-11-07 03:05:05', '2019-11-07 03:05:05'),
(358, 73, NULL, 375, 20, '2019-11-07 03:10:23', '2019-11-07 03:10:23'),
(359, 73, NULL, 376, 20, '2019-11-07 03:10:29', '2019-11-07 03:10:29'),
(360, 73, NULL, 377, 20, '2019-11-07 03:10:32', '2019-11-07 03:10:32'),
(361, 73, NULL, 378, 20, '2019-11-07 03:22:21', '2019-11-07 03:22:21'),
(362, 73, NULL, 379, 20, '2019-11-07 03:22:38', '2019-11-07 03:22:38'),
(363, 73, NULL, 409, 20, '2019-11-07 03:23:09', '2019-11-07 03:23:09'),
(364, 73, NULL, 410, 20, '2019-11-07 03:24:23', '2019-11-07 03:24:23'),
(365, 73, NULL, 411, 20, '2019-11-07 03:24:27', '2019-11-07 03:24:27'),
(366, 73, NULL, 412, 20, '2019-11-07 03:24:28', '2019-11-07 03:24:28'),
(367, 73, NULL, 413, 20, '2019-11-07 03:24:29', '2019-11-07 03:24:29'),
(368, 73, NULL, 414, 30, '2019-11-07 03:27:24', '2019-11-07 03:27:24'),
(369, 73, NULL, 415, 30, '2019-11-07 03:27:28', '2019-11-07 03:27:28'),
(370, 73, NULL, 416, 30, '2019-11-07 03:27:33', '2019-11-07 03:27:33'),
(371, 73, NULL, 417, 30, '2019-11-07 03:27:36', '2019-11-07 03:27:36'),
(372, 73, NULL, 418, 30, '2019-11-07 03:27:39', '2019-11-07 03:27:39'),
(373, 73, NULL, 543, 21, '2019-11-07 03:40:03', '2019-11-07 03:40:03'),
(374, 65, NULL, 543, 21, '2019-11-07 03:43:20', '2019-11-07 03:43:20'),
(375, 65, NULL, 544, 21, '2019-11-07 03:43:44', '2019-11-07 03:43:44'),
(376, 65, NULL, 545, 21, '2019-11-07 03:43:51', '2019-11-07 03:43:51'),
(377, 73, NULL, 546, 21, '2019-11-07 06:27:51', '2019-11-07 06:27:51'),
(378, 73, NULL, 547, 21, '2019-11-07 06:28:36', '2019-11-07 06:28:36'),
(379, 73, NULL, 548, 21, '2019-11-07 06:28:42', '2019-11-07 06:28:42'),
(380, 73, NULL, 544, 21, '2019-11-07 06:29:02', '2019-11-07 06:29:02'),
(381, 73, NULL, 545, 21, '2019-11-07 06:29:04', '2019-11-07 06:29:04'),
(382, 73, NULL, 359, 22, '2019-11-07 06:31:57', '2019-11-07 06:31:57'),
(383, 73, NULL, 542, 23, '2019-11-07 06:32:27', '2019-11-07 06:32:27'),
(384, 73, NULL, 461, 24, '2019-11-07 06:35:43', '2019-11-07 06:35:43'),
(385, 73, NULL, 462, 24, '2019-11-07 06:36:19', '2019-11-07 06:36:19'),
(386, 73, NULL, 463, 24, '2019-11-07 06:37:17', '2019-11-07 06:37:17'),
(387, 73, NULL, 464, 24, '2019-11-07 06:37:23', '2019-11-07 06:37:23'),
(388, 73, NULL, 465, 24, '2019-11-07 06:37:32', '2019-11-07 06:37:32'),
(389, 66, NULL, 461, 24, '2019-11-07 06:39:03', '2019-11-07 06:39:03'),
(390, 66, NULL, 462, 24, '2019-11-07 06:39:20', '2019-11-07 06:39:20'),
(391, 66, NULL, 463, 24, '2019-11-07 06:39:30', '2019-11-07 06:39:30'),
(392, 66, NULL, 464, 24, '2019-11-07 06:39:43', '2019-11-07 06:39:43'),
(393, 66, NULL, 465, 24, '2019-11-07 06:39:58', '2019-11-07 06:39:58'),
(394, 66, NULL, 466, 24, '2019-11-07 06:40:35', '2019-11-07 06:40:35'),
(395, 73, NULL, 380, 25, '2019-11-07 06:42:17', '2019-11-07 06:42:17'),
(396, 73, NULL, 381, 25, '2019-11-07 06:42:21', '2019-11-07 06:42:21'),
(397, 73, NULL, 382, 25, '2019-11-07 06:42:27', '2019-11-07 06:42:27'),
(398, 73, NULL, 383, 25, '2019-11-07 06:42:29', '2019-11-07 06:42:29'),
(399, 73, NULL, 384, 25, '2019-11-07 06:42:31', '2019-11-07 06:42:31'),
(400, 73, NULL, 385, 25, '2019-11-07 06:42:34', '2019-11-07 06:42:34'),
(401, 73, NULL, 386, 25, '2019-11-07 06:42:36', '2019-11-07 06:42:36'),
(402, 73, NULL, 387, 25, '2019-11-07 06:42:39', '2019-11-07 06:42:39'),
(403, 73, NULL, 388, 25, '2019-11-07 06:42:40', '2019-11-07 06:42:40'),
(404, 73, NULL, 389, 25, '2019-11-07 06:42:43', '2019-11-07 06:42:43'),
(405, 73, NULL, 390, 25, '2019-11-07 06:42:44', '2019-11-07 06:42:44'),
(406, 73, NULL, 391, 25, '2019-11-07 06:42:46', '2019-11-07 06:42:46'),
(407, 73, NULL, 392, 25, '2019-11-07 06:42:48', '2019-11-07 06:42:48'),
(408, 73, NULL, 393, 25, '2019-11-07 06:42:51', '2019-11-07 06:42:51'),
(409, 73, NULL, 172, 26, '2019-11-07 06:43:54', '2019-11-07 06:43:54'),
(410, 73, NULL, 173, 26, '2019-11-07 06:44:08', '2019-11-07 06:44:08'),
(411, 73, NULL, 174, 26, '2019-11-07 06:44:17', '2019-11-07 06:44:17'),
(412, 73, NULL, 175, 26, '2019-11-07 06:44:35', '2019-11-07 06:44:35'),
(413, 73, NULL, 176, 26, '2019-11-07 06:45:01', '2019-11-07 06:45:01'),
(414, 73, NULL, 177, 26, '2019-11-07 06:45:09', '2019-11-07 06:45:09'),
(415, 73, NULL, 178, 26, '2019-11-07 06:45:19', '2019-11-07 06:45:19'),
(416, 73, NULL, 179, 26, '2019-11-07 06:45:33', '2019-11-07 06:45:33'),
(417, 73, NULL, 180, 26, '2019-11-07 06:45:58', '2019-11-07 06:45:58'),
(418, 73, NULL, 181, 26, '2019-11-07 06:46:13', '2019-11-07 06:46:13'),
(419, 73, NULL, 360, 28, '2019-11-07 06:49:21', '2019-11-07 06:49:21'),
(420, 73, NULL, 361, 29, '2019-11-07 06:50:55', '2019-11-07 06:50:55'),
(421, 73, NULL, 182, 31, '2019-11-07 06:53:05', '2019-11-07 06:53:05'),
(422, 73, NULL, 183, 31, '2019-11-07 06:53:10', '2019-11-07 06:53:10'),
(423, 73, NULL, 184, 31, '2019-11-07 06:53:14', '2019-11-07 06:53:14'),
(424, 73, NULL, 185, 31, '2019-11-07 06:53:18', '2019-11-07 06:53:18'),
(425, 73, NULL, 186, 31, '2019-11-07 06:53:24', '2019-11-07 06:53:24'),
(426, 73, NULL, 187, 31, '2019-11-07 06:53:33', '2019-11-07 06:53:33'),
(427, 73, NULL, 188, 31, '2019-11-07 06:53:38', '2019-11-07 06:53:38'),
(428, 73, NULL, 189, 31, '2019-11-07 06:53:42', '2019-11-07 06:53:42'),
(429, 73, NULL, 190, 31, '2019-11-07 06:53:47', '2019-11-07 06:53:47'),
(430, 73, NULL, 191, 31, '2019-11-07 06:53:51', '2019-11-07 06:53:51'),
(431, 73, NULL, 192, 31, '2019-11-07 06:53:56', '2019-11-07 06:53:56'),
(432, 73, NULL, 193, 31, '2019-11-07 06:53:59', '2019-11-07 06:53:59'),
(433, 73, NULL, 194, 31, '2019-11-07 06:54:03', '2019-11-07 06:54:03'),
(434, 73, NULL, 195, 31, '2019-11-07 06:54:07', '2019-11-07 06:54:07'),
(435, 73, NULL, 196, 31, '2019-11-07 06:54:12', '2019-11-07 06:54:12'),
(436, 73, NULL, 197, 31, '2019-11-07 06:54:38', '2019-11-07 06:54:38'),
(437, 73, NULL, 198, 31, '2019-11-07 06:54:44', '2019-11-07 06:54:44'),
(438, 73, NULL, 199, 31, '2019-11-07 06:54:48', '2019-11-07 06:54:48'),
(439, 73, NULL, 200, 31, '2019-11-07 06:54:51', '2019-11-07 06:54:51'),
(440, 73, NULL, 201, 31, '2019-11-07 06:54:56', '2019-11-07 06:54:56'),
(441, 73, NULL, 202, 31, '2019-11-07 06:55:00', '2019-11-07 06:55:00'),
(442, 73, NULL, 203, 31, '2019-11-07 06:55:04', '2019-11-07 06:55:04'),
(443, 73, NULL, 204, 31, '2019-11-07 06:55:09', '2019-11-07 06:55:09'),
(444, 73, NULL, 205, 31, '2019-11-07 06:55:13', '2019-11-07 06:55:13'),
(445, 73, NULL, 206, 31, '2019-11-07 06:55:30', '2019-11-07 06:55:30'),
(446, 73, NULL, 207, 31, '2019-11-07 06:55:35', '2019-11-07 06:55:35'),
(447, 73, NULL, 394, 32, '2019-11-07 06:56:29', '2019-11-07 06:56:29'),
(448, 73, NULL, 395, 32, '2019-11-07 06:56:34', '2019-11-07 06:56:34'),
(449, 73, NULL, 396, 32, '2019-11-07 06:56:38', '2019-11-07 06:56:38'),
(450, 73, NULL, 397, 32, '2019-11-07 06:56:41', '2019-11-07 06:56:41'),
(451, 73, NULL, 398, 32, '2019-11-07 06:56:45', '2019-11-07 06:56:45'),
(452, 73, NULL, 399, 32, '2019-11-07 06:56:50', '2019-11-07 06:56:50'),
(453, 73, NULL, 400, 32, '2019-11-07 06:56:53', '2019-11-07 06:56:53'),
(454, 73, NULL, 401, 32, '2019-11-07 06:56:59', '2019-11-07 06:56:59'),
(455, 73, NULL, 402, 32, '2019-11-07 06:57:08', '2019-11-07 06:57:08'),
(456, 73, NULL, 403, 32, '2019-11-07 06:57:10', '2019-11-07 06:57:10'),
(457, 73, NULL, 404, 32, '2019-11-07 06:57:14', '2019-11-07 06:57:14'),
(458, 73, NULL, 405, 32, '2019-11-07 06:57:18', '2019-11-07 06:57:18'),
(459, 73, NULL, 406, 32, '2019-11-07 06:57:21', '2019-11-07 06:57:21'),
(460, 73, NULL, 407, 32, '2019-11-07 06:57:26', '2019-11-07 06:57:26'),
(461, 73, NULL, 408, 32, '2019-11-07 06:57:31', '2019-11-07 06:57:31'),
(462, 73, NULL, 208, 33, '2019-11-07 06:58:10', '2019-11-07 06:58:10'),
(463, 73, NULL, 209, 33, '2019-11-07 06:58:16', '2019-11-07 06:58:16'),
(464, 73, NULL, 210, 33, '2019-11-07 06:58:20', '2019-11-07 06:58:20'),
(465, 73, NULL, 211, 33, '2019-11-07 06:58:24', '2019-11-07 06:58:24'),
(466, 73, NULL, 212, 33, '2019-11-07 06:58:30', '2019-11-07 06:58:30'),
(467, 73, NULL, 213, 33, '2019-11-07 06:58:35', '2019-11-07 06:58:35'),
(468, 73, NULL, 214, 33, '2019-11-07 06:58:39', '2019-11-07 06:58:39'),
(469, 73, NULL, 215, 33, '2019-11-07 06:58:50', '2019-11-07 06:58:50'),
(470, 73, NULL, 216, 33, '2019-11-07 06:58:54', '2019-11-07 06:58:54'),
(471, 73, NULL, 217, 33, '2019-11-07 06:58:58', '2019-11-07 06:58:58'),
(472, 73, NULL, 218, 33, '2019-11-07 06:59:02', '2019-11-07 06:59:02'),
(473, 73, NULL, 219, 33, '2019-11-07 06:59:07', '2019-11-07 06:59:07'),
(474, 66, NULL, 208, 33, '2019-11-07 06:59:53', '2019-11-07 06:59:53'),
(475, 73, NULL, 362, 34, '2019-11-07 07:12:20', '2019-11-07 07:12:20'),
(476, 73, NULL, 420, 35, '2019-11-07 07:14:34', '2019-11-07 07:14:34'),
(477, 73, NULL, 421, 35, '2019-11-07 07:15:44', '2019-11-07 07:15:44'),
(478, 73, NULL, 422, 35, '2019-11-07 07:15:51', '2019-11-07 07:15:51'),
(479, 73, NULL, 423, 35, '2019-11-07 07:16:18', '2019-11-07 07:16:18'),
(480, 66, NULL, 420, 35, '2019-11-07 07:17:01', '2019-11-07 07:17:01'),
(481, 66, NULL, 421, 35, '2019-11-07 07:17:08', '2019-11-07 07:17:08'),
(482, 66, NULL, 422, 35, '2019-11-07 07:17:14', '2019-11-07 07:17:14'),
(483, 66, NULL, 423, 35, '2019-11-07 07:17:25', '2019-11-07 07:17:25'),
(484, 66, NULL, 424, 35, '2019-11-07 07:17:37', '2019-11-07 07:17:37'),
(485, 66, NULL, 425, 35, '2019-11-07 07:17:45', '2019-11-07 07:17:45'),
(486, 66, NULL, 439, 39, '2019-11-07 07:19:55', '2019-11-07 07:19:55'),
(487, 66, NULL, 440, 39, '2019-11-07 07:20:09', '2019-11-07 07:20:09'),
(488, 66, NULL, 441, 39, '2019-11-07 07:20:13', '2019-11-07 07:20:13'),
(489, 66, NULL, 442, 39, '2019-11-07 07:20:20', '2019-11-07 07:20:20'),
(490, 66, NULL, 443, 39, '2019-11-07 07:20:26', '2019-11-07 07:20:26'),
(491, 66, NULL, 444, 39, '2019-11-07 07:20:31', '2019-11-07 07:20:31'),
(492, 66, NULL, 445, 39, '2019-11-07 07:20:39', '2019-11-07 07:20:39'),
(493, 66, NULL, 446, 39, '2019-11-07 07:21:32', '2019-11-07 07:21:32'),
(494, 66, NULL, 447, 39, '2019-11-07 07:21:39', '2019-11-07 07:21:39'),
(495, 66, NULL, 448, 39, '2019-11-07 07:22:10', '2019-11-07 07:22:10'),
(496, 66, NULL, 363, 36, '2019-11-07 07:24:57', '2019-11-07 07:24:57'),
(497, 66, NULL, 364, 37, '2019-11-07 07:25:54', '2019-11-07 07:25:54'),
(498, 66, NULL, 426, 38, '2019-11-07 07:28:57', '2019-11-07 07:28:57'),
(499, 66, NULL, 427, 38, '2019-11-07 07:29:02', '2019-11-07 07:29:02'),
(500, 66, NULL, 428, 38, '2019-11-07 07:29:04', '2019-11-07 07:29:04'),
(501, 66, NULL, 429, 38, '2019-11-07 07:29:07', '2019-11-07 07:29:07'),
(502, 66, NULL, 430, 38, '2019-11-07 07:29:10', '2019-11-07 07:29:10'),
(503, 66, NULL, 431, 38, '2019-11-07 07:29:14', '2019-11-07 07:29:14'),
(504, 66, NULL, 432, 38, '2019-11-07 07:29:17', '2019-11-07 07:29:17'),
(505, 66, NULL, 433, 38, '2019-11-07 07:29:19', '2019-11-07 07:29:19'),
(506, 66, NULL, 434, 38, '2019-11-07 07:29:23', '2019-11-07 07:29:23'),
(507, 66, NULL, 435, 38, '2019-11-07 07:29:26', '2019-11-07 07:29:26'),
(508, 66, NULL, 436, 38, '2019-11-07 07:29:42', '2019-11-07 07:29:42'),
(509, 66, NULL, 437, 38, '2019-11-07 07:29:45', '2019-11-07 07:29:45'),
(510, 66, NULL, 438, 38, '2019-11-07 07:29:49', '2019-11-07 07:29:49'),
(511, 66, NULL, 451, 40, '2019-11-07 07:30:46', '2019-11-07 07:30:46'),
(512, 66, NULL, 452, 40, '2019-11-07 07:31:57', '2019-11-07 07:31:57'),
(513, 66, NULL, 453, 40, '2019-11-07 07:32:05', '2019-11-07 07:32:05'),
(514, 66, NULL, 454, 40, '2019-11-07 07:32:10', '2019-11-07 07:32:10'),
(515, 66, NULL, 455, 40, '2019-11-07 07:32:17', '2019-11-07 07:32:17'),
(516, 66, NULL, 456, 40, '2019-11-07 07:32:24', '2019-11-07 07:32:24'),
(517, 66, NULL, 457, 40, '2019-11-07 07:32:29', '2019-11-07 07:32:29'),
(518, 66, NULL, 458, 40, '2019-11-07 07:32:37', '2019-11-07 07:32:37'),
(519, 66, NULL, 459, 40, '2019-11-07 07:32:42', '2019-11-07 07:32:42'),
(520, 66, NULL, 460, 40, '2019-11-07 07:33:21', '2019-11-07 07:33:21'),
(521, 66, NULL, 221, 41, '2019-11-07 07:34:10', '2019-11-07 07:34:10'),
(522, 66, NULL, 222, 41, '2019-11-07 07:34:12', '2019-11-07 07:34:12'),
(523, 66, NULL, 223, 41, '2019-11-07 07:34:13', '2019-11-07 07:34:13'),
(524, 66, NULL, 224, 41, '2019-11-07 07:34:15', '2019-11-07 07:34:15'),
(525, 66, NULL, 225, 41, '2019-11-07 07:34:17', '2019-11-07 07:34:17'),
(526, 66, NULL, 226, 41, '2019-11-07 07:34:18', '2019-11-07 07:34:18'),
(527, 66, NULL, 227, 41, '2019-11-07 07:34:20', '2019-11-07 07:34:20'),
(528, 66, NULL, 228, 41, '2019-11-07 07:34:21', '2019-11-07 07:34:21'),
(529, 66, NULL, 229, 41, '2019-11-07 07:34:23', '2019-11-07 07:34:23'),
(530, 66, NULL, 230, 41, '2019-11-07 07:34:25', '2019-11-07 07:34:25'),
(531, 66, NULL, 231, 41, '2019-11-07 07:34:27', '2019-11-07 07:34:27'),
(532, 66, NULL, 232, 41, '2019-11-07 07:34:29', '2019-11-07 07:34:29'),
(533, 66, NULL, 233, 41, '2019-11-07 07:34:30', '2019-11-07 07:34:30'),
(534, 66, NULL, 234, 41, '2019-11-07 07:34:32', '2019-11-07 07:34:32'),
(535, 66, NULL, 235, 41, '2019-11-07 07:34:34', '2019-11-07 07:34:34'),
(536, 66, NULL, 236, 41, '2019-11-07 07:34:35', '2019-11-07 07:34:35'),
(537, 66, NULL, 237, 41, '2019-11-07 07:34:37', '2019-11-07 07:34:37'),
(538, 66, NULL, 238, 41, '2019-11-07 07:34:38', '2019-11-07 07:34:38'),
(539, 66, NULL, 239, 41, '2019-11-07 07:34:39', '2019-11-07 07:34:39'),
(540, 66, NULL, 240, 41, '2019-11-07 07:34:40', '2019-11-07 07:34:40'),
(541, 66, NULL, 241, 41, '2019-11-07 07:34:42', '2019-11-07 07:34:42'),
(542, 73, NULL, 327, 59, '2019-11-07 07:43:18', '2019-11-07 07:43:18'),
(543, 73, NULL, 328, 59, '2019-11-07 07:43:44', '2019-11-07 07:43:44'),
(544, 73, NULL, 332, 60, '2019-11-07 08:38:01', '2019-11-07 08:38:01'),
(545, 73, NULL, 333, 60, '2019-11-07 08:38:16', '2019-11-07 08:38:16'),
(546, 73, NULL, 334, 60, '2019-11-07 08:38:29', '2019-11-07 08:38:29'),
(547, 73, NULL, 335, 60, '2019-11-07 08:38:44', '2019-11-07 08:38:44'),
(548, 65, NULL, 142, 12, '2019-11-07 08:44:43', '2019-11-07 08:44:43'),
(549, 65, NULL, 143, 12, '2019-11-07 08:44:48', '2019-11-07 08:44:48'),
(550, 65, NULL, 144, 12, '2019-11-07 08:44:57', '2019-11-07 08:44:57'),
(551, 65, NULL, 528, 16, '2019-11-07 08:47:18', '2019-11-07 08:47:18'),
(552, 65, NULL, 529, 16, '2019-11-07 08:48:26', '2019-11-07 08:48:26'),
(553, 65, NULL, 530, 16, '2019-11-07 08:48:40', '2019-11-07 08:48:40'),
(554, 65, NULL, 531, 16, '2019-11-07 08:48:47', '2019-11-07 08:48:47'),
(555, 65, NULL, 532, 16, '2019-11-07 08:48:50', '2019-11-07 08:48:50'),
(556, 65, NULL, 533, 16, '2019-11-07 08:48:56', '2019-11-07 08:48:56'),
(557, 65, NULL, 534, 16, '2019-11-07 08:49:04', '2019-11-07 08:49:04'),
(558, 65, NULL, 535, 16, '2019-11-07 08:50:41', '2019-11-07 08:50:41'),
(559, 65, NULL, 536, 16, '2019-11-07 08:51:31', '2019-11-07 08:51:31'),
(560, 65, NULL, 537, 16, '2019-11-07 08:51:38', '2019-11-07 08:51:38'),
(561, 65, NULL, 538, 16, '2019-11-07 08:51:44', '2019-11-07 08:51:44'),
(562, 65, NULL, 539, 16, '2019-11-07 08:51:53', '2019-11-07 08:51:53'),
(563, 65, NULL, 540, 16, '2019-11-07 08:51:58', '2019-11-07 08:51:58'),
(564, 65, NULL, 541, 16, '2019-11-07 08:52:05', '2019-11-07 08:52:05'),
(565, 31, NULL, 327, 59, '2019-11-07 14:33:55', '2019-11-07 14:33:55'),
(566, 66, 64, 3, 1, '2019-11-07 09:08:05', '2019-11-07 09:08:05'),
(567, 66, 64, 4, 1, '2019-11-07 09:08:06', '2019-11-07 09:08:06'),
(568, 66, 64, 31, 1, '2019-11-07 09:08:07', '2019-11-07 09:08:07'),
(569, 66, 64, 29, 1, '2019-11-07 09:08:07', '2019-11-07 09:08:07'),
(570, 66, 64, 28, 1, '2019-11-07 09:08:08', '2019-11-07 09:08:08'),
(571, 66, 64, 27, 1, '2019-11-07 09:08:15', '2019-11-07 09:08:15'),
(572, 66, 64, 30, 1, '2019-11-07 09:08:16', '2019-11-07 09:08:16'),
(573, 66, 64, 26, 1, '2019-11-07 09:08:17', '2019-11-07 09:08:17'),
(574, 66, 64, 32, 1, '2019-11-07 09:08:18', '2019-11-07 09:08:18'),
(575, 66, 64, 33, 1, '2019-11-07 09:08:18', '2019-11-07 09:08:18'),
(576, 66, 64, 357, 14, '2019-11-07 09:09:03', '2019-11-07 09:09:03'),
(577, 66, 64, 142, 12, '2019-11-07 09:09:13', '2019-11-07 09:09:13'),
(578, 66, 64, 143, 12, '2019-11-07 09:09:18', '2019-11-07 09:09:18'),
(579, 66, 64, 144, 12, '2019-11-07 09:09:21', '2019-11-07 09:09:21'),
(580, 66, 64, 145, 12, '2019-11-07 09:09:30', '2019-11-07 09:09:30'),
(581, 66, 64, 146, 12, '2019-11-07 09:09:34', '2019-11-07 09:09:34'),
(582, 66, 64, 147, 12, '2019-11-07 09:09:37', '2019-11-07 09:09:37'),
(583, 66, 64, 148, 12, '2019-11-07 09:09:45', '2019-11-07 09:09:45'),
(584, 66, 64, 149, 12, '2019-11-07 09:09:58', '2019-11-07 09:09:58'),
(585, 66, 64, 150, 12, '2019-11-07 09:10:02', '2019-11-07 09:10:02'),
(586, 66, 64, 151, 12, '2019-11-07 09:10:05', '2019-11-07 09:10:05');

-- --------------------------------------------------------

--
-- Table structure for table `student_tempo_trainers`
--

CREATE TABLE `student_tempo_trainers` (
  `id` int(11) NOT NULL,
  `sub_level_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `beep_time` text,
  `keystrock_time` text,
  `ear_type` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `class_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `class_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'English', '1', '2019-09-03 05:04:04', NULL),
(2, '1', 'Math', '1', '2019-09-03 05:04:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_levels`
--

CREATE TABLE `sub_levels` (
  `id` int(11) NOT NULL,
  `level_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext NOT NULL,
  `audio` text NOT NULL,
  `status` enum('1','0') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_levels`
--

INSERT INTO `sub_levels` (`id`, `level_id`, `title`, `description`, `audio`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Level 1.1 Introduction Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer voice says sound of symbol while Student looks at the screen.</p>\r\n\r\n<p>2. Student repeats sound and Trainer presses NEXT to go to the next symbol.</p>\r\n\r\n<p>3. Trainer will have Student repeat the exercise multiple times with Trainer voice.</p>\r\n\r\n<p>4. Trainer can repeat activity and have Student say the sound without verbal prompt.</p>', '1572959142_Asilomar .wav', '1', '2019-11-05 13:05:42', '2019-11-05 13:05:42'),
(2, 1, 'Level 1.2 Voice Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer voice says sound of one symbol.</p>\r\n\r\n<p>2. Student repeats sound and chooses symbol on screen which highlights.</p>\r\n\r\n<p>3. Trainer indicates &ndash; correct /NEXT or incorrect/REPEAT and DONE when task is complete</p>', '1571894265_At Peace.wav', '1', '2019-11-05 13:06:56', '2019-11-05 13:06:56'),
(3, 1, 'Level 1.3 Intro Task combining sounds', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer voice says sound of blend while Student looks at the screen.</p>\r\n\r\n<p>2. Student repeats sound.</p>\r\n\r\n<p>3. Trainer will have Student repeat the exercise multiple times with Trainer voice.</p>\r\n\r\n<p>4. Trainer may have Student say the sound without verbal prompt.</p>\r\n\r\n<p>5. Trainer chooses NEXT to go to next item and DONE when task is complete.</p>', '1571894478_High Tide.wav', '1', '2019-11-05 13:07:16', '2019-11-05 13:07:16'),
(4, 1, 'Level 1.4 Voice Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer voice says syllable.</p>\r\n\r\n<p>2. Student repeats and chooses correct syllable on the screen and item highlights.</p>\r\n\r\n<p>3. Trainer indicates &ndash; correct/NEXT or incorrect/REPEAT and DONE when task is complete.</p>', '1571894581_Midnight Imagination.wav', '1', '2019-11-05 13:07:32', '2019-11-05 13:07:32'),
(5, 1, 'Level 1.5 Intro Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer voice says sound of blend while Student looks at the screen.</p>\r\n\r\n<p>2. Student repeats sound and chooses screen and item highlights.</p>\r\n\r\n<p>3. Trainer will have Student repeat the exercise multiple times with Trainer voice.</p>\r\n\r\n<p>4. Trainer may have Student say the sound without verbal prompt and choose DONE when task is complete.</p>', '1571894689_My Guru.wav', '1', '2019-11-05 13:08:00', '2019-11-05 13:08:00'),
(6, 1, 'Level 1.6.a Voice Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer says one of the syllables.</p>\r\n\r\n<p>2. Student repeats chooses correct syllable on the screen which highlights.</p>\r\n\r\n<p>3. Trainer indicates &ndash; correct/NEXT or incorrect/REPEAT and DONE when task is complete</p>', '1571894771_My Joyful Soul.wav', '1', '2019-11-05 13:08:14', '2019-11-05 13:08:14'),
(7, 1, 'Level 1.6.b Voice Task A', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer says one syllable.</p>\r\n\r\n<p>2. Student&nbsp;repeats and chooses correct syllable on the screen and it highlights.</p>\r\n\r\n<p>3. Trainer indicates &ndash; correct/NEXT or incorrect/REPEAT and DONE when task is complete.</p>', '1571894876_Not All Is Lost.wav', '1', '2019-11-05 13:08:30', '2019-11-05 13:08:30'),
(8, 1, 'Level 1.8 Flash Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Student&nbsp;says syllable to blank screen</p>\r\n\r\n<p>2. Trainer presses NEXT and three choices appear on screen and Student&nbsp; chooses the syllable that was flashed and item highlights.</p>\r\n\r\n<p>3. Trainer indicates -correct/NEXT or incorrect/REPEAT task and DONE when task is complete.</p>', '1571894981_Ocean Meditation.wav', '1', '2019-11-05 13:09:02', '2019-11-05 13:09:02'),
(9, 1, 'Level 1.9 Attention Task', '<p>The pacing of the flash or beat default is 2 seconds.&nbsp;&nbsp; The numeral or letter displayed for the item is followed by a number of boxes which are shown at the 2 second tempo.&nbsp; The student counts the boxes as they appear and then when a ___ (line)appears, Student says and drag/drop the number or letter that would have been next, from the items at the bottom of screen to the line. This task trains Student to pay attention to the information on the screen and record information rapidly. &nbsp;&nbsp;A numeral or letter either flashes on the screen or Trainer&nbsp;says number or letter.&nbsp; Student repeats what he saw/heard.&nbsp;&nbsp; Trainer presses the START button.&nbsp; Then Student sees (flashes of box symbols&nbsp;&nbsp;&nbsp; &nbsp;visual), hears &nbsp;beats (sound from metronome). &nbsp;&nbsp;Student counts the number of boxes or beats that occur after the original symbol.</p>\r\n\r\n<p>1. A numeral or letter flashes on the screen, or Trainer says a number or letter.&nbsp; Student says the item and Trainer presses start.&nbsp; The item is followed by a series of either boxes or the sound of a beat.</p>\r\n\r\n<p>2. Student counts or says alphabet letters to each of the boxes or beats until a ____ (line) appears and symbols (letters/numbers) appear at bottom of screen.</p>\r\n\r\n<p>3. Student says numeral or letter that would come next and drags/drops the number/letter on the line.</p>\r\n\r\n<p>4.&nbsp; Trainer indicates -correct NEXT or incorrect REPEAT.</p>', '1571895162_Sedona Whisper.wav', '1', '2019-10-30 08:27:14', '2019-10-30 08:27:14'),
(10, 1, 'Level 1.10 Voice Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer says a syllable/word.</p>\r\n\r\n<p>2. Student repeats syllable/word and touches item on screen and it highlights.</p>\r\n\r\n<p>3. Trainer chooses to go to next item &ndash; NEXT or repeat item REPEAT and DONE when task is complete.</p>', '1571895935_Tender Love.wav', '1', '2019-11-05 13:09:27', '2019-11-05 13:09:27'),
(11, 1, 'Level 1.11 Flash Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Student says syllable to blank screen.</p>\r\n\r\n<p>2. Trainer presses START button and 3 choices appear.</p>\r\n\r\n<p>3. Student chooses item Student heard and it highlights on the screen.</p>\r\n\r\n<p>4. Trainer chooses to go to next item &ndash; NEXT or repeat item &ndash; REPEAT and DONE when task is complete.</p>', '1571896030_Asilomar .wav', '1', '2019-11-05 13:09:47', '2019-11-05 13:09:47'),
(12, 2, 'Level 2.1 Flash Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Student says phrase to a blank screen.</p>\r\n\r\n<p>2. Trainer chooses REPEAT to re-do task or NEXT to go to next item and DONE when task is complete. .</p>', '1571898344_Asilomar .wav', '1', '2019-11-05 13:10:01', '2019-11-05 13:10:01'),
(66, 11, 'Level 5.3:  Language Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. &nbsp;Begin the task by activating the music program.&nbsp;</p>\r\n\r\n<p>2. &nbsp;Trainer will use the words in the Trainer&rsquo;s Manual. &nbsp;This a totally oral task. &nbsp;No reading.&nbsp;</p>\r\n\r\n<p>3. &nbsp;Trainer will say a simple sentence and Student will repeat it back.</p>\r\n\r\n<p>4. &nbsp; If Student has difficulty with remembering the sentence, Trainer will break it into phrases and guide Student in repeating it in phrases as suggested in the Trainer&rsquo;s Manual. &nbsp;&nbsp;</p>\r\n\r\n<p>5. &nbsp;When task is complete, Trainer will press DONE to complete the activity&nbsp;<br />\r\n&nbsp;<br />\r\n&nbsp;</p>', '1573102258_Not All Is Lost.wav', '1', '2019-11-07 04:50:58', '2019-11-07 04:50:58'),
(13, 2, 'Level 2.2 Fill-in-blank task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer dictates sentence.</p>\r\n\r\n<p>2. Student chooses letter from bottom of screen and fills in on the line to complete word.</p>\r\n\r\n<p>3. Student reads sentence.</p>\r\n\r\n<p>4. If answer is correct, Trainer chooses NEXT or if error Trainer chooses REPEAT and item repeats and DONE when task is complete.</p>', '1571898432_At Peace.wav', '1', '2019-11-05 13:10:25', '2019-11-05 13:10:25'),
(14, 2, 'Level 2.3 Rhythm Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer presses start and Student reads items in rhythm.</p>\r\n\r\n<p>2. Trainer chooses to repeat task REPEAT button and DONE when task is complete.</p>', '1571898609_High Tide.wav', '1', '2019-11-05 13:10:36', '2019-11-05 13:10:36'),
(15, 2, 'Level 2.4 Rhythm Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer presses START and Student reads in rhythm.</p>\r\n\r\n<p>2. Trainer chooses to repeat task REPEAT button and DONE when task is complete.</p>', '1571898680_Midnight Imagination.wav', '1', '2019-11-05 13:10:47', '2019-11-05 13:10:47'),
(16, 2, 'Level 2.5 Fill-in-blank task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer dictates word.</p>\r\n\r\n<p>2. Student chooses missing letters from letters at the bottom of the screen and fills in on lines.</p>\r\n\r\n<p>3. Student reads word.</p>\r\n\r\n<p>4. If answer is correct, trainer chooses NEXT or if error Trainer chooses REPEAT and item repeats. Trainer chooses DONE when task is complete.</p>', '1571898760_My Guru.wav', '1', '2019-11-05 13:11:05', '2019-11-05 13:11:05'),
(17, 2, 'Level 2.6 Reading Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer scores NEXT if correct or remains with the word to assist Student with recognition</p>\r\n\r\n<p>2. Trainer chooses DONE when task is complete</p>', '1571898791_My Joyful Soul.wav', '1', '2019-11-05 13:11:17', '2019-11-05 13:11:17'),
(62, 1, 'Level 1.28 Rhythm Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer presses START and Student reads items in rhythm.</p>\r\n\r\n<p>2. Trainer chooses to repeat task REPEAT button and DONE when task is complete.</p>', '1571898675_Ocean Meditation.wav', '1', '2019-11-05 13:11:29', '2019-11-05 13:11:29'),
(63, 1, 'Level 1.29 Rhythm exercise', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer presses START and Student reads items in rhythm.</p>\r\n\r\n<p>2. Trainer chooses to repeat task REPEAT button and DONE when task is complete.</p>', '1571898805_Tender Love.wav', '1', '2019-11-05 13:11:41', '2019-11-05 13:11:41'),
(18, 2, 'Level 2.7 Rhyming task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Student says each word and chooses the two that rhyme and they light up.</p>\r\n\r\n<p>2. Trainer scores NEXT or REPEAT. Trainer chooses DONE when task is complete</p>', '1571898892_Not All Is Lost.wav', '1', '2019-11-05 13:11:56', '2019-11-05 13:11:56'),
(19, 2, 'Level 2.8 Rhyming task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Student repeats word. Trainer presses START and three words appear.</p>\r\n\r\n<p>2. Student reads each of the words and selects the word that rhymes with the one Student heard.</p>\r\n\r\n<p>3. Trainer scores correct &ndash; NEXT or REPEAT and DONE when task is complete.</p>', '1571898884_Ocean Meditation.wav', '1', '2019-11-05 13:12:10', '2019-11-05 13:12:10'),
(20, 2, 'Level 2.9 Fill-in-the-blank', '<p>1. A word with a missing letter appears in the center of the screen and a row of letters appear at the bottom of the screen.</p>\r\n\r\n<p>2. Student will move the letters, one at a time, to the line (drag/drop) and read the word. Student will tell Trainer if it is a real word or a made-up (nonsense) word.</p>\r\n\r\n<p>3. Student will press the Go button to clear the line and drop/drag the next letter and repeat the task.</p>\r\n\r\n<p>4. Trainer will press NEXT button when all of the letters have been tried to go to the next item.</p>\r\n\r\n<p>5. Trainer will choose DONE when task is complete.</p>', '1571898959_Sedona Whisper.wav', '1', '2019-10-24 06:35:59', '2019-10-24 06:35:59'),
(21, 2, 'Level 2.10 Introduction Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer says sound of cluster. Student repeats sound.</p>\r\n\r\n<p>2. Student reads words.</p>\r\n\r\n<p>3. Trainer repeats task by having Student read words with rhythm of Trainer clapping.</p>\r\n\r\n<p>4. Trainer presses START to move to next cluster. After the last set Trainer chooses DONE to go to another activity or stop.</p>', '1571899023_Tender Love.wav', '1', '2019-11-05 13:12:36', '2019-11-05 13:12:36'),
(22, 2, 'Level 2.11 Rhythm Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Student reads words to beat of metronome (or Trainer may turn sound off and clap/tap a beat).</p>\r\n\r\n<p>2. Trainer moves to REPEAT task or DONE to go to new activity</p>', '1571899000_Asilomar .wav', '1', '2019-11-05 13:12:45', '2019-11-05 13:12:45'),
(23, 2, 'Level 2.12 Spelling Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer says a word and Student places the letters on the line.</p>\r\n\r\n<p>2. Student chooses NEXT to clear lines if needs to re-do task or NEXT to move to next word or DONE to go to next activity.</p>', '1571899050_At Peace.wav', '1', '2019-11-05 13:12:58', '2019-11-05 13:12:58'),
(24, 2, 'Level 2.13 Spelling Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin and a blank screen appears. Trainer presses START and a cluster is flashed on the screen.</p>\r\n\r\n<p>2. S verbalizes the cluster that appeared on the screen. Trainer corrects with REPEAT if needed or chooses NEXT to bring up line and letter choices.</p>\r\n\r\n<p>3. Trainer says a word that uses the cluster from list in Trainer Manual.</p>\r\n\r\n<p>4. S choose letters from bottom of the screen to form word on the line.</p>\r\n\r\n<p>5. Trainer chooses NEXT to bring up new cluster and continues with task until Trainer chooses DONE.</p>', '1571899109_High Tide.wav', '1', '2019-10-24 06:38:29', '2019-10-24 06:38:29'),
(25, 2, 'Level 2.14 Introduction Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin, Trainer explains about long vowel sounds.</p>\r\n\r\n<p>2. A pair of words appears on the screen and Trainer says first word (bit) and Student repeats it.</p>\r\n\r\n<p>3. Trainer says second word and Student repeats it. Trainer asks Student to identify the vowel sound in each word.</p>\r\n\r\n<p>4. Trainer presses NEXT to move to the next pair and DONE when task is complete.</p>', '1571899108_Midnight Imagination.wav', '1', '2019-10-24 06:38:29', '2019-10-24 06:38:29'),
(26, 2, 'Level 2.15 Flash Word', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. A blank screen appears with a START button. Trainer presses START and a word is flashed (2 second exposure) and then a blank screen appears. Student says the word that Student saw.</p>\r\n\r\n<p>2. Trainer chooses REPEAT for error or NEXT to continue task. Two words appear.</p>\r\n\r\n<p>3. Student chooses the word Student saw and chooses the word which highlights.</p>\r\n\r\n<p>4. Trainer chooses NEXT to go to next item sequence flash sequence and DONE to go to a new activity.</p>', '1571899201_My Guru.wav', '1', '2019-10-24 06:40:01', '2019-10-24 06:40:01'),
(28, 2, 'Level 2.16 Rhythm Task', '<p>1. Trainer&nbsp;reviews Instructional Screen and presses GO to begin. Grid of words appears on the screen.</p>\r\n\r\n<p>2. Trainer&nbsp;uses START button to begin task and Student reads words with rhythm.</p>\r\n\r\n<p>3. Trainer may choose Repeat task. Trainer chooses DONE to go to new activity</p>', '1571899232_My Joyful Soul.wav', '1', '2019-10-24 06:40:32', '2019-10-24 06:40:32'),
(29, 2, 'Level 2.17 Rhythm Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. Grid appears on the screen.</p>\r\n\r\n<p>2. Trainer presses START and Student&nbsp;says words in rhythm.</p>\r\n\r\n<p>3. Trainer chooses to repeat task REPEAT button or DONE go to new activity.</p>', '1571899249_Not All Is Lost.wav', '1', '2019-10-24 06:40:49', '2019-10-24 06:40:49'),
(30, 2, 'Level 2.18 Reading Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. A sentence appears on the screen. Student reads the sentence and verbally fills-in the missing word.</p>\r\n\r\n<p>2. Student chooses the missing word which is on the bottom of the screen to put in the blank.</p>\r\n\r\n<p>3. If Student is correct, Trainer chooses NEXT item or if there is an error, Trainer chooses REPEAT of item and Trainer chooses DONE to go to new activity.</p>', '1571899298_Ocean Meditation.wav', '1', '2019-10-24 06:41:38', '2019-10-24 06:41:38'),
(31, 2, 'Level 2.19 Flash Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin A blank screen appears. Trainer presses START button and a phrase is shown for 2 seconds.&nbsp;</p>\r\n\r\n<p>2. Student says the phrase.</p>\r\n\r\n<p>3. Trainer chooses REPEAT to correct, or NEXT to move to next phrase and DONE to go to new activity.</p>', '1571899353_Sedona Whisper.wav', '1', '2019-10-24 06:42:33', '2019-10-24 06:42:33'),
(32, 2, 'Level 2.20 Introduction Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin and a blank screen appears. Trainer presses START and a word appears on screen.</p>\r\n\r\n<p>2. Trainer says word while Student looks at the screen.</p>\r\n\r\n<p>3. Student repeats word and Trainer chooses NEXT to go to next word.</p>\r\n\r\n<p>4. Trainer will have Student repeat the exercise multiple times and provide voice as needed or Trainer has Student say the word without verbal prompt. Trainer will choose DONE when task is complete.</p>', '1571899346_Tender Love.wav', '1', '2019-10-24 06:42:26', '2019-10-24 06:42:26'),
(33, 2, 'Level 2.21 Flash Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. A blank screen will appear with a START button. Trainer will use START button to begin flash word for 2 seconds followed by a blank screen.</p>\r\n\r\n<p>2. Student will say word Student saw.</p>\r\n\r\n<p>3. Trainer will REPEAT if error or choose NEXT if correct. When task is complete Trainer will choose DONE.</p>', '1571899434_Asilomar .wav', '1', '2019-10-24 06:43:54', '2019-10-24 06:43:54'),
(34, 2, 'Level 2.22 Rhythm Task', '<p>1. Trainer&nbsp;reviews Instructional Screen and presses GO to begin and a grid of words appears on the screen.</p>\r\n\r\n<p>2. Trainer&nbsp;will press START to activate metronome beat or choose OFF to turn off sound and use clap/tap to set pace.</p>\r\n\r\n<p>3. Student will say the words to the rhythm.</p>\r\n\r\n<p>4. Trainer&nbsp;will choose to repeat task REPEAT or DONE to move to another activity.</p>', '1571899452_High Tide.wav', '1', '2019-10-24 06:44:12', '2019-10-24 06:44:12'),
(35, 2, 'Level 2.23 Reading', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. A blank screen appears. Trainer presses START and the first sentence appears with three word choices at the bottom of the screen.</p>\r\n\r\n<p>2. Student reads the sentence and then chooses -the word to place in the blank and reads the sentence to make certain it makes sense.</p>\r\n\r\n<p>3. Trainer chooses to REPEAT task or NEXT to go to next item and DONE to move to another activity.</p>', '1571899448_At Peace.wav', '1', '2019-10-24 06:44:08', '2019-10-24 06:44:08'),
(36, 2, 'Level 2.24 Rhythm Reading', '<p>1. Trainer selects metronome beat or Off on the Introduction Screen and presses GO to begin</p>\r\n\r\n<p>2. Grid appears on the screen.</p>\r\n\r\n<p>3. Trainer chooses START to begin task. If Trainer selects Off &ndash; Trainer may clap, tap or not provide beat as S reads the words. S says words to beat.</p>\r\n\r\n<p>4. Trainer selects REPEAT or DONE to go to next task</p>', '1571899535_Midnight Imagination.wav', '1', '2019-10-24 06:45:35', '2019-10-24 06:45:35'),
(37, 2, 'Level 2.25 Rhythm Reading', '<p>1. Trainer reviews Instructional Screen and presses GO to begin Trainer selects metronome beat or Off on the Introduction Screen. Grid appears on the screen and Trainer chooses Go/Start to begin task. If Trainer selects Off &ndash; Trainer may clap, tap or not provide beat as Student reads the words.</p>\r\n\r\n<p>2. Student says words to beat.</p>\r\n\r\n<p>3. Trainer selects REPEAT or DONE to go to next task.</p>', '1571899540_My Guru.wav', '1', '2019-10-24 06:45:40', '2019-10-24 06:45:40'),
(38, 2, 'Level 2.26 Introductory Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin Blank screen appears with Go/Start option. Trainer presses button to show first item.</p>\r\n\r\n<p>2. Trainer says sound of blend while Student looks at the screen.</p>\r\n\r\n<p>3. Student repeats sound, Trainer chooses NEXT to go to next symbol.</p>\r\n\r\n<p>4. Trainer will have Student repeat the exercise multiple times with voice on or Trainer can have Student say the sound without verbal prompt. Trainer chooses DONE when task is complete.</p>', '1571899586_My Joyful Soul.wav', '1', '2019-10-24 06:46:26', '2019-10-24 06:46:26'),
(39, 2, 'Level 2.27 Spelling Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. Blank screen appears with START button. Trainer presses START for first item to appear.</p>\r\n\r\n<p>2. Trainer reads item (word with a blank for the vowel). (Words are in T Manual)</p>\r\n\r\n<p>3. Student repeats word and chooses vowel from bottom of screen and places it on line.</p>\r\n\r\n<p>4. Trainer chooses REPEAT if error, or NEXT for next item and DONE when task is complete</p>', '1571899639_Not All Is Lost.wav', '1', '2019-10-24 06:47:19', '2019-10-24 06:47:19'),
(40, 2, 'Level 2.28 Rhythm Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin. Trainer chooses START and first item appears.</p>\r\n\r\n<p>2. Trainer says rhyming word and Student repeats word.</p>\r\n\r\n<p>3. Student chooses from the letters at the bottom of the screen to place letter on line and says new word.</p>\r\n\r\n<p>4. Trainer chooses REPEAT or NEXT or DONE if task is complete.</p>', '1571899672_Sedona Whisper.wav', '1', '2019-10-24 06:47:52', '2019-10-24 06:47:52'),
(41, 2, 'Level 2.29 Introduction Task', '<p>1. Trainer reviews Instructional Screen and presses GO to begin and word appears on screen.</p>\r\n\r\n<p>2. Trainer says word and Student repeats.</p>\r\n\r\n<p>3. Trainer chooses to repeat item or go to NEXT item.</p>\r\n\r\n<p>4. Trainer can have Student say the sound without verbal prompt for review</p>', '1571899703_Tender Love.wav', '1', '2019-10-24 06:48:23', '2019-10-24 06:48:23'),
(42, 1, 'Level 1.12 Rhythm Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1.&nbsp; Trainer presses START and Student starts reading items to rhythm</p>\r\n\r\n<p>2. &nbsp;Trainer chooses to repeat task REPEAT button and DONE when task is complete.&nbsp;</p>', '1571896281_At Peace.wav', '1', '2019-11-05 13:13:23', '2019-11-05 13:13:23'),
(43, 1, 'Level 1.13 Rhythm Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer presses START and Student reads words with rhythm.</p>\r\n\r\n<p>2. &nbsp;Trainer chooses to repeat task REPEAT button or &nbsp;DONE when task is complete.&nbsp;</p>', '1571897869_High Tide.wav', '1', '2019-11-05 13:13:32', '2019-11-05 13:13:32'),
(44, 1, 'Level 1.14 Rhythm Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer presses START and Student reads the items to the rhythm pace.</p>\r\n\r\n<p>2. Trainer chooses to repeat task REPEAT button and DONE when task is complete.&nbsp;</p>', '1571897955_Midnight Imagination.wav', '1', '2019-11-05 13:13:42', '2019-11-05 13:13:42'),
(45, 1, 'Level 1.15 Fill-in-blank', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. A syllable with a blank appears on screen &ndash; like __nt.</p>\r\n\r\n<p>2. Trainer dictates syllable &ndash; like ant.</p>\r\n\r\n<p>3. Student chooses letter from bottom of screen and fills in letter on the line to make the word. Student says word.</p>\r\n\r\n<p>4. Trainer chooses NEXT or if error Trainer chooses REPEAT and item repeats with blank line again.</p>\r\n\r\n<p>5. Trainer chooses DONE when task is complete.</p>', '1571898003_My Guru.wav', '1', '2019-11-05 13:14:03', '2019-11-05 13:14:03'),
(46, 1, 'Level 1.16 Intro-Task', '<p>These tasks introduce new information &ndash; sound/symbol, sound clusters/ syllables, and words. After the instruction screen each task has a letter, syllable, or word that appears singly on the screen.&nbsp; The Trainer&nbsp;voice says the sound, syllable or word while the student&nbsp;sees it on the screen.&nbsp; Student&nbsp;repeats the sound and Trainer scores by touching REPEAT (for error) or NEXT (for correct) button. &nbsp;</p>\r\n\r\n<p>Trainer may have Student repeat the total task multiple times.&nbsp; Trainer will have Student do the task without the voice prompt to check if Student can read the symbol/cluster or word without a prompt.</p>', '1571898066_My Joyful Soul.wav', '1', '2019-11-04 04:48:56', '2019-11-04 04:48:56'),
(47, 1, 'Level 1.17 Voice Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer says sound/word while Student looks at screen.</p>\r\n\r\n<p>2. Student repeats sound and chooses item which highlights.</p>\r\n\r\n<p>3. Trainer chooses REPEAT if error, or NEXT if correct and DONE when task is complete</p>', '1571898093_My Joyful Soul.wav', '1', '2019-11-05 13:14:42', '2019-11-05 13:14:42'),
(48, 1, 'Level 1.18 Flash task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1.A three letter word flashes (2 second default time) on the screen. (man)</p>\r\n\r\n<p>2. Student says word to blank screen. &nbsp;</p>\r\n\r\n<p>3. Trainer presses NEXT if Student says correct or REPEAT if error.</p>\r\n\r\n<p>4. Three lines appear on screen with letter choices at bottom of screen for selection.</p>\r\n\r\n<p>5. Student chooses letters and moves to lines. Letters are placed on the lines __ __ __ in the middle of the screen.</p>\r\n\r\n<p>6. Trainer scores with REPEAT or NEXT and DONE when task is complete.</p>', '1571898168_Ocean Meditation.wav', '1', '2019-11-05 13:14:27', '2019-11-05 13:14:27'),
(49, 1, 'Level 1.19 Spelling task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Student chooses letters from bottom of screen and moves them to lines to create the word.</p>\r\n\r\n<p>2. Trainer chooses REPEAT or NEXT, and DONE when task is complete.</p>', '1571898196_Sedona Whisper.wav', '1', '2019-11-05 13:14:52', '2019-11-05 13:14:52'),
(50, 1, 'Level 1.20 Rhythm reading', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1.selects pulse, or metronome, or turns 0ff and claps or taps to slower pace. Trainer presses GO button and grid appears on the screen.</p>\r\n\r\n<p>2. Trainer presses START and Student says syllables in rhythm.</p>\r\n\r\n<p>3. Trainer chooses to repeat task REPEAT button or DONE when task is complete.</p>', '1571898289_Tender Love.wav', '1', '2019-11-05 13:15:26', '2019-11-05 13:15:26'),
(51, 1, 'Level 1.21 Intro Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer says sound of symbol/word while Student looks at the screen.</p>\r\n\r\n<p>2. Student repeats sound.</p>\r\n\r\n<p>3. Trainer may have Student repeat the exercise multiple times with voice prompt.</p>\r\n\r\n<p>4. Trainer may have Student say the sound without verbal prompt.</p>\r\n\r\n<p>5. Trainer chooses NEXT to go to the next item and DONE when task is complete.</p>', '1571898352_Asilomar .wav', '1', '2019-11-05 13:15:46', '2019-11-05 13:15:46'),
(52, 1, 'Level 1.22 Rhythm Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer presses START and Student says items to rhythm pace.&nbsp;</p>\r\n\r\n<p>2. &nbsp;Trainer chooses repeat task -REPEAT and DONE when task is complete.&nbsp;</p>', '1571898414_At Peace.wav', '1', '2019-11-05 13:15:58', '2019-11-05 13:15:58'),
(53, 1, 'Level 1.23 Flash task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Student will repeat the phrase from memory.</p>\r\n\r\n<p>2. Trainer scores with REPEAT or NEXT and DONE when task is complete</p>', '1571898434_High Tide.wav', '1', '2019-11-05 13:16:11', '2019-11-05 13:16:11'),
(54, 1, 'Level 1.24 Voice Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer presses NEXT and 3 phrases appear on the screen.</p>\r\n\r\n<p>2. Student chooses phrase by highlighting it.</p>\r\n\r\n<p>3. Trainer scores REPEAT or NEXT and DONE when task is complete.</p>', '1571898503_Midnight Imagination.wav', '1', '2019-11-05 13:16:25', '2019-11-05 13:16:25'),
(55, 1, 'Level 1.25 Rhythm Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. selects pulse, metronome, or turns beat off and claps or taps to slower pace. Trainer selects GO button and the item grid will appear.</p>\r\n\r\n<p>2. Trainer presses START and items appear for Student to read in rhythm.</p>\r\n\r\n<p>3. Trainer chooses to repeat task REPEAT button and DONE when task is complete.</p>', '1571898535_My Guru.wav', '1', '2019-11-05 13:16:37', '2019-11-05 13:16:37'),
(56, 1, 'Level 1.26 Rhythm reading', '<p>1. Trainer reviews Instructional Screen and selects pulse, metronome beat, or off for pacing with clap or tap and presses GO.</p>\r\n\r\n<p>2. Grid appears on the screen.</p>\r\n\r\n<p>3. Trainer presses START and Student reads items in rhythm.</p>\r\n\r\n<p>4. Trainer may choose to repeat task - REPEAT button or use DONE button when task is complete</p>', '1571898599_My Joyful Soul.wav', '1', '2019-10-24 06:29:59', '2019-10-24 06:29:59'),
(57, 1, 'Level 1.27 Intro Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Trainer&nbsp;says sound of symbol/ word on the screen.</p>\r\n\r\n<p>2. Student repeats the sound.</p>\r\n\r\n<p>3. Trainer&nbsp;chooses to REPEAT or NEXT and DONE when task is complete.</p>\r\n\r\n<p>4. Trainer may have Student practice without prompt. Trainer&nbsp;selects REPEAT or NEXT to move to next item.</p>', '1571898682_Not All Is Lost.wav', '1', '2019-11-05 13:16:50', '2019-11-05 13:16:50'),
(59, 3, 'Level 3.1: Reading Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Several phrases appear in a column on page.</p>\r\n\r\n<p>2. Trainer and Student read phrases together. Trainer sets pace with careful articulation of words, separating the words, pausing at the end of each phrase, and enough emphasis/expression to make information interesting.</p>\r\n\r\n<p>3. Trainer presses NEXT to go to next screen with phrases.</p>\r\n\r\n<p>4. Trainer chooses REPEAT button in order to repeat the task from the beginning of the selection.</p>', '1571898194_Asilomar .wav', '1', '2019-11-06 13:19:18', '2019-11-06 13:19:18'),
(60, 3, 'Level 3.2   Reading Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Several phrases appear in a column on page.</p>\r\n\r\n<p>2. Trainer and Student read phrases together. Trainer sets pace with careful articulation of words, separating the words, pausing at the end of each phrase, and enough emphasis/expression to make information interesting.</p>\r\n\r\n<p>3. Trainer presses NEXT to go to next screen with phrases.</p>\r\n\r\n<p>4. Trainer chooses REPEAT button in order to repeat the task from the beginning of the selection.</p>', '1571898222_At Peace.wav', '1', '2019-11-06 13:19:34', '2019-11-06 13:19:34'),
(61, 3, 'Level 3.3:  Reading Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. Several phrases appear in a column on page.</p>\r\n\r\n<p>2. Trainer and Student read phrases together. Trainer sets pace with careful articulation of words, separating the words, pausing at the end of each phrase, and enough emphasis/expression to make information interesting.</p>\r\n\r\n<p>3. Trainer presses NEXT to go to next screen with phrases.</p>\r\n\r\n<p>4. Trainer chooses REPEAT button in order to repeat the task from the beginning of the selection.</p>', '1571898391_High Tide.wav', '1', '2019-11-06 13:19:51', '2019-11-06 13:19:51'),
(64, 11, 'Level 5.1:  Language Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1.&nbsp; The task by activating the music program.&nbsp;<br />\r\n2. &nbsp;Trainer will use the words in the Trainer&rsquo;s Manual. &nbsp;This a totally oral task. &nbsp;No reading.&nbsp;<br />\r\n3. &nbsp;Trainer will say three words and Student will repeat them back. &nbsp;<br />\r\n4. &nbsp; If Student has difficulty with remembering three words, Trainer will use two word phrases as suggested in the Trainer&rsquo;s Manual and gradually increase to three word sequences. &nbsp;&nbsp;<br />\r\n5. &nbsp;When task is complete, Trainer will press DONE to complete the activity. &nbsp;<br />\r\n&nbsp;</p>', '1573101729_Not All Is Lost.wav', '1', '2019-11-07 04:42:09', '2019-11-07 04:42:09'),
(65, 11, 'Level 5.2:  Language Task', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1.&nbsp; Begin the task by activating the music program.&nbsp;<br />\r\n2. &nbsp;Trainer will use the words in the Trainer&rsquo;s Manual. &nbsp;This a totally oral task. &nbsp;No reading.&nbsp;<br />\r\n3. &nbsp;Trainer will say a phrase and Student will repeat it back. &nbsp;<br />\r\n4. &nbsp;When task is complete, Trainer will press DONE to complete the activity&nbsp;</p>', '1573101958_Not All Is Lost.wav', '1', '2019-11-07 04:45:59', '2019-11-07 04:45:59'),
(67, 4, 'Level 4.1:   Auditory 1', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. &nbsp;Trainer will instruct Student that Student will hear a beep. &nbsp;Whenever Student hears&nbsp; the beep Student will press a key on the keyboard as quickly as Student can. Trainer will encourage Student to be prepared by having Student &rsquo;s hand near the key.</p>\r\n\r\n<p>2. &nbsp;Trainer will start with headphone providing sound on Right side. &nbsp;Student will be instructed to press the M key for the right side. &nbsp;Student will press the key as quickly as Student can.</p>\r\n\r\n<p>3. &nbsp;When the task is complete a SCORE button will appear on the screen. &nbsp;Trainer will press the SCORE button and the averaged speed of response will appear in milliseconds and the number of errors (missed responses or responses recorded before the beep). &nbsp; &nbsp;</p>\r\n\r\n<p>4. &nbsp; Trainer will record the information for Student on a form from the Trainer&rsquo;s Manual &ndash; Appendix G.&nbsp;</p>\r\n\r\n<p>5. &nbsp;When the task is complete including recording the scoring, &nbsp;Trainer will place the headphone on the left side and repeat the task. For the left side, Student will use the Z key.&nbsp;</p>', '1573107547_Asilomar .wav', '1', '2019-11-07 06:19:07', '2019-11-07 06:19:07'),
(68, 4, 'Level 4.2:  Auditory 2', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. &nbsp;Student will have set of headphones covering both ears. The Instructional Screen has a GO button for Trainer to begin the task. &nbsp;</p>\r\n\r\n<p>2. &nbsp;Trainer will instruct Student that Student will hear a beep. &nbsp;Whenever Student hears the beep Student will press the key on the side that S heard the beep as quickly as Student can. &nbsp;If Student hears it on the R side Student will press the key M, &nbsp;if they hear it on the L side they will press the key Z.&nbsp;</p>\r\n\r\n<p>3. &nbsp;When the task is complete a SCORE button will appear on the screen. &nbsp;Trainer may press the SCORE button. The average speed of response in milliseconds, for each ear L and R and the error report, including number of no response to beep, and response to the wrong side will appear be reported. &nbsp;Trainer will record the information on the form in the Trainer&rsquo;s Manual &ndash; Appendix G.<br />\r\n&nbsp;</p>', '1573107891_Sedona Whisper.wav', '1', '2019-11-07 06:24:51', '2019-11-07 06:24:51'),
(69, 4, 'Level 4.3:  Visual', '<p><strong>Trainer Instructions</strong></p>\r\n\r\n<p>1. &nbsp;Trainer will instruct Student that Student will see a circle on the left and right side of the screen. &nbsp;One of the circles will light up and S will press the key on that side of the keyboard. &nbsp;Student will press the M when the circle on the right side lights up and the Z when the left side lights up. &ndash; as fast as Student can.&nbsp;</p>\r\n\r\n<p>2. &nbsp;When task is complete a SCORE button will appear on the screen. &nbsp;Trainer may press the SCORE button and the averaged speed for the right side will appear and the average speed for the L side will appear as well as the error information. Trainer will record the information on the form in the Trainer&rsquo;s Manual &ndash; Appendix G. &nbsp; Trainer will press DONE to complete the task when the scoring has been recorded.&nbsp;<br />\r\n&nbsp;<br />\r\n&nbsp;</p>', '1573108110_Sedona Whisper.wav', '1', '2019-11-07 06:28:30', '2019-11-07 06:28:30');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `user_id`, `price`, `action`) VALUES
(1, 3, '250', 'Accept'),
(2, 4, '250', 'Accept'),
(3, 1, '250', 'Claim'),
(4, 2, '250', 'Claim'),
(5, 3, '250', 'Claim'),
(6, 4, '250', 'Claim');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `institute_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `gender` text COLLATE utf8mb4_unicode_ci,
  `class_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` text COLLATE utf8mb4_unicode_ci,
  `dob` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_hint` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` text COLLATE utf8mb4_unicode_ci,
  `title` text COLLATE utf8mb4_unicode_ci,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `guest_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `institute_id`, `teacher_id`, `gender`, `class_id`, `name`, `last_name`, `dob`, `email`, `password`, `password_hint`, `type`, `image`, `status`, `remember_token`, `company_name`, `title`, `phone`, `guest_status`, `created_at`, `updated_at`) VALUES
(2, NULL, 0, NULL, NULL, 'admin', '', '', 'admin@oms.com', '$2y$10$lRC6aITo8XOPyOtQgKdze.E866bVknAbmDLEuPYx.lies5L4Xv.8e', NULL, 'admin', '1570646648-Second_logo.png', '1', 'I1t91GjlrvKZ8yx1a3YEucjaOeV5tyYuQ4omcVVgLum6l7W8JjbnZjQEairn', NULL, NULL, NULL, 0, '2018-06-14 01:36:04', '2018-06-14 01:36:04'),
(30, NULL, 0, NULL, NULL, 'oms_I123', '', '', 'oms@institute.com', '$2y$10$.ThY6O3l43O3m7SsM3eo6.GgjjtofJ8/SNcI6GdSLq7.d5hEnt7w6', '222652', 'Institute', '1571837420_84-840383_image-transparent-big-book-clipart-drawing-of-reading.png', '1', 'b2SnpOZCujfZFa8UNOMlmuEV1v2MPTjmqiJuwXlAnQW6W2Hb2kFi9Ba8NUcA', NULL, NULL, NULL, 0, '2019-08-27 10:54:36', '2019-10-23 20:30:20'),
(31, '30', 38, NULL, '1', 'oms Student', 'test', '10/16/2019', 'oms@student.com', '$2y$10$Q4sIJCtw27i8iub4iwt5ZuxK/KjuRfzt7RKfRDajCKNk6VkrxvGi2', '123456', 'student', '1571810190_6978350-book-heart-big-ben-bus-public-phone-london-mood.jpg', '1', 'vvRWmK9qwCPbkpHPhQ4NqHTlgM0QvgeVzSAEKjLbXJho47JQduq7Ye3uyYmQ', NULL, NULL, NULL, 0, '2019-08-27 10:57:08', '2019-10-23 20:28:18'),
(32, '30', 0, NULL, NULL, 'OMS', 'Teacher', '10/08', 'oms@teacher.com', '$2y$10$uPzvoFb5T0nTFDzKAPAzLOucYx0GVcRhg17FpmCZXciQmXAAJySOu', '471846', 'teacher', '1571834481_6978350-book-heart-big-ben-bus-public-phone-london-mood.jpg', '1', 'ghv7QtoSSP7nMpGec5EJ0MvKMpEusuE1Wl5fPlIThBiTRaBoScoiBhvBfdjc', NULL, NULL, NULL, 0, '2019-08-27 10:57:26', '2019-10-23 19:41:21'),
(33, '30', 0, NULL, NULL, 'jatinder', '', '', 'jatinder@teacher.com', '$2y$10$15RMYNtIBiusiC7aOBY5yekQYIC.yXaNxEawLKN3NQaeCdIi.pyc2', '850574', 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-08-27 11:39:18', '2019-08-27 11:39:18'),
(38, '30', 0, NULL, NULL, 'Jk', '', '', 'jk@yopmail.com', '$2y$10$0snm.IF2WUYO/gEY8PSjxOqg6FSWMzNwLWWxdR93ak1t3RWN888KK', '566282', 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-09-10 12:36:39', '2019-09-10 12:36:39'),
(39, NULL, 0, NULL, NULL, 'DEE', '', '', 'dahuja@teacher.com', '$2y$10$GmxsBnp24VYxztWigYN10uFnls9Lfm7vU4e7d6iW6HJlmDRLlvEYy', '973761', 'Institute', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-09-18 07:52:29', '2019-09-18 07:52:29'),
(40, NULL, 0, NULL, NULL, 'Sahil', '', '', 'inst@gmail.com', '$2y$10$YA8qnrt4UKODFU/r/jDm/.kXQAws38x12y.2LyXV6xMs6wGlACziG', NULL, 'Institute', NULL, '1', '9QdhzyHkvDmKAabGJRIeWykbudBGutIHpXlqnCM9ngPY8qYGbL5YalqvzOkE', NULL, NULL, NULL, 0, '2019-09-18 07:59:50', '2019-09-18 07:59:50'),
(41, NULL, 0, NULL, NULL, 'Ptu', '', '', 'Sahil@ptu.com', '$2y$10$.ThY6O3l43O3m7SsM3eo6.GgjjtofJ8/SNcI6GdSLq7.d5hEnt7w6', '222652', 'Institute', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-09-18 08:03:29', '2019-09-18 08:03:29'),
(49, '30', 32, NULL, NULL, 'andy', '', '', 'test@test.com', '$2y$10$U5ym7wl0VzrjAgcL.zq3dO8Atlq5O4Qg//tl6vVIXxK84wUINyapu', '162071', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-04 15:17:45', '2019-10-04 15:17:45'),
(51, '41', NULL, NULL, NULL, 'John', '', '', 'John@gmail.com', '$2y$10$lXxnRyZm4MAS7X8kKtnVqOtcYkphXFXfzJ.gNGuZnD5oCP1NJ2bGO', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-05 13:02:41', '2019-10-05 13:02:41'),
(53, NULL, NULL, NULL, NULL, 'gurjeevan', '', '', 'jee@gmail.com', '$2y$10$UKEnpvp0PucHJvlPz9Afp.tUReXWnENTxh16kIgV.k04qgNmgVOv6', 'admin@123', 'Institute', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-05 19:14:27', '2019-10-05 19:14:27'),
(56, NULL, NULL, NULL, NULL, 'test', '', '', 'test5@gmail.com', '$2y$10$Tsun0GN8alaOkzfb9B/DR.nZb4PXdkOMTdd5LoZZucVOlF1pOMJDi', '434243', 'Institute', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-07 14:23:28', '2019-10-22 15:29:43'),
(59, '30', NULL, NULL, NULL, 'j', '', '', 'j@yopmail.com', '$2y$10$eQgmcEQ.T05U2STsQCRrkuPBBEB/K.an8/VtejqkwJQpXT2Bv1Eo6', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-08 01:36:23', '2019-10-08 01:36:23'),
(60, '30', NULL, NULL, NULL, 'test stu', '', '', 'stu@yopmail.com', '$2y$10$zj0sy8BnPICYBFQxvAQIju0LgTCQUcS1i3Af7Tv73Zz.SY139CHrS', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-08 10:29:50', '2019-10-08 10:29:50'),
(61, '30', NULL, NULL, NULL, 'ATrainer', '', '', 'ATrainer@oms.com', '$2y$10$hAlgwPaxuXmYHMXU.F7Ute2xi8KVPdiTe8eqD31qnBAZDyNPvJ.Uq', '123456', 'teacher', NULL, '1', '6aCH1cV2UmYPEnGy3y1wQcYFb99ThLg6WWEI9KmxTcpOtogR53SN1eYZXGPW', NULL, NULL, NULL, 0, '2019-10-09 02:29:44', '2019-10-09 02:29:44'),
(62, NULL, 61, NULL, NULL, 'Student xyz', '1234565888/12245resfzdcx@#$', '10/25/2019', 'Astudent@oms.com', '$2y$10$Hfw1WDhvoSxWdCfo6aCbJuJRdpxl4ZOf7WmXae9AAYh1B2D/7PWUO', '367063', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 02:32:30', '2019-10-25 16:47:29'),
(63, NULL, NULL, NULL, NULL, 'Institute1', '', '', 'Institute1@oms.com', '$2y$10$xF/3/TvCQBfxO30eYtwoguU8/KlLLqERRzPLetjmSi5DL5L0eewsi', '123456', 'Institute', NULL, '1', '2elpdVaah9C24vyDFwhU372FGTI9G0L5oTBKx0eoQJA7YFEHjevQ3COHKcXv', NULL, NULL, NULL, 0, '2019-10-09 02:40:18', '2019-10-09 02:40:18'),
(64, '63', NULL, NULL, NULL, 'Teacher1', '', '', 'Teacher1@oms.com', '$2y$10$25JuCXj6AF524dWGna2T/OV0ZIlAueBFvdarH6aeWeJONMwYXBOnO', '123456', 'teacher', NULL, '1', 'bi7i3kXzcE97WbLpOdcmypYmxXJ2SkepOBu4irdXlKz5TtWBGf1ZhwjvAcRq', NULL, NULL, NULL, 0, '2019-10-09 02:41:20', '2019-10-09 02:41:20'),
(65, '63', NULL, NULL, NULL, 'Student1', '', '', 'Student1@oms.com', '$2y$10$d9D5MP/3GZvZlWc2hQ/WNe1c/1IzuQ48rU5dZvZx4pQ9xDB6Qg2pm', '123456', 'student', NULL, '1', '5kFjmvW6S6Rq90RYiB0SLhbi53QlG9MnOuCIjUu0TkZsFEAB7xqb677jGasz', NULL, NULL, NULL, 0, '2019-10-09 02:42:11', '2019-10-09 02:42:11'),
(66, NULL, 64, NULL, NULL, 'James', '', '', 'James@oms.com', '$2y$10$YUjpOst99XDnS9hW7mzX1uBg4Lulxywz6vJit89PSfxmehtKGRPHG', '154470', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 02:43:14', '2019-10-09 02:43:14'),
(67, '63', NULL, NULL, NULL, 'Teacher2', '', '', 'Teacher2@oms.com', '$2y$10$NDGnszNgwE4CySRTw9SxA.3ot0Ty2srxDnoxs4m5OormU1MWo0QBy', '123456', 'teacher', NULL, '1', '3PdQ4QKjxgE4wtkRVwAzDumN6nc5DK6xCgSqH9RtJlmEWsHTJCZXH8dXHwjk', NULL, NULL, NULL, 0, '2019-10-09 02:44:38', '2019-10-09 02:44:38'),
(68, '63', NULL, NULL, NULL, 'Teacher3', '', '', 'Teacher3@oms.com', '$2y$10$qouMcvnfPaOsPiT/UjlQdubK2AuzQ3Fzqpt/QydPE5nQc.lz1tM7.', '123456', 'teacher', NULL, '1', 'hJ6XklzaQutjedXw7WikMskQI7UwzSMuIMbeuR9Ma17YDz6iW289KIFS1TYE', NULL, NULL, NULL, 0, '2019-10-09 02:45:14', '2019-10-09 02:45:14'),
(69, '63', NULL, NULL, NULL, 'Teacher4', '', '', 'Teacher4@oms.com', '$2y$10$u9pHYoIYGVv1cIRoI.Q.Su9Rp3KaoQDjyJ6EIhFtFheMkV5X90E7y', '123456', 'teacher', NULL, '1', 'X8EcgxikW11ZcHHdLoKoQsi2ZDiFHE3Y3faz0YzL9UnJ3sxsBLRxUmLkzTMK', NULL, NULL, NULL, 0, '2019-10-09 02:45:49', '2019-10-09 02:45:49'),
(70, '63', NULL, NULL, NULL, 'Student2', '', '', 'Student2@oms.com', '$2y$10$GFwgte3ynTjpLw9LIdpWXeyxTAt1.yIXUujJq.DabKmyyWjhu9HLa', '123456', 'student', NULL, '1', '3VGRjYfI9PqqtiKVIxYN6DmAiwRA1xAUs2kD7CxgJSHyHkh3I5U3eEgYOJ8u', NULL, NULL, NULL, 0, '2019-10-09 02:47:23', '2019-10-09 02:47:23'),
(71, '63', NULL, NULL, NULL, 'Student3', '', '', 'Student3@oms.com', '$2y$10$A1gFVNINo4Vzfn.pribMu.wTyahhyW4OPynQvjoZXxBjF86xD2pga', '123456', 'student', NULL, '1', 'KZxABsXm6Yb1ABcapfyV84YrDZAik6oPoYCU0I74WSLZyHGtF2wftWrBd58k', NULL, NULL, NULL, 0, '2019-10-09 02:47:55', '2019-10-09 02:47:55'),
(72, '63', NULL, NULL, NULL, 'Student4', '', '', 'Student4@oms.com', '$2y$10$s2QdVUxOduE1QBZ2cD/DyeMZ9Mw2FTqkYtMxr7dwGsHCx32l9HyLi', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 02:48:33', '2019-10-09 02:48:33'),
(73, NULL, 67, NULL, NULL, 'Jasmine', '', '', 'Jasmine@gmail.com', '$2y$10$BIvIDuHxRt9.ExNIfLO/keDHaIGHiKb1AiEDNxJG7H73UecjmbtzK', '584545', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 02:49:36', '2019-11-07 06:27:08'),
(74, NULL, 68, NULL, NULL, 'Bobby', '', '', 'Bobby@gmail.com', '$2y$10$nbmbE7A8qvOJsnem9iDCMeNRVsz0JsAtC2rxPOxBnbR8zhTXjrGo6', '879852', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 02:50:30', '2019-10-09 02:50:30'),
(75, NULL, 69, NULL, NULL, 'Smaira', '', '', 'Smaira@oms.com', '$2y$10$l96Wr/FcJ9LGSv.Rwfqyn.41JGckQm1jt57l1WGSoVBoEJH4XPHxe', '345135', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 02:51:42', '2019-10-09 02:51:42'),
(77, '30', NULL, NULL, NULL, 'paramveer', '', '', 'mnprit@gmail.com', '$2y$10$.MTs7rZX/MDVL/1ONls4Ouo5UManpX/4I6F4gVj923zjL7cC.ce.C', 'mnprit12', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 15:23:29', '2019-10-09 15:23:29'),
(78, NULL, NULL, NULL, NULL, 'helix', '', '', 'rajvirkaur79@gmail.com', '$2y$10$r18ZKTpc0LHBExTYvRakX.7vuTAvPGRxISwXOibxCUw.t/8BKYGDK', 'Rajvir@12', 'Institute', NULL, '1', 'mtEVGe1CBwF6t9UGWukhmSR1lftkXQ6wVag8cgjc5lIsL8UDQKzMCjL3w35u', NULL, NULL, NULL, 0, '2019-10-09 15:57:45', '2019-10-09 15:57:45'),
(79, '78', NULL, NULL, NULL, 'mnprit', '', '', 'mnprit1@gmail.com', '$2y$10$hL50KKIO.qc5AUhJeBXjyuv/PUumiLcRWZB8VcFMXf1A1ObQNfD.q', '123456', 'teacher', NULL, '1', 'vI1ZSOTR6DxUiWAMfeQ5lQdOOTxHvHMXCAYlPP6Kf9fYJdEXYzaIHcuOm6XR', NULL, NULL, NULL, 0, '2019-10-09 16:01:07', '2019-10-09 16:01:07'),
(81, NULL, 79, NULL, NULL, 'rajveer', '', '', 'rajvir22@gmail.com', '$2y$10$fiz6AiOTC14vJwqGtc8vkO5oh/9jx.v6x7fjaecl9wTMdV4RSIZve', '839301', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 16:05:19', '2019-10-24 18:56:35'),
(90, NULL, NULL, NULL, NULL, 'sdsadsad', NULL, NULL, 'sad@gmail.com', '$2y$10$ck3wz7wguMh4BiHZv9jIW.HVnK6/pgWclORuXSEcUhg6uiA1dftVe', '43242342343', 'Institute', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 19:28:15', '2019-10-09 19:28:15'),
(94, NULL, 92, NULL, NULL, 'fdsfsdff', 'fdsff', '10/29/2019', 'rfdfsd@gmail.com', '$2y$10$xS4jAkzlSGFrZxJ7WXOnr.B265w51.m1A3mPO5h4XomL5hVbKx.Bm', '34324234234', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 19:55:13', '2019-10-09 19:55:13'),
(95, NULL, NULL, NULL, NULL, 'fdfsd', NULL, NULL, 'dfsd@gmail.com', '$2y$10$v0mzwspBswsKeJy8SyLOZevVb9dMqbf3nmXHaNy6z/4zwcQPT9Pme', '45435345345', 'Institute', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-09 19:55:58', '2019-10-09 19:55:58'),
(97, '63', NULL, NULL, NULL, 'Sam', 'ss', '10/11/1989', 'sam@oms.com', '$2y$10$VX1H0xMR7ypoe4UNpANX9OEHGczSF9MgUVJBh.1z2GILxZZuN9BfK', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-10 02:18:18', '2019-10-10 02:18:18'),
(102, '30', NULL, 'Female', NULL, 'tests', 'teacher', '10/22/2019', 'testte@gmail.coms', '$2y$10$If0C0NfMMePtOSYgOVsSxOa4Pto97BdWWuezpWRCyGC0/QT5/1jEO', '123456', 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-10 16:50:16', '2019-10-10 16:50:46'),
(103, NULL, 32, NULL, NULL, 'Gurjeevan Stu', 'Student', '10/23/2019', 'gurStu@gmail.com', '$2y$10$QQ0yX/PRaB77Qfuizeo3OeCKg18x47OwHXa8rK6XxfiNQOYhXam1e', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-10 18:00:43', '2019-10-10 18:00:43'),
(104, NULL, 32, NULL, NULL, 'Gurjeevan Stu', 'Student', '10/23/2019', 'gurStu@gmail.com', '$2y$10$imfvnmquHbbKxotdNlLSEOPONTuXllNIlfR83POyQw8hj9BseaLwe', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-10 18:01:26', '2019-10-10 18:01:26'),
(105, NULL, 32, NULL, NULL, 'Gurjeevan Stiu 2', '4', '10/29/2019', 'testttt@gmail.com', '$2y$10$76UK1l6v2oyc2bw8gzGSi.lyN9Y0gqspi0chP6ATCwPyov7In9lw6', '344234', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-10 18:01:56', '2019-10-10 18:01:56'),
(106, '30', NULL, 'Female', NULL, 'Gurjeevan Teachers', '323', '10/30/2019', 'Gurtea@gmail.com', '$2y$10$TG4sG1a4h29ipCFxHJbSjOwInDACoMU7.osk2UtvITmUi7HcAFIW6', '123456', 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-10 18:02:36', '2019-10-10 18:02:57'),
(107, '30', NULL, NULL, NULL, 'Gur In Stu', '434weqwe', '10/30/2019', 'dfgdfh@gmail.com', '$2y$10$ecsvEHHmcTOjHYlRFOY3LuhwJMsUhJQ78ffC03RF3OlEEJX9zfBEO', '3244', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-10 18:03:23', '2019-10-10 18:06:30'),
(108, '30', NULL, NULL, NULL, 'gfgfdg', 'gfgfdg', '10/22/2019', 'gfgfd@gmail.com', '$2y$10$tfi4ms7fjJc5grGbE5U82eMiGpRDlJ3zKjoAcYsKPqSSTEvO9G35W', '423434', 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-10 18:20:56', '2019-10-10 18:20:56'),
(109, NULL, 32, NULL, NULL, 'fdsfdf', 'fdsfdf', '10/31/2019', 'gur@gmail.com', '$2y$10$5fIr9ZUM.7Wz7dHCHdkVL.lwhtkLK5lmqIoQ5Olrf00BODA1Ldyo.', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-11 18:57:33', '2019-10-11 18:57:33'),
(111, '30', NULL, NULL, NULL, 'dssads', 'dsdsds', '10/22/2019', 'fdfds@gmail.com', '$2y$10$8XQ6YujxUzVtnV2xXuaYduN.lxd2hTfwQPmH7KwxRMb48aWDjb8qe', '231323', 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-14 12:00:30', '2019-10-24 13:59:21'),
(113, NULL, NULL, NULL, NULL, 'gurjee', 'teach', '10/30/2019', 'gurteacher@gmail.com', '$2y$10$tazErI6TMjqBMItzgmBaj.leXSQEFyHVQuw3s6eCvSIO4Th4UL9Bm', '123456', 'teacher', NULL, '1', 'buSjwLm4DVJEWwK9GLYdHoYFYwm4Rb4P7hhKlY46wCIyryVIgLMatbh4OwvX', NULL, NULL, NULL, 0, '2019-10-22 12:10:23', '2019-10-22 15:19:28'),
(114, NULL, 113, NULL, NULL, 'gurjeevan', 'student', '10/23/2019', 'gurteacherstu@gmail.com', '$2y$10$oA6pdtM.mn4XgEifCJApw.YRfXnO/Jsy/7IdhkZBecJikQ3acuABe', '123456', 'student', NULL, '1', 'sYRSpA8PsRrt9ePJu0fbWZEnoi5TLOn8qxEsmvxO9DFN2kbsf676Lr7ihx12', NULL, NULL, NULL, 0, '2019-10-22 12:11:48', '2019-10-22 12:11:48'),
(116, NULL, NULL, NULL, NULL, 'gurjee', 'stu', '10/31/2019', 'gurstu13@gmail.com', '$2y$10$FvSgUlA8ShaRmxJNqIMDpOsXQ/8mnn2iUX.dT1A5lQFFZR8we9KbW', '123456', 'student', NULL, '1', 'IT2qAluUy1gd9y6R1ah5SciYzQOtKTQOYZ6oPomdKZ5YzVmrnT6mcEVWt4Un', NULL, NULL, NULL, 0, '2019-10-22 12:55:08', '2019-10-22 15:22:09'),
(117, '30', NULL, NULL, NULL, 'ddasd', 'dsadas', '10/02/2019', 'dsfds@gmail.com', '$2y$10$j7i.13h3FlF/rcWr8lvAj.GMd1ceIJj0uSJ869564mWT25KhwTZcS', '2312321', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-23 12:39:14', '2019-10-23 12:39:14'),
(122, NULL, 32, NULL, NULL, 'Abc', 'Abcd', '11/03/2000', 'abc@gmail.com', '$2y$10$JC2oTFF47tiq4x4rIRPax.JtMchITOwLPCLNh8lLfzl84.o3zS6/2', 'qwertya', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-23 17:00:48', '2019-10-23 17:00:48'),
(123, NULL, 32, NULL, NULL, 'test', 'my', '10/09/2019', 'ttttt@gmail.com', '$2y$10$2XeqT2G6bvWLJnwUN7xFKOaqoHIQnFujnyzZEA84Akri0eE7M66u6', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-23 18:07:56', '2019-10-23 18:07:56'),
(127, NULL, NULL, NULL, NULL, 'Gurjeevan', NULL, NULL, 'gurg@gmail.com', NULL, NULL, 'teacher', NULL, '1', 'f9r5Miw3cW5NuGl8TtORCY63f4gqVKZ8JUSctsKvCTd6NK2ugds9hSrpEIrR', NULL, NULL, NULL, 1, '2019-10-24 13:01:22', '2019-10-24 13:01:22'),
(128, NULL, NULL, NULL, NULL, 'dfdsfd', NULL, NULL, 'fsf@gmil.com', NULL, NULL, 'teacher', NULL, '1', 'oC17jCFpaViKL6Vd1XxUmbZWrIoBj9BkTTJlkw1lXx9niwV0Wz6FwXNe9rYq', NULL, NULL, NULL, 1, '2019-10-24 13:12:23', '2019-10-24 13:12:23'),
(131, '30', NULL, NULL, NULL, 'j', 'k', '10/21', 'jatin@yopmail.com', '$2y$10$/tprH9WzdA5pWX64zsl6Xeeh/7FGRUKEGES7ytWraXZeVw.cKSklO', '123456', 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-24 20:41:25', '2019-10-24 20:41:25'),
(132, '30', 131, NULL, NULL, 'stu jk', 'kj', '10/08/2019', 'jk@jk.com', '$2y$10$R3FNpLmtODI9LgSIMiTGzOypYZ2GJKytoZY9zY13mUv6QB2SjiuAC', '123456', 'student', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-24 20:44:43', '2019-10-24 21:17:25'),
(133, NULL, NULL, NULL, NULL, 'samneet', 'kaur', NULL, 'samneetkaur37@gmail.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'Swipecubes', 'Mr.', '8591466789', 1, '2019-10-24 21:20:02', '2019-10-24 21:20:02'),
(134, NULL, NULL, NULL, NULL, 'Joan', 'Smith', NULL, '1@1.com', NULL, NULL, 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 1, '2019-10-24 21:47:16', '2019-10-24 21:47:16'),
(135, NULL, NULL, NULL, NULL, 'dee', 'A', NULL, 'da@da.com', NULL, NULL, 'teacher', NULL, '1', 'qBycmT7aEhyQ0e7VsPnzLTUkaRyBDb5ykx0IALpiXoT0dqXCOT0uWuGjbGO5', NULL, 'Mr.', NULL, 1, '2019-10-24 22:06:57', '2019-10-24 22:06:57'),
(136, NULL, NULL, NULL, NULL, 'J', 'S', NULL, 'j@s.com', NULL, NULL, 'teacher', NULL, '1', 'VpZWLrfS02VQ8XqOhea0nvpFPDsMiozg4zy5wXG45syAXbdz8Iyc2in86zqA', NULL, 'Dr.', NULL, 1, '2019-10-24 22:09:33', '2019-10-24 22:09:33'),
(137, NULL, NULL, NULL, NULL, 'MIchelle', 'Rozsi', NULL, 'mrozsi@rialto.k12.ca.us', NULL, NULL, 'teacher', NULL, '1', 'hRlv3w3oe4SqQWIqSs13XP5jEzDjjweurns3try7TwEkN3hs1pj4DEKUkMiM', 'RialtoUSd', 'Mrs.', NULL, 1, '2019-10-24 22:51:34', '2019-10-24 22:51:34'),
(138, NULL, NULL, NULL, NULL, 'annette', 'phillips', NULL, 'aphillips@cvwest.org', NULL, NULL, 'teacher', NULL, '1', 'OpVoTPhNVrcJDvHjVkwNahCrYXqHV8lJMbbdKYdfAbXBc1PL5wcDcc1VuERC', 'Learn 4 Life', NULL, '559419700', 1, '2019-10-24 22:58:38', '2019-10-24 22:58:38'),
(139, NULL, NULL, NULL, NULL, 'Eileen', 'Wright', NULL, 'emayelian@gmail.com', NULL, NULL, 'teacher', NULL, '1', 'KhM8BoUBlz3Qwkdbnr9ryfgaVrBBj8ddWZxlrwKjZ2jsQYdDSWoDCA9QCQv6', NULL, NULL, '5623883174', 1, '2019-10-24 23:47:32', '2019-10-24 23:47:32'),
(140, NULL, NULL, NULL, NULL, 'Brittani', 'Isaac', NULL, 'bisaac@sylvan.k12.ca.us', NULL, NULL, 'teacher', NULL, '1', 'muWzQHcK2dMZEVMaNZUwPcybFzXlf3jV9xGlssHEJXcJt0JBQaDZrpVszEVt', 'Sylvan Union School District', 'Mrs.', '559-361-6028', 1, '2019-10-24 23:54:18', '2019-10-24 23:54:18'),
(141, NULL, NULL, NULL, NULL, 'janene', 'Bouse', NULL, 'JBOUSE@CHUSD.ORG', NULL, NULL, 'teacher', NULL, '1', 'MvVLFpNUrGglFCIHcwzUNJNzJDdUW6ixb51OBQQL4TUNdwnz2667iqODIXXm', NULL, NULL, '360-624-0521', 1, '2019-10-24 23:59:15', '2019-10-24 23:59:15'),
(142, NULL, NULL, NULL, NULL, 'RAMLIN', 'TATARI', NULL, 'ramlin.tatari@gmail.com', NULL, NULL, 'teacher', NULL, '1', 'vQy7QcDO2XKCE8V2nZHBdo0FW3PGD8MI0wjGpR6aNuJ43VmxquMNL2edejrV', 'cabrillo unified', 'Miss.', '2096813111', 1, '2019-10-25 00:06:38', '2019-10-25 00:06:38'),
(143, NULL, NULL, NULL, NULL, 'David', 'Looney', NULL, 'dlooney@sjcoe.net', NULL, NULL, 'teacher', NULL, '1', 'qBe9yEon6jXA9j5TPZDqj8I7yvYlY7P6Q1xMyyMJnxq1dYxYPafYkaPZplIJ', 'San Joaquin county Office of Education', NULL, '2094719311', 1, '2019-10-25 00:10:01', '2019-10-25 00:10:01'),
(144, NULL, NULL, NULL, NULL, 'Kristie', 'Calton', NULL, 'kcalton@sandi.net', NULL, NULL, 'teacher', NULL, '1', 'hNTNkiXAllbLo4Am9YDUXU5qJNxDKiCWMdnjhTutTS411EfAHOyqX61bqrWU', 'San Diego Unified', 'Miss.', NULL, 1, '2019-10-25 00:34:10', '2019-10-25 00:34:10'),
(145, NULL, NULL, NULL, NULL, 'joan', 'Smith', NULL, 'joan_smith@comcast.net', NULL, NULL, 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 1, '2019-10-25 01:18:09', '2019-10-25 01:18:09'),
(146, NULL, NULL, NULL, NULL, 'karen', 'Smith', NULL, 'ksmith@paramount.k12.ca.us', NULL, NULL, 'teacher', NULL, '1', 'M775a83mwGGobzBClqXJ0zL8FCv7Hdvl74YJJDTW2lgOObNl00so4WtwJ4ea', NULL, NULL, NULL, 1, '2019-10-25 01:31:43', '2019-10-25 01:31:43'),
(147, NULL, NULL, NULL, NULL, 'Lidia', 'Medrano', NULL, 'lidia.medrano05@gmail.com', NULL, NULL, 'teacher', NULL, '1', 'gHNOgolJ4tOEN63h6uI2cigm4YarCUyIzQ05yE5wGqCE3NIU79Q8alW0gPGU', NULL, NULL, NULL, 1, '2019-10-25 01:37:40', '2019-10-25 01:37:40'),
(148, NULL, NULL, NULL, NULL, 'Jeanie', 'Lohr', NULL, 'jeanette_lohr@redlands.k12.ca.us', NULL, NULL, 'teacher', NULL, '1', 'dfcatreEdYok8ZjEaXMdARpH986jLIO0k1Xm70Pbr9VRG4yYMNsrVMvXP80k', 'Redlands Unified', 'Dr.', '9093072430', 1, '2019-10-25 02:13:09', '2019-10-25 02:13:09'),
(149, NULL, NULL, NULL, NULL, 'Karina', 'Lampitt', NULL, 'Callkarina@yahoo.com', NULL, NULL, 'teacher', NULL, '1', 'p0Bc9X3b4TXg6JwfkZ4JwCqFRBkU4ktnCOA0jn0uYz1a29Psc9a8y1m4T2UL', NULL, NULL, '7602179030', 1, '2019-10-25 03:01:31', '2019-10-25 03:01:31'),
(150, NULL, NULL, NULL, NULL, 'J', 'A', NULL, 'da@da1.com', NULL, NULL, 'teacher', NULL, '1', 'ZxccRR0qEzJA9DMLFda7hq48DEFdMbKZBPHTQPnDafCx7RdUGvWSH8oTr8Vb', NULL, NULL, NULL, 1, '2019-10-25 04:14:14', '2019-10-25 04:14:14'),
(151, NULL, NULL, NULL, NULL, 'Sam', 'ss', NULL, 'sam@gmail.com', NULL, NULL, 'teacher', NULL, '1', 'zmcU10mVIaUFQ13iha3DuIEdT1XeBr93nH1zumVyIYBCEmy3imGsLDx6pieJ', 'Sam and company', 'Mr.', '9646026998', 1, '2019-10-25 06:15:34', '2019-10-25 06:15:34'),
(152, NULL, NULL, NULL, NULL, 'Ss', 'Ds', NULL, 'sam@ymail.com', NULL, NULL, 'teacher', NULL, '1', 'DTrHXalDeO9GvkU7Famg0G83mZu3ZDJp0TVHA2AHdmNCA7IK1GPDysvnrs5X', 'Dd', 'Mr.', '8054336474', 1, '2019-10-25 06:26:33', '2019-10-25 06:26:33'),
(153, NULL, NULL, NULL, NULL, 'asd', 'ads', NULL, 'aakash.twe@gmail.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'ad', 'Mrs.', 'ad', 1, '2019-10-25 06:30:57', '2019-10-25 06:30:57'),
(154, NULL, NULL, NULL, NULL, 'D', 'D', NULL, 'd@d.com', NULL, NULL, 'teacher', NULL, '1', 'poYQjiWqz8pK7w3poFFBrZ8VRkh04k4V4qUHIFa2wqP14FhvHw56qDwpxiiT', 'D', 'Mr.', NULL, 1, '2019-10-25 06:40:39', '2019-10-25 06:40:39'),
(155, NULL, NULL, NULL, NULL, 'S', 'S', NULL, 's@s.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'S', 'Miss.', 'S', 1, '2019-10-25 06:47:47', '2019-10-25 06:47:47'),
(156, NULL, NULL, NULL, NULL, 'Sports Star', 'ads', NULL, 'aakash.twae@gmail.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'ad', NULL, '987545', 1, '2019-10-25 07:12:43', '2019-10-25 07:12:43'),
(157, NULL, NULL, NULL, NULL, 'vagwb', 'rvbgt', NULL, 'ss@ssght.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'DE', 'Dr.', NULL, 1, '2019-10-25 07:21:50', '2019-10-25 07:21:50'),
(158, NULL, NULL, NULL, NULL, 'rhytr', 'etr', NULL, 'dderr@ss.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'fs', 'Mrs.', NULL, 1, '2019-10-25 07:24:46', '2019-10-25 07:24:46'),
(159, NULL, NULL, NULL, NULL, 'HNKLHNJKL', 'KJBJKB', NULL, 'IIU@DD.COM', NULL, NULL, 'teacher', NULL, '1', NULL, 'HKL', 'Mrs.', NULL, 1, '2019-10-25 07:26:13', '2019-10-25 07:26:13'),
(160, NULL, NULL, NULL, NULL, 'bkjb', 'kjbjk', NULL, 'jhj@wwe.com', NULL, NULL, 'teacher', NULL, '1', 'l2upeXdvSr4RtaQzMzhP25AEN6XXkdF74GP4ZuwcoiOceEx57lJ9O8WA3fL8', 'bkjbk', 'Mrs.', NULL, 1, '2019-10-25 07:28:46', '2019-10-25 07:28:46'),
(161, NULL, NULL, NULL, NULL, 'nlknkl', 'nlkn', NULL, 'samkk@ss.com', NULL, NULL, 'teacher', NULL, '1', 'BeLCPLOj5FUpdSRLQhhojhRvKxfGhDKLeLDUAMhvhUYirqeK7sUN8pIrC6YK', 'bkjbjk', 'Mr.', NULL, 1, '2019-10-25 07:29:24', '2019-10-25 07:29:24'),
(162, NULL, NULL, NULL, NULL, 'nklhn', 'lknlk', NULL, 'kkiu@wrt.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'nlknl', 'Mr.', NULL, 1, '2019-10-25 07:30:23', '2019-10-25 07:30:23'),
(163, NULL, NULL, NULL, NULL, 'nbjhg', 'kjjk', NULL, 'gu@jujyu.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'jkkj', 'Mr.', NULL, 1, '2019-10-25 07:34:07', '2019-10-25 07:34:07'),
(164, NULL, NULL, NULL, NULL, 'samkk', 'klkl', NULL, 'kk@ert.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'hkljhl', 'Dr.', NULL, 1, '2019-10-25 07:34:47', '2019-10-25 07:34:47'),
(165, NULL, NULL, NULL, NULL, 'jgjh', 'ghjgh', NULL, 'tyt@wetr.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'gvjg', 'Mr.', NULL, 1, '2019-10-25 07:35:37', '2019-10-25 07:35:37'),
(166, NULL, NULL, NULL, NULL, 'ret', 'rtt', NULL, 'rt@dd.com', NULL, NULL, 'teacher', NULL, '1', '3hTl93bvi4KhNwXlOwM5tmsN78A4nM13u2LGyE2e982DiPEPWpOloJdVMKVO', 'dsd', 'Dr.', NULL, 1, '2019-10-25 07:37:53', '2019-10-25 07:37:53'),
(167, NULL, NULL, NULL, NULL, 'kdsfoj', 'ihjkij', NULL, 'kjh@joij.com', NULL, NULL, 'teacher', NULL, '1', 'AiBjqZR3ioZEflIZcFBmcrKo5jugpJ4FEvB2pGyjrKK2OGuSEsEM8y95FdHR', 'hnolkijn', 'Mrs.', NULL, 1, '2019-10-25 07:39:14', '2019-10-25 07:39:14'),
(168, NULL, NULL, NULL, NULL, 'Sports Star', 'ads', NULL, 'sam07.sumit@gasmail.com', NULL, NULL, 'teacher', NULL, '1', 'ybl29fV47WDhlH8cOjoo6tIgEPXlQfuojslarwpPLqrUaeBA6uSIhWwolcng', 'ad', 'Mr.', NULL, 1, '2019-10-25 07:40:43', '2019-10-25 07:40:43'),
(169, NULL, NULL, NULL, NULL, 'asd', 'ads', NULL, 'sam07.sumit@gmail.coma', NULL, NULL, 'teacher', NULL, '1', NULL, 'ad', 'Mr.', NULL, 1, '2019-10-25 07:53:14', '2019-10-25 07:53:14'),
(170, NULL, NULL, NULL, NULL, 'asd', 'asd', NULL, 'asd@a.s', NULL, NULL, 'teacher', NULL, '1', 'xTru5LHJpNVIGNayvq1sSxM5nTLb6mlgjqZlvf736Ci0XA2CG6yh3lr0m5yZ', 'asd', 'Mrs.', NULL, 1, '2019-10-25 07:57:00', '2019-10-25 07:57:00'),
(171, NULL, NULL, NULL, NULL, 'asd', 'asd', NULL, 'asd@a.d', NULL, NULL, 'teacher', NULL, '1', 'KikY8vELfzTl8Ys3fQyveSjhP5N7ZdljhtXHpPQ4nFpk8CX5GspCg7cj3BL1', 'asd', 'Dr.', NULL, 1, '2019-10-25 08:00:19', '2019-10-25 08:00:19'),
(172, NULL, NULL, NULL, NULL, 'I', 'I', NULL, 'i@i.com', NULL, NULL, 'teacher', NULL, '1', 'cGMbyV6GyOoZyucmZGRWb79DaIRQRw5skIL856jyYmj5Xy9NN7SX2qSTwbQ4', NULL, 'Mrs.', NULL, 1, '2019-10-25 08:08:12', '2019-10-25 08:08:12'),
(173, NULL, NULL, NULL, NULL, 'Fgh', NULL, NULL, 'fhhh@gmail.com', NULL, NULL, 'teacher', NULL, '1', 'xVCNTa9zx7wAkCPLqI8MAGLNrbl2cgRQ5oJTIsMDw3ratTj6Actue4ywfgxJ', NULL, NULL, NULL, 1, '2019-10-25 08:40:51', '2019-10-25 08:40:51'),
(174, NULL, NULL, NULL, NULL, 'Fty', NULL, NULL, 'gghhh@gmail.com', NULL, NULL, 'teacher', NULL, '1', 'y2zD8D4SCKOwC8foykJSdMmT9IHCMgjY8n6xBRGi4K7Juvae3GFJbWgihPj7', NULL, NULL, NULL, 1, '2019-10-25 08:51:39', '2019-10-25 08:51:39'),
(175, NULL, NULL, NULL, NULL, 'Vhh', NULL, NULL, 'fgh@gmail.com', NULL, NULL, 'teacher', NULL, '1', NULL, NULL, NULL, NULL, 1, '2019-10-25 08:53:59', '2019-10-25 08:53:59'),
(176, NULL, NULL, NULL, NULL, 'gfhgf', NULL, NULL, 'dhsdhag@gmail.com', NULL, NULL, 'teacher', NULL, '1', 'J0RUlVSxF1kiYGyQtBzD60kxgoYZt5ZUYlnrF6TSNkfM5kJydo2SUCOYmcJV', NULL, NULL, NULL, 1, '2019-10-25 11:22:01', '2019-10-25 11:22:01'),
(177, NULL, NULL, NULL, NULL, 'sfdfdsf', '9055454164646546546', '10/25/2019', 'fdfdfdsfsds@gmail.com', '$2y$10$CwtLy4Hdo24XujEddatvjehm.ZAAEP2UEdoYU25decBL0fgxzYpFK', '65164165165+5+6468', 'teacher', NULL, '1', 'jcsgvVnj13XqFFx5gnNmZRo2uUAl4yKxoed9m8YLpxPwNF1YB4snGNTD7erZ', NULL, NULL, NULL, 1, '2019-10-25 11:30:30', '2019-10-25 16:46:05'),
(178, NULL, NULL, NULL, NULL, 'Geg', 'Ss', NULL, 'tue@gmaiol.com', NULL, NULL, 'teacher', NULL, '1', 'cG1QvlYgDuYSfdx7BCwKcsDBMjespfi4FBIHNvmozst4EkgMMoOELETspLsi', 'Gshhs', 'Mrs.', NULL, 1, '2019-10-25 11:37:18', '2019-10-25 11:37:18'),
(179, NULL, NULL, NULL, NULL, 'Hdhs', 'Heye', NULL, 'sam@cc.com', NULL, NULL, 'teacher', NULL, '1', 'Fog2CqPH2LyLuxVeffkW4EysLZuyWrYma2TbKK2liG5okXjLOTWtrLi6cBq5', 'Hsheh', 'Mrs.', NULL, 1, '2019-10-25 11:39:45', '2019-10-25 11:39:45'),
(180, NULL, NULL, NULL, NULL, 'Sj', 'Gdg', NULL, 'tty@hi.com', NULL, NULL, 'teacher', NULL, '1', NULL, 'Bshe', 'Mr.', NULL, 1, '2019-10-25 11:41:13', '2019-10-25 11:41:13'),
(181, NULL, NULL, NULL, NULL, 'dsffd', NULL, NULL, 'fdsfd@gmail.com', NULL, NULL, 'teacher', NULL, '1', 'fkjWW8cfU2lcBBmRHxMt5zJZIe04ZuOPZo83kboseafAq35M7IomDWWEPOdD', NULL, NULL, NULL, 1, '2019-10-25 11:53:52', '2019-10-25 11:53:52'),
(182, NULL, NULL, NULL, NULL, 'bkjb', 'kjbbj', NULL, 'ju@ju.com', NULL, NULL, 'teacher', NULL, '1', 'wkdPFswqDLQTlFoczqubJkGTW3l8joqS3YrDdlWPWlZNpCVnwJMX4vNwFHe7', 'mklmk', 'Mr.', NULL, 1, '2019-10-25 15:27:01', '2019-10-25 15:27:01'),
(183, NULL, NULL, NULL, NULL, 'hh', 'jbjb', NULL, 'jjj@jij.com', NULL, NULL, 'teacher', NULL, '1', 'XzAIQhOJcR8Gc4PeQ4ujhRlWTn2zzQybJ77J9mGY5ecXSwrsTKqIEus5ndb6', 'kjbjk', 'Mr.', NULL, 1, '2019-10-25 15:28:49', '2019-10-25 15:28:49'),
(184, NULL, NULL, NULL, NULL, 'jbjk', 'jbbk', NULL, 'kjbbjkjbk@kjkjkjb.com', NULL, NULL, 'teacher', NULL, '1', 'Unptb86NJA8UzGoP0eu15URvgdERPJ2bd2Pi7q9qWm9gzgo6O6Okf36RkZnj', 'jm', 'Mr.', NULL, 1, '2019-10-25 15:33:39', '2019-10-25 15:33:39'),
(185, NULL, NULL, NULL, NULL, 'bkjbj', 'jkbjkb', NULL, 'kbbk@wt.com', NULL, NULL, 'teacher', NULL, '1', '9fJsCSIfdvEVd3kV8vFM2MeqgO0DP3RgdLEc0F26B9abNenN6s2595RmpdyV', 'kjbjb', 'Dr.', NULL, 1, '2019-10-25 15:34:23', '2019-10-25 15:34:23'),
(186, NULL, NULL, NULL, NULL, 'sahil', NULL, NULL, 'sa@gg.df', NULL, NULL, 'teacher', NULL, '1', 'wiwt4tHGyxOzQYz2GINEEgA5Sw2XLXnDX29hfV7YPKcqTiy1w0aWWP05j0GT', NULL, 'Dr.', NULL, 1, '2019-10-25 15:42:59', '2019-10-25 15:42:59'),
(187, NULL, NULL, NULL, NULL, 'chatbot', 'dsad', NULL, 'oms@institute.comz', NULL, NULL, 'teacher', NULL, '1', 'V95pMlUyd3dJHrDAmBMPFizAaWCnD1DYhbQ7e7l5XdJ01glF6sCUKfc01TWT', NULL, 'Dr.', NULL, 1, '2019-10-25 15:51:52', '2019-10-25 15:51:52'),
(188, '30', NULL, NULL, NULL, 'Trainer', 'Yopmail', '10/01', 'Trainer@yopmail.com', '$2y$10$b5QezNEEs6SIalW6ALPCs.2mU3VLZOaMf6AxbKaZS4QSQyUDSUvwy', '12345', 'teacher', NULL, '1', 'GmxGOos52vjNMT6bEw6KTUPw185vpyKoutng3aqC6CnS1Y6u4rqhjJvUs8NY', NULL, NULL, NULL, 0, '2019-10-25 16:23:38', '2019-10-25 16:23:57'),
(190, NULL, NULL, NULL, NULL, 'live', NULL, NULL, 'ad@oms.com', '$2y$10$tLmW3dAFZgvyFBw2BEOnOO4zTiqo/F.P0pL23lTH9c5Em.e7F.t.u', '123456', 'Institute', NULL, '1', NULL, NULL, NULL, NULL, 0, '2019-10-25 16:27:38', '2019-10-25 16:27:38'),
(192, NULL, NULL, NULL, NULL, 'jjkb', 'jk', NULL, 'hh@gmailc.com', NULL, NULL, 'teacher', NULL, '1', 'auMBQ6KE0F7V1d7AZkUGb8hPVJQxpwggIljC3UhtCtFF7U6Rk3SAKWP4F6F5', 'lknlnl', 'Dr.', NULL, 1, '2019-10-25 16:35:34', '2019-10-25 16:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(2, 'teacher', '1', '2019-10-09 08:28:41', '2019-03-07 02:06:53'),
(3, 'student', '1', '2019-08-26 10:43:06', '2019-03-07 02:07:00'),
(4, 'Institute', '1', '2019-10-05 07:31:38', '2019-08-27 10:09:27'),
(5, 'admin', '1', '2019-10-07 08:15:03', '2019-09-19 16:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `vuecruddata`
--

CREATE TABLE `vuecruddata` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guest_users`
--
ALTER TABLE `guest_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level_sub_levels`
--
ALTER TABLE `level_sub_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_managements`
--
ALTER TABLE `package_managements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session_days`
--
ALTER TABLE `session_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_details`
--
ALTER TABLE `student_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_level_details`
--
ALTER TABLE `student_level_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_tempo_trainers`
--
ALTER TABLE `student_tempo_trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_levels`
--
ALTER TABLE `sub_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vuecruddata`
--
ALTER TABLE `vuecruddata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `guest_users`
--
ALTER TABLE `guest_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `level_sub_levels`
--
ALTER TABLE `level_sub_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `package_managements`
--
ALTER TABLE `package_managements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=671;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `session_days`
--
ALTER TABLE `session_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `student_details`
--
ALTER TABLE `student_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `student_level_details`
--
ALTER TABLE `student_level_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=587;

--
-- AUTO_INCREMENT for table `student_tempo_trainers`
--
ALTER TABLE `student_tempo_trainers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sub_levels`
--
ALTER TABLE `sub_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vuecruddata`
--
ALTER TABLE `vuecruddata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

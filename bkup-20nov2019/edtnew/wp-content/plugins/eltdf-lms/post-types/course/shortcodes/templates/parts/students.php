<?php
$students_output = array("" => "");
$students        = get_post_meta( get_the_ID(), 'eltdf_course_users_attended', true );
	if (is_array($students) && !empty($students)){ 
		$students_number = count( $students );
	}
		return $students_output;
?>
<div class="eltdf-students-number-holder">
	<span class="eltdf-student-icon"></span>
	<span class="eltdf-student-label">
		<?php echo esc_html( $students_number ); ?>
		<?php esc_html_e( 'Students', 'eltdf-lms' ) ?>
	</span>
</div>
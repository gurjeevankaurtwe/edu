<?php

namespace ElatedCore\CPT\Shortcodes\Testimonials;

use ElatedCore\Lib;

class Testimonials implements Lib\ShortcodeInterface {
	private $base;
	
	public function __construct() {
		$this->base = 'eltdf_testimonials';
		
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
		
		//Testimonials category filter
		add_filter( 'vc_autocomplete_eltdf_testimonials_category_callback', array( &$this, 'testimonialsCategoryAutocompleteSuggester', ), 10, 1 ); // Get suggestion(find). Must return an array
		
		//Testimonials category render
		add_filter( 'vc_autocomplete_eltdf_testimonials_category_render', array( &$this, 'testimonialsCategoryAutocompleteRender', ), 10, 1 ); // Get suggestion(find). Must return an array
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'                      => esc_html__( 'Elated Testimonials', 'eltdf-core' ),
					'base'                      => $this->base,
					'category'                  => esc_html__( 'by ELATED', 'eltdf-core' ),
					'icon'                      => 'icon-wpb-testimonials extended-custom-icon',
					'allowed_container_element' => 'vc_row',
					'params'                    => array(
						array(
							'type'        => 'dropdown',
							'param_name'  => 'type',
							'heading'     => esc_html__( 'Type', 'eltdf-core' ),
							'value'       => array(
								esc_html__( 'Standard', 'eltdf-core' ) => 'standard',
								esc_html__( 'Elegant', 'eltdf-core' )  => 'elegant',
								esc_html__( 'Boxed', 'eltdf-core' )    => 'boxed'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'skin',
							'heading'     => esc_html__( 'Skin', 'eltdf-core' ),
							'value'       => array(
								esc_html__( 'Default', 'eltdf-core' ) => '',
								esc_html__( 'Light', 'eltdf-core' )   => 'light',
							),
							'save_always' => true
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'number',
							'heading'    => esc_html__( 'Number of Testimonials', 'eltdf-core' )
						),
						array(
							'type'        => 'autocomplete',
							'param_name'  => 'category',
							'heading'     => esc_html__( 'Category', 'eltdf-core' ),
							'description' => esc_html__( 'Enter one category slug (leave empty for showing all categories)', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'number_of_visible_items',
							'heading'     => esc_html__( 'Number Of Visible Items', 'eltdf-core' ),
							'value'       => array(
								esc_html__( 'One', 'eltdf-core' )   => '1',
								esc_html__( 'Two', 'eltdf-core' )   => '2',
								esc_html__( 'Three', 'eltdf-core' ) => '3',
								esc_html__( 'Four', 'eltdf-core' )  => '4',
								esc_html__( 'Five', 'eltdf-core' )  => '5',
								esc_html__( 'Six', 'eltdf-core' )   => '6'
							),
							'save_always' => true,
							'dependency'  => Array( 'element' => 'type', 'value' => 'boxed' ),
							'group'       => esc_html__( 'Slider Settings', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'slider_loop',
							'heading'     => esc_html__( 'Enable Slider Loop', 'eltdf-core' ),
							'value'       => array_flip( esmarts_elated_get_yes_no_select_array( false, true ) ),
							'save_always' => true,
							'group'       => esc_html__( 'Slider Settings', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'slider_autoplay',
							'heading'     => esc_html__( 'Enable Slider Autoplay', 'eltdf-core' ),
							'value'       => array_flip( esmarts_elated_get_yes_no_select_array( false, true ) ),
							'save_always' => true,
							'group'       => esc_html__( 'Slider Settings', 'eltdf-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'slider_speed',
							'heading'     => esc_html__( 'Slide Duration', 'eltdf-core' ),
							'description' => esc_html__( 'Default value is 5000 (ms)', 'eltdf-core' ),
							'group'       => esc_html__( 'Slider Settings', 'eltdf-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'slider_speed_animation',
							'heading'     => esc_html__( 'Slide Animation Duration', 'eltdf-core' ),
							'description' => esc_html__( 'Speed of slide animation in milliseconds. Default value is 600.', 'eltdf-core' ),
							'group'       => esc_html__( 'Slider Settings', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'slider_navigation',
							'heading'     => esc_html__( 'Enable Slider Navigation Arrows', 'eltdf-core' ),
							'value'       => array_flip( esmarts_elated_get_yes_no_select_array( false, true ) ),
							'save_always' => true,
							'group'       => esc_html__( 'Slider Settings', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'slider_pagination',
							'heading'     => esc_html__( 'Enable Slider Pagination', 'eltdf-core' ),
							'value'       => array_flip( esmarts_elated_get_yes_no_select_array( false, true ) ),
							'save_always' => true,
							'group'       => esc_html__( 'Slider Settings', 'eltdf-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'slider_navigation_skin',
							'heading'     => esc_html__( 'Slider Navigation/Pagination Skin', 'eltdf-core' ),
							'value'       => array(
								esc_html__( 'Default', 'eltdf-core' ) => '',
								esc_html__( 'Light', 'eltdf-core' )   => 'light',
							),
							'save_always' => true,
							'group'       => esc_html__( 'Slider Settings', 'eltdf-core' )
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'type'                    => 'standard',
			'skin'                    => '',
			'number'                  => '-1',
			'category'                => '',
			'number_of_visible_items' => '3',
			'slider_loop'             => 'yes',
			'slider_autoplay'         => 'yes',
			'slider_speed'            => '5000',
			'slider_speed_animation'  => '600',
			'slider_navigation'       => 'yes',
			'slider_pagination'       => 'yes',
			'slider_navigation_skin'  => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['type'] = ! empty( $params['type'] ) ? $params['type'] : $args['type'];
		
		$holder_classes = $this->getHolderClasses( $params );
		$inner_classes  = $params['type'] === 'elegant' ? 'eltdf-owl-custom-slider' : '';
		
		$query_args    = $this->getQueryParams( $params );
		$query_results = new \WP_Query( $query_args );
		
		$data_attr            = $this->getSliderData( $params );
		
		$html = '<div class="eltdf-testimonials-holder ' . $holder_classes . ' clearfix">';
			$html .= '<div class="eltdf-testimonials eltdf-owl-slider ' . $inner_classes . '" ' . esmarts_elated_get_inline_attrs( $data_attr ) . '>';
			
			if ( $query_results->have_posts() ):
				while ( $query_results->have_posts() ) : $query_results->the_post();
					$text     = get_post_meta( get_the_ID(), 'eltdf_testimonial_text', true );
					$author   = get_post_meta( get_the_ID(), 'eltdf_testimonial_author', true );
					$position = get_post_meta( get_the_ID(), 'eltdf_testimonial_author_position', true );
					
					$params['current_id'] = get_the_ID();
					$params['text']       = $text;
					$params['author']     = $author;
					$params['position']   = $position;
					
					$html .= eltdf_core_get_cpt_shortcode_module_template_part( 'testimonials', 'testimonials-' . $params['type'], '', $params );
				
				endwhile;
			else:
				$html .= esc_html__( 'Sorry, no posts matched your criteria.', 'eltdf-core' );
			endif;
			
			$html .= '</div>';
		
			if ( $params['type'] === 'elegant' ) {
				$html .= '<div class="eltdf-testimonial-image-nav eltdf-owl-slider eltdf-owl-custom-slider">';
					if ( $query_results->have_posts() ) {
						while ( $query_results->have_posts() ) : $query_results->the_post();
							$html .= '<div class="eltdf-testimonial-image">' . get_the_post_thumbnail( get_the_ID(), array( 94, 94 ) ) . '</div>';
						endwhile;
					}
				$html .= '</div>';
				
				$html .= '<div class="eltdf-testimonial-author-nav eltdf-owl-slider eltdf-owl-custom-slider">';
					if ( $query_results->have_posts() ) {
						while ( $query_results->have_posts() ) : $query_results->the_post();
							$current_id = get_the_ID();
							$author = get_post_meta( $current_id, 'eltdf_testimonial_author', true );
							$position = get_post_meta( $current_id, 'eltdf_testimonial_author_position', true );
							
							$html .= '<div class="eltdf-testimonial-author-nav-item">';
								if ( ! empty( $author ) ) {
									$html .= '<h4 class="eltdf-testimonial-author">' . esc_html( $author ) . '</h4>';
								}
								
								if ( ! empty( $position ) ) {
									$html .= '<h6 class="eltdf-testimonials-author-job">' . esc_html( $position ) . '</h6>';
								}
							$html .= '</div>';
						endwhile;
					}
				$html .= '</div>';
			}
			
			wp_reset_postdata();
		
		$html .= '</div>';
		
		return $html;
	}
	
	private function getHolderClasses( $params ) {
		$holderClasses = array();
		
		$holderClasses[] = 'eltdf-testimonials-' . $params['type'];
		$holderClasses[] = ! empty( $params['skin'] ) ? 'eltdf-testimonials-' . $params['skin'] : '';
		$holderClasses[] = ! empty( $params['slider_navigation_skin'] ) ? 'eltdf-testimonials-nav-' . $params['slider_navigation_skin'] : '';
		
		return implode( ' ', $holderClasses );
	}
	
	private function getQueryParams( $params ) {
		$args = array(
			'post_status'    => 'publish',
			'post_type'      => 'testimonials',
			'orderby'        => 'date',
			'order'          => 'DESC',
			'posts_per_page' => $params['number']
		);
		
		if ( $params['category'] != '' ) {
			$args['testimonials-category'] = $params['category'];
		}
		
		return $args;
	}
	
	private function getSliderData( $params ) {
		$slider_data = array();
		
		$slider_data['data-number-of-items']        = ! empty( $params['number_of_visible_items'] ) && $params['type'] === 'boxed' ? $params['number_of_visible_items'] : '1';
		$slider_data['data-slider-margin']          = $params['type'] === 'boxed' ? 24 : '';
		$slider_data['data-enable-loop']            = ! empty( $params['slider_loop'] ) ? $params['slider_loop'] : 'no';
		$slider_data['data-enable-autoplay']        = ! empty( $params['slider_autoplay'] ) ? $params['slider_autoplay'] : 'no';
		$slider_data['data-slider-speed']           = ! empty( $params['slider_speed'] ) ? $params['slider_speed'] : '5000';
		$slider_data['data-slider-speed-animation'] = ! empty( $params['slider_speed_animation'] ) ? $params['slider_speed_animation'] : '600';
		$slider_data['data-enable-navigation']      = $params['type'] !== 'elegant' && ! empty( $params['slider_navigation'] ) ? $params['slider_navigation'] : 'no';
		$slider_data['data-enable-pagination']      = $params['type'] !== 'elegant' && ! empty( $params['slider_pagination'] ) ? $params['slider_pagination'] : 'no';
		
		return $slider_data;
	}
	
	/**
	 * Filter testimonials categories
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function testimonialsCategoryAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS testimonials_category_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'testimonials-category' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );
		
		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['slug'];
				$data['label'] = ( ( strlen( $value['testimonials_category_title'] ) > 0 ) ? esc_html__( 'Category', 'eltdf-core' ) . ': ' . $value['testimonials_category_title'] : '' );
				$results[]     = $data;
			}
		}
		
		return $results;
		
	}
	
	/**
	 * Find testimonials category by slug
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function testimonialsCategoryAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get portfolio category
			$testimonials_category = get_term_by( 'slug', $query, 'testimonials-category' );
			if ( is_object( $testimonials_category ) ) {
				
				$testimonials_category_slug  = $testimonials_category->slug;
				$testimonials_category_title = $testimonials_category->name;
				
				$testimonials_category_title_display = '';
				if ( ! empty( $testimonials_category_title ) ) {
					$testimonials_category_title_display = esc_html__( 'Category', 'eltdf-core' ) . ': ' . $testimonials_category_title;
				}
				
				$data          = array();
				$data['value'] = $testimonials_category_slug;
				$data['label'] = $testimonials_category_title_display;
				
				return ! empty( $data ) ? $data : false;
			}
			
			return false;
		}
		
		return false;
	}
}
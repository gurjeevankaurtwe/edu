(function($) {
	'use strict';
	
	var masonryElementsHolder = {};
	eltdf.modules.masonryElementsHolder = masonryElementsHolder;

	masonryElementsHolder.eltdfInitElementsHolderResponsiveStyle = eltdfInitMasonryElementsHolderResponsiveStyle;


	masonryElementsHolder.eltdfOnDocumentReady = eltdfOnDocumentReady;
	
	$(document).ready(eltdfOnDocumentReady);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function eltdfOnDocumentReady() {
		eltdfInitMasonryElements();
		eltdfInitMasonryElementsHolderResponsiveStyle();
	}

	function eltdfInitMasonryElements() {
		var container = $('.eltdf-masonry-elements-holder');
		
		if (container.length) {
			container.each(function () {
				var thisMasonryElements = $(this);

				eltdfResizeMasonryElements(thisMasonryElements);

				eltdfMasonryElements(thisMasonryElements);
				$(window).resize(function () {
					eltdfResizeMasonryElements(thisMasonryElements);
					eltdfMasonryElements(thisMasonryElements);
				});
			});
		}
	}

	function eltdfMasonryElements(container) {
		container.waitForImages(function () {
			container.isotope({
				itemSelector: '.eltdf-masonry-elements-item',
				resizable: false,
				layoutMode: 'packery',
				packery: {
					columnWidth: '.eltdf-masonry-elements-grid-sizer'
				}
			});
			container.addClass('eltdf-appeared');
			
			setTimeout(function() {
				container.isotope('layout');
			}, 800);
		});
	}

	function eltdfResizeMasonryElements(container) {
		var size = container.find('.eltdf-masonry-elements-grid-sizer').width();

		var defaultMasonryItem = container.find('.eltdf-square');
		var largeWidthMasonryItem = container.find('.eltdf-large-width');
		var largeHeightMasonryItem = container.find('.eltdf-large-height');
		var largeWidthHeightMasonryItem = container.find('.eltdf-large-width-height');

		defaultMasonryItem.css('height', size);
		largeWidthMasonryItem.css('height', size);
		largeHeightMasonryItem.css('height', Math.round(2 * size));

		if (eltdf.windowWidth > 600) {
			largeWidthHeightMasonryItem.css('height', Math.round(2 * size));
		} else {
			largeWidthHeightMasonryItem.css('height', size);
		}

		var items = container.find('.eltdf-masonry-elements-item');

		items.each(function () {
			var item = $(this);
			var itemHeight = item.find('.eltdf-masonry-elements-item-inner-helper').height();
			var itemChildHeight = item.find('.eltdf-masonry-elements-item-inner-tc').height();

			if (itemChildHeight > itemHeight) {
				item.css('height', 'auto');
			}
		});
		
		setTimeout(function(){
			var slider = container.find('.eltdf-owl-slider');
			
			if (slider.length) {
				slider.each(function(){
					var thisSlider = $(this);
					
					thisSlider.trigger('refresh.owl.carousel');
				});
			}
		}, 800);
	}

	/*
	 **	Elements Holder responsive style
	 */
	function eltdfInitMasonryElementsHolderResponsiveStyle() {
		var masonryElementsHolder = $('.eltdf-masonry-elements-holder');

		if (masonryElementsHolder.length) {
			masonryElementsHolder.each(function () {
				var thisMasonryElementsHolder = $(this),
					masonryElementsHolderItem = thisMasonryElementsHolder.find('.eltdf-masonry-elements-item-inner'),
					style = '',
					responsiveStyle = '';
				
				masonryElementsHolderItem.each(function () {
					var thisItem = $(this),
						itemClass = '',
						largeLaptop = '',
						smallLaptop = '',
						ipadLandscape = '',
						ipadPortrait = '',
						mobileLandscape = '',
						mobilePortrait = '';

					if (typeof thisItem.data('item-class') !== 'undefined' && thisItem.data('item-class') !== false) {
						itemClass = thisItem.data('item-class');
					}
					if (typeof thisItem.data('1280-1440') !== 'undefined' && thisItem.data('1280-1440') !== false) {
						largeLaptop = thisItem.data('1280-1440');
					}
					if (typeof thisItem.data('1024-1280') !== 'undefined' && thisItem.data('1024-1280') !== false) {
						smallLaptop = thisItem.data('1024-1280');
					}
					if (typeof thisItem.data('768-1024') !== 'undefined' && thisItem.data('768-1024') !== false) {
						ipadLandscape = thisItem.data('768-1024');
					}
					if (typeof thisItem.data('600-768') !== 'undefined' && thisItem.data('600-768') !== false) {
						ipadPortrait = thisItem.data('600-768');
					}
					if (typeof thisItem.data('480-600') !== 'undefined' && thisItem.data('480-600') !== false) {
						mobileLandscape = thisItem.data('480-600');
					}
					if (typeof thisItem.data('480') !== 'undefined' && thisItem.data('480') !== false) {
						mobilePortrait = thisItem.data('480');
					}

					if (largeLaptop.length || smallLaptop.length || ipadLandscape.length || ipadPortrait.length || mobileLandscape.length || mobilePortrait.length) {

						if (largeLaptop.length) {
							responsiveStyle += "@media only screen and (min-width: 1280px) and (max-width: 1440px) {.eltdf-masonry-elements-item-inner." + itemClass + " { padding: " + largeLaptop + " !important; } }";
						}
						if (smallLaptop.length) {
							responsiveStyle += "@media only screen and (min-width: 1024px) and (max-width: 1280px) {.eltdf-masonry-elements-item-inner." + itemClass + " { padding: " + smallLaptop + " !important; } }";
						}
						if (ipadLandscape.length) {
							responsiveStyle += "@media only screen and (min-width: 768px) and (max-width: 1024px) {.eltdf-masonry-elements-item-inner." + itemClass + " { padding: " + ipadLandscape + " !important; } }";
						}
						if (ipadPortrait.length) {
							responsiveStyle += "@media only screen and (min-width: 600px) and (max-width: 768px) {.eltdf-masonry-elements-item-inner." + itemClass + " { padding: " + ipadPortrait + " !important; } }";
						}
						if (mobileLandscape.length) {
							responsiveStyle += "@media only screen and (min-width: 480px) and (max-width: 600px) {.eltdf-masonry-elements-item-inner." + itemClass + " { padding: " + mobileLandscape + " !important; } }";
						}
						if (mobilePortrait.length) {
							responsiveStyle += "@media only screen and (max-width: 480px) {.eltdf-masonry-elements-item-inner." + itemClass + " { padding: " + mobilePortrait + " !important; } }";
						}
					}
				});

				if (responsiveStyle.length) {
					style = '<style type="text/css">' + responsiveStyle + '</style>';
				}

				if (style.length) {
					$('head').append(style);
				}
			});
		}
	}
	
})(jQuery);